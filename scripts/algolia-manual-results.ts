import { AlgoliaType } from '../src/utils/siteStructure'

const links: AlgoliaType[] = [
  {
    name: 'Figma',
    objectId: 'designers-figma',
    type: 'designers',
    parentName: 'Links',
    uri: '/designers/get-started#figma',
  },
  {
    name: 'Abstract',
    objectId: 'designers-abstract',
    type: 'designers',
    parentName: 'Links',
    uri: '/designers/get-started#abstract',
  },
  {
    name: 'Get Figma',
    objectId: 'designers-get-figma',
    type: 'designers',
    parentName: 'Links',
    uri: '/designers/get-started#get-figma',
  },
  {
    name: 'Get Abstract',
    objectId: 'designers-get-abstract',
    type: 'designers',
    parentName: 'Links',
    uri: '/designers/get-started#get-abstract',
  },
  {
    name: 'Onboarding',
    objectId: 'onboarding',
    type: 'Tutorial',
    parentName: 'Tutorial',
    uri: '/onboarding',
  },
]
export default links
