import dotenv from 'dotenv'
import axios from 'axios'

let envFilePath = './.env.development'
if (process.env.NODE_ENV === 'production') {
  envFilePath = './.env'
}
dotenv.config({
  path: envFilePath,
})

const apiBaseUrl = process.env.NEXT_PUBLIC_STRAPI_BASE_URL

async function getDeveloperComponents() {
  try {
    const res = await axios.get(`${apiBaseUrl}/developer-components`)
    return res.data
  } catch (error) {
    console.log('ERR: failed to get data', error.message)
  }
}

async function deleteDeveloperComponent(id) {
  try {
    await axios.delete(`${apiBaseUrl}/developer-components/${id}`)
  } catch (error) {
    console.log('ERR: failed to delete ' + id, error.message)
  }
}

const deleteAllDeveloperComponentsFromStrapi = async () => {
  // console.log('ad', process.env.NEXT_PUBLIC_STRAPI_BASE_URL)
  const developerComponents = await getDeveloperComponents()
  console.log(`Deleting ${developerComponents.length} componenets from Strapi`)
  for (let i = 0; i < developerComponents.length; i++) {
    const devComp = developerComponents[i]
    await deleteDeveloperComponent(devComp.id)
  }
}

console.log('Strapi url:', apiBaseUrl)
// Keep this commented when commiting for safety..
// deleteAllDeveloperComponentsFromStrapi()
