import dotenv from 'dotenv'
import algoliasearch from 'algoliasearch'
import { AlgoliaType, getSiteStructure, Node } from '../src/utils/siteStructure'
import manualAlgoliaItems from './algolia-manual-results'

let envFilePath = './.env.development'
if (process.env.NODE_ENV === 'production') {
  envFilePath = './.env'
}
dotenv.config({
  path: envFilePath,
})

function getAlgoliaItemsRecursively(node: Node): AlgoliaType[] {
  if (node.isLeaf()) {
    return [node.getAlgoliaItem()]
  }
  return node.children
    .flatMap(c => getAlgoliaItemsRecursively(c))
    .filter((c: AlgoliaType) => c)
}

function createSearchItems() {
  const siteStructure = getSiteStructure()
  // Get component data from site structure
  const devItems = getAlgoliaItemsRecursively(siteStructure.developers)
  const desItems = getAlgoliaItemsRecursively(siteStructure.designers)

  // Use this to add manual search indices for Algolia to parse
  const manualItems = manualAlgoliaItems.map(result => ({
    framework: '',
    disabled: false,
    ...result,
  }))
  // console.log('alg items', devItems.concat(desItems))

  return devItems.concat(desItems).concat(manualItems)
}

async function uploadAlgoliaData(searchItems: AlgoliaType[]) {
  try {
    const client = algoliasearch(
      process.env.NEXT_PUBLIC_ALGOLIA_APP_ID!,
      process.env.ALGOLIA_ADMIN_KEY!
    )
    const index = client.initIndex(process.env.NEXT_PUBLIC_ALGOLIA_INDEX_NAME!)
    await index.setSettings({
      searchableAttributes: [
        'name',
        'parentName',
        // 'parent.name',
        // 'description',
        'framework',
        'mode',
      ],
      attributesToRetrieve: [
        'name',
        'uri',
        'type',
        'parentName',
        // 'parent',
        // 'mode',
        // 'description',
        'framework',
      ],
    })

    console.log('Uploading items to algolia..')
    // This empties the index, for escape duplicates
    index.clearObjects()
    // This updates the index, with new data. Be aware that this is the final data sent to algolia
    index.saveObjects(searchItems, {
      autoGenerateObjectIDIfNotExist: true,
    })
    console.log('Done')
  } catch (error) {
    console.log(error)
  }
}

async function main() {
  const searchItems = createSearchItems()
  // console.log(searchItems)
  // WARNING: This actually updates data, comment this to test function
  await uploadAlgoliaData(searchItems)
}

main()
