#!/bin/bash

# NOTE: Run this on the prod server, i.e. log in to server first

set -e

if [ $# -eq 0 ]
  then
    echo "Usage: $0 <git branch>"
    exit 1
fi

LOG_PREFIX="DDS PROD:"

BRANCH=$1

REPODIR="/root/vattenfall-dds"

# Go to workdir
cd $REPODIR

# Fetch code
echo "$LOGPREFIX Updating repo.."
git fetch --all
git checkout $BRANCH
git reset --hard origin/$BRANCH

# Install deps
echo "$LOGPREFIX Installing deps.."
rm -rf node_modules || true
yarn

# build
echo "$LOGPREFIX Building.."
yarn build

# restart service
systemctl restart vattenfall-stage.service
