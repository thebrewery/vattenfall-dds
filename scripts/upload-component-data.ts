import dotenv from 'dotenv'
import axios from 'axios'
import { componentGroups } from '../src/utils/siteStructure'

let envFilePath = './.env.development'
if (process.env.NODE_ENV === 'production') {
  envFilePath = './.env'
}
dotenv.config({
  path: envFilePath,
})

const apiBaseUrl = process.env.NEXT_PUBLIC_STRAPI_BASE_URL

async function postDeveloperComponent(data: {}) {
  try {
    await axios.post(`${apiBaseUrl}/developer-components`, data)
  } catch (error) {
    // @ts-ignore
    console.log('ERR: failed to update data', error.message)
  }
}
async function getDeveloperComponents() {
  try {
    const res = await axios.get(`${apiBaseUrl}/developer-components`)
    return res.data
  } catch (error) {
    // @ts-ignore
    console.log('ERR: failed to get data', error.message)
  }
}

const postCMSData = async () => {
  console.log('Pushing components to strapi')

  const developerComponents = await getDeveloperComponents()
  // @ts-ignore
  const idMap = developerComponents.reduce((acc, cur) => {
    acc[cur.identifier] = cur
    return acc
  }, {})

  let count = 0

  for (let i = 0; i < Object.values(componentGroups).length; i++) {
    const componentGroup = Object.values(componentGroups)[i]
    const components = componentGroup.children
    for (let j = 0; j < components.length; j++) {
      const {
        name,
        id,
        category,
        sketchUrl,
        // photoshopUrl,
        // abstractUrl,
        // adobeXdUrl,
        figmaUrl,
        frameworkReviews,
        guidelineUri,
        // notInCms,
      } = components[j]
      if (
        // notInCms ||
        idMap[id]
      )
        continue

      console.log('Uploading component:', id)
      await postDeveloperComponent({
        name,
        identifier: id,
        category,
        sketchUrl,
        // photoshopUrl,
        // abstractUrl,
        // adobeXdUrl,
        figmaUrl,
        frameworkReviews,
        guidelineUri,
      })
      count += 1
    }
  }
  console.log(`Uploaded ${count} components`)
}

console.log('Strapi url:', apiBaseUrl)
postCMSData()
