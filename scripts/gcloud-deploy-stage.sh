#!/bin/bash

# Todo: Check that the configration exists
# If not, add it with
# gcloud init
# And name it "vattenfall-ds"

# gcloud config configurations list |grep vattenfall-ds | wc -l
gcloud config configurations activate vattenfall-ds

docker build --no-cache -t stage-vattenfallds --build-arg mode=dev --platform linux/amd64 -f stage.Dockerfile .
docker tag stage-vattenfallds eu.gcr.io/vattenfall-design-system/stage-vattenfallds
docker push eu.gcr.io/vattenfall-design-system/stage-vattenfallds

gcloud compute instances update-container stage-vf-docker-instance --container-image eu.gcr.io/vattenfall-design-system/stage-vattenfallds --zone europe-north1-a
gcloud compute instances reset stage-vf-docker-instance --zone europe-north1-a
