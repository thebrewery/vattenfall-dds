# To update fonts

Go to [IcoMoon](https://icomoon.io) and import selection.json.
Select all icons in the interface, and now you have to do two exports.

## Generate font

Since we use a SVG-based font for all our icons, so everytime a new icon is added to our library, we should update it.

- Go to generate font and under preferences, check that class-prefix is set to 'vf-icon-' and the font name is set to 'vf-icons', and then click 'Download'.
- After downloading unpack the zip-file and take the new 'vf-icons.woff' and replace the old one (in 'public/assets/fonts/icons').
- Open 'style.css' in the export-folder and copy everything **except** the font-declaration (located in 'public/assets/fonts/fonts.scss') and the general ruleset for all classes prefixed with vf-icons (located in 'src/style/icons.style.scss')
- Paste and replace the rest in 'src/style/\_icons.scss'.

## Generate SVGs

We should also provide a way, to just download all SVGs as a zip. We link to this file on the dds-website, on our introduction to 'Icons'. This file should be updated at the same time as the fonts.

- Go to 'Generate SVG & More' and click download. The only thing we want from here is a folder that contains all our SVGs.
- Unpack the zip-file and find the SVG-folder.
- Zip this one and replace the zip-file located in 'public/assets/fonts/icons', named 'vf-icons.zip'.
