/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        'hall-display': [
          'Vattenfall Hall Display NORDx',
          'Helvetica',
          'sans-serif',
        ],
        hall: ['Vattenfall Hall NORDx', 'Helvetica', 'sans-serif'],
      },
      colors: {
        'solar-yellow': 'var(--solar-yellow)',
        'ocean-blue': 'var(--ocean-blue)',
        'aura-white': 'var(--aura-white)',
        'coal-black': 'var(--coal-black)',
        'magnetic-gray': 'var(--magnetic-gray)',
        'dark-green': 'var(--dark-green)',
        'dark-blue': 'var(--dark-blue)',
        pink: 'var(--pink)',
        'dark-purple': 'var(--dark-purple)',
        green: 'var(--green)',
        red: 'var(--red)',
        purple: 'var(--purple)',
        'light-yellow': 'var(--light-yellow)',
        'light-green': 'var(--light-green)',
        'light-blue-alt': 'var(--light-blue-alt)',
        'light-blue': 'var(--light-blue)',
        'light-red': 'var(--light-red)',
        'light-grey': 'var(--light-grey)',
        'ash-blue': 'var(--ash-blue)',
        'dark-ash-blue': 'var(--dark-ash-blue)',
        'lighter-grey': 'var(--lighter-grey)',
        'medium-grey': 'var(--medium-grey)',
        'medium-dark-grey': 'var(--medium-dark-grey)',
        'dark-grey': 'var(--dark-grey)',
        hydro: 'var(--hydro)',
        wind: 'var(--wind)',
        solar: 'var(--solar)',
        biomass: 'var(--biomass)',
        coal: 'var(--coal)',
        gas: 'var(--gas)',
        nuclear: 'var(--nuclear)',
        'district-heating': 'var(--district-heating)',
      },
      fontSize: {
        'h0-desktop': [
          '4.5rem',
          {
            // 72px for desktop and tablet
            lineHeight: '5.25rem', // 84px
            fontWeight: '700',
          },
        ],
        'h0-mobile': [
          '2.4rem',
          {
            // 38px for mobile
            lineHeight: 1, // 84px
            fontWeight: '700',
          },
        ],
        'h1-desktop': [
          '3.25rem',
          {
            // 52px for desktop and tablet
            lineHeight: '3.875rem', // 62px
            fontWeight: '700',
          },
        ],
        'h1-mobile': [
          '2.125rem',
          {
            // 34px for mobile
            lineHeight: '2.5rem', // 40px
            fontWeight: '700',
          },
        ],
        'h2-desktop': [
          '2.25rem',
          {
            // 36px for desktop and tablet
            lineHeight: '2.625rem', // 42px
            fontWeight: '700',
          },
        ],
        'h2-mobile': [
          '1.75rem',
          {
            // 28px for mobile
            lineHeight: '2rem', // 32px
            fontWeight: '700',
          },
        ],
        'h3-desktop': [
          '1.75rem',
          {
            // 28px for desktop and tablet
            lineHeight: '2.25rem', // 36px
            fontWeight: '700',
          },
        ],
        'h3-mobile': [
          '1.375rem',
          {
            // 22px for mobile
            lineHeight: '1.75rem', // 28px
            fontWeight: '700',
          },
        ],
        'h4-desktop': [
          '1.375rem',
          {
            // 22px for desktop and tablet
            lineHeight: '1.75rem', // 28px
            fontWeight: '700',
          },
        ],
        'h4-mobile': [
          '1.125rem',
          {
            // 18px for mobile
            lineHeight: '1.5rem', // 24px
            fontWeight: '700',
          },
        ],
        'body-large': [
          '1.25rem',
          {
            // 20px
            lineHeight: '2.25rem', // 36px
            fontWeight: '400', // Regular
          },
        ],
        'body-small': [
          '1rem',
          {
            // 16px
            lineHeight: '1.75rem', // 28px
            fontWeight: '400', // Regular
          },
        ],
        'intro-desktop': [
          '1rem', // 16px
          {
            lineHeight: '2.25rem', // 36px
            fontWeight: '700', // Bold
          },
        ],
        'intro-mobile': [
          '0.75rem', // 12px
          {
            lineHeight: '1.375rem', // 22px
            fontWeight: '400', // Regular
          },
        ],
        'preamble-desktop': [
          '1.5rem', // 24px
          {
            lineHeight: '2.25rem', // 36px
            fontWeight: '500', // Medium
          },
        ],
        'preamble-mobile': [
          '1.25rem', // 20px
          {
            lineHeight: '1.75rem', // 28px
            fontWeight: '500', // Medium
          },
        ],
      },
    },
  },
  corePlugins: {
    preflight: false,
  },
  prefix: 'tw-',
  plugins: [],
}
