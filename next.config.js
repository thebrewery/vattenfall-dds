const { dependencies } = require('./package.json')

const withTM = require('next-transpile-modules')([
  'react-instantsearch',
  'react-markdown',
])

module.exports = withTM({
  // i18n: {
  //   locales: ["en"],
  //   defaultLocale: "en",
  // },
  env: {
    VF_VANILLA_VERSION: dependencies['@vf-dds/vf-dds-vanilla'],
  },
  compiler: {
    styledComponents: true,
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    })
    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader',
    })
    config.module.rules.push({
      test: /\.m?js$/,
      type: 'javascript/auto',
      resolve: {
        fullySpecified: false,
      },
    })

    return config
  },
})
