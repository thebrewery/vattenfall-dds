# Vattenfall DDS

This is the design platform for Vattenfall to use as a reference in various projects.

## Installation

```
npm install
```

## Development

You'll probably want to either run the backend on your computer or use the staging backend.
Make sure you have .env file with NEXT_PUBLIC_STRAPI_BASE_URL set to your backend.
You should be able to find env-files (.env vattenfall-dds) for stage and dev in 1password, or ask someone
else who is working on the project.

Start the project with

```
npm run dev
```

### Standards

This project uses certain techniques, such as

- BEM - https://en.bem.info/methodology/quick-start/
- semver - https://semver.org/

## Algolia

We use algolia to create a search index for pages, used by the search input on top of the page.

First you need to make sure the site structure data in src/utils/siteStructure.ts is up to date.

To upload new site structure data to algolia, make sure you have the proper keys in your env file (and correct index name).
You need to set these env vars: ALGOLIA_APP_ID, ALGOLIA_SEARCH_KEY, ALGOLIA_ADMIN_KEY and ALGOLIA_INDEX_NAME.

NOTE: When developing the script you should probably use either a test account or at least not use the same index as is used in prod.

Then open scripts/update-algolia.mjs and uncomment the line in main() that uploads data. (remember to comment out the line afterwards so we don't have any accidents)

Finally, run the script with:

```
npm run updateAlgolia
```

### Updating prod index

At the moment the algolia script only use whatever we find in the code (i.e. not fetching anything from Strapi), so you can update your env-file
with the credentials for the prod algolia account and run from your computer, but be extra careful since it will immediately be used on the prod site.

## Deployment

### Stage

Push the stage branch, and a build will trigger at Vercel for [https://dds-stage.vercel.app](https://dds-stage.vercel.app).

Environment variables are set per project in Vercel, so if they need to changed it is at Vercel, where you should change it.

### Production

Production (https://digitaldesign.vattenfall.com) is built using pipelines on Bitbucket. A build will be trigger when master is pushed.

Environment variables are set by project in Bitbucket, so change them there.

Site is hosted at Inleed.

## Styling

This site uses a mix of `styled-components` and basic SCSS. We still try to maintain the BEM model, but we also promote using whatever fits best.

## Reports

Reports are sent to Zendesk, login details is stored in 1pass.

## Design system

Our design system is built to fit two types of end users, Designers & Developers. Therefore we split our tree in two different views, to be able shown more direct content for the final user. This is fully utilized yet, and could be improved.

### Designers & Developers

Both these views are structured from `src/utils/siteStructure.ts`, with their own independent trees being built there. If we want to add a category, component or something else to our site structure, we need to add it here.

### Sandbox

We use [codesandbox](https://codesandbox.io) to visualize our components. The ids are collected from our separate [package](@vf-dds/vf-dds-vanilla), where we generate the sandboxes. Read more in the related docs.

## Article Components

We use something we called `article-components` to display different components, based in our CMS. These are contained under something called `Content_blocks`.

`semi-circle-image` corresponds to `SemiCircle` & `articlepage.semi-circle-image`.
`two-column-content` corresponds to `TwoColumnContent` & `articlepage.two-column-content`.
`three-article-column` corresponds to `ThreeArticleColumn` & `articlepage.three-article-column`.
`two-column-even` corresponds to `TwoColumnEven` & `articlepage.two-column-even`.
`two-column-spaced` corresponds to `TwoColumnSpaced` & `articlepage.two-column-spaced`.
`full-width-content` corresponds to `FullWidthContent` & `articlepage.full-width-content`.

You can see an example of how to use this in `index.tsx` in `contentBlockMap`.

## Brandtoolbox Content

We've created some content, that is iframed inside Vattenfall's Brandtoolbox.

# **OLD DOCS FOR GC**

### Google cloud

Make sure to install the Google [Cloud SDK]('https://cloud.google.com/sdk/docs/install'), so you have `gcloud` command line tool installed.

Login to google cloud and configure the project (use the account hourse.agency.developer@gmail.com if you're not added in some other way)

```sh
gcloud auth login
```

You should also create a new configuration (for instance using gcloud init) with the name 'vattenfall-ds'.
A working config should look similar to this:

NAME IS_ACTIVE ACCOUNT PROJECT DEFAULT_ZONE DEFAULT_REGION
vattenfall-ds True house.agency.developer@gmail.com vattenfall-design-system europe-north1-a europe-north1

We use Compute engine for stage and prod, you can list them with

```
gcloud compute instances list
```

The disks on the instances may occassionally fill up due to old Docker images. This can cause unexpected behaviour, and is hard to debug as no error messages may appear on Google Cloud.
To prevent this, be sure to occasionally prune the unused Docker images by ssh (in the hamburger menu on gcloud go to "Compute Engine -> "VM instances" then press the instance you want to manage. Under "remote access" press SSH) paste the command: `docker image prune -a` . If the disk happens to already be full, just increase the disk space temporarily on Google Cloud, (Left menu -> Disks -> vf-docker-instance -> Edit -> [increase size] ), then prune the images.

### Docker (for old environment)

In order to deploy you need to have [Docker]('https://docs.docker.com/get-docker/') installed on your machine.
You can also, perhaps as a pre-step to deploying, also run your code through docker by for instance using the following commands

Build docker file

```
docker build -t stage-vattenfallds --build-arg mode=dev -f stage.Dockerfile .
```

Run container:

```
docker run --name stage-vattenfallds --rm -t -d -p 4567:80 stage-vattenfallds
```

Now you should be able to go to localhost:4567 in your browser

When finished you can stop the container with

```
docker kill stage-vattenfallds
```
