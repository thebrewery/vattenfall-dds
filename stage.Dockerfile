# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:16.13.0 as build-stage
ARG mode
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install

COPY . .

# RUN CI=true yarn development
RUN yarn stage-build
RUN yarn stage-export
# RUN cp -r ./dist /app/build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.21.3
COPY --from=build-stage /app/out /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
# COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/conf/nginx/stage.nginx.conf /etc/nginx/conf.d/default.conf
