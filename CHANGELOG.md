#### 1.6.0 (January 26th, 2023)

### New components

We added several new components! Look for the indicator resembling a small yellow circle that entails that a new assets or component has been added.

### Tailwind support

We have started a new iniative to write future components using tailwind-css, to make them easier to implement. These components should be used with caution for now, since none of this is finalized.

### FAQ

On the contact [page](https://digitaldesign.vattenfall.com/contact), we added a small FAQ. Check it out!

#### 1.5.0 (January 17th, 2023)

### New onboarding page

Look at our new onboarding [page](https://digitaldesign.vattenfall.com/onboarding), where you can quick information where to start as a designer or developer!

### Campaign assets

Check out some new components, developed for THE EDIT-campaign under the new category 'Campaign assets'.

#### 1.4.0 (October 26th, 2022)

### Updates main layout of article page, which now aligns with Future Vision!

Check out our latest articles too see the new look!

### Overview page for Articles

We have updated our overview-page for Articles, which now also aligns with Future Vision.

### The DDS platform is fully typed and is aimed for launch!

We integrated TypeScript, so now we have typesafety and the reliability of TS. We aim to release the DDS platform code for your viewing pleasure soon.

---

#### 1.3.0 (September 9th, 2022)

### Updates the visuals on landing page, which are now aligned with Future Vision!

New components and copy, which springs from Future Vision.

### Site content structure update.

We updated our editorial pages, with support for more complex, new components that are aligned with Future Vision.

### New indicators for new content

We added the option to tag new content, which can be seen on our component viewing pages.

---

#### 1.2.0 (May 24th, 2022)

### New carousel component

We built and integrated a new carousel (called Exposition Carousel), which can be used as a Hero component which you can see on our landing page.

### Multiple sandbox support

We integrated support to have multiple sandboxes on one page, instead of a single, big sandbox. This creates an opportunity to have better page performance while maintaining the possibility to show alot of content, but now with more restraint. This can be shown on pages with different variants, which will in turn render different sandboxes.
This change does not affect users of CSS or JavaScript.

### New content highlights

We have started highlighting the _latest_ components in our library with a yellow marker. This makes it easier to find our latest changes and new components.

---

#### 1.1.0 (April 4th, 2022)

### General clean up

- Changed all font sizes from pixel values to rem/em.
- Bug fix related to typography not being imported properly.
- Adds information text, example and the ability to use an alternate grid, based on flex instead of grid.
- Changes dependencies, to be more future proof.
- Updated cdn links, used in examples to be more stable, by using unpkg.
- Many minor bug fixes, related to styles not displaying properly in some browsers.

---

#### 1.0.0 (November 23, 2021)

#### New refactored menu structure

- Reworked the menu structure in to a more category driven structure.
- Renamed components to more fit their role, in the DDS.
- Splits the DDS in to two modes - Designers & Developers.

#### Sandbox

- We integrated [code-sandbox](https://codesandbox.io/), for our examples.
- This should act as a bridge, between Designers & Developers in an environment where both parts can view and edit.
- Another feature this opens up for, is for you the user, to fork our sandboxes and create your own examples and share them with us!

---
