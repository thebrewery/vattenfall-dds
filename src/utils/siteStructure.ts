/**
 * Here we create a tree data structure for the developer and designer parts of the DDS.
 * This structure is used both to generate the navigation on the site and to create the algolia index.
 */

import { ModeEnum } from '../context/state'

export const modeEnum = {
  DEVELOPERS: 'developers' as const,
  DESIGNERS: 'designers' as const,
  NULL: '' as const,
}

// This is kind of unused for now, since we only support vanillajs
export const frameworkEnum = {
  NONE: '',
  VANILLA: 'javascript',
}

const baseSiteStructure = {
  developers: {
    name: 'Developers',
    id: 'developers',
    children: [
      {
        name: 'Design Principles',
        id: 'design-principles',
        children: null,
      },
      {
        name: 'Accessibility',
        id: 'accessibility',
        children: null,
      },
      {
        name: 'Colours',
        id: 'colours',
        children: null,
      },
      {
        name: 'Data Visualisation',
        id: 'data-visualisation',
        children: null,
      },
      // Components will be added through the var componentGroups
    ],
  },
  designers: {
    name: 'Designers',
    id: 'designers',
    children: [
      {
        name: 'Design Principles',
        id: 'design-principles',
        children: null,
      },
      {
        name: 'Accessibility',
        id: 'accessibility',
        children: null,
      },
      {
        name: 'Colours',
        id: 'colours',
        children: null,
      },
      {
        name: 'Data Visualisation',
        id: 'data-visualisation',
        children: null,
      },
    ],
  },
}

const vanillaGitUrl =
  'https://bitbucket.org/thebrewery/vf-dds-vanilla/src/master/src/components/'

// TODO; CHECK WHICH ONES HAVE JS, and requires giturl for real

export type FrameWorkReviewProps = {
  region: string
  status: string
  framework: string
  exists: boolean
}

export type ComponentType = {
  name: string
  id: string
  category: string
  gitUrl?: string
  guidelineUri?: string
  sketchUrl?: string
  figmaUrl?: string
  newComponentIndicator?: boolean
  frameworkReviews?: FrameWorkReviewProps[]
  disabled?: boolean
  disabledInDevelopers?: boolean
  hideInSidebar?: boolean
}

type CategoryType = {
  name: string
  id: string
  newComponentIndicator?: boolean
  hideInSidebar?: boolean
  children: ComponentType[]
}

export type AlgoliaType = {
  name: string
  objectId: string
  type: string
  parentName: string
  uri: string
}

export const componentGroups: CategoryType[] = [
  {
    name: 'Dialogues',
    id: 'dialogues',
    children: [
      {
        name: 'GDPR Consent',
        gitUrl: vanillaGitUrl + 'dialogues/',
        id: 'gdpr-consent',
        category: 'dialogues',
        // guidelineUri: '/designers/components/dialogues/gdpr-consent',
        sketchUrl: 'TRUE',
        newComponentIndicator: true,
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Message banners',
        gitUrl: vanillaGitUrl + 'dialogues/',
        id: 'message-banners',
        category: 'dialogues',
        // guidelineUri: '/designers/components/dialogues/message-banners',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Modal',
        gitUrl: vanillaGitUrl + 'dialogues/',
        id: 'modal',
        category: 'dialogues',
      },
      {
        name: 'Notification',
        gitUrl: vanillaGitUrl + 'dialogues/',
        id: 'notification',
        category: 'dialogues',
        // guidelineUri: '/designers/components/dialogues/notification',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Tooltip',
        gitUrl: vanillaGitUrl + 'dialogues/',
        id: 'tooltip',
        category: 'dialogues',
        guidelineUri: '/designers/components/dialogues/tooltip',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
  {
    name: 'Accessibility Guidelines',
    id: 'accessibility-guidelines',
    hideInSidebar: true,
    children: [
      {
        name: 'Accessibility for Developers',
        id: 'developer-principles',
        category: 'accessibility-guidelines',
        newComponentIndicator: false,
        hideInSidebar: true,
      },

      {
        name: 'Accessibility for Designers',
        id: 'designer-principles',
        category: 'accessibility-guidelines',
        newComponentIndicator: false,
        hideInSidebar: true,
      },
      {
        name: 'Accessibility for Editors',
        id: 'editor-principles',
        category: 'accessibility-guidelines',
        newComponentIndicator: false,
        hideInSidebar: true,
      },
    ],
  },
  {
    name: 'Campaign assets',
    id: 'campaign-assets',
    children: [
      // {
      //   name: 'Text Hero Block',
      //   id: 'text-hero-block',
      //   category: 'campaign-assets',
      // },
      // {
      //   name: 'Animated Key Number',
      //   id: 'animated-key-number',
      //   category: 'campaign-assets',
      // },
      // {
      //   name: 'Highlight Information Container Block',
      //   id: 'highlight-information-container-block',
      //   category: 'campaign-assets',
      // },
      {
        name: 'The Edit',
        id: 'the-edit',
        category: 'campaign-assets',
        newComponentIndicator: true,
      },
      {
        name: 'Industrial Emissions Face Mist',
        id: 'industrial-emissions-face-mist',
        category: 'campaign-assets',
        newComponentIndicator: true,
      },
    ],
  },
  {
    name: 'Graphics & Motion',
    id: 'graphics-and-motion',
    children: [
      {
        name: 'Illustrations',
        id: 'illustrations',
        guidelineUri: '/designers/graphics-and-motion/illustrations',
        disabledInDevelopers: true,
        category: 'graphics-and-motion',
      },
      {
        name: 'Info graphics',
        id: 'info-graphics',
        guidelineUri: '/designers/graphics-and-motion/info-graphics',
        disabledInDevelopers: true,
        category: 'graphics-and-motion',
      },

      {
        name: 'Motion',
        id: 'motion',
        guidelineUri: '/designers/graphics-and-motion/motion',
        category: 'graphics-and-motion',
      },
    ],
  },
  {
    name: 'Icons',
    id: 'icons',
    children: [
      {
        name: 'Intro',
        id: 'icons-intro',
        gitUrl: vanillaGitUrl + 'icons/',
        category: '/designers/components/icons',
      },
      {
        name: 'Usage',
        id: 'usage',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/usage',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Action',
        id: 'action',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/action',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Favicon',
        id: 'favicon',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/favicon',
        sketchUrl: 'TRUE',
        disabled: true,
      },
      {
        name: 'Identification',
        id: 'identification',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/identification',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Navigation',
        id: 'navigation',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/navigation',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Smart home',
        id: 'smart-home',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/smart-home',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Social media',
        id: 'social-media',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/social-media',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Status',
        id: 'status',
        gitUrl: vanillaGitUrl + 'icons/',
        category: 'icons',
        guidelineUri: '/designers/components/icons/status',
        disabled: true,
      },
    ],
  },
  {
    name: 'Indicators',
    id: 'indicators',
    children: [
      {
        gitUrl: vanillaGitUrl + 'indicators/',
        name: 'Badge',
        id: 'badge',
        category: 'indicators',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Pagination',
        id: 'pagination',
        gitUrl: vanillaGitUrl + 'indicators/',
        category: 'indicators',
        // guidelineUri: '/designers/components/indicators/pagination',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Preloader',
        id: 'preloader',
        gitUrl: vanillaGitUrl + 'indicators/',
        category: 'indicators',
        // guidelineUri: '/designers/components/indicators/preloader',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Progress indicator',
        id: 'progress-indicator',
        gitUrl: vanillaGitUrl + 'indicators/',
        category: 'indicators',
        // guidelineUri: '/designers/components/indicators/progress-indicator',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Progress',
        id: 'progress',
        gitUrl: vanillaGitUrl + 'indicators/',
        category: 'indicators',
        // guidelineUri: '/designers/components/indicators/progress-indicator',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Spinner',
        id: 'spinner',
        gitUrl: vanillaGitUrl + 'indicators/',
        category: 'indicators',
        guidelineUri: '/designers/components/indicators/spinner',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
  {
    name: 'Input',
    id: 'input',
    children: [
      {
        name: 'Dropdown',
        id: 'dropdown',
        gitUrl: vanillaGitUrl + 'input/',
        category: 'input',
        guidelineUri: '/designers/components/input/dropdown',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Field',
        id: 'field',
        gitUrl: vanillaGitUrl + 'input/',
        category: 'input',
        guidelineUri: '/designers/components/input/field',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Number',
        id: 'number',
        gitUrl: vanillaGitUrl + 'input/',
        category: 'input',
        guidelineUri: '/designers/components/input/number',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Search',
        id: 'search',
        gitUrl: vanillaGitUrl + 'input/',
        category: 'input',
      },
      {
        name: 'Textarea',
        id: 'textarea',
        gitUrl: vanillaGitUrl + 'input/',
        category: 'input',
        guidelineUri: '/designers/components/input/textarea',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
  {
    name: 'Logos',
    id: 'logos',
    children: [
      {
        name: 'Key Principles',
        id: 'key-principles',
        gitUrl: vanillaGitUrl + 'logos/',
        category: 'logos',
        guidelineUri: '/designers/components/logos/key-principles',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Colourways',
        id: 'colourways',
        gitUrl: vanillaGitUrl + 'logos/',
        category: 'logos',
        disabled: true,
      },
      {
        name: 'Horizontal',
        id: 'horizontal',
        gitUrl: vanillaGitUrl + 'logos/',
        category: 'logos',
        guidelineUri: '/designers/components/logos/horizontal',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Stacked',
        id: 'stacked',
        gitUrl: vanillaGitUrl + 'logos/',
        category: 'logos',
        guidelineUri: '/designers/components/logos/stacked',
        sketchUrl: 'TRUE',
      },
      {
        name: 'Symbol',
        id: 'symbol',
        gitUrl: vanillaGitUrl + 'logos/',
        category: 'logos',
        disabled: true,
      },
      {
        name: 'Top bar',
        id: 'top-bar',
        gitUrl: vanillaGitUrl + 'logos/',
        category: 'logos',
        // guidelineUri: '/designers/components/logos/top-bar',
        sketchUrl: 'TRUE',
      },
    ],
  },
  {
    name: 'Modules',
    id: 'modules',
    children: [
      {
        name: 'Article',
        id: 'article',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/article',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Carousel',
        id: 'carousel',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        guidelineUri: '/designers/components/modules/carousel',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Exposition Carousel',
        id: 'expo-carousel',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        guidelineUri: '/designers/components/modules/expo-carousel',
        sketchUrl: 'TRUE',
        newComponentIndicator: true,
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Footer',
        id: 'footer',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/footer',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Hero',
        id: 'hero',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        guidelineUri: '/designers/components/modules/hero',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Image',
        id: 'image',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/image',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Image module',
        id: 'image-module',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/image-module',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Investors',
        id: 'investors',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        newComponentIndicator: true,
      },
      {
        name: 'Linked list',
        id: 'linked-list',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/linked-list',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Media',
        id: 'media',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/media',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'News',
        id: 'news',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        disabled: true,
      },
      {
        name: 'Numbers',
        id: 'numbers',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        newComponentIndicator: true,
      },
      {
        name: 'Offset image',
        id: 'offset-image',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/offset-image',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'People',
        id: 'people',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        newComponentIndicator: true,
      },
      {
        name: 'Quote',
        id: 'quote',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/quote',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Receipt',
        id: 'receipt',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        disabled: true,
      },
      {
        name: 'Subscription',
        id: 'subscription',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/subscription',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Testimonials',
        id: 'testimonials',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        newComponentIndicator: true,
      },
      {
        name: 'Upload',
        id: 'upload',
        gitUrl: vanillaGitUrl + 'modules/',
        category: 'modules',
        // guidelineUri: '/designers/components/modules/upload',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
  {
    name: 'Navigation',
    id: 'navigation',
    children: [
      {
        name: 'Header',
        id: 'header',
        gitUrl: vanillaGitUrl + 'navigation/',
        category: 'navigation',
        // guidelineUri: '/designers/components/navigation/header',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Tabs',
        id: 'tabs',
        gitUrl: vanillaGitUrl + 'navigation/',
        category: 'navigation',
        // guidelineUri: '/designers/components/navigation/tabs',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Text links',
        id: 'text-links',
        gitUrl: vanillaGitUrl + 'navigation/',
        category: 'navigation',
        guidelineUri: '/designers/components/navigation/text-links',
        sketchUrl: 'TRUE',
        disabled: true,
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Search',
        id: 'search',
        gitUrl: vanillaGitUrl + 'navigation/',
        category: 'navigation',
        guidelineUri: '/designers/components/navigation/search',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
  {
    name: 'Selection',
    id: 'selection',
    children: [
      {
        name: 'Buttons',
        id: 'buttons',
        gitUrl: vanillaGitUrl + 'selection/',
        category: 'selection',
        guidelineUri: '/designers/components/selection/buttons',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Calendar',
        id: 'calendar',
        gitUrl: vanillaGitUrl + 'selection/',
        category: 'selection',
        // guidelineUri: '/designers/components/selection/calendar',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Checkbox',
        id: 'checkbox',
        gitUrl: vanillaGitUrl + 'selection/',
        category: 'selection',
        guidelineUri: '/designers/components/selection/checkbox',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Option',
        id: 'option',
        gitUrl: vanillaGitUrl + 'selection/',
        category: 'selection',
        guidelineUri: '/designers/components/selection/option',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Radio button',
        id: 'radio-button',
        gitUrl: vanillaGitUrl + 'selection/',
        category: 'selection',
        guidelineUri: '/designers/components/selection/radio-button',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Toggle',
        id: 'toggle',
        gitUrl: vanillaGitUrl + 'selection/',
        disabled: true,
        category: 'selection',
      },
      {
        name: 'Value',
        id: 'value',
        gitUrl: vanillaGitUrl + 'selection/',
        category: 'selection',
        newComponentIndicator: true,
      },
      {
        name: 'Slider',
        id: 'slider',
        gitUrl: vanillaGitUrl + 'selection/',
        category: 'selection',
        newComponentIndicator: true,
      },
    ],
  },
  {
    name: 'Structure',
    id: 'structure',
    children: [
      {
        name: 'Dividers',
        id: 'dividers',
        gitUrl: vanillaGitUrl + 'structure/',
        category: 'structure',
      },
      {
        name: 'Grid',
        id: 'grid',
        gitUrl: vanillaGitUrl + 'structure/',
        category: 'structure',
        guidelineUri: '/designers/components/structure/grid',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Alternate grid',
        id: 'alternate-grid',
        gitUrl: vanillaGitUrl + 'structure/',
        category: 'structure',
        guidelineUri: '/designers/components/structure/alternate-grid',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Responsive',
        id: 'responsive',
        gitUrl: vanillaGitUrl + 'structure/',
        category: 'structure',
        guidelineUri: '/designers/components/structure/responsive',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Sizing',
        id: 'sizing',
        gitUrl: vanillaGitUrl + 'structure/',
        category: 'structure',
        guidelineUri: '/designers/components/structure/sizing',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Spacing',
        id: 'spacing',
        gitUrl: vanillaGitUrl + 'structure/',
        category: 'structure',
        guidelineUri: '/designers/components/structure/spacing',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
  {
    name: 'Tables & Lists',
    id: 'tables-and-lists',
    children: [
      {
        name: 'Table',
        id: 'table',
        gitUrl: vanillaGitUrl + 'tables-and-lists/',
        category: 'tables_and_lists',
        // guidelineUri: '/designers/components/tables-and-lists/table',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Text list',
        id: 'text-list',
        gitUrl: vanillaGitUrl + 'tables-and-lists/',
        category: 'tables_and_lists',
        // guidelineUri: '/designers/components/tables-and-lists/text-list',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Link list',
        id: 'link-list',
        gitUrl: vanillaGitUrl + 'tables-and-lists/',
        category: 'tables_and_lists',
        // guidelineUri: '/designers/components/tables-and-lists/link-list',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Numbered list',
        id: 'numbered-list',
        gitUrl: vanillaGitUrl + 'tables-and-lists/',
        category: 'tables_and_lists',
        // guidelineUri: '/designers/components/tables-and-lists/numbered-list',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
  {
    name: 'Typography',
    id: 'typography',
    children: [
      {
        name: 'Introduction',
        id: 'typography-introduction',
        category: 'typography',
      },
      {
        name: 'Headers',
        id: 'headers',
        gitUrl: vanillaGitUrl + 'typography/',
        category: 'typography',
        guidelineUri: '/designers/components/typography/headers',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Intro',
        id: 'typography-intro',
        gitUrl: vanillaGitUrl + 'typography/',
        category: 'typography',
        guidelineUri: '/designers/components/typography/typography-intro',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Emphasis',
        id: 'emphasis',
        gitUrl: vanillaGitUrl + 'typography/',
        category: 'typography',
        disabled: true,
      },
      {
        name: 'Bodycopy',
        id: 'bodycopy',
        gitUrl: vanillaGitUrl + 'typography/',
        category: 'typography',
        guidelineUri: '/designers/components/typography/bodycopy',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      // {
      //   name: 'Errors',
      //   id: 'errors',
      //   gitUrl: vanillaGitUrl + 'typography/',
      //   category: 'typography',
      //   guidelineUri: '/designers/components/typography/errors',
      //   sketchUrl: 'TRUE',
      //   disabled: true,
      //   frameworkReviews: [
      //     {
      //       region: 'GLOBAL',
      //       status: 'ALLOWED',
      //       framework: 'VANILLA',
      //       exists: true,
      //     },
      //   ],
      // },
      {
        name: 'Link',
        id: 'link',
        gitUrl: vanillaGitUrl + 'typography/',
        category: 'typography',
        guidelineUri: '/designers/components/typography/link',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
      {
        name: 'Status',
        id: 'status',
        gitUrl: vanillaGitUrl + 'typography/',
        category: 'typography',
        guidelineUri: '/designers/components/typography/status',
        sketchUrl: 'TRUE',
        frameworkReviews: [
          {
            region: 'GLOBAL',
            status: 'ALLOWED',
            framework: 'VANILLA',
            exists: true,
          },
        ],
      },
    ],
  },
]

export class Node {
  name: string
  id: string
  parent: Node | null
  // For future use, if we implement framework specific views and components
  framework: string
  children: Node[]
  isComponentCategory: boolean
  isComponent: boolean
  gitUrl: string | null
  disabled: boolean
  newComponentIndicator: boolean
  hideInSidebar: boolean
  constructor(
    name: string,
    id: string,
    parent: Node | null,
    framework = frameworkEnum.NONE,
    isComponentCategory = false,
    isComponent = false,
    gitUrl: string | null = null,
    disabled = false,
    newComponentIndicator = false,
    hideInSidebar = false
  ) {
    this.name = name
    this.id = id
    this.parent = parent
    this.framework = framework
    this.children = []
    this.isComponentCategory = isComponentCategory
    this.isComponent = isComponent
    this.gitUrl = gitUrl
    this.disabled = disabled
    this.newComponentIndicator = newComponentIndicator
    this.hideInSidebar = hideInSidebar
  }

  getRoot(): Node {
    if (this.parent === null) {
      return this
    }
    return this.parent.getRoot()
  }

  addChild(childNode: Node) {
    if (childNode) {
      this.children.push(childNode)
    }
  }

  isLeaf() {
    return this.children.length === 0
  }

  getSlugs(): string[] {
    let slugs: string[] = []
    if (this.parent) {
      slugs = this.parent.getSlugs()
    }
    if (this.isComponentCategory) {
      slugs.push('components')
    }
    slugs.push(this.id)
    return slugs
  }

  getUri() {
    // Note: This might be needed if we decide to add framework specific views and components
    // const frameworkPath = this.framework ? `/${this.framework}` : ''
    const frameworkPath = ''

    let uri = ''
    if (this.parent) {
      uri = this.parent.getUri()
    }
    const ownPath = (this.isComponentCategory ? 'components/' : '') + this.id
    return `${uri}/${ownPath}${frameworkPath}`
  }

  getMode(): string {
    if (this.parent === null) {
      return this.id
    }
    return this.parent.getMode()
  }

  getAlgoliaItem() {
    if (this.children.length > 0) {
      // We only index leafs
      return {} as AlgoliaType
    }

    const parentName = this.parent ? this.parent.name : ''

    return {
      name: this.name,
      objectId: this.getUri().replace('/', '').replace(/\//g, '-'), // First replace replaces first /, second replace converts all / to -
      type: this.getMode(),
      framework: this.framework || frameworkEnum.NONE,
      parentName,
      uri: this.getUri(),
      disabled: this.disabled || false,
    } as AlgoliaType
  }
}

function getDeveloperTree() {
  // Get base structure and create tree
  const rootInfo = baseSiteStructure.developers

  const developerTreeRoot = new Node(rootInfo.name, rootInfo.id, null)

  rootInfo.children.forEach(topLink => {
    developerTreeRoot.addChild(
      new Node(topLink.name, topLink.id, developerTreeRoot)
    )
  })

  // Loop through groups and add component links
  componentGroups.forEach(componentGroup => {
    // Check if group contains new components
    const hasNewComponent = componentGroup.children.some(
      child => child.newComponentIndicator
    )

    const hasHideInSidebar = componentGroup.children.some(
      child => child.hideInSidebar
    )

    // Create group node (category)
    const groupNode = new Node(
      componentGroup.name,
      componentGroup.id,
      developerTreeRoot,
      frameworkEnum.NONE,
      true,
      undefined,
      null,
      undefined,
      hasNewComponent,
      hasHideInSidebar
    )

    // Go through each component in group
    componentGroup.children.forEach(component => {
      // This removes the listing in the developer view if the component is disabled
      if (component.disabledInDevelopers) {
        groupNode.addChild(
          new Node(
            component.name,
            component.id,
            groupNode,
            frameworkEnum.VANILLA,
            false,
            true,
            component.gitUrl,
            true,
            component.newComponentIndicator,
            component.hideInSidebar
          )
        )

        return
      }

      // Create component link/node
      groupNode.addChild(
        new Node(
          component.name,
          component.id,
          groupNode,
          frameworkEnum.VANILLA,
          false,
          true,
          component.gitUrl,
          component.disabled,
          component.newComponentIndicator,
          component.hideInSidebar
        )
      )
    })

    // Add group item (including components) to site structure
    developerTreeRoot.addChild(groupNode)
  })
  return developerTreeRoot
}

function getDesignerTree() {
  // Get base structure and create tree
  const rootInfo = baseSiteStructure.designers

  const designerTreeRoot = new Node(rootInfo.name, rootInfo.id, null)

  rootInfo.children.forEach(topLink => {
    designerTreeRoot.addChild(
      new Node(topLink.name, topLink.id, designerTreeRoot)
    )
  })

  componentGroups.forEach(componentGroup => {
    // Same concept as in developer tree, check comments there
    const hasNewComponent = componentGroup.children.some(
      child => child.newComponentIndicator
    )

    const hideInSidebars = componentGroup.children.some(
      child => child.hideInSidebar
    )

    const groupNode = new Node(
      componentGroup.name,
      componentGroup.id,
      designerTreeRoot,
      frameworkEnum.NONE,
      true,
      undefined,
      null,
      undefined,
      hasNewComponent,
      hideInSidebars
    )

    componentGroup.children.forEach(component => {
      groupNode.addChild(
        new Node(
          component.name,
          component.id,
          groupNode,
          undefined,
          false,
          true,
          null,
          component.disabled,
          component.newComponentIndicator
        )
      )
    })
    designerTreeRoot.addChild(groupNode)
  })

  return designerTreeRoot
}

export function getSiteStructure() {
  return {
    [modeEnum.DEVELOPERS]: getDeveloperTree(),
    [modeEnum.DESIGNERS]: getDesignerTree(),
  } as Record<ModeEnum[keyof ModeEnum], Node>
}
