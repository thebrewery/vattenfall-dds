if (!typeof window !== undefined && typeof gtag === 'function') {
  window.addEventListener('click', ev => {
    const target = ev.target
    'dataset' in target &&
      target.dataset['gaAction'] != null &&
      gtag('event', target.dataset['gaAction'], {
        event_label: target.dataset['gaLabel'],
        event_category: target.dataset['gaCategory'] ?? 'general',
      })
  })
}
