import { Dispatch, SetStateAction } from 'react'

export class CommonUtils {
  static groupBy(arr: any[], criteria: any) {
    if (!arr || arr.length < 1) {
      return {}
    }
    return arr.reduce(function (obj, item) {
      // Check if the criteria is a function to run on the item or a property of it
      var key = typeof criteria === 'function' ? criteria(item) : item[criteria]

      // If the key doesn't exist yet, create it
      if (!obj.hasOwnProperty(key)) {
        obj[key] = []
      }

      // Push the value to the object
      obj[key].push(item)

      // Return the object to the next item in the loop
      return obj
    }, {})
  }
}

export function capitalizeFirstLetter(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export const typedKeys = Object.keys as <TObject>(
  obj: TObject
) => (keyof TObject)[]

export const getDuration = async (
  setDuration: Dispatch<SetStateAction<string>>,
  url: string
) => {
  const response = await fetch(`https://vimeo.com/api/oembed.json?url=${url}`)
  const data = await response.json()
  const minutes = Math.floor(data.duration / 60)
  const seconds = data.duration - minutes * 60

  if (isNaN(minutes) || isNaN(seconds)) {
    setDuration('')

    return
  }

  setDuration(`${minutes}:${seconds}`)
}
