import axios from 'axios'
import FormData from 'form-data'
import { FrameWorkReviewProps } from './siteStructure'

const apiBaseUrl = process.env.NEXT_PUBLIC_STRAPI_BASE_URL
const zenDeskRequestUrl = process.env.NEXT_PUBLIC_ZENDESK_REQUEST_URL
const zenDeskUploadUrl = process.env.NEXT_PUBLIC_ZENDESK_UPLOAD_FILE_URL

export type MediaType = {
  alternativeText: string
  caption: string
  created_at: string
  ext: string
  height: number
  id: number
  mime: string
  name: string
  previewUrl: boolean
  provider: string
  size: number
  updated_at: string
  url: string
  width: number
}

export type LinkComponent = {
  linkText: string
  linkHref: string
  icon: string
}

export type CarouselItem = {
  id: number
  linkHref: string
  linkLabel: string
  media: MediaType
  title: string
  subTitle: string
}

export type SemiCircleBlockProps = {
  content: string
  id: number
  image: MediaType
  rightAligned: boolean
}

export type ImageBlockProps = {
  id: number
  image: MediaType
  title: string
  description: string
  link: LinkComponent
}

export type TwoColumnContentProps = {
  content: string
  id: number
  imageBlocks: ImageBlockProps[]
  // rightAligned: boolean
}

export type ThreeArticleColumnProps = {
  articles: [
    {
      article: Article
      linkText: string
    }
  ]
  id: number
  title: string
}

export type ThreeColumnContentProps = {
  cards: [
    {
      description: string
      id: number
      image: MediaType
      link: LinkComponent
      title: string
    }
  ]
  id: number
  subtitle?: string
  title: string
}

export type TwoColumnSpacedProps = {
  content: string
  id: number
  image: MediaType
  imageContent: string
  title: string
  withBgColor: boolean
  link: LinkComponent
}

export type TwoColumnEvenProps = {
  content: string
  id: number
  image?: MediaType
  rightAligned: boolean
  title: string
  link: LinkComponent
}

export type FullWidthContentProps = {
  content: string
  id: number
}

export type AllArticleLayoutProps = {
  articles: Article[]
  title: string
  useBgColor?: boolean
  useNavigation?: boolean
}

export type ArticleBaseProps = { __component: string; useParallax: boolean }

export type ContentBlockProps = SemiCircleBlockProps &
  TwoColumnContentProps &
  ThreeArticleColumnProps &
  TwoColumnSpacedProps &
  TwoColumnEvenProps &
  ArticleBaseProps &
  FullWidthContentProps &
  ThreeColumnContentProps

export type Article = {
  articleId: string
  attachments: any[]
  cardDescription: string
  cardImage: MediaType
  cardTitle: string
  category: string
  content: string
  content_blocks: ContentBlockProps[]
  created_at: string
  hideCardImage?: boolean
  id: number
  postedDate: string
  showOnProd: boolean
  updated_at: string
  description: string
}

export type ArticleListingProps = {
  article: Article
  id: number
  title: string
}

export type SandboxDataType = {
  componentName: string
  mdFile: string
  sandbox_id: string
  assets?: {
    path: string
    type: string
  }[]
}

export type OverviewItemProps = {
  abstractUrl?: string
  adobeXdUrl?: string
  category: string
  created_at: string
  figmaUrl?: string
  frameworkReviews?: FrameWorkReviewProps
  guidelineUri?: string
  id: number
  identifier: string
  name: string
  photoshopUrl?: string
  sketchUrl?: string
  updated_at: string
}

export type OnboardingIntroProps = {
  id: number
  title: string
  linkText: string
  description: string
}

export type OnboardingCardProps = {
  id: number
  category: string
  title: string
  linkToArticle?: string
  videoUrl?: string
  previewImage?: MediaType
}

export type OnboardingPageProps = {
  id: number
  introduction: string
  introductionVideoUrl: string
  title: string
  created_at: string
  updated_at: string
  videoPreviewImage: MediaType
  designerInfo: OnboardingIntroProps
  developerInfo: OnboardingIntroProps
  designerCards: OnboardingCardProps[]
  developerCards: OnboardingCardProps[]
}

export type ContactPageProps = {
  id: number
  heading: string
  introduction: string
  formHeading: string
  formIntroduction: string
  subjectField: string
  nameField: string
  emailField: string
  commentField: string
  typeDropdown: string
  frameworkDropdown: string
  uploadHeading: string
  uploadSubheading: string
  uploadButton: string
  submitButton: string
}

export type FAQPageProps = {
  id: number
  created_at: string
  updated_at: string
  entries: {
    id: number
    question: string
    answer: string
  }[]
}

export const formatAssetUrl = (url: string) => {
  let parsedUrl

  try {
    parsedUrl = new URL(url)
    return url
  } catch (e) {
    return process.env.NEXT_PUBLIC_STRAPI_BASE_URL + url
  }
}

export class API {
  static async getDeveloperComponents() {
    try {
      const ret = await axios(`${apiBaseUrl}/developer-components`)
      return ret.data
    } catch (error) {
      console.log('ERR: failed to fetch from api', error)
    }
  }
  static async getArticles() {
    try {
      const { data } = await axios(`${apiBaseUrl}/articlepage`)
      const { articles } = data

      return articles
    } catch (error) {
      console.log('ERR: failed to fetch from api', error)
    }
  }

  static async getContactData() {
    try {
      const ret = await axios(`${apiBaseUrl}/contact-page`)

      const typedData = ret.data as ContactPageProps
      return typedData
    } catch (error) {
      console.log('ERR: failed to fetch from api')
      throw error
    }
  }

  static async getOnboardingData() {
    try {
      const ret = await axios(`${apiBaseUrl}/onboarding-page`)

      const typedData = ret.data as OnboardingPageProps
      return typedData
    } catch (error) {
      console.log('ERR: failed to fetch from api', error)
    }
  }
  static async getLandingPageData() {
    try {
      const ret = await axios(`${apiBaseUrl}/startpage`)

      const { data } = ret

      return data
    } catch (error) {
      console.log('ERR: failed to fetch from api')
      throw error
    }
  }

  static async getFAQPageData() {
    try {
      const ret = await axios(`${apiBaseUrl}/faq`)

      const typedData = ret.data as FAQPageProps
      return typedData
    } catch (error) {
      console.log('ERR: failed to fetch from api')
      throw error
    }
  }

  static async getArticle(id: string) {
    try {
      const ret = await axios(`${apiBaseUrl}/articles/${id}`)
      return ret.data
    } catch (error) {
      console.log('ERR: failed to fetch from api', error)
    }
  }

  static async postDeveloperComponents(data: {}) {
    try {
      const res = await axios
        .post(`${apiBaseUrl}/developer-components`, data)
        .then(res => {
          console.log('done', res.status)
          return res
        })

      return res.status
    } catch (error) {
      console.log('ERR: failed to update data', error)
    }
  }
  static async sendUploadRequest(
    data: {
      request: {
        requester: { name: string; email: string }
        subject: string
        comment: { body: string; uploads?: any[] }
        custom_fields: { id: number; value: string }[]
      }
    },
    files: Blob[]
  ) {
    const tokens = []

    if (files.length >= 1) {
      // create form data of the files. Needed for sending over http
      const formData = new FormData()
      files.forEach(item => {
        formData.append('attachment', item)
      })

      // get array containing all the files
      const attachments = formData.getAll('attachment')

      // every attachment needs to uploaded separately to the endpoint and we get
      // back a token for that file. Then we push these tokens to the request from the
      // user to "connect" the files to the request.
      for (const file of attachments) {
        const token = await uploadAttachments(file)
        tokens.push(token)
      }
      data.request.comment.uploads = tokens
    }

    try {
      let response = await axios({
        method: 'POST',
        url: zenDeskRequestUrl,
        headers: {
          'content-type': 'application/json',
        },
        data,
      })
      return response.status
    } catch (error: any) {
      return error.response.status
    }
  }
}

const uploadAttachments = async (file: { name: string }) => {
  try {
    let response = await axios({
      method: 'POST',
      url: zenDeskUploadUrl + file.name,
      headers: {
        'content-type': 'application/binary',
      },
      data: file,
    })

    return response.data.upload.token
  } catch (e) {
    console.log('upload attachment error:', e)
  }
}
