export class StrapiUtils {
	static regionEnum = {
		GLOBAL: 'GLOBAL',
		NL: 'NL',
		SV: 'SV'
	}

	static frameworkEnum = {
		VANILLA: 'VANILLA',
		REACT: 'REACT',
		VUE: 'VUE',
		ANGULAR: 'ANGULAR'
	}

	static designEnum = {
		FIGMA: 'figma',
		SKETCH: 'sketch',
		XD: 'xd'
	}

	static reviewStatusEnum = {
		UNKNOWN: 'UNKNOWN',
		PENDING_REVIEW: 'PENDING_REVIEW',
		IN_REVIEW: 'IN_REVIEW',
		CONDITIONAL_USE: 'CONDITIONAL_USE',
		ALLOWED: 'ALLOWED',
		ALLOWED_PENDING_UPDATE: 'ALLOWED_PENDING_UPDATE'
	}

	static countComponents(components, region, framework, designProgram) {
		// Assuming either designLink is set or region+framework is set
		return components.filter((c) => {
			if (designProgram) {
				if (designProgram === this.designEnum.FIGMA && c.figmaUrl) {
					return true
				} else if (designProgram === this.designEnum.SKETCH && c.sketchUrl) {
					return true
				} else if (designProgram === this.designEnum.XD && c.adobeXdUrl) {
					return true
				} else {
					return false
				}
			} else {
				return c.frameworkReviews.some(
					(fr) =>
						fr.exists && fr.framework === framework && fr.region === region
				)
			}
		}).length
	}
}
