import { useState } from 'react'
import styled from 'styled-components'

import { AllArticleLayoutProps, Article } from '../utils/api'
import Card from './aal/Card'
import CategoryFilter from './aal/CategoryFilter'

const AALWrapper = styled.div<{
  useBgColor: boolean
}>`
  .aal {
    &__title {
      ${({ theme }) => theme.h1Typo}
      margin-bottom: 1.875rem;
      text-align: center;

      @media screen and (min-width: 1200px) {
        margin-bottom: 3.5rem;
      }
    }

    &__wrapper {
      align-items: center;
      display: flex;
      flex-direction: column;
      row-gap: 3rem;

      @media screen and (min-width: 1200px) {
        gap: 42px;
      }

      span {
        display: inline-block;
        margin-top: 1.5rem;
      }
    }
  }

  .aal-card {
    padding:1px;
    animation: animateInCard 0.3s ease-in;
    cursor: pointer;
    direction: ltr;
    grid-column: 1 / -1;

    @media screen and (min-width: 768px) {
      grid-column: span 3;
    }

    @media screen and (min-width: 1024px) {
      grid-column: span 6;
    }

    @media screen and (min-width: 1200px) {
      grid-column: span 3;
      grid-row: span 1;
    }

    &--span-double {
      grid-column: 1 / -1;

      @media screen and (min-width: 1200px) {
        grid-column: span 6;
        grid-row: span 2;

        h2 {
          ${({ theme }) => theme.h2Typo};
        }
      }
    }

    &__image-wrapper {
      margin-bottom: 0.75rem;
      overflow: hidden;
      padding-top: 56.25%;
      position: relative;
    }

    &__image {
      inset: 0;
      height: 100%;
      position: absolute;
      object-fit: cover;
      width: 100%;
    }

    &__category {
      background-color: #fff;
      left: 0;
      padding: 0.5rem 0;
      padding-right: 1rem;
      position: absolute;
      top: 0;
    }

    &__title {
      ${({ theme }) => theme.h3Typo};
      margin-bottom: 0.5rem;
      max-width: 550px;
    }

    &__description {
      margin-bottom: 0.75rem;
      max-width: 650px;
    }
  }

  .vf-future-vision__grid.inverted {
    @media screen and (min-width: 1200px) {
      direction: rtl;
    }
  }

  .vf-button__outline--secondary {
    width: fit-content;
  }

  .disabled {
    background-color: #eeeeee;
    border-color: #eee;
    color: #767676;
    cursor: not-allowed;
    opacity: 0.65;
    pointer-events: none;
  }

  @keyframes animateInCard {
    0% {
      opacity: 0;
      transform: translateY(10%);
    }

    100% {
      opacity: 1;
      transform: translateY(0);
    }
  }

  .vf-future-vision__grid {
    margin: 0 !important;
    row-gap: 3rem;

    @media screen and (min-width: 1200px) {
      row-gap: inherit;
    }
  }
`

type GridRow = {
  articles: Article[]
}

const AllArticleLayout = ({
  articles,
  // title, not used for now
  useBgColor = false,
  useNavigation = false,
}: AllArticleLayoutProps) => {
  const [amountOfArticlesShown, setAmountOfArticlesShown] = useState(6)
  const [activeNavItem, setActiveNavItem] = useState<string>('Latest')

  const navigationFilteredArticles =
    activeNavItem === 'Latest'
      ? articles
      : articles.filter(
          article => article.category === activeNavItem.toLowerCase()
        )

  const rowsOfArticles: GridRow[] = []

  for (let i = 0; i < amountOfArticlesShown; i += 3) {
    const gridRow: GridRow = {
      articles: navigationFilteredArticles.slice(i, i + 3),
    }

    rowsOfArticles.push(gridRow)
  }

  return (
    <AALWrapper useBgColor={useBgColor}>
      {useNavigation && (
        <CategoryFilter
          activeNavItem={activeNavItem}
          articles={articles}
          setActiveNavItem={setActiveNavItem}
        />
      )}
      <div className="aal__wrapper">
        {rowsOfArticles.length > 1 &&
          rowsOfArticles.map((articleArray, index) => (
            <div
              className={`vf-future-vision__grid`}
              key={index}
            >
              {articleArray.articles.map(
                (article, articleIndex) =>
                  articleIndex < amountOfArticlesShown && (
                    <Card
                      article={article}
                      articleIndex={articleIndex}
                      isReversed = {index % 2 ? 'inverted' : ''}
                      key={article.articleId}
                    />
                  )
              )}
            </div>
          ))}
        <span
          className={`vf-button vf-button__outline--secondary ${
            amountOfArticlesShown >= articles.length ? 'disabled' : ''
          }`}
          onClick={() => {
            if (amountOfArticlesShown >= articles.length) return

            setAmountOfArticlesShown(oldAmount => oldAmount + 3)
          }}
          tabIndex={0}
        >
          More articles
        </span>
      </div>
    </AALWrapper>
  )
}

export default AllArticleLayout
