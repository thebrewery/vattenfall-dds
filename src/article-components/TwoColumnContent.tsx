import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import styled from 'styled-components'

import { useViewParallax } from '../hooks/useViewParallax'
import {
  ArticleBaseProps,
  formatAssetUrl,
  TwoColumnContentProps,
} from '../utils/api'
import { typedKeys } from '../utils/common'
import { ImageBlock } from './tcc/ImageBlock'

const TwoColumnContentWrapper = styled.div<{ rightAligned?: boolean }>`
  .two-column-content {
    &__title {
      ${({ theme }) => theme.h1Typo};
    }

    &__media-container {
      direction: ltr;
      display: flex;
      flex-direction: column;
      gap: 1.75rem;
      grid-column: 1 / -1;

      @media screen and (min-width: 1024px) {
        gap: 3.5rem;
        grid-column: 8 / span 4;
      }
    }

    &__text-container {
      direction: ltr;
      grid-column: 1 / -1;

      @media screen and (min-width: 1024px) {
        grid-column: 2 / span 5;
      }
    }

    &__image-block {
      background-color: #ecf1f7;
      display: flex;
      flex-direction: column;
      gap: 1rem;

      &-image-wrapper {
        overflow: hidden;
        padding-top: 56.25%;
        position: relative;
        width: 100%;
      }

      &-image {
        position: absolute;
        inset: 0;
        height: 100%;
        object-fit: cover;
        width: 100%;
      }

      &__title {
        ${({ theme }) => theme.h3Typo};
        padding: 0 1rem;
      }

      &__description {
        line-height: 1.5;
        padding: 0 1rem;
      }

      .vf-link--with-arrow {
        margin-left: 1rem;
        margin-bottom: 1rem;
      }
    }

    &__text-container {
      strong {
        font-size: 0.75rem;
        font-weight: 600;
        letter-spacing: 2px;
        text-transform: uppercase;
      }

      a {
        color: ${({ theme }) => theme.colours.link__color};
        display: inline-block;
        text-decoration: none;
        background-color: transparent;
        font-family: 'Vattenfall Hall NORDx';
        font-weight: 500;
        font-style: normal;
      }

      blockquote {
        margin: 0;
        position: relative;

        &::before {
          color: #000;
          content: '”';
          font-size: 8rem;
          left: 0;
          line-height: 1;
          top: 1.8rem;
          position: absolute;
          text-align: center;

          @media screen and (min-width: 1024px) {
            font-size: 17rem;
            top: 0;
          }
        }

        p {
          ${({ theme }) => theme.h3Typo};
          font-weight: 500;
          margin-left: 59px;
          padding: 2.5rem 0;

          @media screen and (min-width: 1024px) {
            margin-left: 110px;
          }
        }
      }

      .image-wrapper {
        // this image should be able to sized to 16/9, 1/1 and to a square or rounded
        padding-top: 56.25%;
        position: relative;
        width: 100%;

        &.round {
          border-radius: 100%;
          overflow: hidden;
          padding-top: 100%;
        }

        &.sixteen-nine {
          padding-top: 56.25%;
        }

        &.square {
          padding-top: 100%;
        }
      }

      .image {
        height: 100%;
        inset: 0;
        object-fit: cover;
        position: absolute;
        width: 100%;
      }
    }
  }

  /* .vf-future-vision__grid {
    direction: ${({ rightAligned }) => (rightAligned ? 'rtl' : 'ltr')};
  } */

  details {
    border-bottom: 1px solid #e6e6e6;
    padding: 20px 0;

    summary {
      list-style: none;
      outline: none;
      font-size: 20px;
      line-height: 28px;
      font-weight: 500;
      position: relative;
      display: flex;
      justify-content: space-between;
      cursor: pointer;

      &::after {
        content: '';
        width: 20px;
        background-image: url("data:image/svg+xml,%3Csvg width='20' height='10' viewBox='0 0 20 10' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M9.99194 9.04225L0.400391 1.57746L1.62574 0L9.99194 6.51408L18.3652 0L19.5905 1.57746L9.99194 9.04225Z' fill='%232071B5'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: center;
      }

      &::marker,
      &::-webkit-details-marker {
        display: none;
      }
    }

    & > * {
      margin: 0;

      &:not(summary) {
        padding-top: 12px;
      }
    }

    &[open] {
      summary {
        margin-bottom: 0;

        &::after {
          transform: rotate(180deg);
        }
      }
    }
  }
`

export const TwoColumnContent = ({
  content,
  id,
  imageBlocks,
  // It seems we don't want this option anymore, but I'll leave it for the future
  // rightAligned = true,
  useParallax = true,
}: TwoColumnContentProps & ArticleBaseProps) => {
  const parallaxRef = useViewParallax<HTMLDivElement>()
  return (
    <TwoColumnContentWrapper
      // rightAligned={rightAligned}
      ref={useParallax ? parallaxRef : null}
    >
      <div className="vf-future-vision__grid">
        <div className="two-column-content__text-container">
          <ReactMarkdown
            rehypePlugins={[rehypeRaw]}
            transformImageUri={formatAssetUrl}
            className="bigger-text-sizes"
            components={{
              img: ({ src, alt }) => {
                const aspectRatioMap = {
                  round: 'round',
                  '16/9': 'sixteen-nine',
                  square: 'square',
                }

                const aspectRatioStyle = typedKeys(aspectRatioMap).find(
                  key => alt?.indexOf(key) !== -1
                )

                const imageModifierClass = aspectRatioStyle
                  ? aspectRatioMap[aspectRatioStyle]
                  : ''

                const altText = alt?.split('aspect-ratio=')[0]

                return (
                  <div className={`image-wrapper ${imageModifierClass}`}>
                    <img className="image" src={src} alt={altText} />
                  </div>
                )
              },
            }}
          >
            {content}
          </ReactMarkdown>
        </div>
        <div className="two-column-content__media-container">
          {imageBlocks.map(imageBlock => (
            <ImageBlock imageBlock={imageBlock} key={imageBlock.id} />
          ))}
        </div>
      </div>
    </TwoColumnContentWrapper>
  )
}
