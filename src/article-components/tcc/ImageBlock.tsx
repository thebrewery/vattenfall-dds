import Link from 'next/link'

import { useViewParallax } from '../../hooks/useViewParallax'
import { formatAssetUrl, ImageBlockProps } from '../../utils/api'

export const ImageBlock = ({ imageBlock }: { imageBlock: ImageBlockProps }) => {
  const parallaxRef = useViewParallax<HTMLDivElement>()

  return (
    <div
      key={`image-block-${imageBlock.id}`}
      className="two-column-content__image-block"
      ref={parallaxRef}
    >
      <div className="two-column-content__image-block-image-wrapper">
        <img
          src={formatAssetUrl(imageBlock.image.url)}
          alt={imageBlock.image.alternativeText}
          className="two-column-content__image-block-image"
        />
      </div>
      <h3 className="two-column-content__image-block__title">
        {imageBlock.title}
      </h3>
      <p className="two-column-content__image-block__description">
        {imageBlock.description}
      </p>
      <Link href={imageBlock.link.linkHref}>
        <a className="vf-link--with-arrow">{imageBlock.link.linkText}</a>
      </Link>
    </div>
  )
}
