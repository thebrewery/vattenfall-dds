import styled from 'styled-components'

import { IMAGE_PARALLAX_PROPS, useViewParallax } from '../hooks/useViewParallax'
import { MediaType, formatAssetUrl } from '../utils/api'

const ArticleLayoutCardStyle = styled.a`
  color: #000;

  .alc {
    &__image-wrapper {
      margin-bottom: 1.5rem;
      overflow: hidden;
      padding-top: 56.25%;
      position: relative;
      width: 100%;
      @media screen and (min-width: 768px) {
        min-height: 285px;
      }
    }

    &__image {
      position: absolute;
      height: 100%;
      object-fit: cover;
      top: 0;
      width: 100%;
    }

    &__title {
      ${({ theme }) => theme.h2Typo};
      margin-bottom: 1rem;
    }

    &__description {
      line-height: 1.5;
      margin-bottom: 1rem;
    }
  }

  &:hover {
    cursor: pointer;

    .future-vision__link::before {
      transform: translate(-125%, 0);
    }
  }
`

type ArticleLayoutCardProps = {
  cardTitle: string
  cardDescription: string
  image: MediaType
  linkHref: string
  linkText: string
  usePrefix?: boolean
}

export const LayoutCard = ({
  cardTitle,
  cardDescription,
  image,
  linkHref,
  linkText,
  usePrefix = false,
}: ArticleLayoutCardProps) => {
  const imageParallaxRef =
    useViewParallax<HTMLImageElement>(IMAGE_PARALLAX_PROPS)

  return (
    <ArticleLayoutCardStyle href={`${usePrefix ? '/articles' : ''}${linkHref}`}>
      <div className="alc__image-wrapper">
        <img
          src={formatAssetUrl(image.url)}
          alt={image.alternativeText}
          className="alc__image"
          ref={imageParallaxRef}
        />
      </div>
      <h2 className="alc__title">{cardTitle}</h2>
      <p className="alc__description">{cardDescription}</p>
      {linkText && (
        <span className="future-vision__link future-vision__link--small">
          {linkText}
        </span>
      )}
    </ArticleLayoutCardStyle>
  )
}
