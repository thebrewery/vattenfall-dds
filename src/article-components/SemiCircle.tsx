import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import styled, { css } from 'styled-components'

import ContentWrapper from '../components/ContentWrapper'
import { useViewParallax } from '../hooks/useViewParallax'
import {
  ArticleBaseProps,
  formatAssetUrl,
  SemiCircleBlockProps,
} from '../utils/api'

const SemiCircleWrapper = styled.div<{ rightAligned?: boolean }>`
  position: relative;

  @media screen and (min-width: 1024px) {
    height: 750px;
  }

  @media screen and (min-width: 1400px) {
    height: 1250px;
  }

  .semi-circle {
    &__content {
      max-width: 625px;
    }

    &__image-wrapper {
      height: 100%;
      max-width: 50%;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);

      ${({ rightAligned }) =>
        rightAligned
          ? css`
              right: 0;
            `
          : css`
              left: 0;
            `};

      @media screen and (min-width: 1400px) {
        max-width: none;
      }
    }

    &__image {
      height: 100%;
      width: 100%;
      ${({ rightAligned }) =>
        rightAligned
          ? css`
              clip-path: circle(50% at 100% 50%);
            `
          : css`
              clip-path: circle(50% at 0 50%);
            `};

      @media screen and (max-width: 1024px) {
        display: none;
      }
    }
  }
`

export const SemiCircle = ({
  content,
  id,
  image,
  rightAligned = false,
  useParallax = true,
}: SemiCircleBlockProps & ArticleBaseProps) => {
  const parallaxRef = useViewParallax<HTMLDivElement>()

  return (
    <SemiCircleWrapper
      rightAligned={rightAligned}
      ref={useParallax ? parallaxRef : null}
    >
      <ContentWrapper
        style={{
          flexDirection: rightAligned ? 'row' : 'row-reverse',
          height: '100%',
          alignItems: 'center',
          justifyContent: 'start',
        }}
      >
        <div className="semi-circle__content">
          <ReactMarkdown
            rehypePlugins={[rehypeRaw]}
            className="bigger-text-sizes"
          >
            {content}
          </ReactMarkdown>
        </div>
      </ContentWrapper>
      {image && (
        <div className="semi-circle__image-wrapper">
          <img
            src={formatAssetUrl(image.url)}
            alt={image.alternativeText}
            className="semi-circle__image"
          />
        </div>
      )}
    </SemiCircleWrapper>
  )
}
