import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import styled, { css } from 'styled-components'

import ContentWrapper from '../components/ContentWrapper'
import { IMAGE_PARALLAX_PROPS, useViewParallax } from '../hooks/useViewParallax'
import {
  ArticleBaseProps,
  TwoColumnSpacedProps,
  formatAssetUrl,
} from '../utils/api'

const TwoColumnSpacedStyle = styled.div<{ withBgColor: boolean }>`
  --imageDesktopHeight: 355px;

  ${({ withBgColor }) =>
    withBgColor
      ? css`
          background-color: #edf1f6;
        `
      : css`
          background-color: #fff;
        `}
  @media screen and (min-width: 768px) {
    margin-bottom: 5.25rem;
    margin-top: calc(var(--imageDesktopHeight) / 2 + 5.25rem);
  }

  .tcss {
    &__grid {
      display: grid;
      grid-template-columns: 1fr;
      gap: 42px;
      @media screen and (min-width: 768px) {
        grid-template-columns: 1fr 1fr;
      }
    }

    &__title {
      ${({ theme }) => theme.h1Typo};
      margin-bottom: 2.5rem;
      max-width: 625px;
    }

    &__content {
      p {
        line-height: 1.5;
        margin-bottom: 1.5rem;
      }
    }

    &__content-container {
      grid-template-columns: 1fr 1fr;
      margin-bottom: 1em;
      transform: translateY(-2.25em);
      @media screen and (min-width: 768px) {
        margin-bottom: 0;
      }
    }

    &__card-container {
      display: flex;
      flex-direction: column;
      align-items: center;
      @media screen and (min-width: 768px) {
        margin-top: calc(var(--imageDesktopHeight) / 2 * -1);
        grid-row: auto;
        margin-right: 84px;
        align-items: flex-end;
      }
    }

    &__image-wrapper {
      border-radius: 50%;
      overflow-x: hidden;
      overflow-y: hidden;
      padding-top: var(--imageDesktopHeight);
      position: relative;
      width: var(--imageDesktopHeight);
      /* Safari will not clip overflow of transformed child, but this works. */
      -webkit-mask-image: -webkit-radial-gradient(white, black);
    }

    &__image {
      height: 100%;
      margin-bottom: 1rem;
      transform: scale(1.2);
      object-fit: cover;
      position: absolute;
      top: 0;
      width: 100%;
    }

    &__image-content {
      max-width: 355px;
      text-align: center;
      padding-bottom: 3.125rem;
      margin-top: 1rem;

      h1 {
        ${({ theme }) => theme.h1Typo};
      }

      h2 {
        ${({ theme }) => theme.h2Typo};
      }

      h3 {
        ${({ theme }) => theme.h3Typo};
      }

      p {
        line-height: 1.5;
      }

      h1,
      h2,
      h3,
      h4,
      p {
        margin-bottom: 1rem;
      }
    }
    &__link-container {
      margin-top: 1.75rem;
    }
  }
`

export const TwoColumnSpaced = ({
  content,
  id,
  image,
  imageContent,
  title,
  withBgColor,
  useParallax = true,
  link: { icon, linkHref, linkText },
}: TwoColumnSpacedProps & ArticleBaseProps) => {
  const parallaxRef = useViewParallax<HTMLDivElement>()
  const imageParallaxRef =
    useViewParallax<HTMLImageElement>(IMAGE_PARALLAX_PROPS)
  const resolvedIcon = icon && icon.replace('_', '-').toLowerCase()
  return (
    <TwoColumnSpacedStyle
      ref={useParallax ? parallaxRef : null}
      withBgColor={withBgColor}
    >
      <ContentWrapper className="tcss__grid">
        <div className="tcss__content-container">
          <h2 className="tcss__title">{title}</h2>
          <ReactMarkdown className="tcss__content" rehypePlugins={[rehypeRaw]}>
            {content}
          </ReactMarkdown>
          {linkHref && linkText && (
            <div className="tcss__link-container">
              {/* {icon && <span className={`vf-icon-${resolvedIcon}`} />} */}
              <a
                className="future-vision__link future-vision__link--small"
                href={linkHref}
              >
                {linkText}
              </a>
            </div>
          )}
        </div>
        <div className="tcss__card-container">
          {image && (
            <div className="tcss__image-wrapper">
              <img
                className="tcss__image"
                src={formatAssetUrl(image.url)}
                alt={image.alternativeText}
                ref={imageParallaxRef}
              />
            </div>
          )}
          <ReactMarkdown
            className="tcss__image-content"
            rehypePlugins={[rehypeRaw]}
          >
            {imageContent}
          </ReactMarkdown>
        </div>
      </ContentWrapper>
    </TwoColumnSpacedStyle>
  )
}
