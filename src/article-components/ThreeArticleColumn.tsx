import styled from 'styled-components'
import ContentWrapper from '../components/ContentWrapper'
import { useViewParallax } from '../hooks/useViewParallax'
import { ArticleBaseProps, ThreeArticleColumnProps } from '../utils/api'
import { LayoutCard } from './LayoutCard'

const ThreeArticleColumnStyle = styled.div`
  .tac {
    &__grid {
      display: grid;
      grid-template-columns: repeat(1, 1fr);
      gap: 42px;
      @media screen and (min-width: 768px) {
        grid-template-columns: repeat(3, 1fr);
      }
    }

    &__title {
      ${({ theme }) => theme.h1Typo}
      text-align: center;
      margin-bottom: 3.75rem;
    }
  }
`

export const ThreeArticleColumn = ({
  articles,
  id,
  title,
  useParallax = true,
}: ThreeArticleColumnProps & ArticleBaseProps) => {
  const parallaxRef = useViewParallax<HTMLDivElement>()

  return (
    <ContentWrapper withVerticalMargin>
      <ThreeArticleColumnStyle ref={useParallax ? parallaxRef : null}>
        <h2 className="tac__title">{title}</h2>
        <div className="tac__grid">
          {articles &&
            articles.map(({ article, linkText }) => {
              const {
                cardTitle,
                cardImage,
                cardDescription,
                articleId: linkHref,
              } = article

              return (
                <LayoutCard
                  usePrefix={true}
                  cardTitle={cardTitle}
                  image={cardImage}
                  cardDescription={cardDescription}
                  linkHref={linkHref}
                  key={linkHref}
                  linkText={linkText}
                />
              )
            })}
        </div>
      </ThreeArticleColumnStyle>
    </ContentWrapper>
  )
}
