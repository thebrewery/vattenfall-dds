import styled from 'styled-components'
import ContentWrapper from '../components/ContentWrapper'
import { useViewParallax } from '../hooks/useViewParallax'
import { ArticleBaseProps, ThreeColumnContentProps } from '../utils/api'
import { LayoutCard } from './LayoutCard'

const ThreeColumnContentStyle = styled.div`
  .tcc {
    &__grid {
      display: grid;
      grid-template-columns: repeat(1, 1fr);
      gap: 42px;
      @media screen and (min-width: 768px) {
        grid-template-columns: repeat(3, 1fr);
      }
    }

    &__title {
      ${({ theme }) => theme.h1Typo}
      text-align: center;
      margin-bottom: 3.75rem;
    }

    &__subtitle {
      text-align: center;
      font-size: 1.25rem;
      margin-bottom: 16px;
    }

    &__footer {
      text-align: center;
      margin-top: 5.5rem;
  }
`

export const ThreeColumnContent = ({
  cards,
  id,
  title,
  subtitle,
  useParallax = true,
}: ThreeColumnContentProps & ArticleBaseProps) => {
  const parallaxRef = useViewParallax<HTMLDivElement>()

  return (
    <ContentWrapper withVerticalMargin className="tcc__contentwrapper">
      <ThreeColumnContentStyle>
        <div className="tcc__header">
          {!subtitle && <p className="tcc__subtitle">Foundation</p>}
          <h2 className="tcc__title">{title}</h2>
        </div>
        <div className="tcc__grid">
          {cards.map(({ description, image, link, id, title }) => {
            return (
              <LayoutCard
                cardDescription={description}
                cardTitle={title}
                image={image}
                linkHref={link.linkHref}
                linkText={link.linkText}
                key={id + title}
              />
            )
          })}
        </div>

        <div className="tcc__footer">
          <a
            href="/articles"
            className="vf-button vf-button__outline--secondary"
          >
            View all resources
          </a>
        </div>
      </ThreeColumnContentStyle>
    </ContentWrapper>
  )
}
