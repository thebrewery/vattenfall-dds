import { useEffect, useRef } from 'react'
import styled from 'styled-components'

import { VattenfallMarkdown } from '../pages/articles/[articleid]'
import { formatAssetUrl, FullWidthContentProps } from '../utils/api'
import { typedKeys } from '../utils/common'
import { copyTargets } from './copy-targets'

const FullWidthContentWrapper = styled.div`
  margin-bottom: 2rem;

  .fwc {
    &__content {
      display: flex;
      flex-direction: column;
      grid-column: 1 / -1;
      margin: 0 auto;
      width: 100%;

      @media screen and (min-width: 1024px) {
        grid-column: 2 / 9;
      }

      h1 {
        ${({ theme }) => theme.h1Typo};
      }

      h2 {
        ${({ theme }) => theme.h2Typo};
      }

      h3 {
        ${({ theme }) => theme.h3Typo};
      }

      blockquote {
        display: flex;
        flex-direction: row;
        margin: 0;
      }

      blockquote::before {
        content: '“';
      }
      blockquote::after {
        content: '”';
      }

      p {
        line-height: 1.5;
      }

      h1,
      h2,
      h3,
      h4,
      p {
        margin-bottom: 1.15rem;
      }

      p:last-of-type {
        margin-bottom: 0;
      }

      a:hover,
      .copy-target:hover {
        text-decoration: underline;
      }

      h1,
      h2 {
        padding-top: 3rem;
      }

      p {
        font-size: ${({ theme }) => theme.fontSize};
        letter-spacing: -0.05px;
        line-height: ${({ theme }) => theme.mediumLineHeight};
        font-family: 'Vattenfall Hall NORDx', 'Vattenfall Hall NORDx',
          sans-serif;
        -webkit-font-smoothing: antialiased;
        margin-bottom: 1.25rem;
      }

      ul {
        margin: 0;
      }

      a,
      .copy-target {
        color: ${({ theme }) => theme.colours.link__color};
        cursor: pointer;
        text-decoration: none;
        background-color: transparent;
        font-family: 'Vattenfall Hall NORDx';
        font-weight: 500;
        font-style: normal;
      }

      img {
        width: 100%;
      }

      .halfwidth {
        width: 50%;
      }
    }
  }

  code {
    align-items: center;
    background: #f6f7fb;
    display: flex;
    gap: 1rem;
    margin-top: 3.25rem;
    padding: 0.5rem 1rem;
    justify-content: space-between;
    word-break: break-all;

    .code-copy {
      align-items: center;
      background: #fff;
      border: 1px solid #767676;
      border-radius: 4px;
      content: '';
      cursor: pointer;
      display: flex;
      font-size: 0.85rem;
      height: fit-content;
      min-height: 38px;
      padding: 0.25rem 0.5rem;
      right: 0;
      top: -3rem;
      position: absolute;
      white-space: nowrap;

      &:hover {
        background: #f6f7fb;
      }
    }
  }

  pre {
    white-space: break-spaces;
  }
`

type CopyTarget = {
  dataset: {
    copyTarget: 'header'
  }
} & HTMLAnchorElement

export const FullWidthContent = ({ content }: FullWidthContentProps) => {
  const codeRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (!codeRef) return

    codeRef.current?.querySelectorAll('code').forEach(el => {
      el.style.position = 'relative'

      const div = document.createElement('div')
      div.classList.add('code-copy')
      const copyToClipboard = 'Copy to clipboard'
      div.textContent = copyToClipboard

      div.addEventListener('click', () => {
        navigator.clipboard.writeText(
          el.innerText.substring(
            0,
            el.innerText.length - copyToClipboard.length
          )
        )

        div.textContent = 'Copied!'

        setTimeout(() => {
          div.textContent = 'Copy to clipboard'
        }, 750)
      })

      el.append(div)

      return () => {
        div.remove()
      }
    })

    codeRef.current?.querySelectorAll('[data-copy-target]').forEach(el => {
      const element = el as CopyTarget

      if (element.dataset.copyTarget === 'header') {
        element.addEventListener('click', () => {
          navigator.clipboard.writeText(copyTargets.header)
        })
      }
    })
  }, [])

  return (
    <FullWidthContentWrapper ref={codeRef}>
      <div className="vf-future-vision__grid">
        <VattenfallMarkdown
          className="fwc__content"
          linkTarget="_blank"
          transformImageUri={formatAssetUrl}
          components={{
            img: ({ src, alt, ...props }) => {
              const aspectRatioMap = {
                halfwidth: 'halfwidth',
              }

              const aspectRatioStyle = typedKeys(aspectRatioMap).find(
                key => alt?.indexOf(key) !== -1
              )

              const imageModifierClass = aspectRatioStyle
                ? aspectRatioMap[aspectRatioStyle]
                : ''

              const altText = alt?.split('aspect-ratio=')[0]

              return imageModifierClass ? (
                <img
                  className={imageModifierClass}
                  src={src}
                  alt={altText}
                  {...props}
                />
              ) : (
                <img src={src} alt={altText} {...props} />
              )
            },
          }}
        >
          {content}
        </VattenfallMarkdown>
      </div>
    </FullWidthContentWrapper>
  )
}
