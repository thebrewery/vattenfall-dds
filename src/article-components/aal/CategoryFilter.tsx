import classNames from 'classnames'
import { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { Article } from '../../utils/api'
import { capitalizeFirstLetter } from '../../utils/common'

const NavWrapper = styled.nav<{
  offset?: number
  navWidth?: number
}>`
  .aal {
    &__navigation {
      border-bottom: #d8d8d8 3px solid;
      display: flex;
      flex-direction: row;
      justify-content: center;
      margin: 0 auto;
      margin-bottom: 1.875rem;
      gap: 13px;
      overflow-x: clip;
      position: relative;
      width: fit-content;

      @media screen and (min-width: 1200px) {
        margin-bottom: 3.5rem;
        gap: 42px;
      }

      &::after {
        background-color: ${({ theme }) => theme.colours.primary__ocean_blue};
        bottom: -3px;
        content: '';
        height: 3px;
        left: ${({ offset }) => offset}px;
        position: absolute;
        transition: left 0.3s ease-in-out, width 0.3s ease-in-out;
        width: ${({ navWidth }) => navWidth}px;
      }
    }

    &__navigation-item {
      cursor: pointer;
      padding: 1rem;
    }
  }
`

const CategoryFilter = ({
  activeNavItem,
  setActiveNavItem,
  articles,
}: {
  activeNavItem: string
  setActiveNavItem: (arg0: string) => void
  articles: Article[]
}) => {
  const [navOffset, setNavOffset] = useState<number>(0)
  const [navWidth, setNavWidth] = useState<number>(0)
  const navRef = useRef<HTMLElement>(null)

  useEffect(() => {
    if (!navRef.current) return

    const activeElement = navRef.current.querySelector(
      '.active'
    ) as HTMLDivElement
    const width = parseInt(window.getComputedStyle(activeElement).width) + 1

    setNavWidth(width)
    // setNavOffset(activeElement.offsetLeft - relevantOffset / 2)
    setNavOffset(activeElement.offsetLeft)
  }, [activeNavItem])

  let allCategories: Set<string> = new Set()
  allCategories.add('Latest')

  articles.forEach(article => {
    const capitalizedCategory = capitalizeFirstLetter(article.category)
    allCategories.add(capitalizedCategory)
  })

  return (
    <NavWrapper className="aal__navigation" ref={navRef}>
      {Array.from(allCategories).map(category => (
        <div
          key={category}
          className={classNames('aal__navigation-item', {
            active: activeNavItem === category,
          })}
          onClick={() => setActiveNavItem(category)}
        >
          <span>{category}</span>
        </div>
      ))}
    </NavWrapper>
  )
}

export default CategoryFilter
