import Link from 'next/link'

import {
  IMAGE_PARALLAX_PROPS,
  useViewParallax,
} from '../../hooks/useViewParallax'
import { VattenfallMarkdown } from '../../pages/articles/[articleid]'
import { Article, formatAssetUrl } from '../../utils/api'

const Card = ({
  article,
  articleIndex,
  isReversed
}: {
  article: Article
  articleIndex: number
  isReversed: string
}) => {
  const imageParallaxRef =
    useViewParallax<HTMLImageElement>(IMAGE_PARALLAX_PROPS)

  const divider = (isReversed == "")? 3 : 1;

  return (
    <Link key={article.articleId} href={`/articles/${article.articleId}`}>
      <div
        className={`aal-card ${
          (((articleIndex + 1) % 3 == 0 && isReversed == "") || (isReversed !== "" && (articleIndex + 1) % 3 == 1) ) ? 'aal-card--span-double' : ''
        }`}
        tabIndex={0}
      >
        <div className="aal-card__image-wrapper">
          <img
            className="aal-card__image"
            alt={article.cardImage.alternativeText}
            src={formatAssetUrl(article.cardImage.url)}
            ref={imageParallaxRef}
          />
          {/* <span className="aal-card__category">
          {capitalizeFirstLetter(article.category)}
        </span> */}
        </div>

        <h2 className="aal-card__title">
          <VattenfallMarkdown linkTarget="_blank">
            {article.cardTitle} 
          </VattenfallMarkdown>
        </h2>

        <VattenfallMarkdown
          className="aal-card__description"
          linkTarget="_blank"
        >
          {article.cardDescription}
        </VattenfallMarkdown>
        <a className="future-vision__link future-vision__link--small">
          Read more
        </a>
      </div>
    </Link>
  )
}

export default Card
