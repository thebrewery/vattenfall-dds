import ReactMarkdown from 'react-markdown'
import styled, { css } from 'styled-components'

import ContentWrapper from '../components/ContentWrapper'
import { useViewParallax } from '../hooks/useViewParallax'

import {
  ArticleBaseProps,
  formatAssetUrl,
  TwoColumnEvenProps,
} from '../utils/api'

const TwoColumnEvenStyle = styled.div<{
  rightAligned: boolean
  imageExists: boolean
}>`
  direction: ${({ rightAligned }) => (rightAligned ? 'rtl' : 'ltr')};
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 42px;
  @media screen and (min-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
  }

  .tce {
    &__image-wrapper {
      min-height: 450px;
      padding-top: 56.25%;
      position: relative;
      width: 100%;
    }

    &__image {
      height: 100%;
      object-fit: cover;
      position: absolute;
      top: 0;
      width: 100%;
    }

    &__content-wrapper {
      align-self: center;
      display: flex;
      direction: ltr;
      flex-direction: column;
      max-width: 500px;

      ${({ imageExists, rightAligned }) =>
        rightAligned &&
        !imageExists &&
        css`
          grid-column-start: none;
        `}
    }

    &__title {
      ${({ theme }) => theme.h1Typo};
      margin-bottom: 2.5rem;
    }

    &__content {
      margin-bottom: 2.5rem;

      h1 {
        ${({ theme }) => theme.h1Typo};
      }

      h2 {
        ${({ theme }) => theme.h2Typo};
      }

      h3 {
        ${({ theme }) => theme.h3Typo};
      }

      p {
        line-height: 1.5;
      }

      h1,
      h2,
      h3,
      h4,
      p {
        margin-bottom: 1.15rem;
      }

      p:last-of-type {
        margin-bottom: 0;
      }
    }

    &__link-container {
      align-items: center;
      color: ${({ theme }) => theme.colours.link__color};
      display: flex;

      & [class^='vf-icon-'],
      [class*=' vf-icon-'] {
        &:before {
          color: #000;
          font-size: 2.5rem;
          padding-right: 1rem;
        }
      }
    }
  }
`

export const TwoColumnEven = ({
  content,
  id,
  image,
  rightAligned,
  title,
  link,
  useParallax = true,
}: TwoColumnEvenProps & ArticleBaseProps) => {
  const parallaxRef = useViewParallax<HTMLDivElement>()
  const { icon, linkHref, linkText } = link ?? {}

  const resolvedIcon = icon && icon.replace('_', '-').toLowerCase()

  return (
    <ContentWrapper withVerticalMargin>
      <TwoColumnEvenStyle
        ref={useParallax ? parallaxRef : null}
        rightAligned={rightAligned}
        imageExists={!!image}
      >
        <div className="tce__image-wrapper">
          {image && (
            <img
              className="tce__image"
              src={formatAssetUrl(image.url)}
              alt={image.alternativeText}
            />
          )}
        </div>
        <div className="tce__content-wrapper">
          <h2 className="tce__title">{title}</h2>
          <ReactMarkdown className="tce__content">{content}</ReactMarkdown>
          {linkHref && linkText && (
            <div className="tce__link-container">
              {icon && <span className={`vf-icon-${resolvedIcon}`} />}
              <a
                className="future-vision__link future-vision__link--small"
                href={linkHref}
              >
                {linkText}
              </a>
            </div>
          )}
        </div>
      </TwoColumnEvenStyle>
    </ContentWrapper>
  )
}
