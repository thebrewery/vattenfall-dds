// Project specific interfaces
declare module 'form-data' {
  import FormData from 'form-data'
  declare const value: FormData.default & {
    getAll(): string[]
  }
  export default value
}

declare module '*.md' {
  declare const value: string
  export default value
}

declare module 'ScrollTime.js' {
  declare const value: any
  export default value
}
