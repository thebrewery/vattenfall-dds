import { useState } from 'react'
import styled from 'styled-components'
import { ContactPageProps } from '../../../utils/api'

const UploadContainer = styled.div`
  ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
    width: 100%;
    display: flex;
    justify-content: center;
    flex-direction: row;
    flex-wrap: wrap;
    margin-bottom: 28px;
    transition: all 0.4s ease;

    li {
      align-items: center;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      height: 40px;
      text-align: center;
      padding: 0;
      margin: 0;
      width: 180px;
      transition: all 0.4s ease;
    }

    img {
      height: 12px;
      line-height: 0.625rem;
      margin: 0 4px;
      cursor: pointer;
    }
  }

  .vf-upload {
    label {
      margin-bottom: 20px;
      box-sizing: border-box;
    }

    .disabled {
      color: ${({ theme }) => theme.tertiary__medium_dark_grey};
      background-color: #eee;
    }

    .errorText {
      color: ${({ theme }) => theme.secondary__red} !important;
    }

    .vf-icon-span {
      line-height: 0.625rem;
      margin: 0 0 0 10px;
      cursor: pointer;
    }

    .vf-icon-span::before {
      font-size: ${({ theme }) => theme.fontSize};
    }

    &__container {
      align-items: center;
      display: flex;
      flex-direction: column;
      padding: 26px 0;
      width: 264px;
    }

    &__btn {
      padding: 9px 28px !important;
    }

    &__text {
      font-size: ${({ theme }) => theme.fontSize};
      font-weight: 500;
      line-height: ${({ theme }) => theme.mediumLineHeight};
      letter-spacing: -0.05px;
    }

    &__size {
      color: #757575;
      font-size: 0.875rem;
      line-height: 1.5rem;
      margin-bottom: 14px;
    }

    &--drag-n-drop {
      align-items: center;
      border: ${({ theme }) => theme.text__dark_color} dashed thin;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding: 28px 0;
      width: 264px;
    }

    &__icon {
      align-items: center;
      color: #ebf2f3;
      display: flex;
      justify-content: center;
      margin: 28px 0;
      width: 100%;

      &::before {
        font-size: 80px !important;
      }
    }
  }

  .upload__success {
    color: #3dc07c;
  }

  // OLD SCSS for DnD

  .vf-file-upload__container,
  .vf-file-upload--dragndrop {
    width: 264px;
    display: flex;
    flex-direction: column;
    align-items: center;

    input[type='file'] {
      display: none;
    }

    // Upload your...
    span:nth-child(1) {
      margin-top: 20px;
      margin-bottom: 20px;
    }

    .vf-file-upload__span--up-to-size {
      color: ${({ theme }) => theme.text__dark_color};
      font-size: 0.875rem;
      margin-bottom: 20px;
    }
    // Error text...
    span:nth-child(3) {
      line-height: 0.875rem;
      font-size: 0.875rem;
    }
  }

  .errorBorder {
    border-color: ${({ theme }) => theme.secondary__red};
  }

  .errorText {
    color: ${({ theme }) => theme.secondary__red};
  }

  .vf-file-upload--dragndrop {
    label {
      margin-bottom: 20px;
      box-sizing: border-box;
    }

    ul {
      list-style-type: none;
      padding: 0;
      margin: 0;
      width: 100%;
      display: flex;
      justify-content: center;
      flex-direction: row;
      flex-wrap: wrap;
      margin-bottom: 28px;
      transition: all 0.4s ease;

      li {
        align-items: center;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        height: 40px;
        text-align: center;
        padding: 0;
        margin: 0;
        width: 180px;
        transition: all 0.4s ease;
      }

      img {
        height: 12px;
        line-height: 0.625rem;
        margin: 0 4px;
        cursor: pointer;
      }
    }

    width: 264px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    border: ${({ theme }) => theme.primary__ocean_blue} dashed thin;
    padding: 28px 0;
    span:nth-child(3) {
      margin-bottom: 10px;
    }
    ul {
      list-style-type: none;
      padding: 0;
      margin: 0;
      width: 100%;
      display: flex;
      justify-content: center;
      flex-direction: row;
      flex-wrap: wrap;
      margin-bottom: 28px;
      transition: all 0.4s ease;
      li {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        text-align: center;
        padding: 0;
        margin: 0;
        width: 180px;
        transition: all 0.4s ease;
      }
    }
    .disabled {
      color: ${({ theme }) => theme.tertiary__medium_dark_grey};
      background-color: #eee;
    }
    .errorText {
      color: ${({ theme }) => theme.secondary__red};
    }
    .vf-icon-span {
      line-height: 0.625rem;
      margin: 0 0 0 10px;
      cursor: pointer;
    }
    .vf-icon-span::before {
      font-size: ${({ theme }) => theme.fontSize};
      padding: 5px;
    }
  }

  .is-dragover {
    border: ${({ theme }) => theme.link__color} dashed thin;
  }

  .box__dragndrop {
    display: none;
  }

  .vf-file-upload__icon {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    color: rgb(235, 242, 243);
    margin: 28px 0;
  }

  .vf-icon-my-documents:before {
    font-size: 5rem !important;
  }
`

export const ReactUpload = ({
  sendFiles,
  pageData,
}: {
  sendFiles: (arg0: Blob[]) => void
  pageData?: ContactPageProps
}) => {
  const [data, setData] = useState<File[]>([])

  const sendData = (files: FileList) => {
    if (!files) return

    const maxFilesSize = 50000000

    Array.from(files).forEach(file => {
      if (file.size > maxFilesSize) return

      const newArray = [...data, file]

      setData(newArray)
      sendFiles(newArray)
    })
  }

  const removeItem = (item: Blob) => {
    const newList = data.filter(listItem => listItem !== item)
    setData(newList)
    sendFiles(newList)
  }

  return (
    <>
      <UploadContainer>
        <div className="vf-upload">
          <div className="vf-upload__container">
            <span className="vf-upload__text">
              {pageData?.uploadHeading ?? 'Upload your file here'}
            </span>
            <span max-size-megabyte="50" className="vf-upload__size">
              {pageData?.uploadSubheading ?? 'Up to 50 MB each'}
            </span>
            <label
              htmlFor="vf-upload__button"
              className="vf-button vf-button__outline--secondary"
            >
              {pageData?.uploadButton ?? 'Attach file(s)...'}
            </label>
            <input
              type="file"
              id="vf-upload__button"
              name="files[]"
              multiple
              style={{ display: 'none' }}
              onChange={e => e.target.files && sendData(e.target.files)}
            />
            <ul id="vf-upload__filelist">
              {data.map((item, i) => (
                <li key={i}>
                  {item.name}
                  <span
                    className="vf-icon-close"
                    onClick={() => {
                      removeItem(item)
                    }}
                  />
                </li>
              ))}
            </ul>
          </div>
        </div>
      </UploadContainer>
      <div className="grey-overlay" />
    </>
  )
}
