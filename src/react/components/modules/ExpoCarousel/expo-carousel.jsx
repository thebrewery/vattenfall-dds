import classNames from 'classnames'
import { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'

const ExpoCarouselWrapper = styled.div`
  height: 100%;
  position: relative;
  overflow: hidden;
  margin: 0 auto;
  max-width: 1780px;
  width: 100%;

  .vf-expo-carousel {
    &__wrapper {
      display: flex;
      height: 100%;
      overflow: scroll hidden;
      scroll-snap-type: x mandatory;
      scroll-snap-stop: always;
      scrollbar-width: none;

      &::-webkit-scrollbar {
        appearance: none;
      }
    }

    &__buttons {
      bottom: 1.875rem;
      display: flex;
      flex-direction: row;
      gap: 0.75rem;
      position: absolute;
      right: 1rem;

      @media screen and (min-width: 768px) {
        bottom: 3rem;
        right: 6rem;
      }
    }

    &__button {
      align-items: center;
      background-color: rgba(255, 255, 255, 0.5);
      border-radius: 50%;
      color: ${({ theme }) => theme.colours.primary__coal_black};
      cursor: pointer;
      display: flex;
      font-size: 0.875rem;
      justify-content: center;
      transition: background-color 0.15s ease-in, color 0.15s ease-in;
      height: 1.5rem;
      width: 1.5rem;

      @media screen and (min-width: 1200px) {
        font-size: 1.25rem;
        height: 2.5rem;
        width: 2.5rem;
      }

      &.active {
        background-color: ${({ theme }) => theme.colours.primary__ocean_blue};
        color: ${({ theme }) => theme.colours.primary__aura_white};
      }
    }

    &__observer {
      & .vf-expo-slide__content-container {
        opacity: 0;

        .vf-button {
          opacity: 0;
        }

        &.fade-in {
          animation: 0.3s ease-in 0.6s forwards fadeAndSlideIn;

          & .vf-button {
            opacity: 1;
            transition: opacity 0.25s ease-in 0.9s;
          }
        }
      }
    }

    @keyframes fadeAndSlideIn {
      0% {
        transform: translateY(30px);
        opacity: 0;
      }

      100% {
        opacity: 1;
        transform: translateY(0);
      }
    }

    &__arrow--prev,
    &__arrow--next {
      background-color: #ffffff;
      background-image: url('/assets/left-caret.svg');
      background-size: 2rem;
      background-position: 2px 3px;
      background-repeat: no-repeat;
      border-radius: 50%;
      cursor: pointer;
      height: 40px;
      top: 50%;
      transition: background-color 0.25s ease-in;
      position: absolute;
      user-select: none;
      -webkit-user-select: none;
      width: 40px;
      z-index: 10;
    }

    &__arrow--prev {
      left: 1rem;
      transform: translateY(-50%);
    }

    &__arrow--next {
      transform: translateY(-50%) rotate(180deg);
      left: auto;
      right: 1rem;
    }
  }

  .vf-expo-carousel__content-anchor {
    align-items: center;
    cursor: pointer;
    display: flex;
    bottom: 1.5rem;
    left: 1rem;
    -webkit-font-smoothing: antialiased;
    position: absolute;
    @media screen and (min-width: 768px) {
      bottom: 3rem;
      left: 5rem;
    }
    @media screen and (min-width: 1200px) {
      display: inline;
      bottom: 3rem;
      left: 6rem;
    }

    &-link {
      align-items: center;
      color: ${({ theme }) => theme.colours.primary__aura_white};
      display: flex;
      font-size: 1rem;
      @media screen and (min-width: 1200px) {
        font-size: 1.25rem;
      }
    }
    &-linkText {
      display: none;
      @media screen and (min-width: 768px) {
        display: inline;
      }
    }

    .vf-icon-down {
      color: ${({ theme }) => theme.colours.primary__aura_white};
      margin-right: 1rem;

      &::before {
        font-size: 2rem;
      }
    }
  }
`
/**
 * @param {HTMLElement} container
 * @param {HTMLElement} item
 */
const scrollToItem = (container, item) => {
  container.scrollTo({ left: item.offsetLeft, behavior: 'smooth' })
}

const ExpoCarousel = ({
  children,
  hideContentAnchor = false,
  contentAnchorLink,
  contentAnchorText,
  showArrows = false,
  autoScroll = false,
  autoScrollTimer = '3000',

  ...rest
}) => {
  const [activeImage, setActiveImage] = useState(0)
  const carouselWrapper = useRef()

  useEffect(() => {
    const importScript = async () => {
      await import('./ScrollTimeline')

      const allCarouselItems = document.querySelectorAll(
        '.vf-expo-carousel__observer'
      )

      if (!window.ScrollTimeline) return
      allCarouselItems.forEach(item => {
        item.querySelector('.vf-expo-slide__media').animate(
          {
            transform: ['translateX(-30%)', 'translateX(0)', 'translateX(30%)'],
          },
          {
            timeline: new ScrollTimeline({
              fill: 'both',
              timeRange: 1,
              orientation: 'horizontal',
              source: carouselWrapper.current,
              scrollSource: carouselWrapper.current,
              scrollOffsets: [
                {
                  target: item,
                  edge: 'end',
                  threshold: 0,
                },
                {
                  target: item,
                  edge: 'start',
                  threshold: 0,
                },
              ],
            }),
          }
        )
      })
    }

    importScript()
  }, [])

  useEffect(() => {
    const childElements = document.querySelectorAll('.vf-expo-slide')
    Array.from(childElements).forEach((el, index) => {
      el.dataset.index = index
    })
  }, [])

  useEffect(() => {
    const observer = new IntersectionObserver(
      entries => {
        const interSectingElement = entries.find(entry => entry.isIntersecting)
        const activeEntry = interSectingElement?.target.dataset.index

        entries.forEach(el => {
          if (!el.isIntersecting) {
            el.target
              .querySelector('.vf-expo-slide__content-container')
              .classList?.remove('fade-in')
          } else {
            interSectingElement.target
              .querySelector('.vf-expo-slide__content-container')
              ?.classList.add('fade-in')
          }
        })

        if (activeEntry) {
          if (interSectingElement.target.querySelector('video')) {
            const video = interSectingElement.target.querySelector('video')
            video.play()
          } else if (carouselWrapper.current.querySelectorAll('video')) {
            const videoList = carouselWrapper.current.querySelectorAll('video')

            videoList.forEach(video => video.pause())
          }

          setActiveImage(parseInt(activeEntry))
        }
      },
      {
        root: carouselWrapper.current,
        threshold: 0.5,
      }
    )

    const elements = carouselWrapper.current.querySelectorAll(
      '.vf-expo-carousel__observer'
    )

    elements.forEach(el => {
      observer.observe(el)
    })

    return () => observer.disconnect()
  }, [])

  useEffect(() => {
    if (autoScroll) {
      const nextSlideTimeout = setTimeout(() => {
        let nextImage
        if (activeImage === children.length - 1) {
          nextImage = carouselWrapper.current.querySelector(
            `.vf-expo-carousel__observer[data-index="0"]`
          )
        } else {
          nextImage = carouselWrapper.current.querySelector(
            `.vf-expo-carousel__observer[data-index="${activeImage + 1}"]`
          )
        }
        scrollToItem(carouselWrapper.current, nextImage)
      }, autoScrollTimer)
      return () => clearTimeout(nextSlideTimeout)
    }
  }, [activeImage, autoScroll, autoScrollTimer, children.length])

  const childrenList = Array.isArray(children) ? children : [children]

  useEffect(() => {
    if (autoScroll) {
      const nextSlideTimeout = setTimeout(() => {
        let nextImage
        if (activeImage === children.length - 1) {
          nextImage = carouselWrapper.current.querySelector(
            `.vf-expo-carousel__observer[data-index="0"]`
          )
        } else {
          nextImage = carouselWrapper.current.querySelector(
            `.vf-expo-carousel__observer[data-index="${activeImage + 1}"]`
          )
        }
        scrollToItem(carouselWrapper.current, nextImage)
      }, autoScrollTimer)

      return () => clearTimeout(nextSlideTimeout)
    }
  }, [activeImage, autoScrollTimer, autoScroll, children.length])

  return (
    <ExpoCarouselWrapper className="vf-expo-carousel" {...rest}>
      {showArrows && (
        <span
          className="vf-expo-carousel__arrow--prev"
          onClick={() => {
            const prevImage = carouselWrapper.current.querySelector(
              `.vf-expo-carousel__observer[data-index="${
                activeImage === 0 ? children.length - 1 : activeImage - 1
              }"]`
            )
            scrollToItem(carouselWrapper.current, prevImage)
          }}
        />
      )}
      <div className="vf-expo-carousel__wrapper" ref={carouselWrapper} tabIndex={0}>
        {children}
      </div>
      {!hideContentAnchor && (
        <div className="vf-expo-carousel__content-anchor">
          <a
            className="vf-expo-carousel__content-anchor-link"
            href={contentAnchorLink}
          >
            <span className="vf-icon-down" />
            <span className="vf-expo-carousel__content-anchor-linkText">
              {contentAnchorText}
            </span>
          </a>
        </div>
      )}
      <div className="vf-expo-carousel__buttons">
        {childrenList.map((_, index) => (
          <span
            key={index}
            className={classNames('vf-expo-carousel__button', {
              active: activeImage === index,
            })}
            tabIndex={0}
            onClick={() => {
              const myIndexImage = carouselWrapper.current.querySelector(
                `.vf-expo-carousel__observer[data-index="${index}"]`
              )
              scrollToItem(carouselWrapper.current, myIndexImage)
            }}
            onKeyDown={ (event) => {
              if (event.key === 'Enter') {
                console.log(event.key);
                console.log(index);
                const myIndexImage = carouselWrapper.current.querySelector(
                  `.vf-expo-carousel__observer[data-index="${index}"]`
                )
                scrollToItem(carouselWrapper.current, myIndexImage)
              }
            }}
          >
            {index + 1}
          </span>
        ))}
      </div>
      {showArrows && (
        <span
          className="vf-expo-carousel__arrow--next"
          onClick={() => {
            const nextImage = carouselWrapper.current.querySelector(
              `.vf-expo-carousel__observer[data-index="${
                activeImage === children.length - 1 ? 0 : activeImage + 1
              }"]`
            )
            scrollToItem(carouselWrapper.current, nextImage)
          }}
        />
      )}
    </ExpoCarouselWrapper>
  )
}

export default ExpoCarousel
