import styled from 'styled-components'

const ExpoSlideWrapper = styled.div`
  flex-shrink: 0;
  min-height: 75vh;
  scroll-snap-align: center;
  position: relative;
  width: 100% !important;
  overflow-x: hidden;

  @media screen and (min-width: 768px) {
    min-height: 60vh;
    max-height: none;
  }

  @media screen and (min-width: 1024px) {
    aspect-ratio: 16/8;
  }

  .vf-expo-slide {
    &__container {
      align-items: center;
      display: flex;
      justify-content: center;
      inset: 0;
      position: absolute;
    }

    &__content-container {
      align-items: center;
      display: flex;
      flex-direction: column;
      max-width: 300px;

      @media screen and (min-width: 375px) {
        max-width: 345px;
      }

      @media screen and (min-width: 768px) {
        max-width: none;
      }
    }

    &__bg-video {
      height: 100%;
      inset: 0;
      left: 0;
      position: absolute;
    }

    &__media {
      height: 100%;
      width: 100%;
      position: absolute;
      top: 0;
      left: 0;
      object-fit: cover;
    }
  }

  .vf-component__large-heading,
  .vf-component__sub-heading,
  .vf-link--with-arrow {
    color: ${({ theme }) => theme.colours.primary__aura_white};
  }

  .vf-component__large-heading {
    font-size: 2.4rem;
    line-height: 1;
    text-align: center;
    margin-bottom: ${props => (props.hasSubTitle ? 0 : 1.5)}rem;
    padding-bottom: ${props => (props.hasSubTitle ? 16 : 'inherit')}px;

    @media screen and (min-width: 768px) {
      font-size: 3rem;
    }

    @media screen and (min-width: 1200px) {
      font-size: 4.5rem;
    }
  }
  .vf-component__sub-heading {
    font-size: 1rem;
    line-height: 1.5;
    max-width: 90%;
    width: 800px;
    margin: 0 auto;
    margin-bottom: 1.5rem;
    text-align: center;
    font-weight: 500;
    font-synthesis: none;

    @media screen and (min-width: 768px) {
      font-size: 1.1rem;
    }

    @media screen and (min-width: 1200px) {
      font-size: 1.5rem;
    }
  }
`

const ExpoSlide = ({
  backgroundImage,
  video,
  heading,
  linkLabel,
  linkHref,
  altText,
  subTitle,
  ...rest
}) => {
  return (
    <ExpoSlideWrapper
      className="vf-expo-slide vf-expo-carousel__observer"
      hasSubTitle={!!subTitle}
      {...rest}
    >
      {backgroundImage && (
        <img
          src={backgroundImage}
          className="vf-expo-slide__media"
          alt={altText}
        />
      )}
      {video && (
        <video
          className="vf-expo-slide__bg-video vf-expo-slide__media"
          muted
          loop
          playsInline
        >
          <source src={video} type="video/mp4" />
          Your browser doesn't support this video format.
        </video>
      )}
      <div className="vf-expo-slide__container">
        <div className="vf-expo-slide__content-container">
          <h1 className="vf-component__large-heading">{heading}</h1>
          {subTitle && <p className="vf-component__sub-heading">{subTitle}</p>}
          {linkLabel && linkHref && (
            <a
              className="vf-button vf-button--lg vf-button__primary"
              href={linkHref}
            >
              {linkLabel}
            </a>
          )}
        </div>
      </div>
    </ExpoSlideWrapper>
  )
}

export default ExpoSlide
