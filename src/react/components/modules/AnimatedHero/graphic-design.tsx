import { memo } from 'react'
import { cn } from '../../../../utils/tailwindUtils'

export default memo(function GraphicDesign() {
  return (
    <div className="tw-relative">
      <AnimatedElementWrapper
        className={cn(
          'tw-w-[54.11%]',
          '-tw-translate-y-8',
          `group-[[data-animate='true']]:tw-translate-y-0`
        )}
      >
        <VerticalCard />
      </AnimatedElementWrapper>
      <AnimatedElementWrapper
        className={cn(
          'tw-w-[64.31%]',
          'tw-absolute -tw-bottom-8 tw-left-[5.25rem]',
          'lg:-tw-bottom-[4.5rem] sm:tw-left-[8.25rem]',
          'tw-translate-y-8',
          `group-[[data-animate='true']]:tw-translate-y-0`
        )}
      >
        <HorizontalCard />
      </AnimatedElementWrapper>
      <AnimatedElementWrapper
        className={cn(
          'tw-w-[43.42%]',
          'tw-absolute tw-right-0 tw-top-8 md:tw-top-16',
          'tw-translate-y-4',
          `group-[[data-animate='true']]:tw-translate-y-0`
        )}
      >
        <Squares />
      </AnimatedElementWrapper>
      <AnimatedElementWrapper
        className={cn(
          'tw-w-[21.71%]',
          'tw-absolute -tw-right-1.5 tw-top-10',
          'md:-tw-right-2 md:tw-top-20',
          'tw-translate-y-6 -tw-translate-x-4',
          `group-[[data-animate='true']]:tw-translate-y-0 group-[[data-animate='true']]:tw-translate-x-0`
        )}
      >
        <MouseArrow />
      </AnimatedElementWrapper>
    </div>
  )
})

function AnimatedElementWrapper({
  children,
  className,
}: {
  children: React.ReactNode
  className?: string
}) {
  return (
    <div
      className={cn(
        'tw-transition-transform tw-duration-[5s] tw-ease-out',
        className
      )}
    >
      {children}
    </div>
  )
}

// Child Components
function VerticalCard() {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 329 461"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d_2827_2727)">
        <rect x="20" y="4" width="289" height="421" fill="white" />
        <rect
          x="20.5"
          y="4.5"
          width="288"
          height="420"
          stroke="url(#paint0_linear_2827_2727)"
        />
      </g>
      <rect x="52" y="313" width="225" height="48" fill="#F2F2F2" />
      <mask
        id="mask0_2827_2727"
        style={{ maskType: 'alpha' }}
        maskUnits="userSpaceOnUse"
        x="52"
        y="36"
        width="225"
        height="253"
      >
        <rect x="52" y="36" width="225" height="253" fill="#E6E6E6" />
      </mask>
      <g mask="url(#mask0_2827_2727)">
        <rect x="52" y="38" width="225" height="251" fill="#F2F2F2" />
        <circle cx="39" cy="163" r="126" fill="#2071B5" />
        <circle cx="291" cy="162" r="126" fill="#FFDA00" />
      </g>
      <defs>
        <filter
          id="filter0_d_2827_2727"
          x="0"
          y="0"
          width="329"
          height="461"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="16" />
          <feGaussianBlur stdDeviation="10" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_2827_2727"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_2827_2727"
            result="shape"
          />
        </filter>
        <linearGradient
          id="paint0_linear_2827_2727"
          x1="20"
          y1="4"
          x2="309"
          y2="425"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#E5F3FE" />
          <stop offset="1" stopColor="#D2D2D2" />
        </linearGradient>
      </defs>
    </svg>
  )
}

function HorizontalCard() {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 391 288"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d_2819_5820)">
        <rect x="20" y="4" width="351" height="248" fill="white" />
        <rect
          x="20.5"
          y="4.5"
          width="350"
          height="247"
          stroke="url(#paint0_linear_2819_5820)"
        />
      </g>
      <rect x="52" y="36" width="287" height="132" fill="#F2F2F2" />
      <ellipse cx="128.535" cy="103.98" rx="40.246" ry="40.26" fill="white" />
      <ellipse cx="206.056" cy="80.22" rx="23.7517" ry="23.76" fill="#2071B5" />
      <ellipse
        cx="243.003"
        cy="126.75"
        rx="21.1126"
        ry="21.12"
        fill="#FFDA00"
      />
      <ellipse cx="270.218" cy="80.22" rx="13.0305" ry="13.2" fill="black" />
      <ellipse cx="293.308" cy="115.86" rx="9.40172" ry="9.24" fill="#4E4848" />
      <rect x="52" y="184" width="287" height="28" fill="#F2F2F2" />
      <defs>
        <filter
          id="filter0_d_2819_5820"
          x="0"
          y="0"
          width="391"
          height="288"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="16" />
          <feGaussianBlur stdDeviation="10" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_2819_5820"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_2819_5820"
            result="shape"
          />
        </filter>
        <linearGradient
          id="paint0_linear_2819_5820"
          x1="20"
          y1="4"
          x2="367.16"
          y2="257.263"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#E5F3FE" />
          <stop offset="1" stopColor="#D2D2D2" />
        </linearGradient>
      </defs>
    </svg>
  )
}

function Squares() {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 264 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect width="40" height="40" fill="black" />
      <rect x="56" width="40" height="40" fill="#4E4848" />
      <rect x="112" width="40" height="40" fill="white" />
      <rect x="168" width="40" height="40" fill="#2071B5" />
      <rect x="224" width="40" height="40" fill="#FFDA00" />
    </svg>
  )
}

function MouseArrow() {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 132 163"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d_2821_6692)">
        <path
          d="M111.511 126.293L109.738 4L20.448 84.2699L74.1892 87.0272L111.511 126.293Z"
          fill="white"
        />
        <path
          d="M110.992 125.022L109.254 5.10745L21.6824 83.8325L74.2148 86.5279L74.4141 86.5381L74.5516 86.6827L110.992 125.022Z"
          stroke="url(#paint0_linear_2821_6692)"
        />
      </g>
      <defs>
        <filter
          id="filter0_d_2821_6692"
          x="0.449219"
          y="0"
          width="131.062"
          height="162.293"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="16" />
          <feGaussianBlur stdDeviation="10" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_2821_6692"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_2821_6692"
            result="shape"
          />
        </filter>
        <linearGradient
          id="paint0_linear_2821_6692"
          x1="109.738"
          y1="4"
          x2="-6.61415"
          y2="88.6735"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#D2D2D2" />
          <stop offset="1" stopColor="#E5F3FE" />
        </linearGradient>
      </defs>
    </svg>
  )
}
