import { useState, memo } from 'react'
import { cn } from '../../../../utils/tailwindUtils'
import GraphicDashboard from './graphic-dashboard'
import GraphicFigma from './graphic-figma'

import { useKeenSlider } from 'keen-slider/react'
import GraphicDesign from './graphic-design'
import GraphicGetInTouch from './graphic-get-in-touch'

type SliderItem = {
  id: number
  title: string
  subTitle: string
  linkHref: string
  linkLabel: string
}

type AnimatedHeroProps = {
  sliderItems: SliderItem[]
}

const ANIMATED_GRAPHICS = [
  GraphicDashboardColumn,
  GraphicFigmaColumn,
  GraphicDesignColumn,
  GraphicGetInTouchColumn,
]

// Slider Settings
const SLIDER_DELAY = 5000 // Delay before next slide
const SLIDER_TRANSITION_DURATION = 2000 // Duration of slide transition

export default memo(function AnimatedHero({ sliderItems }: AnimatedHeroProps) {
  const { sliderRef, instanceRef, loaded, opacities, currentSlide } =
    useHeroSlider(sliderItems.length)

  return (
    <section className="tw-max-w-[1780px] tw-mx-auto tw-min-h-[calc(100dvh-60px)] tw-flex tw-flex-col tw-bg-[#F2F2F2]">
        <div
          className={cn(
            'tw-transition-colors tw-duration-500 tw-ease-in',
            'tw-mx-auto tw-pb-4 tw-pt-10 xl:tw-py-12',
            'tw-relative tw-w-full tw-max-w-[111.25rem]',
            'tw-flex-1 tw-flex tw-flex-col tw-items-center tw-px-[2.625rem]'
          )}
        >
          <div className="tw-h-full tw-relative tw-flex-1 tw-flex tw-flex-col tw-justify-center tw-gap-6 tw-w-full tw-max-w-[1565px]">
            <div
              ref={sliderRef}
              className={cn(
                'tw-relative tw-grid tw-grid-cols-1 active:tw-cursor-grabbing',
              )}
            >
              {sliderItems.map((item, index) => {
                const GraphicComponent = ANIMATED_GRAPHICS[index] || null

                return (
                  <div
                    key={item.id}
                    className={cn(
                      'tw-col-start-1 tw-row-start-1 tw-opacity-0',
                      'tw-h-full tw-flex tw-flex-col xl:tw-flex-row tw-items-center tw-justify-center tw-gap-24 xl:tw-gap-0 xl:tw-justify-between',
                      'tw-transition-opacity tw-ease-out',
                      currentSlide != index && 'tw-pointer-events-none'
                    )}
                    style={{ opacity: opacities[index] }}
                  >
                    {/* Left Column - Text */}
                    <div
                      className={cn(
                        'tw-text-center sm:tw-max-w-[65%]',
                        'xl:tw-text-left xl:tw-w-[40.13%] xl:tw-max-w-[38.375rem]'
                      )}
                    >
                      <h1 className="tw-text-h0-mobile lg:tw-text-h0-desktop">
                        {item.title}
                      </h1>
                      <p className="tw-text-body-large tw-mt-6 lg:tw-mt-8">
                        {item.subTitle}
                      </p>

                      {item.linkLabel && (
                        <a
                          href={item.linkHref}
                          className="vf-button vf-button--lg vf-button__primary tw-mt-8 lg:tw-mt-10"
                        >
                          {item.linkLabel}
                        </a>
                      )}
                    </div>

                    {/* Right Column - Graphic */}
                    {GraphicComponent && (
                      <GraphicComponent
                        isAnimate={loaded && currentSlide === index}
                      />
                    )}
                  </div>
                )
              })}
            </div>

            {/* Slider Dots */}
            {loaded && instanceRef.current && (
              <div
                className={cn(
                  'tw-w-full tw-flex tw-justify-center tw-gap-2 tw-mx-auto',
                  'xl:tw-w-auto xl:tw-absolute xl:tw-bottom-4 xl:tw-left-1/2 xl:-tw-translate-x-1/2'
                )}
              >
                {sliderItems.map((_, index) => (
                  <span
                    key={index}
                    className={cn(
                      'tw-bg-medium-grey tw-p-0 tw-border-none tw-w-2 tw-h-2 tw-rounded-full tw-pointer-events-none',
                      currentSlide === index
                        ? 'tw-bg-ocean-blue'
                        : 'tw-bg-medium-grey'
                    )}
                  ></span>
                ))}
              </div>
            )}
          </div>

          {/* Invisible desktop slider controls */}
          {loaded && instanceRef.current && (
            <>
              <div
                onClick={() => instanceRef.current?.prev()}
                className="tw-cursor-pointer tw-absolute tw-top-0 tw-left-0 tw-h-full tw-w-[6.8%]"
              ></div>
              <div
                onClick={() => instanceRef.current?.next()}
                className="tw-cursor-pointer tw-absolute tw-top-0 tw-right-0 tw-h-full tw-w-[6.8%]"
              ></div>
            </>
          )}
        </div>
    </section>
  )
})

// Hooks
function useHeroSlider(slideLength: number) {
  const [currentSlide, setCurrentSlide] = useState(0)
  const [loaded, setLoaded] = useState(false)

  const [opacities, setOpacities] = useState<number[]>([])

  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>(
    {
      slides: slideLength,
      loop: true,
      defaultAnimation: { duration: SLIDER_TRANSITION_DURATION },
    },
    [
      slider => {
        let timeout: ReturnType<typeof setTimeout>

        function clearNextTimeout() {
          clearTimeout(timeout)
        }
        function nextTimeout() {
          clearTimeout(timeout)
          timeout = setTimeout(() => {
            slider.next()
          }, SLIDER_DELAY)
        }

        slider.on('created', () => {
          setLoaded(true)

          nextTimeout()
        })

        slider.on('slideChanged', () => {
          setCurrentSlide(slider.track.details.rel)
        })

        slider.on('detailsChanged', () => {
          const new_opacities = slider.track.details.slides.map(
            slide => slide.portion
          )
          setOpacities(new_opacities)
        })

        slider.on('dragStarted', clearNextTimeout)
        slider.on('animationEnded', nextTimeout)
        slider.on('updated', nextTimeout)

        slider.on('destroyed', () => {
          clearNextTimeout()
        })
      },
    ]
  )

  return { sliderRef, instanceRef, loaded, opacities, currentSlide }
}

// Graphics
function GraphicDashboardColumn({ isAnimate = false }: { isAnimate: boolean }) {
  return (
    <div
      data-animate={isAnimate}
      className={cn(
        'tw-group tw-w-full sm:tw-w-[80%] xl:tw-w-[52.94%] tw-shrink-0 tw-opacity-0',
        isAnimate && 'tw-opacity-100'
      )}
    >
      <GraphicDashboard />
    </div>
  )
}

function GraphicFigmaColumn({ isAnimate = false }: { isAnimate: boolean }) {
  return (
    <div
      data-animate={isAnimate}
      className={cn(
        'tw-group tw-w-auto md:tw-w-[18rem] xl:tw-w-[29.875rem] tw-shrink-0',
        isAnimate && 'tw-opacity-100'
      )}
    >
      <GraphicFigma />
    </div>
  )
}

function GraphicDesignColumn({ isAnimate = false }: { isAnimate: boolean }) {
  return (
    <div
      data-animate={isAnimate}
      className={cn(
        'tw-group tw-w-auto tw-shrink-0',
        'tw-w-full md:tw-w-[38rem] xl:tw-h-[32rem]',
        isAnimate && 'tw-opacity-100'
      )}
    >
      <GraphicDesign />
    </div>
  )
}

function GraphicGetInTouchColumn({
  isAnimate = false,
}: {
  isAnimate: boolean
}) {
  return (
    <div
      data-animate={isAnimate}
      className={cn(
        'tw-group tw-w-auto tw-shrink-0',
        'tw-w-full md:tw-w-[40rem] ',
        isAnimate && 'tw-opacity-100'
      )}
    >
      <GraphicGetInTouch />
    </div>
  )
}
