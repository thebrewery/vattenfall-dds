import { ComponentProps, ReactNode } from 'react'
import styled from 'styled-components'

const EyeCatcherContainer = styled.a<{ href: string; target: string }>`
  align-items: center;
  background: ${({ theme }) => theme.colours.primary__solar_yellow};
  border-radius: 50%;
  color: ${({ theme }) => theme.colours.primary__coal_black};
  display: flex;
  flex-direction: column;
  font-family: 'Vattenfall Hall Bold NORDx';
  font-size: 15px;
  font-weight: 700;
  height: 115px;
  justify-content: center;
  padding: 0 0.5rem;
  position: absolute;
  text-align: center;
  top: -180px;
  right: 20px;
  width: 115px;
  z-index: 1;

  &:hover {
    cursor: pointer;
  }

  .intro-arrow {
    color: ${({ theme }) => theme.colours.text__dark_color};
    display: block;
    font-size: ${({ theme }) => theme.fontSize};
    margin-top: 3px;

    &:before {
      content: '';
      display: inline-block;
      width: 19px;
      height: 23px;
      background-color: transparent;
      background-repeat: no-repeat;
      background-position: 0 0;
      background-size: 15px 16px;
      vertical-align: middle;
      background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMjBweCIgaGVpZ2h0PSIxOHB4IiB2aWV3Qm94PSIwIDAgMjAgMTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8ZGVmcz4KICAgICAgICA8cG9seWdvbiBpZD0icGF0aC0xIiBwb2ludHM9IjggMTkgMjMuNDQ4MTc1MiAxOSAxNy40MzQzMDY2IDI1LjM0MjY0NzcgMTkuMDQwMTQ2IDI3IDI3LjggMTcuOTk1ODc3MiAxOS4wNDAxNDYgOSAxNy40MzQzMDY2IDEwLjY0OTEwNjcgMjMuNDQ4MTc1MiAxNyA4IDE3Ij48L3BvbHlnb24+CiAgICA8L2RlZnM+CiAgICA8ZyBpZD0iU3ltYm9scyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9IkNvbXBvbmVudHMtLy1JY29ucy0vLVN5bWJvbC0vLUFyQ29tcG9uZW50cy0vLVRhYmxlLS8tUm93LS8tUHJpbWFyeS0vLVJpZ2h0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC4wMDAwMDAsIC05LjAwMDAwMCkiPgogICAgICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBmaWxsPSJ3aGl0ZSI+CiAgICAgICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPgogICAgICAgICAgICA8L21hc2s+CiAgICAgICAgICAgIDx1c2UgaWQ9Ik1hc2siIGZpbGw9IiMyMjIyMjIiIGZpbGwtcnVsZT0ibm9uemVybyIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=);
    }
  }
`

type EyeCatcherProps = {
  children: ReactNode
  href: string
  target?: string
  useDownloadIcon?: boolean
}

export const EyeCatcher = ({
  children,
  href,
  target,
  useDownloadIcon,
  ...props
}: EyeCatcherProps & ComponentProps<'a'>) => {
  return (
    // @ts-ignore
    <EyeCatcherContainer
      href={href}
      target={target ? target : '_self'}
      {...props}
    >
      {children}
      <span className={useDownloadIcon ? 'vf-icon-download' : 'intro-arrow'} />
    </EyeCatcherContainer>
  )
}
