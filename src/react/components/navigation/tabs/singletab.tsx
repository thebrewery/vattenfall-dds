import styled, { css } from 'styled-components'

const ReactTabStyling = styled.li<{ active: boolean }>`
  background: #fff;
  border: none;
  border-bottom: 1px solid #ccc;
  cursor: pointer;
  color: #222222;
  display: inline-block;
  font-size: 1rem;
  font-weight: 500;
  letter-spacing: -0.05px;
  line-height: 28px;
  margin-right: -5px;
  min-width: 130px;
  max-width: 33%;
  outline: none;
  padding: 16px 14px 7px 14px;
  position: relative;

  &:hover {
    border-bottom: solid 4px #1964a3;
    padding-bottom: 4px;
  }

  ${({ active }) =>
    active &&
    css`
      border-bottom: solid 4px #1964a3;
      color: #1964a3;
      padding-bottom: 4px;
    `}
`

export const ReactTab = ({ active, ...props }: { active: boolean }) => (
  <ReactTabStyling className="vf-tab" active={active}>
    <a tabIndex={0} role="button" {...props} />
  </ReactTabStyling>
)
