import styled, { css } from 'styled-components'

const ReactTabsWrapper = styled.ul<{ wrap: boolean; centered: boolean }>`
  background: #fff;
  display: flex;
  ${({ wrap }) => wrap && 'flex-wrap: wrap;'}
  list-style: none;
  padding: 0;

  .vf-tab {
    ${({ centered }) =>
      centered &&
      css`
        flex-grow: 1;
        text-align: center;
      `}
  }
`

type ReactTabsProps = {
  wrap: boolean
  centered: boolean
}

export const ReactTabs = ({
  wrap,
  centered = true,
  ...props
}: ReactTabsProps) => (
  <ReactTabsWrapper
    className="vf-tab-bar"
    wrap={wrap}
    centered={centered}
    {...props}
  />
)
