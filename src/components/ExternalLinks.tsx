import styled from 'styled-components'

type ExternalLinksProps = {
  links: {
    text: string
    url: string
  }[],
  classes? : string
}

const Container = styled.div`
  .vf-component__external-links {
    display: flex;
    flex-wrap: wrap;
    grid-row-gap: 12px;
    grid-column-gap: 4px;
  }

  .vf-icon-open-new-window {
    font-size: 18px;
    font-weight: 500;
    line-height: normal;
    width: 49.5%;
    display: flex;
    gap: 12px;
  }
  .vf-icon-open-new-window.black-hover span{
    color:black;
  }
  .vf-icon-open-new-window.black-hover:hover span{
    color: #1964a3;
  }
`

export const ExternalLinks = ({ links, classes }: ExternalLinksProps) => (
  <Container>
    <div className="vf-component__external-links">
      {links.map((item, index) => (
        <a
          className={"vf-icon-open-new-window "+classes}
          href={item.url}
          target="_blank"
          rel="noreferrer"
          key={index}
        >
          <span>{item.text}</span>
        </a>
      ))}
    </div>
  </Container>
)
