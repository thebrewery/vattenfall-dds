import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

const NewSignUpModalContainer = styled.div`
  background: rgba(0, 0, 0, 0.4);
  height: 100vh;
  left: 0;
  pointer-events: auto;
  position: fixed;
  top: 0;
  width: 100vw;
  z-index: 99;

  .new-signup-modal__wrapper {
    align-items: center;
    background: white;
    border: thin solid #999999;
    display: flex;
    flex-direction: column;
    justify-content: center;
    left: 50%;
    max-width: 552px;
    padding: 20px;
    position: absolute;
    text-align: center;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
  }

  .new-signup-modal__content-container {
    padding: 24px 24px;
  }

  .vf-icon-close {
    cursor: pointer;
    position: absolute;
    right: 20px;
    top: 20px;

    &::before {
      font-size: ${({ theme }) => theme.largeFontSize};
    }
  }

  .vf-button {
    &:hover {
      cursor: pointer;
    }
  }
`

type NewSignUpModalProps = {
  setNewSignUpModal: (newSignUpModal: boolean) => void
  statusCode: number
}

export const NewSignUpModal = ({
  setNewSignUpModal,
  statusCode,
}: NewSignUpModalProps) => {
  const [header, setHeader] = useState('')
  const [paragraph, setParagraph] = useState('')

  useEffect(() => {
    if (statusCode === 201 || statusCode === 200) {
      setHeader('Thank you for subscribing to our newsletter.')
      setParagraph('')
    } else {
      setHeader("We've run into an issue.")
      setParagraph(
        'Something went wrong processing your request, please try again later.'
      )
    }
  }, [statusCode])

  return (
    <NewSignUpModalContainer onClick={() => setNewSignUpModal(false)}>
      <div
        className="new-signup-modal__wrapper"
        onClick={e => e.stopPropagation()}
      >
        <span
          className="vf-icon-close"
          onClick={() => setNewSignUpModal(false)}
        />
        <div className="new-signup-modal__content-container">
          <h2 className="vf-component__heading">{header}</h2>
          <p className="vf-component__paragraph">{paragraph}</p>
          <button
            onClick={() => setNewSignUpModal(false)}
            type="submit"
            className="vf-button vf-button--lg vf-button__primary"
            value="Send"
          >
            OK
          </button>
        </div>
      </div>
    </NewSignUpModalContainer>
  )
}
