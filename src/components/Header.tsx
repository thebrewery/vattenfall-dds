import classNames from 'classnames'
import Link from 'next/link'
import { useContext, useState } from 'react'
import styled from 'styled-components'

import PageProvider from '../context/state'
import { modeEnum } from '../utils/siteStructure'

const Container = styled.div`
  background: white;

  top: 0;
  left: 0;
  position: sticky;
  right: 0;
  z-index: 2;

  .vf__logotype {
    cursor: pointer;
    height: 60px;
    width: 186px;
  }

  .header {
    &__main {
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      margin: 0 auto;
      box-sizing: content-box;
      padding: 0 5px;

      @media screen and (min-width: 1024px) {
        display: flex;
        grid-template-columns: repeat(3, 1fr);
        height: 60px;
        justify-content: space-between;
        max-width: 1565px;
        padding: 0 16px;
      }

      @media screen and (min-width: 1200px) {
        padding: 0 42px;
      }
    }

    &__hamburger,
    &__close {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      margin-right: 20px;
      font-size: 1.5rem;

      @media screen and (min-width: 1024px) {
        display: none;
      }
    }

    &__hamburger {
      &::before {
        content: '\\e986';
        font-family: 'vf-icons';
      }
    }

    &__close {
      &::before {
        content: '\\e980';
        font-family: 'vf-icons';
      }
    }

    &__nav {
      display: none;

      &.show {
        @media screen and (max-width: 1024px) {
          display: flex;
          flex-direction: column;
          grid-column: span 3;
        }
      }

      @media screen and (min-width: 1024px) {
        display: flex;
      }
    }

    &__extra-nav {
      display: none;
      @media screen and (min-width: 1024px) {
        background: ${({ theme }) => theme.lightBlueBackground};
        display: flex;
        justify-content: center;
        height: 56px;
      }
    }

    &__extra-nav--mobile {
      background: ${({ theme }) => theme.lightBlueBackground};
      color: #2f5795;

      @media screen and (min-width: 1024px) {
        display: none;
      }
    }

    &__extra-nav-link {
      color: black;
      display: flex;
      font-size: 18px;
      font-weight: 500;
      text-decoration: none;
      padding: 10px 2rem;
      &:hover {
        color: ${({ theme }) => theme.colours.primary__ocean_blue};
      }

      @media screen and (min-width: 1024px) {
        align-items: center;
        justify-content: center;
        padding: 20px 40px;
      }
    }

    &__nav-link {
      border-bottom: 1px solid #dcdddc;
      color: black;
      display: flex;
      font-size: 18px;
      font-weight: 500;
      height: 100%;
      text-decoration: none;
      min-height: 54px;
      justify-content: space-between;
      align-items: center;
      padding: 0 1rem;
      width: 100%;
      cursor: pointer;
      &:hover {
        color: ${({ theme }) => theme.colours.primary__ocean_blue};
      }

      @media screen and (min-width: 1024px) {
        align-items: center;
        border: none;
        height: auto;
        padding: 28px 18px;
        justify-content: center;
        width: auto;
      }
    }
    &__add,
    &__minus {
      @media screen and (min-width: 1024px) {
        display: none;
      }
    }

    &__add {
      &::before {
        content: '\\e987';
        font-family: 'vf-icons';
      }
    }

    &__minus {
      &::before {
        content: '\\e984';
        font-family: 'vf-icons';
      }
    }

    &__search {
      align-self: center;
      display: none;
      font-size: 18px;
      line-height: 4.25rem;
      padding-left: 1rem;

      @media screen and (min-width: 1024px) {
        display: block;
        flex: 1;
        padding-left: 0;
      }

      @media screen and (min-width: 1200px) {
        padding-left: 2rem;
      }
    }
  }

  .active {
    background: ${({ theme }) => theme.lightBlueBackground};
    color: #2f5795;
  }
`

export const Header = () => {
  const {
    currentMode,
    setCurrentMode,
    activeFramework,
    activeSearchBox,
    setActiveSearchBox,
    setActiveCategoryId,
    setActiveComponentId,
    activeCategoryId,
    activeMenuItem,
    setActiveMenuItem,
    subMenu,
  } = useContext(PageProvider)

  const [show, setShow] = useState(false)

  const toggleSearchBox = (state: boolean) => {
    setActiveSearchBox(state)
    setShow(false)
  }

  const toggleShow = (state: boolean) => {
    setShow(state)
    setActiveSearchBox(false)
  }

  const navClick = (value: string) => {
    setShow(false)
    setActiveSearchBox(false)
    setActiveMenuItem(value)
  }

  const hide = () => {
    setShow(false)
    setActiveSearchBox(false)
    setActiveMenuItem('')
    setCurrentMode(modeEnum.NULL)
    setActiveCategoryId('')
  }

  const renderSubMenu = () => (
    <>
      <Link href="/introduction">
        <a
          className={classNames('header__extra-nav-link', {
            active: activeCategoryId === 'introduction',
          })}
          onClick={() => {
            setActiveCategoryId('introduction')
            setActiveComponentId('')
          }}
        >
          Introduction
        </a>
      </Link>
      <Link href="/designers/get-started">
        <a
          className={classNames('header__extra-nav-link', {
            active: currentMode === modeEnum.DESIGNERS,
          })}
          onClick={() => {
            setActiveCategoryId('')
            setActiveComponentId('')
          }}
        >
          Designers
        </a>
      </Link>
      <Link href={`/developers/get-started`}>
        <a
          className={classNames('header__extra-nav-link', {
            active: currentMode === modeEnum.DEVELOPERS,
          })}
          onClick={() => {
            setActiveCategoryId('')
            setActiveComponentId('')
          }}
        >
          Developers
        </a>
      </Link>
    </>
  )

  return (
    <Container>
      <div className="header__main">
        <Link href="/">
          <img
            className="vf__logotype"
            src="/assets/img/logo.svg"
            onClick={hide}
            alt="Vattenfall's logotype"
          />
        </Link>
      </div>
    </Container>
  )
}
