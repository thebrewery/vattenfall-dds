import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Secondary = () => {
  return (
    <Container className="vf-component__container">
      <h1 className="vf-component__large-heading">Secondary</h1>
      <p className="vf-component__paragraph">
        Documentation and examples for our Secondary.
      </p>
    </Container>
  )
}
