import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Tones = () => {
  return (
    <Container className="vf-component__container">
      <h1 className="vf-component__large-heading">Tones</h1>
      <p className="vf-component__paragraph">
        Documentation and examples for our Tones.
      </p>
    </Container>
  )
}
