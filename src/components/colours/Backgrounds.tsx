import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Backgrounds = () => {
  return (
    <Container className="vf-component__container">
      <h1 className="vf-component__large-heading">Backgrounds</h1>
      <p className="vf-component__paragraph">
        Documentation and examples for our Backgrounds.
      </p>
    </Container>
  )
}
