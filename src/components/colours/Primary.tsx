import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Primary = () => {
  return (
    <Container className="vf-component__container">
      <h1 className="vf-component__large-heading">Primary</h1>
      <p className="vf-component__paragraph">
        Documentation and examples for our Primary.
      </p>
    </Container>
  )
}
