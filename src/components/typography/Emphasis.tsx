import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Emphasis = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for our Emphasis.
      </p>
    </Container>
  )
}
