import styled from 'styled-components'

const StatusContainer = styled.div`
  .code-example {
    margin-top: 2rem;
  }
`

export const Status = () => {
  return (
    <StatusContainer>
      <p className="vf-component__paragraph">
        Class for displaying a couple of different states.
      </p>

      <code className="code-example">{`<span className='vf-utility__error'>Error</span>`}</code>
      <span className="vf-utility__error">Error</span>

      <code className="code-example">{`<span className='vf-utility__metadata'>Metadata</span>`}</code>
      <span className="vf-utility__metadata">Metadata</span>

      <code className="code-example">{`<span className='vf-utility__success'>Success</span>`}</code>
      <span className="vf-utility__success">Success</span>

      <code className="code-example">{`<span className='vf-utility__warning'>Warning</span>`}</code>
      <span className="vf-utility__warning">Warning</span>
    </StatusContainer>
  )
}
