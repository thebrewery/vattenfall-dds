import React from 'react'

export const Link = () => {
  return (
    <div className="vf-component__container">
      <p className="vf-component__paragraph">Class for displaying links.</p>

      <code className="code-example">{`<span className='vf-utility__link'>Link</span>`}</code>
      <span className="vf-utility__link">Link</span>
    </div>
  )
}
