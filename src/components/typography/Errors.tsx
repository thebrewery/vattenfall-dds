import React from 'react'

export const Errors = () => {
	return (
		<div className='vf-component__container'>
			<p className='vf-component__paragraph'>
				Class for displaying a error state.
			</p>

			<code>{`<span className='vf-utility__error'>Error</span>`}</code>

			<hr />

			<span className='vf-utility__error'>Error</span>
		</div>
	)
}
