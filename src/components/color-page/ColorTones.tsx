import styled from 'styled-components'
import Circle from './Circle'

const ColorTonesStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`
const ColorTones = () => {
  return (
    <ColorTonesStyle>
      <div className="color-row">
        <Circle color={'#d1266c'} percentage="100%" />
        <Circle color={'#df6797'} percentage="70%" />
        <Circle color={'#e892b5'} percentage="50%" />
        <Circle color={'#f1bdd3'} percentage="30%" />
        <Circle color={'#fae9f1'} percentage="10%" />
      </div>
    </ColorTonesStyle>
  )
}

export default ColorTones
