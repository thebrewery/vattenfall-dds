import styled from 'styled-components'
import Circle from './Circle'

const InChargeMapAppColorsStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`
const InChargeMapAppColors = () => {
  return (
    <InChargeMapAppColorsStyle>
      <div className="color-row">
        <Circle color={'#FFDA00'} colorName="Yellow" />
        <Circle
          color={'#FFFFFF'}
          colorName="White"
          border="1px solid #000000"
        />
        <Circle color={'#81E0A8'} colorName="Solar" />
        <Circle color={'#2071B5'} colorName="Blue" />
      </div>
    </InChargeMapAppColorsStyle>
  )
}

export default InChargeMapAppColors
