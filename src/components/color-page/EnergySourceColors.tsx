import styled from 'styled-components'
import Circle from './Circle'

const EnergySourceColorsStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`
const EnergySourceColors = () => {
  return (
    <EnergySourceColorsStyle>
      <div className="color-row">
        <Circle color={'#2DA55D'} colorName="Hydro" />
        <Circle color={'#4FCC51'} colorName="Wind" />
        <Circle color={'#81E0A8'} colorName="Solar" />
        <Circle color={'#375E4E'} colorName="Biomass" />
      </div>
      <div className="color-row">
        <Circle color={'#E88A74'} colorName="Coal" />
        <Circle color={'#D85067'} colorName="Gas" />
        <Circle color={'#203D5D'} colorName="Nuclear" />
        <Circle color={'#A376CC'} colorName="District heating" />
      </div>
    </EnergySourceColorsStyle>
  )
}

export default EnergySourceColors
