import styled from 'styled-components'

const CircleStyle = styled.div<{ border: string | undefined; color: string }>`
  .circle {
    padding-top: 100%;
    position: relative;
    background-color: ${props => props.color};
    border-radius: 50%;
    border: ${props => props.border};
  }
  .text {
    margin-top: 10px;
    text-align: center;
    font-weight: bold;
    letter-spacing: 0;
  }
`
type CircleProps = {
  color: string
  colorName?: string
  border?: string
  percentage?: string
  rgb?: string
}

const Circle = ({ color, colorName, border, percentage, rgb }: CircleProps) => {
  return (
    <CircleStyle className="Circle" border={border} color={color}>
      <div className="circle" />
      <div>
        {colorName && <p className="text">{colorName}</p>}
        {color && (
          <p style={{ textAlign: 'center', letterSpacing: 0 }} className="mb-0">
            {percentage ? percentage : color}
          </p>
        )}
      </div>
    </CircleStyle>
  )
}

export default Circle
