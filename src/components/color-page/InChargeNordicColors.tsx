import styled from 'styled-components'
import Circle from './Circle'

const InChargeNordicColorsStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`
const InChargeNordicColors = () => {
  return (
    <InChargeNordicColorsStyle>
      <h4 style={headingStyle}>Primary Colours</h4>
      <div className="color-row">
        <Circle color={'#46EE76'} colorName="InCharge Green" />
        <Circle color={'#2071B5'} colorName="InCharge Blue" />
        <Circle color={'#4E4B4B'} colorName="InCharge Grey" />
      </div>
      <h4 style={headingStyle}>Colour CTA</h4>
      <div className="color-row">
        <Circle
          color={'#46EE76'}
          colorName="Vattenfall Green"
          rgb={'70,238,118'}
        />
      </div>
    </InChargeNordicColorsStyle>
  )
}

const headingStyle = {
  paddingBottom: '40px',
  paddingTop: '20px',
}

export default InChargeNordicColors
