import styled from 'styled-components'
import Circle from './Circle'

const TertiaryColorsStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`
const TertiaryColors = () => {
  return (
    <TertiaryColorsStyle>
      <div className="color-row">
        <Circle color={'#869BAD'} colorName="Ash Blue" />
        <Circle color={'#69788C'} colorName="Ash Blue Dark" />
        <Circle color={'#E6E6E6'} colorName="Lighter Grey" />
        <Circle color={'#CCCCCC'} colorName="Light Grey" />
      </div>
      <div className="color-row">
        <Circle color={'#999999'} colorName="Medioum Grey" />
        <Circle color={'#666666'} colorName="Dark Grey" />
      </div>
    </TertiaryColorsStyle>
  )
}

export default TertiaryColors
