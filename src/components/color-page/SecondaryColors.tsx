import styled from 'styled-components'
import Circle from './Circle'

const SecondaryColorsStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`
const SecondaryColors = () => {
  return (
    <SecondaryColorsStyle>
      <div className="color-row">
        <Circle color={'#005C63'} colorName="Dark Green" />
        <Circle color={'#1E324F'} colorName="Dark Blue" />
        <Circle color={'#D1266B'} colorName="Pink" />
        <Circle color={'#85254B'} colorName="Dark Purple" />
      </div>
      <div className="color-row">
        <Circle color={'#3DC07C'} colorName="Green" />
        <Circle color={'#F93B18'} colorName="Red" />
        <Circle color={'#9B62C3'} colorName="Purple" />
      </div>
    </SecondaryColorsStyle>
  )
}

export default SecondaryColors
