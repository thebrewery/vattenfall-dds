import styled from 'styled-components'
import Circle from './Circle'

const BackgroundColorsStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`

const BackgroundColors = () => {
  return (
    <BackgroundColorsStyle>
      <div className="color-row">
        <Circle color={'#FFFEE5'} colorName="Light Yellow" />
        <Circle color={'#EDF9F3'} colorName="Light Green" />
        <Circle color={'#EBF2F3'} colorName="Light Blue alt." />
        <Circle color={'#EDF1F6'} colorName="Light Blue" />
        <div className="vf-col"></div>
        <div className="vf-col"></div>
      </div>
      <div className="color-row">
        <Circle color={'#FEF0EA'} colorName="Light Red" />
        <Circle color={'#F2F2F2'} colorName="Light Grey" />
        <div className="vf-col"></div>
        <div className="vf-col"></div>
        <div className="vf-col"></div>
        <div className="vf-col"></div>
      </div>
    </BackgroundColorsStyle>
  )
}

export default BackgroundColors
