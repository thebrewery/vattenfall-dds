import styled from 'styled-components'
import Circle from './Circle'

const PrimaryColorsStyle = styled.div`
  .color-row {
    display: flex;
    width: 100%;
    margin-bottom: 20px;
    .Circle {
      width: ${100 / 5}%;
      margin-left: 10px;
    }
  }
`

const PrimaryColors = () => {
  return (
    <PrimaryColorsStyle>
      <div className="color-row">
        <Circle color={'#FFDA00'} colorName="Solar Yellow" />
        <Circle color={'#2071B5'} colorName="Ocean Blue" />
        <Circle
          color={'#FFFFFF'}
          colorName="Aura White"
          border="1px solid #000000"
        />
      </div>
      <div className="color-row">
        <Circle color={'#000000'} colorName="Coal Black" />
        <Circle color={'#4E4B48'} colorName="Magnetic Gray" />
      </div>
    </PrimaryColorsStyle>
  )
}

export default PrimaryColors
