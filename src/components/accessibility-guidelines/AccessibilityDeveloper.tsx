import Link from 'next/link'
import styled from 'styled-components'
import { DataChecklist } from '../data-visualization/DataChecklist'
import { ExternalLinksExtended } from '../ExternalLinksExtended'
import AccordionGuidelines from '../accessibility/AccordionGuidelines'

const AccessibilityDeveloperStyle = styled.div`
  max-width: 900px;

  h2 {
    font-size: 2.25rem;
    line-height: 2.625rem;
    margin-bottom: 8px;
  }

  h3 {
    font-size: 28px;
    font-weight: 700;
    line-height: 36px;
  }

  .vf-component {
    &__container {
      max-width: 100%;
      border-bottom: 0;
      margin-bottom: 55px;

      .vf-guideline__checklist {
        padding-bottom: 40px;
      }
    }

    &__paragraph {
      font-size: 20px;
      font-weight: 400;
      line-height: 36px;
    }
  }

  .vf-accessibility {
    &__anchor-links {
      background-color: #f2f2f2;
      padding: 16px 20px 0 16px;
      max-width: 905px;

      a {
        padding-right: 12px;
        margin-right: 8px;
        font-size: 18px;
        font-style: normal;
        font-weight: 500;
        line-height: normal;
        border-right: 1px solid #000;
        display: inline-block;
        margin-bottom: 16px;

        &:last-of-type {
          border-right: 0;
          padding-right: 0;
          margin-right: 0;
        }
      }
    }
  }
`

const accordionData = [
  {
    name: 'Semantic HTML',
    content:
      'A semantic HTML element clearly describes its meaning to both the browser and the developer. It helps telling the assistive technology tools exactly what it is they are ‘reading’ on the page and what items there are to interact with. Finally semantic code allows users to skip between sections or scan the page more easily therefore making the task of digesting the page more user friendly. ',
    media: [
      {
        source: '/assets/accessibility/Semantic_HTML_Do.png',
        type: 'do',
        title: 'Do',
        label:
          'Implement semantic HTML throughout your pages which defines the role and importance of the element on the page.',
      },
      {
        source: '/assets/accessibility/Semantic_HTML_Dont.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Use HTML tags that don’t convey the elements meaning and functionality to assistive technology.',
      },
    ],
  },
  {
    name: 'Well-structured HTML ',
    content:
      'Screen readers process web pages in a linear manner - top down and one element at a time, therefore it helps structuring your pages in a logical way in terms of consuming and understanding the content. To do this use Semantic HTML to segment different sections of the page (headings and subheadings, paragraphs, header and footer, navigational elements and main content) to pick out the key sections. On a structured web page screen readers can better understand the layout of the page, preview sections and skip other content. ',
    media: [
      {
        source: '/assets/accessibility/Well-structuredHTML_Do.png',
        title: 'Do',
        type: 'do',
        label:
          'Structure the HTML in the same way as users should consume the content visually ie breaking up into different sections, using hierarchy in headings, header and footer and so on, so the right content is read at the right time.',
      },
      {
        source: '/assets/accessibility/Well-structuredHTML_Dont.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Avoid using long blocks of running text, lack of spacing or navigational elements.',
      },
    ],
  },
  {
    name: 'HTML lang Attribute',
    content:
      'HTML lang Attribute - The lang attribute specifies the language of the webpage in the HTML code meaning the primary language is identified and screen readers know how to pronounce the content of the page that is read out by the reader. If the lang attributes are not included the content may be read out in a way that makes it difficult to understand because of the pronunciation.',
    media: [
      {
        source: '/assets/accessibility/HTML_lang_Att_Do.png',
        title: 'Do',
        type: 'do',
        label: 'Include the laguage of the site in the code.',
      },
      {
        source: '/assets/accessibility/HTML_lang_Att_Dont.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Leave the code snippet that specified the language blank or forget to update an adaptation from one language into another with the old master language.',
      },
    ],
  },
  {
    name: 'Descriptive Hyperlinks',
    content:
      'Descriptive Hyperlinks - Another important action users take on webpages is clicking on links to other pages where it is common to to use short call to actions such as ‘Click here’. In terms of accessibility the recommendation is to use more descriptive hyperlinks that gives more information on where the link will take you, especially if the page has multiple links where using standard and same CTA:s can be confusing for someone using a screen reader.',
    media: [
      {
        source: '/assets/accessibility/Descripted_Hyperlinks_DO.png',
        type: 'do',
        title: 'Do',
        label:
          'Include some detail in the text of hyperlinks that helps the user understand what they might expect if clicking the link. For example ‘Read the sustainability report’, ‘Apply for a contract’ etc',
      },
      {
        source: '/assets/accessibility/Descripted_Hyperlinks_DONT.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Repeat the same generic CTA buttons that doesn’t give the user any context to what will happen when they click the button or the link  such as ‘Click here’, ‘Read more’ etc',
      },
    ],
  },
  {
    name: 'Image Alt Text',
    content:
      'Image Alt Text – In addition to being good for SEO ranking (search engines crawl site content for images and provide information from Alt tags to rank the sites higher if done correctly) Alt tags are important for accessibility. If an image lacks the alt attribute screen readers will either read the file name, title attribute if it exists or skip the image meaning the context for the rest of the page may be unclear to people with a visual impairment. Decorative images may not need an alt text but it’s best practice to use for all.',
    media: [
      {
        source: '/assets/accessibility/Image_Alt_txt_DO.png',
        type: 'do',
        title: 'Do',
        label:
          'Use descriptive alt texts that provide enough context and make it easy for user to understand. For example:  <img src="/sample_image.jpg" alt="Two women with laptops open, professionally dressed, in a meeting discussion in an office environment">',
      },
      {
        source: '/assets/accessibility/Image_Alt_txt_DONT.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Avoid simplification and using vague and generic language to describe the visuals. For example: <img src="/sample_image.jpg" alt="Two women in a meeting">',
      },
    ],
  },
  {
    name: 'ARIA Roles',
    content:
      'ARIA Roles - ARIA, the Accessible Rich Internet Applications web standards are a set of standards to make user interface controls more accessible for users of assistive technologies. They have a specific set of HTML attribute guidelines that together with a DOM (Document Object Model) can better support assistive technologies with information around how to handle a specific element on the page as well as if a state of the element has changed (such as a button being active) which improves browsing and usage of webpages greatly.',
    media: [
      {
        source: '/assets/accessibility/ARIA_DO.png',
        type: 'do',
        title: 'Do',
        label:
          'Include aria roles for all element states such as active, inactive.',
      },
      {
        source: '/assets/accessibility/ARIA_DONT.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Forget to include states in the ARIA roles or DOM details to allow good browsing levels for all users.',
      },
    ],
  },{
    name: 'Mobile zooming enabled',
    content:
      'Enabling mobile zooming allows users to easily pinch-to-zoom on a website when using a mobile device, enhancing the readability and accessibility especially for those with visual impairments. This approach helps maintain a user-friendly and accessible browsing experience across all devices.',
    media: [
      {
        source: '/assets/accessibility/mobile_zooming_enabled_do.png',
        type: 'do',
        title: 'Do',
        label:
          'Implement the zoom function using the code in the example above to ensure mobile users are also able to view content in larger size if needed.',
      },
      {
        source: '/assets/accessibility/mobile_zooming_enabled_dont.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Disable or exclude the zoom functionality code, see an example above where mobile zooming isn’t possible.',
      },
    ],
  },
]

export const AccessibilityDeveloper = () => {
  return (
    <AccessibilityDeveloperStyle className="vf-accessibility__container">
      <div className="vf-component__container">
        <div>
          <p className="vf-component__paragraph">
            As a developer your focus from an accessibility perspective is on
            making sure the HTML code is compliant with accessibility
            requirements and you also have an important role in the audit and
            testing stage of the process. For a developer it’s important to
            understand the principles and success criteria on a more detailed
            level.
          </p>
        </div>

        <div className="vf-accessibility__anchor-links">
          <Link href="#get-started">Get Started</Link>
          <Link href="#best-practices">Best Practices</Link>
          <Link href="#tools">Tools</Link>
          <Link href="#checklists">Checklists</Link>
        </div>
      </div>

      <div className="vf-component__container" id="get-started">
        <h2>Get Started</h2>

        <div>
          <p className="vf-component__paragraph">
            <ul>
              <li>
                Form an accessibility team with colleagues from different skill
                areas such as UX, Design, Content creation and Project
                Owner/Manager.
              </li>
              <li>
                Decide with your development team which tools to use. We've
                listed some approved tools further down the page.
              </li>
              <li>
                Use an existing template for your testing process, we have
                created a template available for you to use.
              </li>
              <li>
                Start with our suggested process and adapt it to suit your team.
              </li>
            </ul>
          </p>
        </div>
      </div>

      <div className="vf-component__container" id="best-practices">
        <h2>Best Practices and Dos & Dont’s</h2>
        <p className="vf-component__paragraph">
          As a developer, your job is to ensure the HTML code of your site or
          app works well with tools for people with disabilities that use
          assistive technology tools to browse websites. This involves writing
          and organizing the code in specific ways. There are several parts of
          HTML that assist in this:
        </p>

        <AccordionGuidelines data={accordionData} />
      </div>

      <div className="vf-component__container" id="checklists">
        <h2>Accessibility checklist for developers</h2>

        <p className="vf-component__paragraph">
          Below you can find a checklist of steps that should be part of your
          accessibility process.
        </p>
        <DataChecklist
          items={[
            'Read up on the principles and success criteria in WCAG 2.1 (and 2.2) to make sure you understand the requirements and tasks included for your role. ',
            'Do you need support via training? We have listed a few recommended training providers in different markets to help on this.   ',
            'Plan an Audit - Define the scope and objectives with focus to highlight all updates required to meet both conformance level A and AA, (any AAAs that can be met without infringing on brand guidelines should also be considered).',
            'Select the relevant development tools and methoodologies to use - Remember testing should be completed using both automated and manual processes to lower the risk of issues being missed off. ',
            'If you need a template for documenting the issues, severity level and estimated complexity level to address, use our template below.',
            'Address the issues raised in a set order of priority, if resources are not available to do all. Start with the lowest compliance level first.',
            'When all updates are complete - always re-test to ensure everything has been resolved.',
            'Ongoing - Stay up to date and build in regular Accessibility best practice and testing into your process - in the end this will save time!',
          ]}
        />
      </div>

      <div className="vf-component__container" id="tools">
        <h2>Recommended tools, templates and plugins for developers</h2>
        <div className="vf-component__paragraph">
          Here you will find a list of tools and plugins, for designers, that
          have been selected for their functionality, ease of use and overall
          reliability. Please check the use with your IT department - a general
          release by IT is still pending at the time of publication of the
          guidelines 19.06.2024.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'Colour Contrast Checker',
              url: 'https://colourcontrast.cc/',
              desc: 'An easy to use online colour contrast and typeface checker tool.',
              url_label: 'Colour contrast tool',
            },
            {
              text: 'Accessibility Insights for Web',
              url: 'https://accessibilityinsights.io/docs/web/overview/',
              desc: 'A browser extention for developers helping to find and fix issues and compliance against WCAG (2.1) AA.',
              url_label: 'Browser insights extension',
            },
            {
              text: 'Accessibility Audit template',
              url: 'https://admin.dds.nordx.dev/uploads/Vattenfall_Accessibility_Audit_Template_June2024_b3475511a0.xlsx',
              desc: 'A downloadable template created to help you get started auditing your digital products for WCAG issues.',
              url_label: 'Audit template',
            },
            {
              text: 'HTML CodeSniffer',
              url: 'https://github.com/squizlabs/HTML_CodeSniffer',
              desc: 'A Javascript application helping you to check your HTML code for WCAG non-compliance issues.',
              url_label: 'Application to scan code',
            },
            {
              text: 'A11ly project checklist',
              url: 'https://www.a11yproject.com/checklist/',
              desc: 'A comprehensive checklist that helps to target and check the most common accessibility issues.',
              url_label: 'Online checklist',
            },
            {
              text: 'Mac VoiceOver',
              url: 'https://support.apple.com/guide/mac-help/have-your-mac-speak-text-thats-on-the-screen-mh27448/mac',
              desc: 'Mac’s built in voice over tool to help you test how accessible this fature is on your website.',
              url_label: 'Voice over tool',
            },
          ]}
        />
      </div>

      <div className="vf-component__container">
        <h2>Training</h2>
        <div className="vf-component__paragraph">
          Here is a list of accessibility training courses recommended by teams
          within or working with Vattenfall. Bear in mind some courses are
          market specific.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'edX Intro to Web Accessibility',
              url: 'https://www.edx.org/learn/web-accessibility/the-world-wide-web-consortium-w3c-introduction-to-web-accessibility',
              desc: "W3C's extensive intruduction course to Web Accessibility, useful for all roles.",
              url_label: 'Introduction course – Free option',
            },
            {
              text: 'LinkedIn Learning',
              url: 'https://www.linkedin.com/learning/topics/accessibility',
              desc: 'LinkedIn Learning has a range of (free with a business account) Accessibility courses for different roles and levels.',
              url_label: 'LinkedIn courses – Free with account',
            },
            {
              text: 'W3cx Developer courses',
              url: 'https://www.edx.org/school/w3cx',
              desc: "W3C's Developer courses including more in-depth parts covering accessibility.",
              url_label: 'W3C for developer – Free option',
            },
            {
              text: 'Funka Academy Develop',
              url: 'https://www.funka.com/en/funka-academy/course-packages-for-roles-and-functions/accessibility-for-developers---course-package/',
              desc: 'A 5-course developer package covering intro, universal design, technology, mobile apps and documents.',
              url_label: 'Application to scan code',
            },
            {
              text: 'A11y Accessible Code',
              url: 'https://www.a11y-collective.com/product/accessible-code/',
              desc: "A11y collective's short course with focus on writing accessible code, including tips and tricks.",
              url_label: 'Write accessible code',
            },
            {
              text: 'Udacity web accessibility',
              url: 'https://www.udacity.com/course/web-accessibility--ud891',
              desc: "Udacity's developer focused course for making your web applications accessible.",
              url_label: 'Web accessibility',
            },
          ]}
        />
      </div>

      <div className="vf-component__container" id="resources">
        <h2>Helpful Resources</h2>
        <div className="vf-component__paragraph">
          For additional resources, WCAG2 recommendations and Vattenfall's
          commitment to accessibility visit the links below.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'WCAG 2 Overview',
              url: 'https://www.w3.org/TR/WCAG21/',
              desc: "WCAG 2.1 Accessibility guidelines, created by the World Wide Web (W3) Consortium, which is what we're adhering to in our work.",
              url_label: 'WCAG 2.1 guidelines',
            },
            {
              text: 'World Wide Web Consortium',
              url: 'https://www.w3.org/',
              desc: "W3C's homepage with extensive information around everything that covers accessibility including latest updates.",
              url_label: "W3C's homepage",
            },
            {
              text: 'A11y fundamentals',
              url: 'https://www.a11yproject.com/',
              desc: "The A11y project's, a community driven organisation, covering fundamentals and principles behind accessible design.",
              url_label: 'A11y Project Website',
            },
            {
              text: 'European Accessibility Act',
              url: 'https://ec.europa.eu/social/main.jsp?catId=1202',
              desc: "A link to the European Commission's directive the European Accessibility Act, in full.",
              url_label: 'Accessibility Act',
            },
            {
              text: 'European Disability Forum',
              url: 'https://www.edf-feph.org',
              desc: 'Website of the independent non-governmental organisation, EDF,  representing people with disabilities within the EU.',
              url_label: 'EDF website',
            },
            {
              text: 'W.H.O. / Disability',
              url: 'https://www.who.int/europe/health-topics/disability#tab=tab_1',
              desc: "World Health Organisation's framework for achieving the highest attainable standard of health for persons with disabilities (including digital accessibility).",
              url_label: 'W.H.O. Disability framework',
            },
          ]}
        />
      </div>
    </AccessibilityDeveloperStyle>
  )
}
