import Link from 'next/link'
import styled from 'styled-components'
import { DataChecklist } from '../data-visualization/DataChecklist'
import { ExternalLinksExtended } from '../ExternalLinksExtended'
import AccordionGuidelines from '../accessibility/AccordionGuidelines'

const AccessibilityEditorStyle = styled.div`
  max-width: 900px;

  h2 {
    font-size: 2.25rem;
    line-height: 2.625rem;
    margin-bottom: 8px;
  }

  h3 {
    font-size: 28px;
    font-weight: 700;
    line-height: 36px;
  }

  .vf-component {
    &__container {
      max-width: 100%;
      border-bottom: 0;
      margin-bottom: 55px;

      .vf-guideline__checklist {
        padding-bottom: 40px;
      }
    }

    &__paragraph {
      font-size: 20px;
      font-weight: 400;
      line-height: 36px;
    }
  }

  .vf-accessibility {
    &__anchor-links {
      background-color: #f2f2f2;
      padding: 16px 20px 0 16px;
      max-width: 905px;

      a {
        padding-right: 12px;
        margin-right: 8px;
        font-size: 18px;
        font-style: normal;
        font-weight: 500;
        line-height: normal;
        border-right: 1px solid #000;
        display: inline-block;
        margin-bottom: 16px;

        &:last-of-type {
          border-right: 0;
          padding-right: 0;
          margin-right: 0;
        }
      }
    }
  }
`

const accordionData = [
  {
    name: 'Links',
    content:
      "Navigation should follow common patterns and use clear and descriptive text, this includes link texts such as the page-URL and links taking the user away from the page. In other words, the link text should make sense even if it's the only text read on the page.  ",
    media: [
      {
        source: '/assets/accessibility/Links_Do.png',
        type: 'do',
        title: 'Do',
        label:
          'Write descriptive link texts to ensure users understand where the link leads, even when viewed out of context. ',
      },
      {
        source: '/assets/accessibility/Links_Dont.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Use generic link texts like ’Read more’ and ’Click here’, especially if you have a lot of links on the same page.  ',
      },
    ],
  },
  {
    name: 'Image Alt text',
    content:
      'Image Alt text – In addition to being good for SEO ranking (search engines crawl site content for images and provide information from Alt tags to rank the sites higher if done correctly). If an image lacks the alt attribute screen readers will either read the file name, title attribute if it exists or skip the image meaning the context for the rest of the page may be unclear to people with a visual impairment. Decorative images may not need an alt text but it’s best practice to use for all.',
    media: [
      {
        source: '/assets/accessibility/Img_Alt_txt_Do.png',
        title: 'Do',
        type: 'do',
        label:
          'Use desrcriptive alt texts that provide enough context for the user such as alt=: “A woman wearing headphones and a scarf holds a smartphone and smiles at the camera while standing outdoors with a cityscape in the background”',
      },
      {
        source: '/assets/accessibility/Img_Alt_txt_Dont.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Make the alt text too generic so visually impaired users may miss important context. For example alt= “A woman wearing headphones and holds a smartphone”',
      },
    ],
  },
  {
    name: 'Level for headlines',
    content:
      'Headings play a crucial role in accessibility, as they establish a content hierarchy. There are six available heading levels, from H1 to H4 (at Vattenfall we use down to H4), ranked in descending order of importance.',
    media: [
      {
        source: '/assets/accessibility/Level_for_headlines_Do.png',
        title: 'Do',
        type: 'do',
        label:
          'Consider the semantic purpose of headings, meaning that they help convey meaning and clarity for the user and helps assistive technology to present content in the right way.',
      },
      {
        source: '/assets/accessibility/Level_for_headlines_Dont.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Use only formatting text as headings for the text to be bold, stand out and attract attention from the user as it will create a confusing user experience with assistive technology . ',
      },
    ],
  },
  {
    name: 'Written content',
    content:
      'Aim to keep written content as clear and concise as possible to ensure all customers and visitors are able to understand the content.  Use bullets or tables where relevant to explain lists and data in a way that helps users better understand the content.',
    media: [
      {
        source: '/assets/accessibility/Written_DO.png',
        title: 'Do',
        type: 'do',
        label:
          'Keep complicated formulations, wording and acronyms to a minimum and provide an explanation or more information to support when needed.',
      },
      {
        source: '/assets/accessibility/Written_DONT.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Use uninformative or uneccessarily complicated formulations or unexplained acronyms in your copy, link descriptions and headings that assume that the user understands the content and information shared without further explanation. ',
      },
    ],
  },
  {
    name: 'Accessible files',
    content:
      'If uploading and making files available for download it’s also important that files meet shared accessibility guidelines (such as colour contrast, alt texts, text size etc), see our templates in Brand Toolbox (PPT, PDF, Word) for already compliant documents.',
    media: [
      {
        source: '/assets/accessibility/Files_DO.png',
        title: 'Do',
        type: 'do',
        label:
          'Give descriptive titles to the document available for download and ensure the document itself meets accessibility requirements for contrast, alt texts, text sizing etc.',
      },
      {
        source: '/assets/accessibility/Files_DONT.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Avoid using only numbers in file names which impact accessibility and SEO ranking negatively. Forget about contrast, appropriate text size and alt texts in your downloadable files.',
      },
    ],
  },
  {
    name: 'Headlines & page titles',
    content:
      'Use clear headings and break up text into sections to help users of assistive technology and search engines process the content in the correct order . The headline of your pages and page title should be unique, understandable to the user and have a clear connection to the topic or content of the page.  Ensure that both headlines and page titles accurately describe the content and purpose and that language is simple and unique (no duplicated content).',
    media: [
      {
        source: '/assets/accessibility/headlines_page_titles_do.png',
        title: 'Do',
        type: 'do',
        label:
          'Ensure that headlines and titles match the content and is understandable to all visitors of the page.',
      },
      {
        source: '/assets/accessibility/headlines_page_titles_dont.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Avoid using complex or too generic wording in headlines and page titles that make the content of the page unclear to the user.',
      },
    ],
  },
]

export const AccessibilityEditor = () => {
  return (
    <AccessibilityEditorStyle className="vf-accessibility__container">
      <div className="vf-component__container">
        <div>
          <p className="vf-component__paragraph">
            As a Content editor, CMS editor, or other employee working with
            content, your accessibility focus should cover all the POUR
            principles. If you're using a CMS tool, it's beneficial to choose
            one that meets accessibility requirements from the start as this can
            reduce a lot of the manual work you may need to do. However, this
            doesn't eliminate the need for manual checks before publishing the
            final content on the pages. As a bonus following the recommendations
            in the steps below also helps improve SEO-ranking which has a proven
            positive effect on sales.
          </p>
        </div>

        <div className="vf-accessibility__anchor-links">
          <Link href="#get-started">Get Started</Link>
          <Link href="#best-practices">Best Practices</Link>
          <Link href="#tools">Tools</Link>
          <Link href="#checklists">Checklists</Link>
        </div>
      </div>

      <div className="vf-component__container" id="get-started">
        <h2>Get Started</h2>

        <div>
          <p className="vf-component__paragraph">
            <ul>
              <li>
                Form an accessibility team with colleagues from different skill
                areas such UX, Design, Development and Project Owner/Manager.
              </li>
              <li>
                Decide within your team which tools to use. We've listed some
                approved tools further down the page.
              </li>
              <li>
                Use an existing template for your testing process, we have
                created a template available for you to use, also listed below.
              </li>
              <li>
                Start with our suggested process and adapt it to suit your team.
              </li>
              <li>
                Build accessibility into your day to day process including how
                you work with content and setting time aside for checks along
                the way.
              </li>
            </ul>
          </p>
        </div>
      </div>

      <div className="vf-component__container" id="best-practices">
        <h2>Best Practices and Dos & Dont’s</h2>
        <p className="vf-component__paragraph">
          Editors and content managers are key in maintaining the quality of
          content and making sure that content meets the set accessibility
          standards and expectations. Here are a few best practices to follow to
          improve web accessibility with your content.
        </p>

        <AccordionGuidelines data={accordionData} />
      </div>

      <div className="vf-component__container" id="checklists">
        <h2>Accessibility checklist for editors</h2>

        <p className="vf-component__paragraph">
          Below you can find a checklist of steps that should be part of your
          accessibility process.
        </p>
        <DataChecklist
          items={[
            'Define the target audience for your information and what the user is supposed to do with this information. ',
            'Go over all texts written to ensure that language is simple and straightforward, using the readability index helps decide what is a suitable level.',
            'Format links and headings so that they can be read easily and understood without explanatory text.',
            'All uploaded PDFs or documents available to users must also meet accessibility standards, Brand Toolbox has a pre-approved template for PPT (soon to be followed by Word) that you can use but different software and design tools often have their own plugins or available tools, for example InDesign, Figma, Adobe Acrobat, Powerpoint, Word etc.',
            'Plan an Audit - Define the scope and objectives with focus to highlight all updates required to meet both conformance level A and AA, in addition any AAA updates that can be met without infringing on brand guidelines should also be considered.',
            'Select the tools and methodologies to use - Remember testing should be completed using both automated and manual processes to lower the risk of issues being missed off.',
          ]}
        />
      </div>

      <div className="vf-component__container" id="tools">
        <h2>Tools, templates and plugins for editors</h2>
        <div className="vf-component__paragraph">
          Here you will find a list of tools and plugins, for designers, that
          have been selected for their functionality, ease of use and overall
          reliability. Please check the use with your IT department - a general
          release by IT is still pending at the time of publication of the
          guidelines 19.06.2024.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'Colour contrast checker',
              desc: 'An easy to use online colour contrast and typeface checker tool.',
              url: 'https://colourcontrast.cc/',
              url_label: 'Colour contrast tool',
            },
            {
              text: 'Accessibility insights for web',
              desc: 'A browser extention for developers helping to find and fix issues and compliance against WCAG (2.1) AA.',
              url: 'https://accessibilityinsights.io/docs/web/overview/',
              url_label: 'Browser insights extension',
            },
            {
              text: 'Accessibility Audit template',
              desc: 'A downloadable template created to help you get started auditing your digital products for WCAG issues.',
              url: 'https://admin.dds.nordx.dev/uploads/Vattenfall_Accessibility_Audit_Template_June2024_b3475511a0.xlsx',
              url_label: 'Audit template',
            },
            {
              text: 'Sa11y site evaluation',
              desc: 'Sa11y’s quality assurance assitance that visually highlights content issues and provides tips on how to fix.',
              url: 'https://sa11y.netlify.app/overview/',
              url_label: 'Site evaluation tool',
            },
            {
              text: 'Reciteme audit checker',
              desc: 'Comprehensive accessibility checker that highlights issues with color contrast, screen reader compatibility, font size, headings, alt text, navigation, readability.',
              url: 'https://reciteme.com/product/website-accessibility-checker/',
              url_label: 'Recite me',
            },
            {
              text: 'A11yproject checklist',
              desc: 'A comprehensive checklist that helps to target and check the most common accessibility issues.',
              url: 'https://www.a11yproject.com/checklist/',
              url_label: 'Online checklist',
            },
          ]}
        />
      </div>

      <div className="vf-component__container">
        <h2>Training</h2>
        <div className="vf-component__paragraph">
          Here is a list of accessibility training courses recommended by teams
          within or working with Vattenfall. Bear in mind some courses are
          market specific.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'edX Intro to Web Accessibility',
              url: 'https://www.edx.org/learn/web-accessibility/the-world-wide-web-consortium-w3c-introduction-to-web-accessibility',
              desc: 'W3C’s extensive intruduction course to Web Accessibility, useful for all roles.',
              url_label: 'Introduction course - Free option',
            },
            {
              text: 'LinkedIn Learning',
              url: 'https://www.linkedin.com/learning/topics/accessibility',
              desc: 'LinkedIn Learning has a range of (free with a business account) Accessibility courses for different roles and levels.',
              url_label: 'LinkedIn courses - Free with account',
            },
            {
              text: 'A11ly Content for the web',
              url: 'https://www.a11y-collective.com/courses/writing-accessible-content-for-the-web/',
              desc: 'A11y collective’s content course with focus on writing content that is readable and understandable by everyone.',
              url_label: 'Web content course',
            },
            {
              text: 'Funka Academy Plain Language',
              url: 'https://www.funka.com/en/funka-academy/course-packages-for-roles-and-functions/accessibility-for-editors---course-package/',
              desc: 'Course with focus on creating plain language for good written content accessibility. Includes background on different disabilities, the reading process in addition to how to write.',
              url_label: 'Plain Language course',
            },
            {
              text: 'Funka Academy Documents',
              url: 'https://www.funka.com/en/funka-academy/course-packages-for-roles-and-functions/creating-accessible-documents---course-package2/',
              desc: 'A number of courses with focus on making documents accessible (including) Word, PPT, Excel and PDFs.',
              url_label: 'Accessible documents courses',
            },
          ]}
        />
      </div>

      <div className="vf-component__container" id="resources">
        <h2>Helpful Resources</h2>
        <div className="vf-component__paragraph">
          For additional resources, WCAG2 recommendations and Vattenfall's
          commitment to accessibility visit the links below.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'WCAG 2 Overview',
              url: 'https://www.w3.org/TR/WCAG21/',
              desc: "WCAG 2.1 Accessibility guidelines, created by the World Wide Web (W3) Consortium, which is what we're adhering to in our work.",
              url_label: 'WCAG 2.1 guidelines',
            },
            {
              text: 'World Wide Web Consortium',
              url: 'https://www.w3.org/',
              desc: "W3C's homepage with extensive information around everything that covers accessibility including latest updates.",
              url_label: "W3C's homepage",
            },
            {
              text: 'A11y fundamentals',
              url: 'https://www.a11yproject.com/',
              desc: "The A11y project's, a community driven organisation, covering fundamentals and principles behind accessible design.",
              url_label: 'A11y Project Website',
            },
            {
              text: 'European Accessibility Act',
              url: 'https://ec.europa.eu/social/main.jsp?catId=1202',
              desc: "A link to the European Commission's directive the European Accessibility Act, in full.",
              url_label: 'Accessibility Act',
            },
            {
              text: 'European Disability Forum',
              url: 'https://www.edf-feph.org',
              desc: 'Website of the independent non-governmental organisation, EDF,  representing people with disabilities within the EU.',
              url_label: 'EDF website',
            },
            {
              text: 'W.H.O. / Disability',
              url: 'https://www.who.int/europe/health-topics/disability#tab=tab_1',
              desc: "World Health Organisation's framework for achieving the highest attainable standard of health for persons with disabilities (including digital accessibility).",
              url_label: 'W.H.O. Disability framework',
            },
          ]}
        />
      </div>
    </AccessibilityEditorStyle>
  )
}
