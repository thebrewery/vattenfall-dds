import Link from 'next/link'
import styled from 'styled-components'
import { DataChecklist } from '../data-visualization/DataChecklist'
import { ExternalLinksExtended } from '../ExternalLinksExtended'
import AccordionGuidelines from '../accessibility/AccordionGuidelines'

const AccessibilityDesignerStyle = styled.div`
  max-width: 900px;

  h2 {
    font-size: 2.25rem;
    line-height: 2.625rem;
    margin-bottom: 8px;
  }

  h3 {
    font-size: 28px;
    font-weight: 700;
    line-height: 36px;
  }

  .vf-component {
    &__container {
      max-width: 100%;
      border-bottom: 0;
      margin-bottom: 55px;

      .vf-guideline__checklist {
        padding-bottom: 40px;
      }
    }

    &__paragraph {
      font-size: 20px;
      font-weight: 400;
      line-height: 36px;
    }
  }

  .vf-accessibility {
    &__anchor-links {
      background-color: #f2f2f2;
      padding: 16px 20px 0 16px;
      max-width: 905px;

      a {
        padding-right: 12px;
        margin-right: 8px;
        font-size: 18px;
        font-style: normal;
        font-weight: 500;
        line-height: normal;
        border-right: 1px solid #000;
        display: inline-block;
        margin-bottom: 16px;

        &:last-of-type {
          border-right: 0;
          padding-right: 0;
          margin-right: 0;
        }
      }
    }
  }
`

const accordionData = [
  {
    name: 'Contrast Ratio',
    content:
      'The contrast ratio is a measure of the difference in luminance between two colours, typically between text and it’s background. It is expressed as a ratio (e.g. 4.5:1). The higher the contrast ratio, the more distinguishable the text is from the background.  ',
    media: [
      {
        source: '/assets/accessibility/Contrast_Ratio_Do.png',
        type: 'do',
        title: 'Do',
        label:
          'Test the contrast ratio of all important elements in your design to make sure it is a minimum of 4.5:1 (the contrast is less of an issue if the elements are purely decorative).',
      },
      {
        source: '/assets/accessibility/Contrast_Ratio_Dont.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Use colours for text and background (including images) that have too low contrast in comparison, to convey important information. Images with patterns or a lot of different shades can be particularly tricky.',
      },
    ],
  },
  {
    name: 'Content Hierarchy',
    content:
      'From a design perspective it is important to work with visual content hierarchy and spacing of content in a way that make it easy for the user to understand what is a new section. As a result this improves the ability for assistive tools to correctly process the content. This attention to detail in design can significantly enhance the usability and accessibility, making it more user-friendly and inclusive, especially for those who rely on assistive tools.',
    media: [
      {
        source: '/assets/accessibility/Content_Hierarchy_Do.png',
        title: 'Do',
        type: 'do',
        label:
          'Separate different text blocks and sections by using correct hierarchy headings and spacing to make it clear when one section ends and another one starts.',
      },
      {
        source: '/assets/accessibility/Content_Hierarchy_Dont.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Use the same heading hierarchy throughout and use large text blocks without any separation.',
      },
    ],
  },
  {
    name: 'Information Description',
    content:
      "When designing interfaces, it's crucial not to rely solely on colour to highlight critical information, such as warning states, which may be problematic for users with colour blindness. In addition to using colour, also include text or symbols to convey the same information. These alternative methods can ensure that the vital information is accessible to all users, regardless of their ability to perceive colour differences.",
    media: [
      {
        source: '/assets/accessibility/Info_Desc_Do.png',
        title: 'Do',
        type: 'do',
        label:
          'Use additional ways of communicating warning or highlight states such as missing information in forms by either adding bold to highlight copy or icons to attract the users attention.',
      },
      {
        source: '/assets/accessibility/Info_Desc_Dont.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Rely solely on the colour red in warning states or a small change in background colour to highlight something important you’re trying to convey to the user.',
      },
    ],
  },
  {
    name: 'Buttons & interactive elements',
    content:
      'It is essential to ensure that all interactive elements within design, such as buttons, links, and form controls, are not only easily identifiable but also usable. These elements should be designed in such a manner that they stand out, making their functionality clear for users. At the same time, these interactive elements must also be usable, meaning they should function as expected to provide a good user experience.',
    media: [
      {
        source: '/assets/accessibility/Buttons_DO.png',
        title: 'Do',
        type: 'do',
        label:
          'Ensure interactive elements such as buttons or forms have design and interactions built in to clearly communicate the different states.',
      },
      {
        source: '/assets/accessibility/Buttons_DONT.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Forget to include different interaction or inactive states in the design and development process, including the appropriate accessible code.',
      },
    ],
  },
  {
    name: 'Tables',
    content:
      'To make tables with larger amounts of data accessible and easy for everyone to read make sure to include a clear header row and keep the table structure simple. Use colours that contrast well and ensure that text size is easy to read. The table should be created in code, and not be displayed as an image and all fields should have alt texts.',
    media: [
      {
        source: '/assets/accessibility/Tables_DO.png',
        title: 'Do',
        type: 'do',
        label:
          'Follow other accessibility guidelines to do with colour contrast, text size, include headers for each column and keep the structure simple to ensure the information presented can be seen and interpreted easily.',
      },
      {
        source: '/assets/accessibility/Tables_DONT.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Cram too much information into tables, include emtpy cells or information with no relevance as it can lead to a confusing experience for users of assistive tools, cognitive or visual challenges.',
      },
    ],
  },
  {
    name: 'Forms',
    content:
      'Forms are another example where a lot of information may be collated in smaller spaces which needs extra consideration in design to make sure they meet all POUR principles. Ensure all form fields have a descriptive label adjacent to the field and supportive text to help users fill out the forms. Adhere to contrast and text size guidelines for legibility and keep them simple. Another important consideration is how to deal with notifications or error states where the user may need to act upon the message communicated. Finally forms also must be coded in a way that it’s easy for the user to understand what is being asked for in each field, what is mandatory and if there is an error state activated.',
    media: [
      {
        source: '/assets/accessibility/Forms_DO.png',
        title: 'Do',
        type: 'do',
        label:
          'Ensure fields and text in or in close proximity to form fields or tables is large enough to read and clear enough to understand. Mark or highlight mandatory fields in forms and keep as simple as possible, ideally using common UI patterns.',
      },
      {
        source: '/assets/accessibility/Forms_DONT.png',
        title: `Don't`,
        type: 'dont',
        label:
          'Overcomplicate or introduce completely new UI patterns in your forms. Use text or contrast that reduces legibility too much. Use only colour to convey error states or important noticifications.',
      },
    ],
  },
  {
    name: 'Background colours',
    content:
      "Background colours fall within the category referred to as non-text contrast that need to adhere to a 3:1 ratio when used as part of a functional design. Therefore when selecting digital background colours for accessibility, it's important to stick to the right contrast ratio when they have a function other than decorative. All of our background colours fail the required ratio for function but are fine to use when purely decorative and not being able to see the colour confuses the user-.",
    media: [
      {
        source: '/assets/accessibility/background_colors_do.png',
        type: 'do',
        title: 'Do',
        label:
          'Use background colours for decorative purposes, to separate sections on the pages and build in space. Make sure the elements on the colour block, such as text and buttons, still meet required contrast ratios.',
      },
      {
        source: '/assets/accessibility/background_colors_dont.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Use background colours (yellow is the most challenging colour in terms of contrast) when they have a function in the design, in the example above it’s hard to see if the text in the yellow block is a headline to the text below making the experience more confusing.',
      },
    ],
  },
  {
    name: 'Audio/Video',
    content:
      'For video and audio content select tools that support captions and subtitles that can be read by screen readers. This is particularly important for those who have difficulties hearing or are visually impaired, as these features enable them to fully engage with the content.',
    media: [
      {
        source: '/assets/accessibility/audio_video_do.png',
        type: 'do',
        title: 'Do',
        label:
          'Before choosing audio and video player tools or services, make sure you look into the support they provide as default when it comes to assistive technology.',
      },
      {
        source: '/assets/accessibility/audio_video_dont.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Use tools that don’t support captions, subtitles or assistive technologies.',
      },
    ],
  },
  {
    name: 'Interactive content',
    content:
      'Use interactive content, such as overlays and slideshows, carefully to make sure the most important information is available to all users. While these elements can add interest and engagement to your site, they must be employed in a manner that ensures the most critical information is readily accessible to all users.',
    media: [
      {
        source: '/assets/accessibility/interactive_content_do.png',
        type: 'do',
        title: 'Do',
        label:
          'Design interactive content that can be easily navigated by users with motoring challenges and using assistive tools.',
      },
      {
        source: '/assets/accessibility/interactive_content_dont.png',
        type: 'dont',
        title: `Don't`,
        label:
          'Use interactive components to display need to know and critical steps in a user journey if it isn’t fully accessible using assistive tools. Make buttons or other control functions too small or moving so they are hard to click. ',
      },
    ],
  },
]

export const AccessibilityDesigner = () => {
  return (
    <AccessibilityDesignerStyle className="vf-accessibility__container">
      <div className="vf-component__container">
        <div>
          <p className="vf-component__paragraph">
            As a designer, your role is to enhance the accessibility of digital
            products from a UX and design perspective. This involves adhering to
            widely recognized functionality, structural patterns in digital
            design and other visual communication patterns. Users of websites
            and apps are accustomed to specific navigation methods and ways of
            using digital products across various devices. A drastic change away
            from these methods and patterns can make it challenging for many
            users to adapt and understand how to use the website.
          </p>
        </div>

        <div className="vf-accessibility__anchor-links">
          <Link href="#get-started">Get Started</Link>
          <Link href="#best-practices">Best Practices</Link>
          <Link href="#tools">Tools</Link>
          <Link href="#checklists">Checklists</Link>
        </div>
      </div>

      <div className="vf-component__container" id="get-started">
        <h2>Get Started</h2>

        <div>
          <p className="vf-component__paragraph">
            <ul>
              <li>
                Form an accessibility team with colleagues from different skill
                areas such as Development, Content creation and Project
                Owner/Manager.
              </li>
              <li>
                Decide within your team which UX and Designer tools to use.
                We've listed some approved tools further down the page.
              </li>
              <li>
                Use an existing template for your testing process, we have
                created a template available for you to use.
              </li>
              <li>
                Start with our suggested process and adapt it to suit your team.
              </li>
            </ul>
          </p>
        </div>
      </div>

      <div className="vf-component__container" id="best-practices">
        <h2>Best Practices and Dos & Dont’s</h2>
        <p className="vf-component__paragraph">
          As a UX or UI designer the responsibility is not only to ensure
          compliance with legal standards but also enhance the overall user
          experience and inclusivity of the digital products you’re working on.
          Here are a few best practices to follow to improve web accessibility.
        </p>

        <AccordionGuidelines data={accordionData} />
      </div>

      <div className="vf-component__container" id="checklists">
        <h2>Accessibility checklist for designers</h2>

        <p className="vf-component__paragraph">
          Below you can find a checklist of steps that should be part of your
          accessibility process.
        </p>
        <DataChecklist
          items={[
            'Read up on the principles and success criteria in WCAG 2.1 (and 2.2) to make sure you understand the requirements and tasks included for your role. ',
            'Do you need support via training? We have listed a few recommended training providers in different markets to help on this.   ',
            'Plan an Audit - Define the scope and objectives with focus to highlight all updates required to meet both conformance level A and AA (any AAAs that can be met without infringing on brand guidelines should also be considered).',
            'Select the design tools and methodologies to use - Remember testing should be completed using both automated and manual processes to lower the risk of issues being missed off. ',
            'If you need a template for documenting the issues, severity level and estimated complexity level to address, you can use our template to start.',
            'Address the issues raised in a set order of priority, if resources are not available to do all. Start with the lowest compliance level first (A to AAA).',
            'When all updates scheduled are made - re-test to ensure everything has been resolved.',
          ]}
        />
      </div>

      <div className="vf-component__container" id="tools">
        <h2>Recommended tools and plugins for designers</h2>
        <div className="vf-component__paragraph">
          Here you will find a list of tools and plugins, for designers, that
          have been selected for their functionality, ease of use and overall
          reliability. Please check the use with your IT department - a general
          release by IT is still pending at the time of publication of the
          guidelines 19.06.2024.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'Colour Contrast Checker',
              url: 'https://colourcontrast.cc/',
              desc: 'An easy to use online colour contrast and typeface checker tool.',
              url_label: 'Colour contrast tool',
            },
            {
              text: 'Figma plugins / tools',
              url: 'https://www.figma.com/community/category/accessibility?resource_type=mixed&editor_type=all&price=all&sort_by=all_time&creators=all',
              desc: 'Available Figma plug-ins to help with your ongoing work creating accessible web design.',
              url_label: 'Figma Tools',
            },
            {
              text: 'Design annotation guide',
              url: 'https://www.youtube.com/watch?v=Y35jmpS8lQM&t=4s',
              desc: 'A YouTube guide to annotating your designs with accessibility information, incorporating it early in the process.',
              url_label: 'YouTube annotation guide ',
            },
            {
              text: 'Designing for Web Accessibility',
              url: 'https://www.w3.org/WAI/tips/designing/',
              desc: "W3C's guide to designing for Web Accessibility, including dos and dont's and visual examples to the most common issues.",
              url_label: 'Designing for Accessibility',
            },
            {
              text: 'Color contrast simulator',
              url: 'https://developer.paciellogroup.com/color-contrast-checker/',
              desc: "TPGi's free colour contrast checker to help you designing for individuals with colour blindness or low vision impairments.",
              url_label: 'Simulator tool',
            },
            {
              text: 'A11y project checklist',
              url: 'https://www.a11yproject.com/checklist/',
              desc: 'A comprehensive checklist that helps to target and check the most common accessibility issues.',
              url_label: 'Online checklist',
            },
          ]}
        />
      </div>

      <div className="vf-component__container">
        <h2>Training</h2>
        <div className="vf-component__paragraph">
          Here is a list of accessibility training courses recommended by teams
          within or working with Vattenfall. Bear in mind some courses are
          market specific.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'edX Intro to Web Accessibility',
              url: 'https://www.edx.org/learn/web-accessibility/the-world-wide-web-consortium-w3c-introduction-to-web-accessibility',
              desc: "W3C's extensive intruduction course to Web Accessibility, useful for all roles.",
              url_label: 'Introduction course - Free option',
            },
            {
              text: 'LinkedIn Learning',
              url: 'https://www.linkedin.com/learning/topics/accessibility',
              desc: 'LinkedIn Learning has a range of (free with a business account) Accessibility courses for different roles and levels.',
              url_label: 'LinkedIn courses - Free with account',
            },
            {
              text: 'Accessible Tech. Design',
              url: 'https://accessible-eu-centre.ec.europa.eu/content-corner/news/sign-accessibleeus-new-accessible-technology-design-course-2023-11-10_en',
              desc: "European Commission's course that teaches the basics of universal accessibility in digital environments.",
              url_label: 'Accessible Tech Design course',
            },
            {
              text: 'Funka Academy UX Focus',
              url: 'https://www.funka.com/en/funka-academy/course-packages-for-roles-and-functions/accessibility-for-ux-frontend-designers---course-package/',
              desc: 'Course with UX focus for those working with UX or usability desing. Including design for all publishing, mobile apps and documents.',
              url_label: 'Accessible UX course',
            },
            {
              text: 'A11y Accessible Design',
              url: 'https://www.a11y-collective.com/product/accessible-design-bundle/',
              desc: "A11y collective's two course package with focus on general foundations and accessible design.",
              url_label: 'Accessible design course',
            },
            {
              text: 'Funka Academy Creative Focus',
              url: 'https://www.funka.com/en/funka-academy/course-packages-for-roles-and-functions/accessibility-for-creatives---course-package/',
              desc: 'Course with reative focus for those working in creative roles. Includes universal design, UX, video, mobile, communication in plain language and more.',
              url_label: 'Creative accessibility course',
            },
          ]}
        />
      </div>

      <div className="vf-component__container" id="resources">
        <h2>Helpful Resources</h2>
        <div className="vf-component__paragraph">
          For additional resources, WCAG2 recommendations and Vattenfall's
          commitment to accessibility visit the links below.
        </div>

        <ExternalLinksExtended
          links={[
            {
              text: 'WCAG 2 Overview',
              url: 'https://www.w3.org/TR/WCAG21/',
              desc: "WCAG 2.1 Accessibility guidelines, created by the World Wide Web (W3) Consortium, which is what we're adhering to in our work.",
              url_label: 'WCAG 2.1 guidelines',
            },
            {
              text: 'World Wide Web Consortium',
              url: 'https://www.w3.org/',
              desc: "W3C's homepage with extensive information around everything that covers accessibility including latest updates.",
              url_label: "W3C's homepage",
            },
            {
              text: 'A11y fundamentals',
              url: 'https://www.a11yproject.com/',
              desc: "The A11y project's, a community driven organisation, covering fundamentals and principles behind accessible design.",
              url_label: 'A11y Project Website',
            },
            {
              text: 'European Accessibility Act',
              url: 'https://ec.europa.eu/social/main.jsp?catId=1202',
              desc: "A link to the European Commission's directive the European Accessibility Act, in full.",
              url_label: 'Accessibility Act',
            },
            {
              text: 'European Disability Forum',
              url: 'https://www.edf-feph.org',
              desc: 'Website of the independent non-governmental organisation, EDF,  representing people with disabilities within the EU.',
              url_label: 'EDF website',
            },
            {
              text: 'W.H.O. / Disability',
              url: 'https://www.who.int/europe/health-topics/disability#tab=tab_1',
              desc: "World Health Organisation's framework for achieving the highest attainable standard of health for persons with disabilities (including digital accessibility).",
              url_label: 'W.H.O. Disability framework',
            },
          ]}
        />
      </div>
    </AccessibilityDesignerStyle>
  )
}
