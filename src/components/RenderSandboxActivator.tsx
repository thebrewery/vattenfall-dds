import classNames from 'classnames'
import Link from 'next/link'

import { SandboxDataType } from '../utils/api'

type RenderSandboxActivatorProps = {
  sandboxObj: SandboxDataType
  activeSandbox: SandboxDataType
  heading: string
}

export const RenderSandboxActivator = ({
  sandboxObj,
  heading,
  activeSandbox,
}: RenderSandboxActivatorProps) => {
  const { componentName } = sandboxObj

  const { componentName: activeComponentName } = activeSandbox
  const thisUrl = window.location.pathname

  return (
    <Link href={`${thisUrl}?variant=${componentName}`} scroll={false}>
      {heading && (
        <a
          className={classNames('variant-link', {
            active: componentName === activeComponentName,
          })}
          key={sandboxObj.sandbox_id}
        >
          {heading}
        </a>
      )}
    </Link>
  )
}
