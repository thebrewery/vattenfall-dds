import React from 'react'

const Alert = () => {
	return (
		<svg
			xmlns='http://www.w3.org/2000/svg'
			width='43'
			height='40'
			viewBox='0 0 43 40'
		>
			<g fill='none' fillRule='evenodd' stroke='none' strokeWidth='1'>
				<g fill='#F93B18' fillRule='nonzero' transform='translate(-269 -1019)'>
					<g transform='translate(269 1019)'>
						<path d='M21.349.062L.109 39.892h42.48L21.35.061zm0 9.333L35.27 35.488H7.441L21.35 9.395z'></path>
						<path d='M19.163 17.922H23.551000000000002V26.713H19.163z'></path>
						<path d='M22.884 28.17a2.907 2.907 0 00-1.52-.372h-.201c-.729.031-1.396.249-1.923.853-.573.682-.697 1.504-.589 2.388.14 1.07.713 1.752 1.644 2.077.418.14.852.186 1.286.155 1.35-.077 2.388-.914 2.512-2.511.078-1.101-.372-2.093-1.21-2.59z'></path>
					</g>
				</g>
			</g>
		</svg>
	)
}

export default Alert
