import { ComponentProps } from 'react'

export const PlayButton = ({ ...props }: ComponentProps<'svg'>) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="80"
    height="80"
    fill="none"
    viewBox="0 0 80 80"
    className="play-button"
    {...props}
  >
    <circle cx="40" cy="40" r="40" fill="#fff" opacity="0.9"></circle>
    <path
      stroke="#000"
      strokeWidth="4"
      d="M33 25.816l20.486 14.178L33 54.182V25.816z"
    ></path>
  </svg>
)
