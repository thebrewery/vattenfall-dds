import React from 'react'

const Checkmark = () => {
	return (
		<svg
			xmlns='http://www.w3.org/2000/svg'
			width='40'
			height='40'
			viewBox='0 0 40 40'
		>
			<g fill='none' fillRule='evenodd' stroke='none' strokeWidth='1'>
				<g>
					<circle cx='20' cy='20' r='20' fill='#2071B5'></circle>
					<path
						fill='#FFF'
						fillRule='nonzero'
						stroke='#FFF'
						d='M16.2670446 28L8.66666667 20.4005196 10.269734 18.7984411 16.2670446 24.7950435 29.0643989 12 30.6666667 13.6020785z'
					></path>
				</g>
			</g>
		</svg>
	)
}

export default Checkmark
