import styled from 'styled-components'

type IconCards = {
  text: string
  icon: string
  title?: string
  bgColor: string
}

const Container = styled.div`
  position: relative;

  .vf-component {
    &__info {
      padding: 30px;
      display: flex;
      gap: 6;
      max-width: 900px;
      flex-direction: column;
      height: 100%;

      p {
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 22.585px;
      }
    }

    &__color {
      padding: 16px 16px 25px 25px;

      &--light-blue {
        background: ${({ theme }) => theme.colours.bg__light_blue};
      }
      &--light-grey {
        background: ${({ theme }) => theme.colours.bg__light_grey};
      }
    }

    &__info-icon::before {
      font-size: 3.25rem;
    }

    &__info-title {
      font-size: 17.746px;
      /* font-style: normal; */
      font-weight: 700;
      line-height: 22.585px;
    }
  }
`

export const IconCards = ({ text, icon, title, bgColor }: IconCards) => {
  return (
    <Container>
      <div
        className={`vf-component__info vf-component__color--${bgColor}`}
        data-tip
      >
        {/* Icon */}
        <span className={`vf-component__info-icon ${icon}`} />
        {/* Title */}
        {title && <strong className="vf-component__info-title">{title}</strong>}
        {/* Text */}
        {/* <div
          dangerouslySetInnerHTML={{ __html: text }}
          className="vf-component__paragraph"
        /> */}
        <p>{text}</p>
      </div>
    </Container>
  )
}
