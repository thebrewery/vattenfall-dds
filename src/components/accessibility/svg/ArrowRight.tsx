import React from 'react'

type ArrowRight = {
  className?: string
}

const ArrowRight = ({ className }: ArrowRight) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="58"
      height="89"
      viewBox="0 0 58 89"
      fill="none"
      className={className}
    >
      <path
        d="M48.3482 88.001C48.9484 88.4299 49.7826 88.291 50.2115 87.6908L57.2009 77.9105C57.6298 77.3103 57.4909 76.4761 56.8908 76.0472C56.2906 75.6183 55.4563 75.7571 55.0275 76.3573L48.8147 85.051L40.121 78.8382C39.5209 78.4093 38.6866 78.5482 38.2577 79.1483C37.8288 79.7485 37.9677 80.5827 38.5679 81.0116L48.3482 88.001ZM0.44827 2.52275C25.0106 23.9438 42.2219 53.5743 47.8073 87.1335L50.4424 86.695C44.7521 52.5056 27.2183 22.3247 2.20407 0.509475L0.44827 2.52275Z"
        fill="#2071B5"
      />
    </svg>
  )
}

export default ArrowRight
