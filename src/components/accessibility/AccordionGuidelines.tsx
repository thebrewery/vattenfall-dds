import '@vf-dds/vf-dds-vanilla/src/style/component-css/buttons.css'
import '@vf-dds/vf-dds-vanilla/src/style/component-css/dropdown.css'
import { PropsWithChildren, useEffect, useRef, useState } from 'react'
import rehypeRaw from 'rehype-raw'
import styled, { keyframes } from 'styled-components'

import ReactMarkdown from 'react-markdown'
import type {} from 'styled-components/cssprop'

const AccordionContainer = styled.div`
  width: 100%;

  /* box-shadow: 0 0 0 1px black; */

  h3 {
    margin-bottom: 0.65rem;

    font-size: 28px;
    font-weight: 700;
    line-height: 2.25rem;

    &:first-child {
      margin-top: 0;
    }
  }

  h4 {
    font-size: 1.375rem;
    font-weight: 700;
    line-height: 1.75rem;
  }

  p {
    line-height: 2.25rem;
    font-size: 1.25rem;
  }
  ul {
    padding: 0 1em;
  }

  .vf-link--with-arrow {
    margin-top: 1rem;
  }

  .vf-dropdown {
    &__images {
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      grid-gap: 22px;
      max-width: 900px;
      margin: 2rem 0;
    }

    &__image-block {
      & {
        font-size: 0.938rem;
        font-weight: 500;
        line-height: 2.188rem;
      }

      img {
        width: 100%;
        height: auto;
      }
    }

    &__box {
      padding: 9px 37px 9px 16px;
      min-height: 137px;

      p {
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 23px;
        margin-top: 6px;
      }

      &.do {
        background-color: ${({ theme }) => theme.colours.bg__light_blue};
      }

      &.dont {
        background-color: ${({ theme }) => theme.colours.bg__light_red};
      }
    }

    &__semantic {
      border: 1px solid #ccc;
    }
  }
`

interface AccordionProps {
  title?: string
  data: {
    name: string
    content: string | undefined
    media?: {
      source: string
      label: string
      type: string
      title: string
    }[]
  }[]
}

export default function AccordionGuidelines({ data, title }: AccordionProps) {
  const [selectedFormat, setSelectedFormat] = useState(
    data[0] as (typeof data)[number]
  )

  return (
    <AccordionContainer>
      <div style={{ marginBottom: '3em' }}>
        <div
          className="vf-dropdown-wrapper"
          style={{ width: '100%', margin: '0.5rem 0 1.5rem' }}
        >
          <select
            className="vf-dropdown__semantic"
            style={{ width: '100%' }}
            value={selectedFormat.name}
            title={title}
            aria-label={title}
            name="accessibility-guidelines"
            onChange={e => {
              const value = e.currentTarget.value
              setTimeout(() => {
                setSelectedFormat(data.find(format => format.name === value)!)
              }, 175)
            }}
          >
            {data.map(format => (
              <option
                disabled={!format.content}
                key={format.name}
                value={format.name}
                selected={selectedFormat === format}
              >
                {format.name}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div>
        <h3>{selectedFormat.name}</h3>
        <ReactMarkdown>{selectedFormat.content ?? ''}</ReactMarkdown>

        {selectedFormat.media && (
          <div className="vf-dropdown__images">
            {selectedFormat.media.map((image, index) => (
              <div className="vf-dropdown__image-block" key={index}>
                <img src={image.source} alt={image.label} />

                <div className={`vf-dropdown__box ${image.type}`}>
                  <h4>{image.title}</h4>
                  <p>{image.label}</p>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </AccordionContainer>
  )
}
