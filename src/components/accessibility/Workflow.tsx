import styled from 'styled-components'

import ArrowLeft from '../../components/accessibility/svg/ArrowLeft'
import ArrowRight from '../../components/accessibility/svg/ArrowRight'
import ArrowDown from '../../components/accessibility/svg/ArrowDown'
import { MouseEvent, useRef, useState } from 'react'

const Container = styled.div`
  //   workflow

  .vf-component__accessibility-workflow {
    flex-direction: row;
    display: flex;
    gap: 60px;
    margin-bottom: 70px;
    align-items: flex-start;
    scroll-margin-top: 130px;

    @media only screen and (max-width: 769px) {
      flex-direction: column;
    }
  }

  .vf-component__workflow {
    &-graphic {
      width: 50%;
      display: flex;
      flex-direction: column;
      gap: 30px;
      position: relative;
      @media only screen and (max-width: 769px) {
        width: 100%;
        max-width: 370px;
        margin: 0 auto;
      }

      position: sticky;
      top: 120px;

      &-title {
        font-size: 20px;
        font-weight: 700;
        line-height: 110%;
        max-width: 98px;
        position: absolute;
        text-align: center;
        top: 58%;
        left: 50%;
        transform: translate(-50%, -50%);
      }

      &--top {
        display: flex;
        justify-content: center;
        position: relative;

        .arrow-left {
          position: absolute;
          left: 40px;
          bottom: -20px;
        }

        .arrow-right {
          position: absolute;
          right: 45px;
          bottom: -20px;
        }
      }

      .arrow-down {
        position: absolute;
        bottom: -10px;
        left: 50%;
        transform: translateX(-50%);
      }

      &--bottom {
        display: flex;
        justify-content: space-between;
      }

      .vf-guideline__intro-example--round {
        width: 153px;
        height: 153px;
        gap: 4px;
        transition: background 0.1s ease-out;
        border: none;
        cursor: pointer;

        &:hover,
        &.active {
          background: #639ecc;
        }
      }

      .vf-guideline__intro-icon::before {
        font-size: 3rem;
      }

      .vf-guideline__intro-label {
      }
    }

    &-accordion {
      width: 50%;
      @media only screen and (max-width: 769px) {
        width: 100%;
      }

      & h4 {
        font-weight: 500;
        font-size: 20px;
        line-height: 28px;
      }
      strong {
        font-weight: 500;
        font-size: 16px;
        line-height: 28px;
      }
      & p {
        font-weight: 400;
        font-size: 16px;
        line-height: 28px;
      }
      & .subtitle {
        font-size: 14px;
        line-height: 28px;
        strong {
          font-size: 14px;
          font-weight: 500;
        }
      }
      & .workflow-section {
        margin-top: 20px;
        &:first-of-type {
          margin-top: 0;
        }
      }
      & ul {
        margin: 0;
      }
      & li {
        margin-bottom: 28px;
        &:last-of-type {
          margin: 0;
        }
      }
    }
  }
`

type Phase = 'kick-off-audit' | 'implementation' | 'testing-launch'

export default function Workflow() {
  const wrapperRef = useRef<HTMLDivElement>(null)
  const [activePhase, setActivePhase] = useState<Phase | null>(null)

  const handleToggle = (phase: Phase) => (e: MouseEvent) => {
    e.preventDefault()
    setActivePhase(prevPhase => {
      if (prevPhase === phase) {
        return null
      }

      if (window.innerWidth > 768) {
        wrapperRef.current?.scrollIntoView({
          behavior: 'instant' as ScrollBehavior,
          block: 'start',
        })
      }

      return phase
    })
  }

  return (
    <Container>
      <div ref={wrapperRef} className="vf-component__accessibility-workflow">
        <div className="vf-component__workflow-graphic">
          <div className="vf-component__workflow-graphic--top">
            <ArrowLeft className="arrow-left" />
            <button
              onClick={handleToggle('kick-off-audit')}
              className={
                `vf-guideline__intro-example--round vf-guideline_color--light_blue ` +
                (activePhase === 'kick-off-audit' ? ' active' : '')
              }
              data-tip
              data-for="1"
              aria-label="Kick off & audit"
            >
              {/* Icon */}
              <span className={`vf-guideline__intro-icon vf-icon-handshake`} />
              <div className="vf-guideline__intro-label">Kick off & Audit</div>
            </button>
            <ArrowRight className="arrow-right" />
          </div>
          <span className="vf-component__workflow-graphic-title">
            Ongoing work
          </span>
          <div className="vf-component__workflow-graphic--bottom">
            <div>
              <button
                onClick={handleToggle('testing-launch')}
                className={
                  `vf-guideline__intro-example--round vf-guideline_color--light_blue ` +
                  (activePhase === 'testing-launch' ? ' active' : '')
                }
                data-tip
                data-for="1"
                aria-label="Testing & Launch"
              >
                {/* Icon */}
                <span className={`vf-guideline__intro-icon vf-icon-checkbox`} />
                <div className="vf-guideline__intro-label">
                  Testing & Launch
                </div>
              </button>
            </div>

            <div>
              <button
                onClick={handleToggle('implementation')}
                className={
                  `vf-guideline__intro-example--round vf-guideline_color--light_blue ` +
                  (activePhase === 'implementation' ? ' active' : '')
                }
                data-tip
                data-for="1"
                aria-label="Implementation"
              >
                {/* Icon */}
                <span
                  className={`vf-guideline__intro-icon vf-icon-favourite`}
                />
                <div className="vf-guideline__intro-label">Implementation</div>
              </button>
            </div>
          </div>
          <ArrowDown className="arrow-down" />
        </div>

        <div className="vf-component__workflow-accordion">
          <h3>A three phase process</h3>

          <div className="vf-component__paragraph">
            This process involves a series of steps to build accessibility into
            the design work flow. Use the dropdown menu below to learn about
            each step, its requirements, the tools to use, and how to
            collaborate.
          </div>

          <div className="vf-component__accordion">
            <details
              open={activePhase === 'kick-off-audit'}
              onClick={handleToggle('kick-off-audit')}
            >
              <summary>1. Kick Off & Audit</summary>
              <div className="workflow-section">
                <h4>Project Kick Off</h4>
                <p>
                  Kick off project by appointing project leads from each area
                  (UX, Design, Dev, Editor), decide who is responsible for
                  leading the testing process (Testing lead) and enrol all roles
                  in necessary online trainings and knowledge sharing sessions,
                  as needed.
                </p>
                <p className="subtitle">
                  <strong>Applies to:</strong> All roles
                </p>
              </div>
              <div className="workflow-section">
                <h4>Scope & Define</h4>
                <p>
                  Scope the project together and agree the accessibility
                  standards you will test against, at Vattenfall we should
                  follow WCAG 2.1 AA.
                </p>
                <p className="subtitle">
                  <strong>Applies to:</strong> All roles
                </p>
              </div>
              <div className="workflow-section">
                <h4>Tools</h4>
                <p>
                  Gather the tools, resources and checklists needed to launch
                  the audit and testing process. Agree who does what.
                </p>
                <p className="subtitle">
                  <strong>Applies to:</strong> All roles
                </p>
              </div>
              <div className="workflow-section">
                <h4>Audit</h4>
                <p>
                  Perform audits using automated tools and manual testing,
                  documenting the results with issues identified to be resolved
                  to meet the right standards.
                </p>
                <p className="subtitle">
                  <strong>Applies to:</strong> Developer, Testing lead
                </p>
              </div>
              <div>
                <ul>
                  <li>
                    <strong>Automated testing</strong>
                    <p>
                      Automated testing uses plugins that quickly scan for
                      common accessibility issues on websites. These tools are
                      efficient for a preliminary analysis and can save time by
                      identifying obvious problems. See the specific role pages
                      for recommended tools.
                    </p>
                    <p className="subtitle">
                      <strong>Applies to:</strong> Testing lead
                    </p>
                  </li>

                  <li>
                    <strong>Manual Testing</strong>
                    <p>
                      Automated tools can miss certain issues that require human
                      judgement. Manual testing involves a hands-on approach to
                      identify these subtler problems and should be completed
                      using assistive technology to check perfomance. Unless you
                      have all the tools required internally this is best done
                      by an external provider.
                    </p>
                    <p className="subtitle">
                      <strong>Applies to:</strong> Testing lead
                    </p>
                  </li>
                </ul>
              </div>
            </details>

            <details
              open={activePhase === 'implementation'}
              onClick={handleToggle('implementation')}
            >
              <summary>2. Implementation</summary>
              <div className="workflow-section">
                <h4>Analyse</h4>
                First step in the implementation phase is to analyse findings
                and make a list of all relevant issues that have been
                identified. Prioritise issues according to urgency,
                implementation time and impact on user experience.
                <p className="subtitle">
                  <strong>Applies to:</strong> Testing lead
                </p>
              </div>
              <div className="workflow-section">
                <h4>Document</h4>
                Create a list of suggested implementation actions and a timeline
                before moving forward with fixing issues.
                <p className="subtitle">
                  <strong>Applies to:</strong> Testing lead
                </p>
              </div>
              <div className="workflow-section">
                <h4>Apply fixes</h4>
                Implement the necessary code changes, design adjustments, and
                content updates to resolve the identified accessibility issues.
                Start with A and AA as a priority.
                <p className="subtitle">
                  <strong>Applies to:</strong> All roles (depending on issues
                  identified)
                </p>
              </div>
            </details>

            <details
              open={activePhase === 'testing-launch'}
              onClick={handleToggle('testing-launch')}
            >
              <summary>3. Testing & Launch</summary>
              <div className="workflow-section">
                <h4>Final tests</h4>
                <p>
                  After applying fixes, conduct iterative rounds of testing
                  (both automated and manual) to verify that the issues have
                  been resolved and no new issues have appeared.
                </p>
                <p className="subtitle">
                  <strong>Applies to:</strong> Testing Lead and relevant team
                </p>
              </div>
              <div className="workflow-section">
                <h4>Create Accessibility Statement</h4>
                <p>
                  Create a page for your accessibility statement, update with
                  relevant details around level of compliance, how you are
                  working with accessibility and ways for users to contact you.
                  .
                </p>
                <p className="subtitle">
                  <strong>Applies to:</strong> Content Editor with input from
                  team
                </p>
              </div>
              <div className="workflow-section">
                <h4>Ongoing work</h4>
                <p>
                  Schedule ongoing audits as part of your regular maintenance
                  routine and ways of working. Document all accessibility
                  efforts and save as a reference for future projects. Keep your
                  team and relevant stakeholders informed about the
                  accessibility results, best practices and relevant industry
                  standards.
                </p>
                <p className="subtitle">
                  <strong>Applies to:</strong> All roles
                </p>
              </div>
            </details>
          </div>
        </div>
      </div>
    </Container>
  )
}
