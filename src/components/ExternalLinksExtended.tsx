import styled from 'styled-components'

type ExternalLinksExtendedProps = {
  links: {
    text: string
    desc?: string
    url: string
    url_label: string
  }[]
}

const Container = styled.div`
  .vf-component__external-links {
    display: flex;
    flex-wrap: wrap;
    grid-row-gap: 32px;
    grid-column-gap: 16px;
  }
  .vf-icon-open-new-window {
    font-size: 18px;
    font-weight: 500;
    line-height: normal;
    display: flex;
    gap: 12px;
    margin-top:12px;
  }
  .vf-icon-open-new-window span{
    color:black;
  }
  .vf-icon-open-new-window:hover span{
    color: #2071B5;
  }
  .vf-component__external-links-extended{
    display:flex;
    width: calc(48% - 8px);
    flex-direction:column;
  }
  .vf-component__external-links__title{
    font-weight:700;
    font-size:22px;
    line-height:28px;
  } 
  .vf-component__external-links__desc{
    font-weight:400;
    font-size:16px;
    line-height:28px;
 }
`

export const ExternalLinksExtended = ({ links }: ExternalLinksExtendedProps) => (
  <Container>
    <div className="vf-component__external-links">
      {links.map((item, index) => (
        <div className='vf-component__external-links-extended' key={index}>
            <div className='vf-component__external-links__title'>{item.text}</div>
            {item.desc && <div className='vf-component__external-links__desc'>{item.desc}</div>}
            <a
            className="vf-icon-open-new-window"
            href={item.url}
            target="_blank"
            rel="noreferrer"
            
            >
            <span>{item.url_label}</span>
            </a>
        </div>
      ))}
    </div>
  </Container>
)
