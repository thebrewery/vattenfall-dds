import React from 'react'

import styled from 'styled-components'

const MistWrapper = styled.div``

export const IndustrialEmissionsFaceMist = () => {
  return (
    <MistWrapper className="vf-component__container">
      {/* TODO: update to use vimeo */}
      <video
        style={{ width: '100%', marginBottom: '24px' }}
        src="/assets/mist/video.mp4"
        controls
      />

      <p className="vf-component__paragraph">
        The Industrial Emissions Face Mist campaign page was created to act as
        an informatory landing page about fossil free hydrogen with traffic from
        primarily social media channels of Cara’s (Delevigne, the main
        collaborator of this campaign) and a number of influencers that were
        engaged as part of this campaign. The main target audience was younger
        than the audiences for the regular brand campaigns and the Industrial
        Emissions Face Mist bottle was created to look like a high-end product
        which in turn influenced the design of the page to give it a look and
        feel of a premium brand. At the same time the page was telling the story
        of Vattenfall’s work to create fossil free hydrogen by scrolly-telling -
        visual and parallax effects digital storytelling created by revealing
        bite-sized content when scrolling the page, encouraging the user to
        explore more.
      </p>
      <p className="vf-component__paragraph">
        The design falls into the ’Temporary campaign usage’ category that is
        part of the outer layer of the Brand Expression Model. These assets are
        bespoke and should only be utilized in similar types of activations
        however if you’re interested in doing something similar don’t hesitate
        to contact us to discuss more.
      </p>
      <p className="vf-component__paragraph">
        The designs of the page can be accessed here:
      </p>
      <a
        href="https://www.figma.com/file/ZIIa0D941eQllM99kUm5nz/VF-Mist?type=design&node-id=0%3A3&mode=design&t=xPQpQvNde4LMB9gF-1"
        className="vf-link--with-arrow"
        target="_blank"
        rel="noreferrer"
      >
        Link to Figma
      </a>

      <a
        href="https://vattenfall.com/hydrogen"
        className="vf-link--with-arrow"
        target="_blank"
        rel="noreferrer"
      >
        Link to Site
      </a>
      <a
        href="https://brandtoolbox.vattenfall.com/Styleguide/coreelements/"
        className="vf-link--with-arrow"
        target="_blank"
        rel="noreferrer"
      >
        Brand Expression Model
      </a>
    </MistWrapper>
  )
}
