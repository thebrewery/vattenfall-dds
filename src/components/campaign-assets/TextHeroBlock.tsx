import styled from 'styled-components'

const TextHeroBlockWrapper = styled.div``

export const TextHeroBlock = () => {
  return (
    <TextHeroBlockWrapper className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for Text Hero Block. <br />
        Used by the Vattenfall EDIT{' '}
        <a
          href="https://group.vattenfall.com/investors/annual-and-sustainability-report-2021-the-edit"
          target="_blank"
          rel="noreferrer"
        >
          campaign
        </a>
        .
      </p>
    </TextHeroBlockWrapper>
  )
}
