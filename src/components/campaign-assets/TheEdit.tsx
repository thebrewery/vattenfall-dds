import styled from 'styled-components'

const TheEditWrapper = styled.div``

export const TheEdit = () => {
  return (
    <TheEditWrapper className="vf-component__container">
      <p className="vf-component__paragraph">
        The Edit campaign page was a temporary Newspaper-style design concept
        that was created to give the user a feeling of reading
        headline-newsworthy content about the environment. The design is part of
        assets that fall into the ’Temporary campaign usage’ category that is
        part of the outer layer of the Brand Expression Model. These assets are
        bespoke and should only be utilized in similar types of activations
        however if you’re interested in doing something similar don’t hesitate
        to contact us to discuss more.
      </p>
      <p className="vf-component__paragraph">
        Also take a look at the following Brand Toolbox Styleguide and articles
        to understand more about how temporary assets are used:
      </p>
      <a
        href="https://brandtoolbox.vattenfall.com/Styleguide/coreelements/#page/7B3820BF-E244-4BD8-96D8D5573FF2CD35"
        className="vf-link--with-arrow"
        target="_blank"
        rel="noreferrer"
      >
        Styleguide
      </a>

      <a
        href="https://brandtoolbox.vattenfall.com/news/article/C8C2BAC6-94E2-4ECE-9697B9B191734B7C/"
        className="vf-link--with-arrow"
        target="_blank"
        rel="noreferrer"
      >
        Article
      </a>
    </TheEditWrapper>
  )
}
