import styled from 'styled-components'

const AnimatedKeyNumberWrapper = styled.div``

export const AnimatedKeyNumber = () => {
  return (
    <AnimatedKeyNumberWrapper className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for Animated Key Number.
        <br />
        Used by the Vattenfall EDIT{' '}
        <a
          href="https://group.vattenfall.com/investors/annual-and-sustainability-report-2021-the-edit"
          target="_blank"
          rel="noreferrer"
        >
          campaign
        </a>
        .
      </p>
    </AnimatedKeyNumberWrapper>
  )
}
