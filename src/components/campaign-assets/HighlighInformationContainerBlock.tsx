import styled from 'styled-components'

const HICBWrapper = styled.div``

export const HighlightInformationContainerBlock = () => {
  return (
    <HICBWrapper className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for Highlight Information Container Block.
        <br />
        Used by the Vattenfall EDIT{' '}
        <a
          href="https://group.vattenfall.com/investors/annual-and-sustainability-report-2021-the-edit"
          target="_blank"
          rel="noreferrer"
        >
          campaign
        </a>
        .
      </p>
    </HICBWrapper>
  )
}
