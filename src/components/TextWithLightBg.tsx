import { ComponentProps } from 'react'
import styled from 'styled-components'

const StyledTextWithLightBg = styled.p`
  background-color: rgba(255, 255, 255, 0.6);
  border-radius: 0.75rem;
  bottom: 0.5rem;
  font-size: 0.75rem;
  right: 0.5rem;
  position: absolute;
  padding: 0.1875rem 0.5rem;
  z-index: 2;

  @media screen and (min-width: 1024px) {
    bottom: 0.75rem;
    border-radius: 1rem;
    font-size: 1rem;
    right: 0.75rem;
    padding: 0.5rem 0.625rem;
  }
`

export const TextWithLightBg = ({ children }: ComponentProps<'p'>) => (
  <StyledTextWithLightBg className="onboarding-card__video-length">
    {children}
  </StyledTextWithLightBg>
)
