import styled from 'styled-components'

type InfoBox = {
  text: string
  icon: string
  title?: string
  bgColor?: string
}

const Container = styled.div`
  position: relative;

  .vf-guideline {
    &__info {
      background-color: #fffee5;
      padding: 30px;
      display: flex;
      gap: 25px;
      max-width: 900px;
    }

    &__color {
      &--light-blue {
        background: ${({ theme }) => theme.colours.bg__light_blue};
      }
      &--light-green {
        background: ${({ theme }) => theme.colours.bg__light_green};
      }
    }

    &__info-icon::before {
      font-size: 4.25rem;
    }

    &__info-title {
      font-size: 22px;
      line-height: 28px;
      margin-bottom: 7px;
      display: block;
    }
  }
`

export const InfoBox = ({
  text,
  icon,
  title,
  bgColor = 'light-yellow',
}: InfoBox) => {
  return (
    <Container>
      <div
        className={`vf-guideline__info vf-guideline__color--${bgColor}`}
        data-tip
      >
        {/* Icon */}
        <span className={`vf-guideline__info-icon ${icon}`} />
        <div>
          {/* Title */}
          {title && (
            <strong className="vf-guideline__info-title">{title}</strong>
          )}
          {/* Text */}
          <div
            dangerouslySetInnerHTML={{ __html: text }}
            className="vf-component__paragraph"
          />
        </div>
      </div>
    </Container>
  )
}
