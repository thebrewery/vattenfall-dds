import React from 'react'
import styled from 'styled-components'

const MotionContainer = styled.div`
  img {
    width: 100%;
  }
`

export const Motion = () => (
  <MotionContainer>
    <p className="vf-component__paragraph">
      When designing in accordance with the Vattenfall Digital Design System,
      make sure to think through motion effects and transitions. Each effect
      should fill a purpose. Don’t combine several components with heavy
      animations – strive to add subtle motion.
    </p>
    <p className="vf-component__paragraph">
      ** For the sake of consistency, we use only one default animation for now
      **
    </p>
    <p className="vf-component__paragraph">
      Motion focuses attention on what’s important without creating unnecessary
      distraction.
    </p>

    <p className="vf-component__paragraph">
      Object effects: Scale, Move, Elevation, Colour-change
    </p>
    <img src="/assets/img/Motion.png" alt="Animation curve, cubic bezier." />
    <p className="vf-component__paragraph">
      To read more about the “ease-in-out” motion ID philosophy, please see the
      description and the examples in the Video & Motion guidelines chapter:{' '}
      <a
        href="https://brandtoolbox.vattenfall.com/Styleguide/brandtwo/#page/70DBC29E-9802-4DDD-9937424FDCB53C25"
        target="_blank"
        rel="noreferrer"
      >
        https://brandtoolbox.vattenfall.com/Styleguide/brandtwo/#page/70DBC29E-9802-4DDD-9937424FDCB53C25
      </a>
    </p>
  </MotionContainer>
)
