import styled from 'styled-components'
import { FAQEntry } from './FAQEntry'

const FAQContainer = styled.div`
  display: flex;
  flex-direction: column;

  .faq {
    &-entry {
      display: flex;
      flex-direction: column;
      justify-content: center;
      max-width: 1020px;
      min-height: 80px;
      position: relative;

      &:not(:first-of-type) {
        &::before {
          background-color: #ccc;
          content: '';
          height: 2px;
          position: absolute;
          top: 0;
          width: 100%;
        }
      }

      &:last-of-type {
        &::after {
          background-color: #ccc;
          bottom: 0;
          content: '';
          height: 2px;
          position: absolute;
          width: 100%;
        }
      }
    }

    &-heading {
      cursor: pointer;
      display: flex;
      justify-content: space-between;
      gap: 1.5rem;
      font-size: 1.375rem;
      font-weight: normal;
      padding-right: 0.75rem;

      @media screen and (min-width: 768px) {
        font-size: 1.75rem;
      }
    }

    &-answer {
      font-size: 1rem;
      line-height: 1.33;

      @media screen and (min-width: 768px) {
        font-size: 1.25rem;
      }
    }
  }

  .vf-icon-arrow-down {
    transition: transform 0.15s ease-in;
  }

  .active .vf-icon-arrow-down {
    transform: rotate(180deg);
  }
`

export const FAQ = ({
  entries,
}: {
  entries: { question: string; answer: string }[]
}) => {
  return (
    <FAQContainer>
      {entries.map(entry => (
        <FAQEntry entry={entry} key={entry.question} />
      ))}
    </FAQContainer>
  )
}
