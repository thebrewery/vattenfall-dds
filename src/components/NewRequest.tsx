import { FormEvent, useState } from 'react'
import styled from 'styled-components'

import { ReactUpload } from '../react/components/modules/upload'
import { API, ContactPageProps } from '../utils/api'
import { NewRequestModal } from './NewRequestModal'

const Container = styled.div`
  line-height: 2rem;
  max-width: 800px;
  padding: 45px 0;

  .vf-input {
    @media screen and (min-width: 768px) {
      width: 500px;
    }

    &__dropdown {
      @media screen and (min-width: 768px) {
        width: 500px;
      }
    }
  }

  a {
    text-decoration: none;
  }

  .button-wrapper {
    display: flex;
    @media screen and (min-width: 768px) {
      width: 500px;
    }
  }

  .vf-upload {
    display: flex;
    justify-content: center;
    border: #999999 dashed thin;
    margin-bottom: 24px;
    @media screen and (min-width: 768px) {
      width: 500px;
    }
  }

  .vf-upload__container {
    padding: 0;
    padding-top: 20px;
  }
`

declare const gtag: (type: string, action: string, opts: {}) => {}

const NewRequest = ({ data }: { data: ContactPageProps }) => {
  const [loading, setLoading] = useState<boolean>(false)
  const [statusCode, setStatusCode] = useState<number>(0)
  const [showModal, setShowModal] = useState<boolean>(false)
  const [subject, setSubject] = useState<string>('')
  const [name, setName] = useState<string>('')
  const [email, setEmail] = useState<string>('')
  const [comment, setComment] = useState<string>('')
  const [mode, setMode] = useState<string>('Designer')
  const [framework, setFramework] = useState<string>('None selected')
  const [files, setFiles] = useState<Blob[]>([])

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault()
    setLoading(true)

    'gtag' in window &&
      gtag('event', 'contact_request', {
        event_label: mode,
        event_category: framework,
      })

    const data = {
      request: {
        requester: { name, email },
        subject,
        comment: { body: comment },
        custom_fields: [
          { id: 360016456719, value: framework }, // framework selection
          { id: 360016469620, value: mode }, // developer/designer
          { id: 360016469640, value: 'New request' }, //new request/report issue
          // { id: 360016469660, value: componentUri }, // mostly unused, as far as I can see?
          // { id: 360016456899, value: guidelineUri }, // mostly unused, as far as I can see?
          // { id: 360016469680, value: fullUrl } // where the request is sent from
        ],
      },
    }

    let status = await API.sendUploadRequest(data, files)

    setShowModal(true)
    setStatusCode(status)
    setLoading(false)
  }

  return (
    <Container>
      <h1 className="vf-component__large-heading">{data.formHeading}</h1>
      <p className="vf-component__paragraph">{data.formIntroduction}</p>
      <div>
        <form onSubmit={handleSubmit}>
          <div className="vf-input__container">
            <input
              className="vf-input"
              id="new-request__subject"
              value={subject}
              onChange={e => setSubject(e.target.value)}
              type="text"
              title='Subject'
              role='none'
              placeholder=''
              aria-label='Subject'
            />
            <label
              className={`vf-input__label ${subject ? 'has-value' : ''}`}
              htmlFor="new-request__subject"
            >
              {data.subjectField}
            </label>
          </div>
          <div className="vf-input__container">
            <input
              className="vf-input"
              id="new-request__name"
              value={name}
              onChange={e => setName(e.target.value)}
              required
              pattern="[a-zA-Z\u00C0-\u024F\u1E00-\u1EFF\s]+" 
              title='Name'
              role='none'
              placeholder=''
              aria-label='Name'
              
            />
            <label
              className={`vf-input__label ${name ? 'has-value' : ''}`}
              htmlFor="new-request__name"
            >
              {data.nameField}
            </label>
          </div>
          <div className="vf-input__container">
            <input
              className="vf-input"
              name="new-request__email"
              value={email}
              type="email"
              required
              onChange={e => setEmail(e.target.value)}
              title='Email'
              role='none'
              placeholder=''
              aria-label='Email'
            />
            <label
              className={`vf-input__label ${email ? 'has-value' : ''}`}
              htmlFor="new-request__email"
            >
              {data.emailField}
            </label>
          </div>
          <div className="vf-input__container">
            <textarea
              id="new-request__comment"
              className="vf-input"
              onChange={e => setComment(e.target.value)}
              rows={4}
              value={comment}
              title='Comment'
              role='none'
              placeholder=''
              aria-label='Comment'
            />

            <label
              className={`vf-input__label ${comment ? 'has-value' : ''}`}
              htmlFor="new-request__comment"
            >
              {data.commentField}
            </label>
          </div>
          <div className="">
            <label className="" htmlFor="new-request__mode">
              <select
                className="vf-input__dropdown"
                onChange={el => setMode(el.target.value)}
                title='Assset Type'
                role='none'
                aria-label='Assset Type'
              >
                <option
                  defaultValue={data.typeDropdown}
                  hidden
                  value={data.typeDropdown}
                >
                  {data.typeDropdown}
                </option>
                <option value="developer">Developer</option>
                <option value="designer">Designer</option>
              </select>
            </label>
          </div>
          {/* <div className="">
            <label className="" htmlFor="new-request__framework">
              <select
                className="vf-input__dropdown"
                  title='Mode'
                  role='none'
                  placeholder=''
                  aria-label='Mode'
                onChange={el => setFramework(el.target.value)}
              >
                <option defaultValue={data.frameworkDropdown} hidden value="">
                  {data.frameworkDropdown}
                </option>
                <option value="javascript">JavaScript</option>
                <option value="react">React</option>
                <option value="angular">Angular</option>
                <option value="all">All</option>
              </select>
            </label>
          </div> */}
          <ReactUpload
            pageData={data}
            sendFiles={(files: Blob[]) => setFiles(files)}
          />
          <div className="button-wrapper">
            <button
              className="vf-button vf-button--lg vf-button__primary"
              type="submit"
              disabled={
                // !framework ||
                !mode || !email || !comment || !name || !subject || loading
              }
            >
              {data.submitButton}
            </button>
          </div>
        </form>
      </div>
      {showModal && (
        <NewRequestModal statusCode={statusCode} setShowModal={setShowModal} />
      )}
    </Container>
  )
}

export default NewRequest
