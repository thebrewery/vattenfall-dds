import classNames from 'classnames'
import { useRouter } from 'next/router'
import {
  PropsWithChildren,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import remarkHtml from 'remark-html'
import remarkParse from 'remark-parse/lib'
import styled, { css } from 'styled-components'
import { unified } from 'unified'

import { Literal, Parent } from 'unist'
import PageProvider from '../context/state'
import { SandboxDataType } from '../utils/api'
import { CodeSandbox } from './CodeSandbox'
import { RenderSandboxActivator } from './RenderSandboxActivator'

const implementation = (path: string, type: string) => {
  const htmlImplementation =
    type === 'css'
      ? `<link rel='stylesheet' href='node_modules/@vf-dds/vf-dds-vanilla/${path}' />`
      : type === 'js'
      ? `<script src='node_modules/@vf-dds/vf-dds-vanilla/${path}>`
      : null

  return (
    <div>
      <h3 className="vf-component__small-heading">
        {type === 'css' ? 'CSS' : type === 'js' ? 'JavaScript' : null}
      </h3>
      <h3 className="vf-component__tiny-heading">Import via HTML</h3>
      <pre style={{ marginTop: 0 }} className="vf-component__paragraph">
        {htmlImplementation}
      </pre>

      <h3 className="vf-component__tiny-heading">Import via JavaScript</h3>
      <pre
        style={{ marginTop: 0 }}
        className="vf-component__paragraph"
      >{`import '@vf-dds/vf-dds-vanilla/${path};`}</pre>
    </div>
  )
}

const ComponentPageLayoutStyle = styled.div<{ fromDevelopers: boolean }>`
  ${({ fromDevelopers }) =>
    !fromDevelopers &&
    css`
      padding-bottom: 100px;

      .sandbox-container {
        margin-bottom: 2rem;
        padding-bottom: 2rem;
      }
    `}

  .vf-main-content {
    display: grid;
    grid-template-columns: 1fr auto;
    column-gap: 2rem;
    margin-bottom: 2rem;
  }

  .component-name__link {
    cursor: pointer;
    width: fit-content;
  }

  .vf-link--with-arrow {
    cursor: pointer;
    display: block;
  }

  .active {
    color: #2071b5;
    display: block;
  }

  .vf-guideline__large-heading {
    &.new-component {
      display: flex;
      align-items: center;
      justify-content: space-between;
      position: relative;
      padding-bottom: 2rem;
    }
  }

  .vf-new-component-indicator {
    align-items: center;
    background-color: ${({ theme }) => theme.colours.primary__solar_yellow};
    border-radius: 50%;
    display: flex;
    font-size: 1.75rem;
    justify-content: center;
    height: 7rem;
    margin-right: 3rem;
    user-select: none;
    width: 7rem;
    flex-shrink: 0;
  }

  .vf-extra-content {
    margin: 0;
    margin-bottom: 1.5rem;

    &__heading {
      color: ${({ theme }) => theme.colours.primary__coal_black};
      font-family: ${({ theme }) => theme.largeHeadingFontFamily};
      font-size: ${({ theme }) => theme.mediumHeadingFontSize};
      font-weight: 600;
      padding-bottom: 20px;
      width: 100%;
    }

    &__paragraph {
      font-size: ${({ theme }) => theme.fontSize};
      font-weight: normal;
      letter-spacing: -0.05px;
      line-height: ${({ theme }) => theme.mediumLineHeight};
      font-family: 'Vattenfall Hall NORDx', 'Vattenfall Hall NORDx', sans-serif;
      -webkit-font-smoothing: antialiased;
    }
  }

  .variant-navigation {
    display: flex;
    flex-direction: column;
    height: fit-content;
    text-align: right;
    position: relative;

    &__wrapper {
      grid-column: 2;
    }

    .vf-component__heading {
      margin-right: 1rem;
    }

    &__anchor {
      position: absolute;
      top: 0;
      right: 0;
      height: 100%;

      &::before {
        background-color: #f0f0f0;
        content: '';
        display: block;
        height: calc(100% - 18px);
        margin: 0 auto;
        position: relative;
        top: 6px;
        width: 2px;
      }
    }

    &__indicator {
      background-color: ${({ theme }) => theme.colours.primary__ocean_blue};
      display: inline-block;
      left: 0;
      height: 1.5rem;
      transition: top 0.3s ease-in-out;
      position: absolute;
      width: 2px;
    }
  }

  .variant-link {
    padding: 0.5rem 1rem;
    position: relative;
  }
`

type ComponentPageLayoutProps = {
  componentId: string
  title: string
  isNewComponent?: boolean
  gitComponentSrcUrl?: string
  fromDevelopers?: boolean
}

export const ComponentPageLayout = ({
  componentId,
  title,
  isNewComponent,
  children,
  gitComponentSrcUrl,
  fromDevelopers = false,
}: ComponentPageLayoutProps & PropsWithChildren) => {
  const [ballOffset, setBallOffset] = useState(0)

  const { sandboxIdMap } = useContext(PageProvider)
  const sandboxData: SandboxDataType[] = sandboxIdMap[componentId]
  const { query } = useRouter()

  const activeRef = useRef<HTMLDivElement>(null)

  const modifiedSandboxData = useMemo(() => {
    if (!sandboxData) return []

    return sandboxData.map(innerObj => {
      const { mdFile, componentName } = innerObj

      // Here we parse the markdown file thats comes from our API, and use that as heading and as text.
      const root = unified()
        .use(remarkParse as any)
        .parse(mdFile) as Parent

      const mdHeading = root.children.find(
        branch => branch.type === 'heading'
      ) as Parent

      if (!mdHeading) {
        return {
          ...innerObj,
          description: [componentName, ''] as string[],
          componentName,
        }
      }

      root.children = root.children.filter(child => child !== mdHeading)

      const parsedHeading = mdHeading.children.map(
        child => (child as Literal).value
      )

      const rest = unified()
        .use(remarkHtml)
        .stringify(root as any) as unknown as string

      return { ...innerObj, description: [parsedHeading, rest] as string[] }
    })
  }, [sandboxData])

  const activeSandbox = query.variant
    ? modifiedSandboxData.find(
        sandboxData => query.variant === sandboxData.componentName
      )
    : modifiedSandboxData?.[0]

  useEffect(() => {
    if (!activeSandbox || !activeRef.current) return

    if (sandboxData?.length > 1) {
      const activeLink = activeRef.current.querySelector(
        '.active'
      ) as HTMLAnchorElement
      const relevantOffset = (36 - 24) / 2

      if (!activeLink) return
      setBallOffset(activeLink.offsetTop + relevantOffset)
    }
  }, [activeSandbox, sandboxData?.length])

  const { description } = activeSandbox ?? {}

  const [activeHeading, activeParagraph] = description ?? ['', '']

  return (
    <ComponentPageLayoutStyle fromDevelopers={fromDevelopers}>
      <h1
        className={classNames('vf-guideline__large-heading', {
          'new-component': isNewComponent,
        })}
      >
        {title}
        {isNewComponent && (
          <span className="vf-new-component-indicator">NEW</span>
        )}
      </h1>
      <div className="vf-main-content">
        {children}

        {activeSandbox && sandboxData?.length > 1 && (
          <>
            <div className="variant-navigation__wrapper">
              <h3 className="vf-component__small-heading">Other variants</h3>
              <div className="variant-navigation" ref={activeRef}>
                <div className="variant-navigation__anchor">
                  <span
                    className="variant-navigation__indicator"
                    style={{ top: ballOffset }}
                  />
                </div>
                {sandboxData.map((innerSandboxObj, index) => {
                  const { description } = modifiedSandboxData[index]
                  const [heading] = description

                  return (
                    <RenderSandboxActivator
                      key={innerSandboxObj.sandbox_id}
                      sandboxObj={innerSandboxObj}
                      activeSandbox={activeSandbox}
                      heading={heading}
                    />
                  )
                })}
              </div>
            </div>

            {activeHeading && (
              <div className="vf-extra-content">
                <h2 key={activeHeading} className="vf-extra-content__heading">
                  {activeHeading}
                </h2>
                <div dangerouslySetInnerHTML={{ __html: activeParagraph }} />
              </div>
            )}
          </>
        )}
      </div>

      {sandboxData && activeSandbox && (
        <div className="sandbox-container">
          <CodeSandbox
            key={activeSandbox.sandbox_id}
            fromDevelopers={fromDevelopers}
            componentId={componentId}
            componentGitUrl={gitComponentSrcUrl ?? ''}
            componentName={activeSandbox.componentName}
            sandboxId={activeSandbox.sandbox_id}
            showHeading={sandboxData.length === 1}
          />
        </div>
      )}
      {sandboxData?.map(
        sandboxObj =>
          sandboxObj.assets && (
            <>
              <h3 className="vf-component__heading">Additional dependencies</h3>
              {sandboxObj.assets.map(asset =>
                implementation(asset.path, asset.type)
              )}
            </>
          )
      )}
    </ComponentPageLayoutStyle>
  )
}
