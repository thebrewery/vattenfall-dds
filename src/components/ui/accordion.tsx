import styled from 'styled-components'

const StyledDetails = styled.details`
  border-bottom: 1px solid #e6e6e6;
  padding: 20px 0;
  line-height: 1.5rem;

  summary {
    list-style: none;
    outline: none;
    font-size: 20px;
    line-height: 28px;
    font-weight: 500;
    position: relative;
    display: flex;
    justify-content: space-between;
    cursor: pointer;

    &::after {
      content: '';
      width: 20px;
      background-image: url("data:image/svg+xml,%3Csvg width='20' height='10' viewBox='0 0 20 10' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M9.99194 9.04225L0.400391 1.57746L1.62574 0L9.99194 6.51408L18.3652 0L19.5905 1.57746L9.99194 9.04225Z' fill='%232071B5'/%3E%3C/svg%3E");
      background-repeat: no-repeat;
      background-position: center;
    }

    &::marker,
    &::-webkit-details-marker {
      display: none;
    }
  }

  & > * {
    margin: 0;

    &:not(summary) {
      padding-top: 12px;
    }
  }

  &[open] {
    summary {
      margin-bottom: 0;
      color: #2071b5;

      &::after {
        transform: rotate(180deg);
      }
    }
  }
`

type AccordionProps = {
  headline: string
  className?: string
  children: React.ReactNode
}

export default function Accordion({
  headline,
  className,
  children,
}: AccordionProps) {
  return (
    <StyledDetails className={className}>
      <summary>{headline}</summary>
      <div>{children}</div>
    </StyledDetails>
  )
}
