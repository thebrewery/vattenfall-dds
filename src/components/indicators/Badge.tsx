import React, { useContext, useState } from 'react'
import styled from 'styled-components'

export const Badge = () => {
  return (
    <p className="vf-component__paragraph">
      Documentation and examples for badges, our small count and labeling
      component.
    </p>
  )
}
