import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Preloader = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        To use a preloader, simply create a div element with class{' '}
        <code>vf-preloader</code>. To increase percentage use{' '}
        <code>data-vf-preloader-percentage</code> on the{' '}
        <code>vf-preloader</code> element
      </p>
    </Container>
  )
}
