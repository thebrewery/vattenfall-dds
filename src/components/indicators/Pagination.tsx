import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Pagination = () => {
  return (
    <Container className="vf-component__container">
      <h3 className="vf-component__small-heading">Overview</h3>
      <p className="vf-component__paragraph">
        We use a large block of connected links for our pagination, making links
        hard to miss and easily scalable, all while providing large hit areas.
        Pagination is built with list HTML elements so screen readers can
        announce the number of available links. Use a wrapping{' '}
        <code>{'<nav>'}</code> element to identify it as a navigation section to
        screen readers and other assistive technologies. In addition, as pages
        likely have more than one such navigation section, it’s advisable to
        provide a descriptive aria-label for the <code> {'<nav>'}</code> to
        reflect its purpose. For example, if the pagination component is used to
        navigate between a set of search results, an appropriate label could be{' '}
        <code>aria-label="Search results pages"</code>.
      </p>
    </Container>
  )
}
