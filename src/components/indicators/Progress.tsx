import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Progress = () => {
  return (
    <Container className="vf-component__container">
      <h3 className="vf-component__small-heading">How it works</h3>
      <p className="vf-component__paragraph">
        Progress components are built with two HTML elements, some CSS to ble
        the width, and a few attributes. We don't use the{' '}
        <a
          href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/progress"
          target="_blank"
          rel="noreferrer"
        >
          HTML5 <code>{'<progress>'}</code> element
        </a>
        , ensuring you can stack progress bars, animate them, and place text
        labels over them.
      </p>
      <div className="vf-component__paragraph">
        <li>
          We use the .vf-progress as a wrapper to indicate the max value of the
          progress bar.{' '}
        </li>
        <li>
          We use the inner .vf-progress-bar to indicate the progress so far.
        </li>
        <li>
          The .vf-progress-bar requires an inline style, utility class, or
          custom CSS to set their width.
        </li>{' '}
        <li>
          The .vf-progress-bar also requires some role and aria attributes to
          make it accessible.{' '}
        </li>
        Put that all together, and you have the following examples.
      </div>
    </Container>
  )
}
