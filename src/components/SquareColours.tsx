import styled from 'styled-components'

const SquareColoursContainer = styled.div`
  .colour-preview {
    height: 180px;
    width: 100%;
    margin-bottom: 0.5rem;
  }

  .weak-paragraph {
    font-weight: 400;
    line-height: ${({ theme }) => theme.mediumLineHeight};

    span {
      padding-left: 24px;
    }
  }
`

type SquareColoursProps = {
  title: string
  hex: string
  rgb: string
}

export const SquareColours = ({ title, hex, rgb }: SquareColoursProps) => {
  return (
    <SquareColoursContainer>
      <div style={{ background: hex }} className="colour-preview" />
      <p>
        <strong>{title}</strong>
      </p>
      <p className="weak-paragraph">
        HEX <span>{hex}</span>
      </p>
      <p className="weak-paragraph">
        RGB <span>{rgb}</span>
      </p>
    </SquareColoursContainer>
  )
}
