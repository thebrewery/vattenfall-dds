import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
	.vf-component__git-container {
		width: fit-content;
	}
`

export const NumberedList = () => {
	return (
		<Container className='vf-component__container'>
			<p className='vf-component__paragraph'>
				Numbered List for displaying data.
			</p>
		</Container>
	)
}
