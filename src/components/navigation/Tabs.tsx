import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Tabs = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        To select a tab by default, simply add the
        <code>vf-tab-bar-item--active</code> class to the corresponding
        <code>li</code> element. It is advisable to set up aria attributes as
        below. These will be updated as the selected tab changes. It is
        important to add the <code>vf-tab-bar-container</code> to the container
        element.
      </p>
      <p className="vf-component__paragraph">
        To disable a tab, simply add the <code>vf-tab-bar-item--disabled</code>
        class to the corresponding <code>li</code> element, and add
        <code>aria-disabled="true"</code>.
      </p>
      <p className="vf-component__paragraph">
        Icons are added to the <code>::before</code> pseudo-element on the
        <code>vf-tab-bar-link</code>
        element. To add a placeholder icon, simply add
        <code>vf-icon-placeholder</code> to the <code>vf-tab-bar-link</code>
        element. See Icons for more details.
      </p>
    </Container>
  )
}
