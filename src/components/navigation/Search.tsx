import React from 'react'

export const Search = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        If you import the correct js-file a click event will be bound on{' '}
        <code>vf-header__search-box</code> which will toggle{' '}
        <code>active-search-box</code> on the search icon-element and toggles
        the class <code>active</code> on the search box-element which is hidden
        by default.
      </p>
    </div>
  )
}
