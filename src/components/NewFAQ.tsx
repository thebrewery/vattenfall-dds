import classNames from 'classnames'
import emailjs from 'emailjs-com'
import { FormEvent, useRef, useState } from 'react'
import styled from 'styled-components'

const FAQContainer = styled.div`
  max-width: 1780px;
  margin: 0 auto;
  padding: 120px 0;
  .faq {
    max-width: 1780px;
    margin: 0 auto;
    padding: 120px 0;

    &__wrapper {
      display: flex;
      justify-content: space-between;
      padding-left: 7.75rem;
      /* height: 100%; */
      align-items: center;
    }

    &__content {
      width: 50%;
      max-width: 745px;
    }

    &__headline {
      font-size: 2.25rem;
      line-height: 2.75rem;

      @media screen and (min-width: 768px) {
        font-size: ${({ theme }) => theme.mediumHeadingFontSize};
        line-height: ${({ theme }) => theme.largeLineHeight};
      }
    }

    &__intro {
      max-width: 556px;
      margin-top: 20px;
      margin-bottom: 12px;

      font-size: ${({ theme }) => theme.fontSize};
      line-height: ${({ theme }) => theme.smallLineHeight};
    }

    &__entry-headline {
      font-size: ${({ theme }) => theme.smallHeadingFontSize};
      line-height: ${({ theme }) => theme.largeLineHeight};
    }
  }

  //   Accordion
  details {
    border-bottom: 1px solid #e6e6e6;
    padding: 20px 0;
    line-height: 1.5rem;

    summary {
      list-style: none;
      outline: none;
      font-size: 20px;
      line-height: 28px;
      font-weight: 500;
      position: relative;
      display: flex;
      justify-content: space-between;
      cursor: pointer;

      &::after {
        content: '';
        width: 20px;
        background-image: url("data:image/svg+xml,%3Csvg width='20' height='10' viewBox='0 0 20 10' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M9.99194 9.04225L0.400391 1.57746L1.62574 0L9.99194 6.51408L18.3652 0L19.5905 1.57746L9.99194 9.04225Z' fill='%232071B5'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: center;
      }

      &::marker,
      &::-webkit-details-marker {
        display: none;
      }
    }

    & > * {
      margin: 0;

      &:not(summary) {
        padding-top: 12px;
      }
    }

    &[open] {
      summary {
        margin-bottom: 0;
        color: #2071b5;

        &::after {
          transform: rotate(180deg);
        }
      }
    }
  }
`

type FAQProps = {}

export const FAQ = ({}: FAQProps) => {
  return (
    <FAQContainer>
      <div className="faq__wrapper">
        <div className="faq__content">
          <h2 className="faq__headline">FAQ</h2>
          <p className="faq__intro">
            Is the DDS not quite what you’re looking for? Do you need assets
            like brand guidelines, logos, artwork, campaign material, images,
            icons, fonts, illustrations or music files? You’ll find what you’re
            after in the Brand toolbox.
          </p>
          <div className="faq__entries">
            <h3 className="faq__entry-headline">
              Most commonly asked questions
            </h3>
            <details>
              <summary>What is the Vattenfall Digital Design System?</summary>
              <p>
                The Vattenfall Digital Design System is a comprehensive toolkit
                that provides guidelines, components, and resources for creating
                consistent and user-friendly digital experiences across
                Vattenfall's platforms. It serves as a single source of truth
                for designers and developers, ensuring brand consistency,
                accessibility, and efficiency in the development process. The
                system includes reusable UI components, design patterns, and
                best practices to streamline the creation of cohesive and
                high-quality digital products.
              </p>
            </details>
            <details>
              <summary>Who is the Digital Design System for?</summary>
              <p>
                The Digital Design System is designed for Vattenfall's internal
                teams, including designers, developers, and product managers. It
                aims to empower internal teams to create consistent,
                user-friendly, and efficient digital experiences across
                Vattenfall's platforms.
              </p>
            </details>

            <details>
              <summary>How do I get started?</summary>
              <p>
                To get started with the Digital Design System, you can explore
                the components and resources available on the website. For more
                detailed information, you can refer to the Design System
                Handbook, which provides in-depth guidance on using the system.
              </p>
            </details>
          </div>
        </div>
        <div className="faq__image"></div>
      </div>
    </FAQContainer>
  )
}
