import { ComponentProps } from 'react'
import styled from 'styled-components'

type Props = {
  withVerticalMargin?: boolean
} & ComponentProps<'div'>

const ContentWrapper = styled.div<Props>`
  box-sizing: content-box;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 0 1.5rem;
  max-width: 1530px;
  margin: ${props => (props.withVerticalMargin ? '3em' : 0)} auto;

  &.tcc__contentwrapper {
    transform: translateY(-3.7rem);
    margin-top: 10rem;
    margin-bottom: 0rem;

    @media screen and (min-width: 1024px) {
      transform: translateY(-4.5rem);
    }
  }

  @media screen and (min-width: 1024px) {
    padding: 0 3.75rem;
    margin: ${props => (props.withVerticalMargin ? '5em' : 0)} auto;
  }
`

export default ContentWrapper
