import classNames from 'classnames'
import { FormEvent, useContext, useState } from 'react'
import styled from 'styled-components'

import PageProvider from '../context/state'
import { ReactUpload } from '../react/components/modules/upload'
import { API } from '../utils/api'
import { NewRequestModal } from './NewRequestModal'

const RequestModalContainer = styled.div`
  background: rgba(0, 0, 0, 0.4);
  left: 0;
  height: 100vh;
  pointer-events: auto;
  position: fixed;
  top: 0;
  width: 100vw;
  z-index: 99;

  .upload-wrapper {
    pointer-events: auto;
    position: fixed;
    top: 50%;
    left: 50%;
    border: thin solid #999999;
    transform: translate(-50%, -50%);
    z-index: 1;
    padding: 20px;
    background-color: white;
  }

  h3 {
    font-family: 'Vattenfall Hall Display NORDx', Helvetica, sans-serif;
    font-weight: 600;
    letter-spacing: 0;
    font-size: ${({ theme }) => theme.headingFontSize};
    line-height: ${({ theme }) => theme.largeLineHeight};
    color: #222222;
    text-align: center;
    margin-bottom: 20px;
  }

  form {
    padding-left: 24px;
    padding-right: 24px;
  }

  .request-modal {
    &__input-wrapper {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-gap: 24px;
    }
  }

  .vf-upload {
    display: flex;
    justify-content: center;
    border: #999999 dashed thin;
    margin-bottom: 24px;
  }

  .vf-upload__container {
    padding: 0;
    padding-top: 20px;
  }

  .button-wrapper {
    display: flex;
    justify-content: center;

    .vf-button__outline--dark {
      margin-right: 24px;
    }
  }
`

type RequestModalProps = {
  componentName: string
  closeModal: () => void
}

export const RequestModal = ({
  componentName,
  closeModal,
}: RequestModalProps) => {
  const [subject, setSubject] = useState<string>('')
  const [name, setName] = useState<string>('')
  const [email, setEmail] = useState<string>('')
  const [comment, setComment] = useState<string>('')
  const [files, setFiles] = useState<Blob[]>([])
  const [loading, setLoading] = useState<boolean>(false)
  const [showModal, setShowModal] = useState<boolean>(false)
  const [status, setStatus] = useState<number>(0)

  const { currentMode, activeFramework } = useContext(PageProvider)

  const sendReport = async (e: FormEvent) => {
    e.preventDefault()

    setLoading(true)
    const data = {
      request: {
        requester: { name: name, email: email },
        subject: `${subject} - ${componentName}`,
        comment: { body: comment },
        custom_fields: [
          { id: 360016456719, value: activeFramework }, // framework selection
          { id: 360016469620, value: currentMode }, // developer/designer
          { id: 360016469640, value: 'Report issue' }, //new request/report issue
        ],
      },
    }

    let status = await API.sendUploadRequest(data, files)

    setShowModal(true)
    setStatus(status)
    setLoading(false)
  }

  return (
    <RequestModalContainer onClick={() => closeModal()}>
      <div className="upload-wrapper" onClick={e => e.stopPropagation()}>
        <form onSubmit={sendReport}>
          <h3>Report issue{componentName && ` - ${componentName}`}</h3>
          <div className="request-modal__input-wrapper">
            <div className="request-modal__input--first-container">
              <div className="vf-input__container">
                <input
                  className="vf-input"
                  id="new-request__subject"
                  value={subject}
                  onChange={e => setSubject(e.target.value)}
                  type="text"
                />
                <label
                  className={classNames('vf-input__label', {
                    'has-value': subject,
                  })}
                  htmlFor="new-request__subject"
                >
                  Subject
                </label>
              </div>
              <div className="vf-input__container second-row">
                <input
                  className="vf-input"
                  id="new-request__name"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
                <label
                  className={classNames('vf-input__label', {
                    'has-value': name,
                  })}
                  htmlFor="new-request__name"
                >
                  Name
                </label>
              </div>
              <div className="vf-input__container">
                <input
                  className="vf-input"
                  name="new-request__email"
                  value={email}
                  type="email"
                  required
                  onChange={e => setEmail(e.target.value)}
                />
                <label
                  className={classNames('vf-input__label', {
                    'has-value': email,
                  })}
                  htmlFor="new-request__email"
                >
                  Email
                </label>
              </div>
            </div>
            <div className="vf-input__container">
              <textarea
                id="new-request__comment"
                className="vf-input"
                onChange={e => setComment(e.target.value)}
                rows={5}
                value={comment}
              />

              <label
                className={classNames('vf-input__label', {
                  'has-value': comment,
                })}
                htmlFor="new-request__comment"
              >
                Comment
              </label>
            </div>
          </div>

          <ReactUpload sendFiles={(files: Blob[]) => setFiles(files)} />

          <div className="button-wrapper">
            <button
              className="vf-button vf-button__outline--dark"
              type="button"
              onClick={() => closeModal()}
            >
              Cancel
            </button>
            <button
              className="vf-button vf-button__primary"
              type="submit"
              disabled={!email || !comment || !name || !subject || loading}
            >
              Submit
            </button>
          </div>
        </form>
        {showModal && (
          <NewRequestModal statusCode={status} setShowModal={setShowModal} />
        )}
      </div>
    </RequestModalContainer>
  )
}
