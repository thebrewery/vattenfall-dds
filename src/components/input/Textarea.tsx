import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Textarea = () => {
  return (
    <Container className="vf-component__container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        To use rising/top-aligned label, the input requires a placeholder with a
        value or a empty string. This behaviour is required to keep our Inputs
        free from javascript.
      </p>
    </Container>
  )
}
