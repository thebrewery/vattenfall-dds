import React, { useContext } from 'react'
import ReactTooltip from 'react-tooltip'
import styled, { ThemeContext } from 'styled-components'

const CodeSandboxStyle = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

type CodeSandboxProps = {
  componentId: string
  componentGitUrl: string
  componentName: string
  fromDevelopers: boolean
  sandboxId: string
  showHeading: boolean
}

export const CodeSandbox = ({
  componentId,
  componentGitUrl,
  componentName,
  fromDevelopers,
  sandboxId: sandbox_id,
  showHeading = false,
}: CodeSandboxProps) => {
  const { iframeStyle, GitLogo } = useContext(ThemeContext)

  if (!sandbox_id) {
    return null
  }
  return (
    <CodeSandboxStyle>
      {showHeading && <h2 className="vf-component__heading">Sandbox</h2>}
      <iframe
        src={
          fromDevelopers
            ? `https://codesandbox.io/embed/${sandbox_id}?fontsize=14&hidenavigation=1&theme=light&hidenavigation=1&codemirror=1`
            : `https://codesandbox.io/embed/${sandbox_id}?fontsize=14&hidenavigation=1&theme=light&hidenavigation=1&codemirror=1&view=preview`
        }
        title={componentName.split('-')
        .map(word => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ')+' Sandbox'}
        aria-label={componentName.split('-')
        .map(word => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ')+' Sandbox'}
        role='presentation'
        style={iframeStyle}
        allow="accelerometer; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb;  xr-spatial-tracking"
        sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
      />

      {componentGitUrl && (
        <>
          <ReactTooltip id={sandbox_id} place="top" effect="solid">
            Link to source code
          </ReactTooltip>
          {/* <div
            className="vf-component__git-container"
            data-tip
            data-for={sandbox_id}
          >
            <GitLogo
              href={componentGitUrl}
              className="vf-component__git-logo"
            />
          </div> */}
        </>
      )}
    </CodeSandboxStyle>
  )
}
