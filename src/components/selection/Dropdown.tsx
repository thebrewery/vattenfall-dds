import React from 'react'

export const Dropdown = () => {
	return (
		<p className='vf-component__paragraph'>
			Dropdowns allows the user to make a particular choice within a small space
			and is especially useful for mobile devices. Only one item can be chosen
			from the dropdown list at a time. usage of dropdowns are a good idea to
			use when the number of items to be chosen from exceeds three options.
		</p>
	)
}
