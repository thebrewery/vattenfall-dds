import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Calendar = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        The calendar module utilises Date Range Picker. There are many settings
        that can be customised, and so if anything is not covered here, please
        consult the documentation there.
      </p>
      <h3 className="vf-component__small-heading">Date Range</h3>
      <p className="vf-component__paragraph">
        Simply create a div with class vf-input-container and a class that
        begins vf-calendar (eg. vf-calendar, vf-calendar-one or vf-calendar-two
        would be acceptable) and add an input element with a suitable name, then
        consult the Localisation section.
      </p>
    </Container>
  )
}
