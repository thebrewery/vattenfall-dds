import '@vf-dds/vf-dds-vanilla/src/style/component-css/buttons.css'
import '@vf-dds/vf-dds-vanilla/src/style/component-css/dropdown.css'
import { PropsWithChildren, useEffect, useRef, useState } from 'react'
import rehypeRaw from 'rehype-raw'
import styled, { keyframes } from 'styled-components'

import ReactMarkdown from 'react-markdown'
import type {} from 'styled-components/cssprop'

const AccordionContainer = styled.div`
  width: 100%;
  max-width: 798px;
  /* box-shadow: 0 0 0 1px black; */

  h2,
  h3 {
    font-weight: 500;
    line-height: 1.3;
    margin-top: 1.2em;
    margin-bottom: 0.33em;

    &:first-child {
      margin-top: 0;
    }
  }

  h5 {
    font-size: 0.75rem;
    line-height: 1rem;
    font-weight: bold;
    margin: 0;
  }

  p {
    line-height: 1.45;
    margin: 1.5em 0;
  }
  ul {
    padding: 0 1em;
  }

  .vf-link--with-arrow {
    margin-top: 1rem;
  }

  .vf-dropdown {
    &__images {
      display: grid;
      grid-template-columns: repeat(4, 1fr);
      grid-gap: 27px;
      max-width: 900px;
      margin: 30px 0;
    }

    &__image-block {
      & {
        font-size: 0.938rem;
        font-weight: 500;
        line-height: 2.188rem;
      }

      img {
        width: 250px;
        height: 250px;
        border: 0.66px solid #98928F;
      }
    }
  }
`

interface AccordionProps {
  title: string
  data: {
    name: string
    content: string | undefined
    media?: {
      source: string
      label: string
    }[]
  }[]
}

export default function Accordion({ data, title }: AccordionProps) {
  const [selectedFormat, setSelectedFormat] = useState(
    data[0] as (typeof data)[number]
  )

  return (
    <AccordionContainer>
      <div style={{ marginBottom: '3em' }}>
        <h2 style={{ marginBottom: '0.5em' }}>{title} </h2>
        <div
          className="vf-dropdown-wrapper"
          style={{ width: '100%', margin: '0.5rem 0 1.5rem' }}
        >
          <select
            className="vf-dropdown__semantic"
            style={{ width: '100%' }}
            value={selectedFormat.name}
            title={title}
            role='none'
            aria-label={title}
            onChange={e => {
              const value = e.currentTarget.value
              setTimeout(() => {
                setSelectedFormat(data.find(format => format.name === value)!)
              }, 175)
            }}
          >
            {data.map(format => (
              <option
                disabled={!format.content}
                key={format.name}
                value={format.name}
                selected={selectedFormat === format}
              >
                {format.name}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div>
        <ReactMarkdown>{selectedFormat.content ?? ''}</ReactMarkdown>

        {selectedFormat.media && (
          <div className="vf-dropdown__images">
            {selectedFormat.media.map((image, index) => (
              <div className="vf-dropdown__image-block" key={index}>
                <img src={image.source} alt={image.label} />
                <div className="vf-dropdown__image-label">{image.label}</div>
              </div>
            ))}
          </div>
        )}
      </div>
    </AccordionContainer>
  )
}
