import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }

  pre {
    font-size: 0.75rem;
    margin: 0;
  }
`

export const Option = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        The option component can be used for single to multiple item selection,
        like radio buttons or checkboxes.
      </p>
      <p className="vf-component__paragraph">
        This component can be used by using the css supplied by
        @vf-dds/vf-dds-vanilla and using the HTML supplied in the sandbox below.
      </p>

      <h3 className="vf-component__small-heading">Example of implementation</h3>
      <pre style={{ whiteSpace: 'pre-wrap' }}>
        {`
<link
  rel="stylesheet"
  href="https://unpkg.com/@vf-dds/vf-dds-vanilla@${process.env.VF_VANILLA_VERSION}/src/style/component-css/option.css"
/>
`}
      </pre>
    </Container>
  )
}
