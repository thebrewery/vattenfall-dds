import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Value = () => {
  return (
    <Container className="vf-component__container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        This component uses a new approach for the DDS to providing CSS, using{' '}
        <a href="https://tailwindcss.com/" target="_blank" rel="noreferrer">
          Tailwind
        </a>
        . Note that this component should be considered as experimental, please
        contact <a href="mailto:dds@se.nordddb.com">us</a> if you have an
        interest in using Tailwind.
      </p>
      <p className="vf-component__paragraph">
        Sliders allow users to make selections from a range of values. Sliders
        reflect a range of values along a bar, from which users may select a
        single value such as a numeric value. The input is slidable and the user
        is also able to type in the numeric value.
      </p>
    </Container>
  )
}
