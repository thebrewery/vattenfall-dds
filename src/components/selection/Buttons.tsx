import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Buttons = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        Buttons indicate there are actions on the page. Each of our button types
        have specific purposes. Individual button types are documented below,
        see guidelines to understand basic functionality for all of our
        different buttons.
      </p>
    </Container>
  )
}
