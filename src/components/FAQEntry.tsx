import autoAnimate from '@formkit/auto-animate'
import { useEffect, useRef, useState } from 'react'

export const FAQEntry = ({
  entry,
}: {
  entry: { question: string; answer: string }
}) => {
  const [active, setActive] = useState<boolean>(false)
  const animationRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (!animationRef.current) return

    autoAnimate(animationRef.current)
  }, [])
  return (
    <div ref={animationRef} className={`faq-entry ${active ? 'active' : ''}`}>
      <h3 className="faq-heading" onClick={() => setActive(!active)}>
        {entry.question}
        <span className="vf-icon-arrow-down" />
      </h3>
      {active && <p className="faq-answer">{entry.answer}</p>}
    </div>
  )
}
