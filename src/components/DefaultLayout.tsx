import React, { ComponentPropsWithoutRef, useContext } from 'react'
import styled, { css } from 'styled-components'
import classNames from 'classnames'

import { Header } from './Header'
import { Drawer } from './Drawer'
import changelog from '../../CHANGELOG.md'
import PageProvider from '../context/state'

const Container = styled.div<{
  closedDrawer: boolean
  showSidebar: boolean | undefined
}>`
  .grid {
    min-height: 100vh;

    ${({ closedDrawer, showSidebar }) =>
      showSidebar &&
      css`
        display: grid;
        grid-template-columns: ${closedDrawer ? '1fr' : '150px 1fr'};

        @media screen and (min-width: 1024px) {
          grid-template-columns: ${closedDrawer ? '1fr' : '266px 1fr'};
        }
      `}
  }

  .full-main-content {
    padding: 1.25rem 0;
    position: relative;

    @media screen and (min-width: 1024px) {
      padding: 3.75rem 0;
    }
  }

  .main-content {
    padding: 3rem 1.25rem 0 1.25rem;
    position: relative;

    @media screen and (max-width: 768px) {
      overflow: scroll;
    }

    @media screen and (min-width: 1024px) {
      padding: 3.75rem 3.75rem 0 3.75rem;
    }
  }

  .vf-link--navigation {
    cursor: pointer;
    color: #000;
    opacity: ${({ closedDrawer }) => (closedDrawer ? 1 : 0)};
    transition: opacity 0.3s ease-in;
    position: absolute;
    left: 1.25rem;
    top: 0.5rem;

    @media screen and (min-width: 1024px) {
      left: 3.75rem;
    }
  }

  .bigger-text-sizes {
    h2 {
      font-size: 2.125rem;
      line-height: 2.5rem;
      font-weight: bold;
      margin-bottom: 1rem;

      @media screen and (min-width: 1024px) {
        font-size: 3.3125rem;
        letter-spacing: 0px;
        line-height: 3.875rem;
      }
    }

    h3 {
      font-size: 1.75rem;
      line-height: 2rem;
      font-weight: bold;
      margin-bottom: 0.875rem;

      @media screen and (min-width: 1024px) {
        font-size: 1.75rem;
        line-height: 2.25rem;
      }
    }

    p {
      margin-bottom: 1rem;
      line-height: 1.5;
    }
  }
`

export const DefaultLayout = ({
  children,
  ...rest
}: ComponentPropsWithoutRef<'div'>) => {
  const { closedDrawer, setClosedDrawer, showSidebar } =
    useContext(PageProvider)
  const ddsVersion = changelog.split('####')[1].split('(')[0]

  return (
    <Container closedDrawer={closedDrawer} showSidebar={showSidebar} {...rest}>
      <Header />
      <div className="grid">
        <Drawer ddsVersion={`DDS ${ddsVersion}`} />

        <div
          className={classNames({
            'main-content': showSidebar,
            'full-main-content': !showSidebar,
          })}
        >
          {closedDrawer && (
            <span
              className="vf-link--with-arrow vf-link--navigation"
              onClick={() => setClosedDrawer(false)}
            >
              Open navigation
            </span>
          )}
          {children}
        </div>
      </div>
    </Container>
  )
}
