import Link from 'next/link'
import { PropsWithChildren, useEffect, useRef, useState } from 'react'

import { OnboardingCardProps, OnboardingIntroProps } from '../../utils/api'
import { OnboardingCard } from './OnboardingCard'

const Wrappers = ({
  children,
  withBg,
}: PropsWithChildren & { withBg: boolean }) => {
  return withBg ? (
    <div className="introduction-parts introduction-parts--with-bg">
      <div className="introduction__negative-margin vf-future-vision__grid">
        {children}
      </div>
    </div>
  ) : (
    <div className="introduction-parts vf-future-vision__grid introduction-parts--without-bg">
      {children}
    </div>
  )
}

export const CardWrapper = ({
  withBg,
  cards,
  info,
  link,
}: {
  withBg: boolean
  cards: OnboardingCardProps[]
  info: OnboardingIntroProps
  link: string
}) => {
  const [activeOnboardingCard, setActiveOnboardingCard] = useState<number>(0)
  const [onboardingCards, setOnboardingCards] =
    useState<NodeListOf<HTMLElement>>()
  const onboardingRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (!onboardingRef.current) return
    const listOfObservers = onboardingRef.current.querySelectorAll(
      '.onboarding-card'
    ) as NodeListOf<HTMLElement>

    setOnboardingCards(listOfObservers)
  }, [])

  useEffect(() => {
    if (!onboardingCards) return

    const observer = new IntersectionObserver(
      ([entry]) => {
        if (entry.isIntersecting) {
          const target = entry.target as HTMLElement & { index: number }
          setActiveOnboardingCard(target.index)
        }
      },
      {
        root: onboardingRef.current,
        threshold: 0.5,
      }
    )

    Array.from(onboardingCards).forEach((card, index) => {
      const cardWithIndex = card as HTMLElement & { index: number }
      cardWithIndex.index = index
      observer.observe(cardWithIndex)
    })

    return () => {
      observer.disconnect()
    }
  }, [setActiveOnboardingCard])

  return (
    <Wrappers withBg={withBg}>
      <div className="introduction-parts__texts">
        <h2 className="introduction-parts__heading">{info.title}</h2>
        <p className="introduction-parts__paragraph">{info.description}</p>
      </div>
      <div className="introduction__onboarding-cards" ref={onboardingRef}>
        {cards.map(card => (
          <OnboardingCard card={card} key={card.title} />
        ))}
      </div>
      <Link href={`/${link}/get-started`}>
        <a className="future-vision__link future-vision__link--small">
          {info.linkText}
        </a>
      </Link>
      <div className="onboarding__indicators">
        {cards.map((card, index) => (
          <span
            key={card.title}
            onClick={() => {
              if (!onboardingRef.current || !onboardingCards) return

              onboardingRef.current.scroll({
                left: onboardingCards[index].offsetLeft,
                behavior: 'smooth',
              })
            }}
            className={`onboarding__indicator ${
              index === activeOnboardingCard ? 'active' : ''
            }`}
          />
        ))}
      </div>
    </Wrappers>
  )
}
