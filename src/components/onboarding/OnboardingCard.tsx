import Link from 'next/link'
import { useEffect, useState } from 'react'
import styled from 'styled-components'

import { formatAssetUrl, OnboardingCardProps } from '../../utils/api'
import { getDuration } from '../../utils/common'
import { PlayButtonWithText } from '../js-svg/PlayButtonWithText'
import { TextWithLightBg } from '../TextWithLightBg'

const OnboardingCardWrapper = styled.div`
  border: 1px solid #000;
  display: flex;
  flex-direction: column;
  min-width: 70%;
  min-height: 190px;
  height: 100%;
  transition: transform 0.2s ease-in;
  transform: scale(1);
  scroll-snap-align: start;
  position: relative;
  width: 100%;
  padding:1px;

  .play-button {
    height: 2.375rem;
    width: 2.375rem;
    left: 50%;
    opacity: 0;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
    transition: opacity 0.2s ease-in;
    z-index: 10;

    @media screen and (min-width: 1024px) {
      height: 5rem;
      width: 5rem;
    }
  }

  &:hover {
    @media screen and (min-width: 1024px) {
      transform: scale(1.025);
    }

    .onboarding-card__video-thumbnail {
      opacity: 0.7;
    }
  }

  .play-button-text {
    position: absolute;
    left: 20px;
    bottom: 20px;
    z-index: 99999;
  }

  .onboarding-card {
    &__intro {
      background-color: ${({ theme }) => theme.colours.primary__aura_white};
      display: flex;
      flex-direction: column;
      height: 100%;
      padding:10px;
    }

    &__category {
      font-size: 0.75rem;
      font-weight: normal;
      padding-top: 0.75rem;
      padding-left: 0.5rem;

      @media screen and (min-width: 1024px) {
        padding-left: 1rem;
        line-height: 1.75rem;
      }
    }

    &__title {
      ${({ theme }) => theme.h3Typo};
      font-size: 0.875rem;
      line-height: 1.5;
      margin-bottom: 0.375rem;
      padding-left: 0.5rem;

      @media screen and (min-width: 1024px) {
        margin-bottom: 1rem;
        padding-left: 1rem;
      }
    }

    &__logo {
      height: 1.5rem;
      position: absolute;
      top: 0.75rem;
      right: 0.75rem;
      width: 1.5rem;

      @media screen and (min-width: 1440px) {
        top: 1.5rem;
        right: 1.5rem;
        height: 2.375rem;
        width: 2.375rem;
      }
    }

    &__video-container {
      cursor: pointer;
      overflow: hidden;
      padding-top: 43.5%;
      position: relative;
      width: 100%;
    }

    &__video-thumbnail {
      inset: 0;
      height: 100%;
      opacity: 1;
      object-fit: cover;
      transition: opacity 0.2s ease-in;
      position: absolute;
      width: 100%;
      z-index: 1;
    }

    &__video-bg {
      position: absolute;
      inset: 0;
    }
  }
`

export const OnboardingCard = ({ card }: { card: OnboardingCardProps }) => {
  const [isPlaying, setIsPlaying] = useState<boolean>(false)
  const [duration, setDuration] = useState<string>('')

  useEffect(() => {
    if (!card.videoUrl) return

    getDuration(setDuration, card.videoUrl)
  }, [card.videoUrl])

  const handleKeyDown = (event: { key: string }) => {
    if (event.key === 'Enter') {
      setIsPlaying(true);
    }
  };

  return (
    <OnboardingCardWrapper className="onboarding-card" key={card.title} tabIndex={0} onKeyDown={handleKeyDown}>
      <div className="onboarding-card__intro" >
        <img
          className="onboarding-card__logo"
          src="/assets/img/logo-wo-text.svg"
          alt="Vattenfall's logotype"
        />
        <h3 className="onboarding-card__category">{card.category}</h3>
        <h2 className="onboarding-card__title">{card.title}</h2>
      </div>
      <div
        className="onboarding-card__video-container"
        onClick={() => setIsPlaying(true)}
      >
        {card.videoUrl ? (
          <>
            {!isPlaying ? (
              <>
                <img
                  className="onboarding-card__video-thumbnail"
                  src={
                    card.previewImage?.url
                      ? formatAssetUrl(card.previewImage?.url)
                      : '/assets/img/onboard-preview.png'
                  }
                  alt=""
                />
                {/* {card.videoUrl && duration !== '' && (
                  <TextWithLightBg>{duration}</TextWithLightBg>
                )} */}
                <PlayButtonWithText />
              </>
            ) : (
              <div className="onboarding-card__video-bg">
                <iframe
                  src={card.videoUrl}
                  title={card.title}
                  aria-label={card.title}
                  allow="autoplay; fullscreen; picture-in-picture"
                  allowFullScreen
                  frameBorder="0"
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: 'calc(100% - 1px)',
                  }}
                ></iframe>
              </div>
            )}
          </>
        ) : (
          <>
            {card.linkToArticle ? (
              <Link href={card.linkToArticle}>
                <img
                  className="onboarding-card__video-thumbnail"
                  src={
                    card.previewImage?.url
                      ? formatAssetUrl(card.previewImage?.url)
                      : '/assets/img/onboard-preview.png'
                  }
                  onClick={() => setIsPlaying(true)}
                  alt=""
                />
              </Link>
            ) : null}
          </>
        )}
      </div>
    </OnboardingCardWrapper>
  )
}
