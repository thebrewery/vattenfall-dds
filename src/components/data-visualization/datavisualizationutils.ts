const sections = ['content'] as const

type Section = (typeof sections)[number]
type ContentSplit = Record<Section, string | undefined>

const separator = ':::'

export function splitMarkdown(source: string) {
  let currentSection = null as Section | null
  return source.split(/\r?\n/).reduce((acc, line) => {
    if (line.startsWith(separator)) {
      const section = line.substring(separator.length) as Section
      if (sections.indexOf(section) === -1) {
        throw new Error(
          `Section separator :::${section} is not in valid sections ${sections.join(
            ','
          )}`
        )
      }
      currentSection = section
    } else {
      if (currentSection) {
        acc[currentSection] ??= ''
        acc[currentSection] += `${line}\n`
      }
    }
    return acc
  }, {} as ContentSplit)
}
