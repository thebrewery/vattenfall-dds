import styled from 'styled-components'
import { DesignPrinciplesIcons } from './DesignPrinciplesIcons'
import Link from 'next/link'

const Container = styled.div`
  position: relative;

  .vf-guideline {
    &__intro-img {
      height: auto;
      width: 100%;
    }

    &__intro-examples {
      display: grid;
      grid-template-columns: repeat(4, 1fr);
      grid-gap: 27px;
      max-width: 890px;
    }

    &__intro-example--round {
      width: 195px;
      height: 195px;
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      row-gap: 13px;
    }

    &__intro-icon {
      &::before {
        font-size: 3.25rem;
      }
    }

    &__intro-label {
      font-size: 0.9rem;
      font-weight: 700;
      line-height: 1.05rem;
    }

    &__desc {
      font-size: 0.75rem;
      line-height: normal;
      margin-top: 17px;
    }

    &_color {
      &--light-yellow {
        background: ${({ theme }) => theme.colours.bg__light_yellow};
      }

      &--light_green {
        background: ${({ theme }) => theme.colours.bg__light_green};
      }

      &--light_blue__alt {
        background: ${({ theme }) => theme.colours.bg__light_blue__alt};
      }

      &--light_blue {
        background: ${({ theme }) => theme.colours.bg__light_blue};
      }

      &--light_grey {
        background: ${({ theme }) => theme.colours.bg__light_grey};
      }
    }
  }
`

export const DesignPrinciples = () => {
  return (
    <Container>
      <div className="vf-guideline__intro-examples">
        <DesignPrinciplesIcons
          id="vf-icon-eye"
          label="Focused"
          color="light_blue"
          text="We are focused on the message we bring across and goals we strive for. We create straightforward clear user interfaces."
        />

        <DesignPrinciplesIcons
          id="vf-icon-behaviour"
          label="Competent"
          color="light_blue"
          text="We are trustworthy and competent. We create open proof driven user interfaces."
        />

        <DesignPrinciplesIcons
          id="vf-icon-need"
          label="Personal"
          color="light_blue"
          text="We are relevant and personal. We create a personalised experience,
          with high attention for user interaction and data."
        />

        <DesignPrinciplesIcons
          id="vf-icon-discussion"
          label="Conversational"
          color="light_blue"
          text="We don't inform our customer, we enable an ongoing conversation to activate them. This keeps our focus on the customer needs."
        />
      </div>

      <Link href="/designers/design-principles">
        <a className="vf-link--with-arrow">Read More</a>
      </Link>
    </Container>
  )
}
