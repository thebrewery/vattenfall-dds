:::content

### Comparison Charts

Comparison charts can be used with data from energy sources, consumption and prices to visualizing how different groups, metrics and categories relate to each other. These charts can help customers assess the different options, compare and better understand how they relate to each other to make more informed decisions.

Example Scenarios:

- Consumption Overview for Personal Contracts
- Energy Trade Market Analysis on B2B Pages
