:::content

### Metric Highlight

In context when key figures, data points and statistics need to have the focal attention use metric highlight charts that put special emphasis on any specific statistics data. By visually communicating complex data and statistics in a clear, concise, and engaging way provides a quick and focused overview of the most important data points or statistics to help customers understand and memorize data and statistics.

Example Scenarios:

- Co2 Reduction in %
- Editorial/Brand Content
