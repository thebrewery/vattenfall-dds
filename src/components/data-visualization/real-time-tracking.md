:::content

### Real-Time Tracking

With data from current market prices, hourly rates or data revealing the current state, these charts can be effective to when representing dynamic and rapidly changing information. Providing timely insights to customers and gain immediate awareness of the current condition can create an increased understanding of the current market and situation and will help customers feel more in control.

Example scenarios:

- Real-time meter data on My Pages
- Current market prices on VF Informational pages
