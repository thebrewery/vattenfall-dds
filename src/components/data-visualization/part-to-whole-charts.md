:::content

### Part to Whole Charts

Data from energy sources, prices, market analysis and energy sales to name a few work well in part to whole charts when individual parts or components can communicate how they contribute to a whole. With these charts customers can easily compare the sizes of individual parts relative to the whole. The use of colors and shapes make the information more engaging and memorable.

Example Scenarios:

- Consumption over different sources in B2C and B2B contract overviews
- Energy Sales in B2B channels
