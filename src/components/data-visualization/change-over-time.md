:::content

### Change over time

Change over time charts can be used when you for example need to display kWH consumption, emissions, temperatures etc to show trends, incremental changes or comparisons across multiple categories over a period of time. These types of charts will help customers quickly understand trends, patterns, and changes that will help them make informed decisions about their energy consumption and personal finances.

Example Scenarios:

- Monthly Energy Consumption in My Pages
- CO2 Emissions Intensity
