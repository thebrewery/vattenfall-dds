type DesignIconProps = {
  id: string
  label?: string
  color?: string
  text?: string
}

export const DesignPrinciplesIcons = ({
  id,
  label,
  color,
  text,
}: DesignIconProps) => (
  <>
    <div>
      <div
        className={`vf-guideline__intro-example--round vf-guideline_color--${color}`}
        data-tip
        data-for={id}
      >
        {/* Icon */}
        <span className={`vf-guideline__intro-icon ${id}`} />
        <div className="vf-guideline__intro-label">{label}</div>
      </div>

      {/* Description */}
      <p className="vf-component__paragraph vf-guideline__desc">{text}</p>
    </div>
  </>
)
