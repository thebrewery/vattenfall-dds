import styled from 'styled-components'
import { Checklist } from '../Checklist'

const Container = styled.div`
  position: relative;

  .vf-guideline {
    &__checklist {
      display: flex;
      flex-direction: column;
      row-gap: 30px;
      margin-bottom: 50px;
      max-width: 900px;
    }

    &__intro-example {
      &--checklist {
        display: flex;
        gap: 27px;
        font-size: 1.25rem;
        line-height: 2rem;
        font-weight: 500;

        .vf-icon-checkbox::before {
          font-size: 2.9rem;
        }
      }
    }
  }
`

type DataChecklistProps = {
  items: string[]
}

export const DataChecklist = ({ items }: DataChecklistProps) => (
  <Container>
    <div className="vf-guideline__checklist">
      {items.map((item, index) => (
        <Checklist text={item} key={index} />
      ))}
    </div>
  </Container>
)
