type ChecklistProps = {
  text?: string
}

export const Checklist = ({ text }: ChecklistProps) => (
  <>
    <div>
      <div className={`vf-guideline__intro-example--checklist`} data-tip>
        {/* Icon */}
        <span className={`vf-guideline__intro-icon vf-icon-checkbox`} />
        {/* Text */}
        <div className="vf-guideline__intro-label">{text}</div>
      </div>
    </div>
  </>
)
