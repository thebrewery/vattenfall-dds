import Link from 'next/link'
import ReactMarkdown from 'react-markdown'
import styled from 'styled-components'

import { Article, formatAssetUrl } from '../utils/api'

const ArticleCardContainer = styled.div`
  .article-card {
    strong {
      font-weight: 400;
    }

    &__wrapper {
      background-color: #f2f2f2;
      color: ${({ theme }) => theme.colours.primary__coal_black};
      cursor: pointer;
      display: grid;
      flex-grow: 1;
      height: 100%;
      line-height: 1.2em;
      min-height: 450px;
      grid-template-rows: 1fr auto;
      overflow: hidden;
      position: relative;
    }

    &__image {
      height: 100%;
      object-fit: cover;
      position: absolute;
      inset: 0;
      width: 100%;
    }

    &__content {
      min-height: 225px;
      padding: 1rem;
    }

    &__type::after {
      display: inline-block;
      content: '\\2022';
      font-size: 1.25rem;
      line-height: 0.7em;
      font-family: monospace;
      margin-left: 0.2em;
      margin-right: 0.2em;
    }

    &__type,
    &__date {
      color: ${({ theme }) => theme.colours.articleCardGrey};
      display: inline;
    }

    &__type_and_date {
      margin-bottom: 1rem;
    }

    &__title {
      font-size: 1.375rem;
      font-weight: 600;
      font-family: 'Vattenfall Hall NORDx';
      line-height: ${({ theme }) => theme.mediumLineHeight};
      margin-bottom: 1rem;
    }

    &__text {
      h1,
      h2,
      h3,
      h4,
      p {
        font-size: ${({ theme }) => theme.fontSize};
        font-weight: normal;
        letter-spacing: -0.05px;
        line-height: ${({ theme }) => theme.mediumLineHeight};
        font-family: 'Vattenfall Hall NORDx', 'Vattenfall Hall NORDx',
          sans-serif;
        -webkit-font-smoothing: antialiased;
      }
    }

    &__image-wrapper {
      position: relative;
      padding-top: 56.25%;
    }
  }
`

export function ArticleCard({ article }: { article: Article }) {
  function getDate() {
    let today
    if (article.postedDate) {
      today = new Date(Date.parse(article.postedDate))
    } else {
      today = new Date(Date.parse(article.created_at))
    }

    let dd: string | number = today.getDate()
    let mm: string | number = today.getMonth() + 1
    let yyyy = today.getFullYear()
    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }
    today = yyyy + '-' + mm + '-' + dd
    return today
  }

  function truncateText(text: string) {
    return text.substring(0, 90) + '...'
  }

  function capitalize(text: string) {
    return text.slice(0, 1).toUpperCase() + text.slice(1)
  }

  return (
    <ArticleCardContainer>
      <Link href={`/articles/${article.articleId && article.articleId}`}>
        <a className="article-card">
          <div className="article-card__wrapper">
            <div className="article-card__image-wrapper">
              <img
                src={article.cardImage && formatAssetUrl(article.cardImage.url)}
                className="article-card__image"
                alt={article.cardImage && article.cardImage.alternativeText}
              />
            </div>
            <div className="article-card__content">
              <div className="article-card__type_and_date">
                <div className="article-card__type">
                  {capitalize(article.category)}
                </div>
                <div className="article-card__date">{getDate()}</div>
              </div>
              <h3 className="article-card__title">{article.cardTitle}</h3>
              {article.description && (
                <div className="article-card__text">
                  <ReactMarkdown linkTarget="_blank">
                    {truncateText(article.description)}
                  </ReactMarkdown>
                </div>
              )}
            </div>
          </div>
        </a>
      </Link>
    </ArticleCardContainer>
  )
}
