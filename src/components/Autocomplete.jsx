import Link from 'next/link'
import { Component, createRef } from 'react'
import AutoSuggest from 'react-autosuggest'
import { Highlight, PoweredBy } from 'react-instantsearch-dom'
import {
  connectAutoComplete,
  connectStateResults,
} from 'react-instantsearch/connectors'
import styled from 'styled-components'

const AutocompleteStyle = styled.div`
  /* Use this file for style algolia serach */

  align-items: center;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;

  .suggestion-result-category {
    text-transform: capitalize;
    width: 80px;
    font-weight: 400;
  }
  .suggestion-header hr {
    margin: 0;
  }
  .suggestion-header {
    color: rgb(34, 34, 34);
    font-family: ${({ theme }) => theme.defaultFontFamily};
    font-style: normal;
    font-size: 0.875rem;
    font-weight: 300;
    height: 36px;
    letter-spacing: 0;
    line-height: ${({ theme }) => theme.largeLineHeight};
    padding-left: 1rem;
    padding-right: 1rem;
  }
  .inputContainer {
    position: relative;
  }
  .icon {
    position: absolute;
    top: 21px;
    left: 10px;
    width: 15px;
    z-index: 1;
  }
  .no-suggestions {
    padding-left: 20px;
    display: block;
    font-family: ${({ theme }) => theme.defaultFontFamily};
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    top: 200px;
    position: absolute;
    width: 540px;
    left: calc(50% - 270px);
    color: black;
    border: 1px solid #e6e6e6;
    background-color: #fff;
    font-weight: 300;
    font-size: 0.9rem;
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
    z-index: 3;
    -webkit-box-shadow: 0px 2px 30px 0px rgb(193 193 193 / 33%);
    box-shadow: 0px 2px 30px 0px rgb(193 193 193 / 33%);
  }
  .highlight-container {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    font-weight: 400;
  }
  .highlight-container .subtitle {
    margin-right: 3px;
  }
  .suggestion-result-container {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    height: 40px;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
  }
  .suggestion-result-container .group {
    border-right: 1px black solid;
  }
  .react-autosuggest__container {
    position: relative;
    width: 540px;
  }
  .react-autosuggest__input {
    background: transparent;
    border: none;
    border-radius: 4px;
    color: ${({ theme }) => theme.colours.primary__coal_black};
    box-sizing: border-box;
    font-family: ${({ theme }) => theme.largeHeadingFontFamily};
    font-size: ${({ theme }) => theme.mediumHeadingFontSize};
    font-weight: 500;
    width: 40vw;
    width: 540px;
  }
  .react-autosuggest__input--focused {
    outline: none;
  }

  .react-autosuggest__input::placeholder {
    color: ${({ theme }) => theme.colours.primary__coal_black};
    box-sizing: border-box;
    height: 3rem;
    font-family: ${({ theme }) => theme.largeHeadingFontFamily};
    overflow: visible;
  }
  .react-autosuggest__input--open {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }
  .react-autosuggest__suggestions-container {
    display: none;
  }
  .react-autosuggest__suggestions-container--open {
    display: block;
    font-family: ${({ theme }) => theme.defaultFontFamily};
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    top: 100px;
    position: absolute;
    left: 0px;
    border: 1px solid #e6e6e6;
    background-color: #fff;
    font-weight: 300;
    color: rgb(32, 113, 181);
    font-size: 0.9rem;
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
    z-index: 2;
    -webkit-box-shadow: 0px 2px 30px 0px rgba(193, 193, 193, 0.33);
    box-shadow: 0px 2px 30px 0px rgba(193, 193, 193, 0.33);
    width: 540px;
  }
  .react-autosuggest__section-title {
    padding: 5px 5px;
    font-weight: 600;
  }
  .react-autosuggest__suggestions-list {
    margin: 5px 0 0 !important;
    padding: 0;
    list-style-type: none;
  }
  .react-autosuggest__suggestion {
    cursor: pointer;
    padding: 0 20px;
  }
  .suggestion-result-container > .vertical-divider {
    border: 1px solid rgb(212, 216, 221);
    height: 2rem;
    margin: auto 20px;
  }
  .react-autosuggest__suggestion:hover {
    background-color: rgb(235, 238, 242);
  }

  .react-autosuggest__suggestion--highlighted {
    background-color: rgb(235, 238, 242);
  }

  .suggestion-footer {
    display: flex;
    justify-content: flex-end;
    padding-left: 20px;
  }
  .ais-PoweredBy {
    display: flex;
    justify-content: flex-end;
    margin-top: -10px;
    margin-bottom: -10px;
    align-items: center;
    a {
      display: flex;
    }
  }
  .ais-PoweredBy-text {
    font-size: ${({ theme }) => theme.smallFontSize};
    font-family: ${({ theme }) => theme.defaultFontFamily};
    color: rgb(34, 34, 34);
    margin-right: -18px;
    margin-top: 5px;
  }
  .ais-PoweredBy-logo {
    height: 15px;
  }

  .header__search-box {
    align-items: center;
    background: ${({ theme }) => theme.lightBlueBackground};
    display: flex;
    height: 160px;
    justify-content: center;
    left: 0;
    position: absolute;
    top: 60px;
    width: 100%;

    &-close {
      position: absolute;
      right: 10%;
      top: 50%;
      height: auto;
      transform: translateY(-50%);
      cursor: pointer;
    }
  }

  .header__search-button {
    align-items: center;
    border: none;
    background: ${({ active, theme }) =>
      active ? theme.colours.bg__light_blue : 'transparent'};
    cursor: pointer;
    display: flex;
    font-family: 'Vattenfall Hall NORDx';
    font-size: 18px;
    font-weight: 500;
    height: 60px;
    padding-left: 2rem;
    outline: none;
    width: 100%;

    @media screen and (min-width: 1024px) {
      padding-left: 1rem;
    }

    @media screen and (min-width: 1200px) {
      padding-left: 2rem;
    }

    .vf-icon-search {
      margin-right: 10px;

      &::before {
        font-size: 18px;
      }
    }
    &:hover {
      color: ${({ theme }) => theme.colours.primary__ocean_blue};
    }
  }

  .text-link {
    color: #2071b5;

    &:hover {
      color: #000;
    }

    .vf-icon-request::before {
      font-size: 1rem;
      margin-right: 0.25rem;
    }
  }
`

class Autocomplete extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.currentRefinement,
      noSuggestions: false,
      hasBeenBlured: false,
      hasBeenHighlighted: false,
      activeSearch: false,
    }
    this.autocompleteRef = createRef()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { searchResults } = this.props
    const hasResults = searchResults && searchResults.nbHits !== 0
    const nbHits = searchResults && searchResults.nbHits
    const isInputBlank = this.state.value.trim() === ''

    if (prevProps.searchResults !== searchResults) {
      if (!hasResults && nbHits === 0 && !isInputBlank) {
        this.setState({
          noSuggestions: true,
        })
      }
      if (hasResults && nbHits >= 1) {
        this.setState({
          noSuggestions: false,
          hasBeenBlured: false,
        })
      }
    }

    if (!prevProps.activeSearchBox && this.props.activeSearchBox) {
      this.setInputFocus()
    }
  }

  getSuggestionValue(hit) {
    return hit.name
  }

  componentDidMount() {}

  renderSuggestion = hit => {
    if (hit) {
      const subtitle = hit.parentName && hit.parentName + ' > '
      return (
        <div>
          <div className="suggestion-result-container">
            <div className="suggestion-result-category">{hit.type}</div>
            <div className="vertical-divider" />
            <div className="highlight-container">
              {subtitle && <span className="subtitle">{subtitle}</span>}
              <Highlight attribute="name" hit={hit} tagName="strong" />
            </div>
          </div>
        </div>
      )
    }
  }

  renderFullSearch = () => {
    const query = this.props.currentRefinement
    return (
      <div className="open-more-result">
        <a href={'/search?q=' + query}>See more results</a>
      </div>
    )
  }

  renderSuggestionsContainer = ({ containerProps, children, query }) => (
    <div {...containerProps}>
      {
        <div className="suggestion-header">
          Documentation
          <hr />
        </div>
      }
      {children}
      <div className="suggestion-footer">
        <PoweredBy />
      </div>
    </div>
  )

  onSuggestionsFetchRequested = ({ value }) => {
    this.props.refine(value)
  }

  onSuggestionsClearRequested = () => {
    this.setState({ noSuggestions: false })
    this.props.refine()
  }

  onSuggestionSelected = (_, { suggestion, method }) => {
    this.setState({ hasBeenBlured: true })
    window.location.href = suggestion.uri
  }

  onSuggestionCleared = () => {
    this.setState({ value: '' })
  }
  onBlur = () => {
    this.setState({ value: '' })
    this.setState({ hasBeenBlured: true })
  }
  onFocus = () => {
    this.setState({ hasBeenBlured: false })
  }

  setDisplay = () => {
    if (this.state.noSuggestions && !this.state.hasBeenBlured) {
      return { display: 'block' }
    } else {
      return { display: 'none' }
    }
  }

  handleKeyPress = e => {
    if (e.key === 'Enter' && !this.state.hasBeenHighlighted) {
      const query = this.props.currentRefinement
      this.setState({ hasBeenBlured: true })
      window.location.href = '/search?q=' + query
    }
  }
  onSuggestionHighlighted = () => {
    this.setState({ hasBeenHighlighted: true })
  }

  onChange = (_, { newValue }) => {
    if (!newValue) {
      this.setState({ value: '' })
      this.setState({
        noSuggestions: false,
      })
    }
    this.setState({
      value: newValue,
    })
  }

  setInputFocus() {
    this.autocompleteRef.current.input.focus()
  }

  setInputBlur() {
    this.autocompleteRef.current.input.blur()
  }

  render() {
    const { hits } = this.props
    const { value } = this.state
    const inputProps = {
      placeholder: 'Type here to search',
      onChange: this.onChange,
      onFocus: this.onFocus,
      onKeyPress: this.handleKeyPress,
      value,
    }

    return (
      <AutocompleteStyle
        className="main-search-container"
        active={this.props.activeSearchBox}
      >
        <button
          className="header__search-button"
          onClick={() => {
            this.props.toggleSearchBox(!this.props.activeSearchBox)
          }}
        >
          <span className="vf-icon-search" />
          Search...
        </button>
        {this.props.activeSearchBox && (
          <>
            <div className="header__search-box">
              <AutoSuggest
                ref={this.autocompleteRef}
                suggestions={hits}
                onSuggestionHighlighted={this.onSuggestionHighlighted}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                onSuggestionSelected={this.onSuggestionSelected}
                onSuggestionCleared={this.onSuggestionCleared}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                focusFirstSuggestion={true}
                inputProps={inputProps}
                renderSuggestionsContainer={this.renderSuggestionsContainer}
              />
              <span
                className="header__search-box-close vf-icon-close"
                onClick={() => {
                  this.setInputBlur()
                  this.props.toggleSearchBox(false)
                }}
              />
            </div>
            <div
              ref={node => (this.node = node)}
              style={this.setDisplay()}
              className="no-suggestions"
            >
              No results found for {this.state.value}. Make a request{' '}
              <Link href="/contact">
                <a
                  className="text-link"
                  onClick={() => this.props.toggleSearchBox(false)}
                >
                  <span className="vf-icon-request" />
                  here.
                </a>
              </Link>
            </div>
          </>
        )}
      </AutocompleteStyle>
    )
  }
}

export default connectAutoComplete(connectStateResults(Autocomplete))
