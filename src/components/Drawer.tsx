import classNames from 'classnames'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useContext, useEffect, useState } from 'react'
import ReactTooltip from 'react-tooltip'
import styled from 'styled-components'

import PageProvider from '../context/state'
import { Node } from '../utils/siteStructure'

const manualLinks: { name: string; link: string }[] = [
  {
    name: 'FAQ',
    link: '/faq',
  },
]

const Container = styled.div<{
  showSidebar: boolean | undefined
  closedDrawer: boolean
  subMenu: boolean | undefined
}>`
  display: ${({ showSidebar }) => (!showSidebar ? 'none' : 'flex')};
  flex-direction: column;
  justify-content: space-between;
  position: relative;

  left: 0;

  .drawer {
    &__wrapper {
      background-color: ${({ theme }) => theme.lightMode.backgroundColor};
      border-right: 1px solid #e2e2e2;
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      height: 100vh;
      left: 0;
      position: fixed;
      transition: opacity 0.08s ease-in;
      overflow-x: hidden;
      overflow-y: auto;
      opacity: ${({ closedDrawer }) => (closedDrawer ? '0 !important' : 1)};
      width: ${({ closedDrawer }) =>
        closedDrawer ? '0px !important' : '150px'};

      @media screen and (min-width: 1024px) {
        width: 266px;
      }

      & .vf-icon-close {
        cursor: pointer;
        position: absolute;
        top: 0.5rem;
        right: 0.5rem;

        &::before {
          font-size: 1rem;
        }
      }
    }

    &__container {
      display: flex;
      flex-direction: column;
      margin-top: 24px;

      & > .drawer__link {
        padding-right: 1rem;
      }
    }

    &__dropdown {
      margin: 30px 0;
    }

    &__heading {
      color: #fff;
      line-height: 1.5;
      margin-top: 16px;
      margin-bottom: 16px;
      font-size: 0.875rem;
      padding: 8px 16px 8px 24px;
      width: 75%;
      background-color: ${({ theme }) => theme.colours.primary__ocean_blue};
      font-weight: 600;
    }

    &__link {
      color: ${({ theme }) => theme.colours.primary__coal_black};
      font-size: ${({ theme }) => theme.fontSize};
      font-weight: 500;
      line-height: ${({ theme }) => theme.largeLineHeight};
      padding-left: 1rem;
      position: relative;
      text-decoration: none;
      user-select: none;

      p {
        cursor: pointer;
      }

      @media screen and (min-width: 1024px) {
        padding-left: 2rem;
      }

      &:hover {
        color: ${({ theme }) => theme.colours.primary__ocean_blue};
      }

      &:not(last-child) {
        margin-bottom: 12.5px;
      }

      &-child {
        padding-left: 16px;
      }
    }

    &__sub-link {
      color: black;
      font-size: ${({ theme }) => theme.fontSize};
      font-weight: normal;
      height: 40px;
      line-height: ${({ theme }) => theme.largeLineHeight};
      padding-left: 0.5rem;
      text-decoration: none;

      @media screen and (min-width: 1024px) {
        padding-left: 16px;
      }

      &:hover {
        color: ${({ theme }) => theme.colours.primary__ocean_blue};
      }

      &:not(last-child) {
        margin-bottom: 12.5px;
      }

      &:last-child {
        margin-bottom: 0;
      }
    }

    &__sub-links {
      display: flex;
      flex-direction: column;
    }

    &__dds-version {
      background: #f0f0f0;
      color: black;
      cursor: pointer;
      display: flex;
      font-size: ${({ theme }) => theme.smallFontSize};
      justify-content: center;
      margin-bottom: ${({ subMenu }) => (!subMenu ? '100px' : '156px')};
      margin-left: 22px;
      width: 75px;
      border-radius: 20px;
      padding: 4px;
    }

    &--new-component {
      background-color: ${({ theme }) => theme.colours.primary__solar_yellow};
      border-radius: 100%;
      display: inline-block;
      margin-left: 0.5rem;
      height: 10px;
      width: 10px;
    }
  }

  .active {
    color: ${({ theme }) => theme.colours.primary__ocean_blue};
    margin-bottom: 12.5px;
  }

  .disabled-link {
    cursor: default;
    color: #737373;
    pointer-events: none;
  }
  .close-link {
    position: absolute;
    right: 5%;
    font-size: 1.75rem;
    cursor: pointer;
    transform: rotate(180deg);
    transition: transform 300ms;
    &::before {
      content: '\\2192';
      //
    }
    &:hover {
      transform: rotate(180deg) translateX(20%);
    }
    &:hover::before {
      transform: translateX(-20%);
    }
  }
`

type SiteLinkProps = {
  node: Node
  isActive: boolean
  disabled?: boolean
  showNewIndicator?: boolean
}

const RenderManualLink = ({
  name,
  link,
  isActive,
}: {
  name: string
  link: string
  isActive: boolean
}) => (
  <Link href={link}>
    <a className={classNames('drawer__link', { active: isActive })}>{name}</a>
  </Link>
)

const SiteLink = ({
  node,
  isActive,
  disabled = false,
  showNewIndicator,
}: SiteLinkProps) => {
  return (
    <Link href={node.getUri()}>
      <a
        className={classNames(
          'drawer__link',
          { active: isActive },
          { 'disabled-link': disabled }
        )}
      >
        {node.name}
        {showNewIndicator && (
          <>
            <span
              data-tip
              data-for={node.name}
              className="drawer--new-component"
            />
            <ReactTooltip id={node.name} place="top" effect="solid">
              New asset
            </ReactTooltip>
          </>
        )}
      </a>
    </Link>
  )
}

export const Drawer = ({ ddsVersion = 'DDS 7.19.0' }) => {
  const router = useRouter()
  const path = router.asPath.split('?')[0]

  const containsDev = path.includes('developers')
  const {
    currentMode,
    activeCategoryId,
    setActiveCategoryId,
    siteStructure,
    closedDrawer,
    setClosedDrawer,
    subMenu,
    showSidebar,
  } = useContext(PageProvider)

  const [categoryIdOpen, setCategoryIdOpen] = useState<string | null>(
    activeCategoryId
  )

  useEffect(() => {
    setCategoryIdOpen(activeCategoryId)
  }, [activeCategoryId])

  const treeRoot: Node = siteStructure[currentMode]
  return (
    <Container
      subMenu={subMenu}
      closedDrawer={closedDrawer}
      showSidebar={showSidebar}
    >
      <div className="drawer__wrapper">
        {showSidebar && (
          <a className="close-link" onClick={() => setClosedDrawer(true)}></a>
        )}
        <div className="drawer__container">
          {currentMode &&
            showSidebar &&
            treeRoot.children.map((categoryNode: Node, index: number) => {
              console.log(categoryNode)
              return (
                <React.Fragment key={`${index}__${categoryNode.id}`}>
                  {!categoryNode.children ||
                  categoryNode.children.length < 1 ? (
                    <SiteLink
                      isActive={path === categoryNode.getUri()}
                      node={categoryNode}
                    />
                  ) : (
                    !categoryNode.hideInSidebar && (
                      <div
                        className="drawer__link hello"
                        onClick={e => {
                          const eventTarget = e.target as Element

                          if (eventTarget.nodeName === 'P') {
                            setCategoryIdOpen(
                              categoryNode.id === categoryIdOpen
                                ? null
                                : categoryNode.id
                            )
                          }
                        }}
                        key={`${categoryNode.name}__${index}`}
                      >
                        <p
                          className={`${
                            categoryIdOpen === categoryNode.id ? 'active' : ''
                          }`}
                        >
                          {categoryNode.name}
                          {categoryNode.newComponentIndicator && (
                            <>
                              <span
                                data-tip
                                data-for={categoryNode.name}
                                className="drawer--new-component"
                              />
                              <ReactTooltip
                                id={categoryNode.name}
                                place="top"
                                effect="solid"
                              >
                                New asset
                              </ReactTooltip>
                            </>
                          )}
                        </p>
                        {categoryIdOpen === categoryNode.id ? (
                          <div className="drawer__sub-links">
                            {categoryNode.children.map(child => (
                              <SiteLink
                                isActive={path === child.getUri()}
                                node={child}
                                key={`${child.name}__${index}`}
                                disabled={child.disabled}
                                showNewIndicator={child.newComponentIndicator}
                              />
                            ))}
                          </div>
                        ) : null}
                      </div>
                    )
                  )}
                </React.Fragment>
              )
            })}
          {/* {manualLinks.map(({ name, link }) => (
            <RenderManualLink
              isActive={path === `/${currentMode}${link}`}
              link={`${containsDev ? 'developers' : 'designers'}${link}`}
              name={name}
              key={name}
            />
          ))} */}
        </div>
        <Link href="/release-notes">
          <div className="drawer__dds-version">{ddsVersion}</div>
        </Link>
      </div>
    </Container>
  )
}
