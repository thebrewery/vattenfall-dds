import { cn } from '../utils/tailwindUtils'
import ContentWrapper from './ContentWrapper'
import Accordion from './ui/accordion'
import { useViewParallax } from '../hooks/useViewParallax'
import FaqBackgroundImage from '../../public/assets/blocks/FAQ_block_static_bg.png'
import Image from 'next/image'

export default function FAQBlock() {
  const parallaxRef = useViewParallax<HTMLDivElement>()
  const useParallax = true
  return (
    <section
      className={cn(
        'tw-relative tw-max-w-[111.25rem] tw-mx-auto  tw-overflow-hidden',
        'lg:tw-h-[70rem] xl:tw-h-[92.8125rem]',
        'tw-space-y-20 lg:tw-space-y-0',
        'tw-py-[3.75rem] lg:tw-py-0'
      )}
    >
      <ContentWrapper className="tw-h-full">
        <div
          className="tw-flex tw-items-center tw-justify-between tw-gap-10"
          ref={useParallax ? parallaxRef : null}
        >
          <div className="tw-relative lg:tw-max-w-[46.5625rem] tw-w-full">
            <div
              className={cn(
                'tw-w-full tw-space-y-8',
                'lg:tw-absolute lg:tw-top-1/2 lg:-tw-translate-y-1/2'
              )}
            >
              <div className="tw-space-y-5">
                <h2 className="tw-text-[3.67rem] tw-leading-[3.875rem]">FAQ</h2>
                <p className="tw-leading-6 tw-max-w-[34.75rem]">
                  Is the DDS not quite what you’re looking for? Do you need
                  assets like brand guidelines, logos, artwork, campaign
                  material, images, icons, fonts, illustrations or music files?
                  You’ll find what you’re after in the Brand toolbox.
                </p>
              </div>
              <div>
                <h3 className="tw-text-preamble-desktop">
                  Most commonly asked questions
                </h3>

                <Accordion headline="What is the Vattenfall Digital Design System?">
                  <p>
                    The Vattenfall Digital Design System is a comprehensive
                    toolkit that provides guidelines, components, and resources
                    for creating consistent and user-friendly digital
                    experiences across Vattenfall's platforms.
                  </p>
                </Accordion>
                <Accordion headline="Who is the Digital Design System for?">
                  <p>
                    The Digital Design System is designed for Vattenfall's
                    internal teams and external partners, including designers,
                    developers, and product managers and to some extent content
                    / CMS editors.
                  </p>
                </Accordion>
                <Accordion headline="How do I get started?">
                  <p>
                    To get started with the Digital Design System, start by
                    exploring the Designers and Developer pages, watch our
                    Tutorials in the Onboarding section and browse the pages
                    Guidelines, Components and Modules pages. If you still have
                    questions from there don't hesitate to reach out to us via
                    the contact us form and we'll get back to you as soon as we
                    can.
                  </p>
                </Accordion>

                {/* <div className="tw-mt-10">
                  <a
                    href="/articles"
                    className="vf-button vf-button__outline--secondary"
                  >
                    Read more FAQs
                  </a>
                </div> */}
              </div>
            </div>
          </div>

          {/* Imaginary box so that the text will adjust with the background image */}
          <div className="tw-hidden lg:tw-block tw-w-[24.5rem] xl:tw-w-[36.25rem] tw-h-10 tw-shrink-0"></div>
        </div>
      </ContentWrapper>

      {/* Absolute Background Image */}
      <div
        className={cn(
          'tw-pointer-events-none tw-relative tw-aspect-square',
          'lg:tw-w-[72.02%] lg:tw-min-w-[57rem] xl:tw-min-w-[80rem]',
          'tw-ml-auto tw-translate-x-[45%]',
          'lg:tw-absolute lg:tw-right-0 lg:tw-top-1/2 lg:-tw-translate-y-1/2 lg:tw-translate-x-1/2',
          'tw-rounded-full tw-overflow-hidden'
        )}
      >
        <img
          src={FaqBackgroundImage.src}
          alt="FAQ Background Image"
          // layout="fill"
          // objectFit="cover"
        />
      </div>
    </section>
  )
}
