import React from 'react'

export const MessageBanners = () => {
  return (
    <div className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for message banners.
      </p>
    </div>
  )
}
