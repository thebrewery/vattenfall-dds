import styled from 'styled-components'

const GDPRWrapper = styled.div`
  .gdpr-image-wrapper {
    margin: 2rem 0;
    width: 100%;

    img {
      object-fit: cover;
      height: 100%;
      width: 100%;
    }
  }
`

export const GDPRConsent = () => {
  return (
    <GDPRWrapper className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for GDPR Consent.
      </p>

      <h2 className="vf-component__heading">Real example of consent</h2>
      <p className="vf-component__paragraph">
        When using{' '}
        <a target="_blank" href="https://consentmanager.net" rel="noreferrer">
          consent manager
        </a>{' '}
        tool, a design is created on Vattenfall's company account. It's called
        'DDS-aligned design' and can be viewed below:
      </p>

      <div className="gdpr-image-wrapper">
        <img
          alt="Screenshot of Consent managers cookie consent"
          src="/assets/img/consent-screenshot.png"
        />
      </div>

      <p className="vf-component__paragraph">
        The design is also implemented on this (DDS) website.
      </p>

      <h2 className="vf-component__heading">Custom implementation</h2>
      <p className="vf-component__paragraph">
        If you want to create your own consent manager using another tool, the
        example below can be used to follow design guidelines.
      </p>
    </GDPRWrapper>
  )
}
