export const Tooltip = () => {
  return (
    <div className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for tooltip.
      </p>

      <h3 className="vf-component__small-heading">Standard</h3>
      <p className="vf-component__paragraph">
        To add a Tooltip, simply wrap the content in a span with class
        <code>vf-tooltip</code> and add the Tooltip message as a{' '}
        <code>data-tooltip</code> data attribute with the required text.
      </p>

      <h3 className="vf-component__small-heading">Colours and alignment</h3>
      <p className="vf-component__paragraph">
        The standard Tooltip message is top-aligned and green. To use another
        style, simply add the relevant class(es) to the vf-tooltip element:
      </p>

      <h3 className="vf-component__small-heading">For colours:</h3>
      <ul className="vf-ul--no-margins">
        <li>vf-tooltip--blue</li>
        <li>vf-tooltip--red</li>
        <li>vf-tooltip--blue-solid</li>
        <li>vf-tooltip--green-solid</li>
        <li>vf-tooltip--red-solid</li>
        <li>vf-tooltip--yellow-solid</li>
      </ul>

      <h3 className="vf-component__small-heading">For alignent:</h3>
      <ul className="vf-ul--no-margins">
        <li>vf-tooltip--left</li>
        <li>vf-tooltip--right</li>
        <li>vf-tooltip--bottom</li>
      </ul>
    </div>
  )
}
