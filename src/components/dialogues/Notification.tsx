import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Notification = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        Click on the icon in the blue circle to reveal the notification module.
        The icon can be changed by replacing vf-icon-placeholder with the
        desired icon on the <code>vf-notification-module-icon</code>.
      </p>
      <p className="vf-component__paragraph">
        The settings icon should link to a page where users can control
        settings.
      </p>
    </Container>
  )
}
