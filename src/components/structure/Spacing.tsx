import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
	.vf-component__git-container {
		width: fit-content;
	}
`

export const Spacing = () => {
	return (
		<Container className='vf-guideline__container'>
			<h3 className='vf-component__small-heading'>How it works</h3>
			<p className='vf-component__paragraph'>
				Assign responsive-friendly margin or padding values to an element or a
				subset of its sides with shorthand classes. Includes support for
				individual properties, all properties, and vertical and horizontal
				properties.
			</p>
			<h3 className='vf-component__small-heading'>Spacers</h3>
			<p className='vf-component__paragraph'>
				Vattenfall uses four default custom spacers called Small, Medium, Large
				and Extra Large.
			</p>
			<p className='vf-component__paragraph'>
				The spacings are available as SCSS variables. They are named:
			</p>
			<div className='vf-component__paragraph'>
				<li>
					<code>$spacing-sm // 28px</code>
				</li>
				<li>
					<code>$spacing-md // 44px</code>
				</li>
				<li>
					<code>$spacing-lg // 56px</code>
				</li>
				<li>
					<code>$spacing-xl // 88px</code>
				</li>
			</div>
			<h3 className='vf-component__small-heading'>Notation</h3>
			<p className='vf-component__paragraph'>
				Spacing utilities that apply to all breakpoints, from xs to xl, and have
				no breakpoint abbreviation in them. All of them are based on the{' '}
				<code>$spacer</code>-variable that is set to <code>2rem</code>.
			</p>
			<p className='vf-component__paragraph'>
				The classes are named using the format{' '}
				<code>
					{'{property}'}
					{'{sides}'}-{'{size}'}
				</code>
				.
			</p>
			<div className='vf-component__paragraph'>
				Where <i>property</i> is one of:
				<li>
					m - for classes that set <code>margin</code>
				</li>
				<li>
					p - for classes that set <code>padding</code>
				</li>
			</div>
			<div className='vf-component__paragraph'>
				Where <i>sides</i> is one of:
				<li>
					t - for classes that set <code>margin-top</code> or{' '}
					<code>padding-top</code>
				</li>
				<li>
					b - for classes that set <code>margin-bottom</code> or{' '}
					<code>padding-bottom</code>
				</li>
				<li>
					l - for classes that set <code>margin-left</code> or{' '}
					<code>padding-left</code>
				</li>
				<li>
					r - for classes that set <code>margin-right</code> or{' '}
					<code>padding-right</code>
				</li>
				<li>x - for classes that set both *-left and *-right</li>
				<li>y - for classes that set both *-top and *-bottom</li>
				<li>
					blank - for classes that set a margin or padding on all 4 sides of the
					element
				</li>
			</div>
			<div className='vf-component__paragraph'>
				Where <i>size</i> is one of:
				<li>
					0 - for classes that eliminate the <code>margin</code> or{' '}
					<code>padding</code> by setting it to 0
				</li>
				<li>
					1 - for classes that set the <code>margin</code> or{' '}
					<code>padding</code> to $spacer * .25
				</li>
				<li>
					2 - for classes that set the <code>margin</code> or{' '}
					<code>padding</code> to $spacer * .5
				</li>
				<li>
					3 - for classes that set the <code>margin</code> or{' '}
					<code>padding</code> to $spacer
				</li>
				<li>
					4 - for classes that set the <code>margin</code> or{' '}
					<code>padding</code> to $spacer * 1.5
				</li>
				<li>
					5 - for classes that set the <code>margin</code> or{' '}
					<code>padding</code> to $spacer * 3
				</li>
				<li>
					auto - for classes that set the <code>margin</code> to auto
				</li>
			</div>
		</Container>
	)
}
