import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
	.vf-component__git-container {
		width: fit-content;
	}
`

export const Responsive = () => {
	return (
		<Container className='vf-component__container'>
			<p className='vf-component__paragraph'>
				Documentation and examples for our Responsive. This is shown best if you
				open the sandbox in a new window.
			</p>

			<div className='vf-component__paragraph'>
				<strong>Avaliable classes:</strong>
				<li>
					.vf-u--sm-hidden - element hidden on small screen sizes and smaller
				</li>
				<li>
					.vf-u--md-hidden - element hidden on medium screen sizes and smaller
				</li>
				<li>
					.vf-u--lg-hidden - element hidden on large screen sizes and smaller
				</li>
				<li>
					.vf-u--xl-hidden - element hidden on extra-large screen sizes and
					smaller
				</li>
			</div>
		</Container>
	)
}
