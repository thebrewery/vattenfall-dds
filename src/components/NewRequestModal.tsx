import { useEffect, useState } from 'react'
import styled from 'styled-components'

import Alert from './js-svg/Alert'
import Checkmark from './js-svg/Checkmark'

const NewRequestContainer = styled.div`
  background: white;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  outline: 1px solid ${({ theme }) => theme.colours.tertiary__lighter_grey};
  padding: 2rem;
  position: absolute;
  width: 600px;

  .vf-icon-close {
    cursor: pointer;
    right: 1rem;
    position: absolute;
    top: 1rem;
  }
`

type NewRequestModalProps = {
  statusCode: number
  setShowModal: (showModal: boolean) => void
}

export const NewRequestModal = ({
  statusCode,
  setShowModal,
}: NewRequestModalProps) => {
  const [header, setHeader] = useState('')
  const [paragraph, setParagraph] = useState('')

  const Icon = statusCode === 201 || statusCode === 200 ? Checkmark : Alert

  useEffect(() => {
    if (statusCode === 201 || statusCode === 200) {
      setHeader('Thank you for submitting your request.')
      setParagraph(
        'A confirmation email has been sent to you. We will review your request and get back to you within five working days regarding the next steps.'
      )
    } else if (statusCode === 429) {
      setHeader('Too many requests.')
      setParagraph(
        'The maximum amount of requests to the ticket system has exceeded. Please try again later.'
      )
    } else {
      setHeader("We've run into an issue.")
      setParagraph(
        'Something went wrong processing your request, please try again later.'
      )
    }
  }, [statusCode])

  return (
    <NewRequestContainer>
      <span className="vf-icon-close" onClick={() => setShowModal(false)} />
      <h2 className="vf-component__heading">{header}</h2>
      <Icon />
      <p className="vf-component__paragraph">{paragraph}</p>
    </NewRequestContainer>
  )
}
