import Link from 'next/link'
import { useState } from 'react'
import styled from 'styled-components'
import { ComponentType } from '../utils/siteStructure'
import { StrapiUtils } from '../utils/strapiUtils'

const OverviewItemRowSmallStyle = styled.tr`
  a {
    text-decoration: none;
  }

  .divider {
    z-index: 999999;
    border: 1px solid #d3d3d3c4;
    margin-top: 10px;
    margin-bottom: 20px;
  }

  .modalContainer {
    position: fixed;
    top: 52%;
    left: 50%;
    border: 1px solid black;
    //transform: translate(-38%, -50%);
    transform: translate(-50%, -50%);
    z-index: 100000000;
    padding: 24px;
    background-color: white;
    width: 75%;

    select {
      border: 1px solid #cccccc;
      height: 38px;
      width: 100%;
      border-radius: 4px;
      appearance: none;
      padding: 9px 35px 9px 20px;
      color: #222222;
      font-size: ${({ theme }) => theme.fontSize};
      line-height: ${({ theme }) => theme.smallerLineHeight};
      cursor: pointer;
      background: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzNiAzNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzYgMzY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojMzMzMzMzO30KPC9zdHlsZT4KPGcgaWQ9Ik1hc2siIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE4LjAwMDAwMCwgMTguNTAwMDAwKSBzY2FsZSgxLCAtMSkgdHJhbnNsYXRlKC0xOC4wMDAwMDAsIC0xOC41MDAwMDApICI+Cgk8cG9seWdvbiBpZD0icGF0aC0xXzFfIiBjbGFzcz0ic3QwIiBwb2ludHM9IjE4LDE0LjUgMTAsMjAuMyAxMSwyMS41IDE4LDE2LjUgMjUsMjEuNSAyNiwyMC4zIAkiLz4KPC9nPgo8L3N2Zz4K')
        right center no-repeat;
      // margin-bottom: 24px;
    }

    .buttonContainerParent {
      display: flex;
      justify-content: space-around;
    }

    .buttonContainer {
      background-color: white;
      align-items: center;
      display: flex;
      flex-direction: column;
      justify-content: center;

      button {
        margin-top: 24px;
      }
    }

    .border {
      border: 1px solid;
    }

    .column {
      width: 33%;

      &:nth-child(1) {
        padding-right: 20px;
      }

      &:nth-child(2) {
        border-left: 1px solid #d3d3d3c4;
        border-right: 1px solid #d3d3d3c4;
        padding-left: 20px;
        padding-right: 20px;
      }

      &:nth-child(3) {
        padding-left: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
      }
    }

    .modalContent {
      background-color: white;
      height: auto; // set to auto?
      display: flex;
      justify-content: space-between;

      form {
        display: flex;
        flex-direction: row;

        span {
          margin-top: -10px;
        }

        label {
          margin-bottom: 6px;
        }

        input {
          margin-bottom: 10px;
        }

        .textInformation {
          display: flex;
          flex-direction: column;

          span {
            font-weight: 500;
            margin-top: 0;
            margin-bottom: 10px;
          }
        }

        .radioButtonsContainer {
          display: flex;
          flex-direction: column;
          //border: 1px solid black;
          label {
            font-weight: normal;
          }

          .radioButtons {
            display: flex;

            .group {
              margin-right: 10px;

              label {
                margin-left: 3px;
                font-weight: normal;
              }
            }
          }
        }

        .supportInformation {
          display: flex;
          flex-direction: column;

          .supportItem {
            display: flex;
            flex-direction: column;

            .headerTitle {
              text-transform: capitalize;
            }
          }

          .regionSupportContainer {
            // border: 1px solid black;
          }
        }

        label {
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: bold;
          margin-bottom: 4px;
          line-height: ${({ theme }) => theme.smallerLineHeight};
        }

        textarea {
          font-size: ${({ theme }) => theme.fontSize};
          line-height: ${({ theme }) => theme.smallerLineHeight};
          margin-bottom: 6px;
        }
      }
    }
  }

  .close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
    opacity: 0.5;
  }

  .tableContainer {
    margin: 0 44px 88px 44px;
    padding-bottom: 88px;
    @media (max-width: 999px) {
      margin: 0 10px 0 20px;
    }
    .topContainer {
      display: flex;
      justify-content: space-between;
    }

    .legend {
      font-size: 0.8rem;
      margin-bottom: 18px;
      display: flex;

      > div {
        flex-direction: column;
      }
    }

    .legendText {
      color: rgb(0, 0, 0);
      font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
      font-size: ${({ theme }) => theme.smallFontSize};
      font-weight: bold;
      letter-spacing: -0.04px;
    }

    .circleContainer {
      display: flex;
      align-items: center;
      margin-right: 40px;

      // the circle
      div:first-child {
        margin-right: 8px;
        width: 12px;
        height: 12px;
        border-radius: 50%;
        display: inline;
      }
    }

    .circleTD {
      display: flex;
      justify-content: center;
      border: none;

      //circle in table
      div:first-child {
        width: 12px;
        height: 12px;
        border-radius: 50%;
        display: inline;
      }

      //design circle
      .designLink {
        width: 12px;
        height: 12px;
        border-radius: 50%;
        display: inline;
        background-color: #3dc07c;

        /* &:hover {
          width: 18px !important;
          height: 18px !important;
          top: 2px !important;
          position: absolute !important;
        } */
      }
    }

    .unknown {
      background-color: #e6e6e6;
    }

    .awaitingReview {
      background-image: linear-gradient(to top left, #ffda01 50%, #e6e6e6 50%);
    }

    .inReview {
      background-color: #ffda01;
    }

    .canBeUsedIf {
      background-image: linear-gradient(to top left, #3dc07c 50%, #ffda01 50%);
    }

    .canBeUsed {
      background-color: #3dc07c;
    }

    .canBeUsedSoon {
      background-image: linear-gradient(to top left, #e6652b 50%, #3dc07c 50%);
    }

    table {
      font-family: 'Vattenfall Hall NORDx', Helvetica, sans-serif;
      line-height: normal;
      width: 100%;
      table-layout: fixed;
      border-collapse: collapse;
      border: 1px solid rgb(223, 226, 229);

      tr,
      td,
      th {
        padding: 0.3rem;
        border: 1px solid rgb(223, 226, 229);
        word-wrap: break-word;
      }

      tr:nth-child(even) {
        background: #f6f8fa;
      }

      thead {
        text-align: center;

        .tableHeader {
          color: rgb(0, 0, 0);
          font-family: 'Vattenfall Hall Bold NORDx', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: bold;
          height: 20px;
          letter-spacing: -0.05px;
          text-align: center;

          //1 & child.
          th:nth-child(-n + 2) {
            height: 60px;
          }

          // 3-5 child
          th:nth-child(n + 3):nth-child(-n + 5) {
            height: 40px;
          }

          // 6-8 child. Logos
          th:nth-child(n + 6):nth-child(-n + 8) {
            width: 80px;

            img {
              height: 35px;
              width: auto;
            }
          }

          // read more text cell
          th:last-child {
            font-size: ${({ theme }) => theme.smallFontSize};
            font-weight: bold;
            height: 60px;
            width: 80px;
            letter-spacing: -0.04px;
            text-align: center;
          }
        }

        //global/region cell
        tr:nth-child(2) th {
          height: 20px;
          color: rgb(0, 0, 0);
          font-size: ${({ theme }) => theme.smallFontSize};
          font-weight: bold;
          letter-spacing: -0.04px;
          background: white;
        }
      }

      // parent row
      .row {
        td {
          height: 42px;
        }

        cursor: pointer;

        //Target 1 & 2 child.
        // Component name & guideline text
        td:nth-child(-n + 2) {
          padding-left: 16px;
        }

        a {
          color: rgb(0, 0, 0);
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: bold;
          letter-spacing: -0.05px;

          &:hover {
            color: rgb(0, 93, 141);
            text-decoration: none !important;
          }
        }

        .count {
          text-align: center;
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: normal;
          letter-spacing: -0.05px;
        }

        .isOpen {
          a {
            color: rgb(0, 93, 141);
          }
        }

        //open close button/expand row
        .expand {
          cursor: pointer;
          text-align: center;

          img:first-child {
            height: 15px;
            width: auto;
          }
        }
      }

      .editIcon {
        height: 12px;
        margin-right: 4px;
        width: 12px;
        align-self: center;
        cursor: pointer;
      }

      .nameContainer {
        display: flex;
        justify-content: space-between;
      }

      .expandedRow {
        font-size: 0.7rem;
        line-height: normal;
        padding-left: 28px;

        // component name
        .nameContainer:first-child {
          padding-left: 28px;
          font-size: 0.8rem;
          line-height: normal;
          text-align: left;
        }

        //Guideline text
        td:nth-child(2) {
          padding-left: 16px;
        }

        .nameContainer > a {
          color: rgb(0, 0, 0);
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.smallFontSize};
          font-weight: bold;

          &:hover {
            color: rgb(0, 93, 141);
            text-decoration: none !important;
          }
        }
      }

      tfoot {
        // total components text
        tr > td:first-child {
          font-family: 'Vattenfall Hall Bold NORDx', Helvetica, sans-serif;
          padding-left: 16px;
          padding-top: 0.3rem;
          height: 42px;
          padding-bottom: 0.3rem;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: 700;
          text-align: left;
        }

        tr > td:nth-child(n + 2) {
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
        }

        text-align: center;
      }
    }
  }

  .tooltip {
    position: relative;
    display: inline-block;
    cursor: pointer;
  }

  .tooltip .tooltipText {
    visibility: hidden;
    opacity: 0;
    background-color: ${({ theme }) => theme.colours.primary__ocean_blue};
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 8px;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: -40%;

    transition: opacity 0.3s transform 0.3s;
    white-space: nowrap;
    pointer-events: none;
    transform: translateY(-100%);
  }

  .tooltip .tooltipText::after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: ${({ theme }) => theme.colours.primary__ocean_blue}
      transparent transparent transparent;
  }

  .tooltip:hover .tooltipText {
    visibility: visible;
    opacity: 1;
    transform: translateY(0);
  }
`

const OverviewItemRowStyle = styled.tr`
  transition: background-color 200ms;
  a {
    text-decoration: none;
  }
  td:first-child {
    position: relative;
    color: ${({ theme }) => theme.colours.primary__ocean_blue};
    &::after {
      color: black;
      content: '\\e981';
      font-family: 'vf-icons';
      position: absolute;
      right: 10px;
      top: 50%;
      transform: translateY(-45%);
      opacity: 0;
      transition: opacity 50ms;
    }
    &.isOpen::after {
      content: '\\e98c';
      opacity: 1;
    }
  }
  :hover td:first-child::after {
    opacity: 1;
  }

  .divider {
    z-index: 999999;
    border: 1px solid #d3d3d3c4;
    margin-top: 10px;
    margin-bottom: 20px;
  }

  .modalContainer {
    position: fixed;
    top: 52%;
    left: 50%;
    border: 1px solid black;
    //transform: translate(-38%, -50%);
    transform: translate(-50%, -50%);
    z-index: 100000000;
    padding: 24px;
    background-color: white;
    width: 75%;

    select {
      border: 1px solid #cccccc;
      height: 38px;
      width: 100%;
      border-radius: 4px;
      appearance: none;
      padding: 9px 35px 9px 20px;
      color: #222222;
      font-size: ${({ theme }) => theme.fontSize};
      line-height: ${({ theme }) => theme.smallerLineHeight};
      cursor: pointer;
      background: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzNiAzNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzYgMzY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojMzMzMzMzO30KPC9zdHlsZT4KPGcgaWQ9Ik1hc2siIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE4LjAwMDAwMCwgMTguNTAwMDAwKSBzY2FsZSgxLCAtMSkgdHJhbnNsYXRlKC0xOC4wMDAwMDAsIC0xOC41MDAwMDApICI+Cgk8cG9seWdvbiBpZD0icGF0aC0xXzFfIiBjbGFzcz0ic3QwIiBwb2ludHM9IjE4LDE0LjUgMTAsMjAuMyAxMSwyMS41IDE4LDE2LjUgMjUsMjEuNSAyNiwyMC4zIAkiLz4KPC9nPgo8L3N2Zz4K')
        right center no-repeat;
      // margin-bottom: 24px;
    }

    .buttonContainerParent {
      display: flex;
      justify-content: space-around;
    }

    .buttonContainer {
      background-color: white;
      align-items: center;
      display: flex;
      flex-direction: column;
      justify-content: center;

      button {
        margin-top: 24px;
      }
    }

    .border {
      border: 1px solid;
    }

    .column {
      width: 33%;

      &:nth-child(1) {
        padding-right: 20px;
      }

      &:nth-child(2) {
        border-left: 1px solid #d3d3d3c4;
        border-right: 1px solid #d3d3d3c4;
        padding-left: 20px;
        padding-right: 20px;
      }

      &:nth-child(3) {
        padding-left: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
      }
    }

    .modalContent {
      background-color: white;
      height: auto; // set to auto?
      display: flex;
      justify-content: space-between;

      form {
        display: flex;
        flex-direction: row;

        span {
          margin-top: -10px;
        }

        label {
          margin-bottom: 6px;
        }

        input {
          margin-bottom: 10px;
        }

        .textInformation {
          display: flex;
          flex-direction: column;

          span {
            font-weight: 500;
            margin-top: 0;
            margin-bottom: 10px;
          }
        }

        .radioButtonsContainer {
          display: flex;
          flex-direction: column;
          //border: 1px solid black;
          label {
            font-weight: normal;
          }

          .radioButtons {
            display: flex;

            .group {
              margin-right: 10px;

              label {
                margin-left: 3px;
                font-weight: normal;
              }
            }
          }
        }

        .supportInformation {
          display: flex;
          flex-direction: column;

          .supportItem {
            display: flex;
            flex-direction: column;

            .headerTitle {
              text-transform: capitalize;
            }
          }

          .regionSupportContainer {
            // border: 1px solid black;
          }
        }

        label {
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: bold;
          margin-bottom: 4px;
          line-height: ${({ theme }) => theme.smallerLineHeight};
        }

        textarea {
          font-size: ${({ theme }) => theme.fontSize};
          line-height: ${({ theme }) => theme.smallerLineHeight};
          margin-bottom: 6px;
        }
      }
    }
  }

  .close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
    opacity: 0.5;
  }

  .tableContainer {
    margin: 0 44px 88px 44px;
    padding-bottom: 88px;
    @media (max-width: 999px) {
      margin: 0 10px 0 20px;
    }
    .topContainer {
      display: flex;
      justify-content: space-between;
    }

    .legend {
      font-size: 0.8rem;
      margin-bottom: 18px;
      display: flex;

      > div {
        flex-direction: column;
      }
    }

    .legendText {
      color: rgb(0, 0, 0);
      font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
      font-size: ${({ theme }) => theme.smallFontSize};
      font-weight: bold;
      letter-spacing: -0.04px;
    }

    .circleContainer {
      display: flex;
      align-items: center;
      margin-right: 40px;

      // the circle
      div:first-child {
        margin-right: 8px;
        width: 12px;
        height: 12px;
        border-radius: 50%;
        display: inline;
      }
    }

    .circleTD {
      display: flex;
      justify-content: center;
      border: none;

      //circle in table
      div:first-child {
        width: 12px;
        height: 12px;
        border-radius: 50%;
        display: inline;
      }

      //design circle
      .designLink {
        width: 12px;
        height: 12px;
        border-radius: 50%;
        display: inline;
        background-color: #3dc07c;

        /* &:hover {
          width: 18px !important;
          height: 18px !important;
          top: 2px !important;
          position: absolute !important;
        } */
      }
    }

    .unknown {
      background-color: #e6e6e6;
    }

    .awaitingReview {
      background-image: linear-gradient(to top left, #ffda01 50%, #e6e6e6 50%);
    }

    .inReview {
      background-color: #ffda01;
    }

    .canBeUsedIf {
      background-image: linear-gradient(to top left, #3dc07c 50%, #ffda01 50%);
    }

    .canBeUsed {
      background-color: #3dc07c;
    }

    .canBeUsedSoon {
      background-image: linear-gradient(to top left, #e6652b 50%, #3dc07c 50%);
    }

    table {
      font-family: 'Vattenfall Hall NORDx', Helvetica, sans-serif;
      line-height: normal;
      width: 100%;
      table-layout: fixed;
      border-collapse: collapse;
      border: 1px solid rgb(223, 226, 229);

      tr,
      td,
      th {
        padding: 0.3rem;
        border: 1px solid rgb(223, 226, 229);
        word-wrap: break-word;
      }

      tr:nth-child(even) {
        background: #f6f8fa;
      }

      thead {
        text-align: center;

        .tableHeader {
          color: rgb(0, 0, 0);
          font-family: 'Vattenfall Hall Bold NORDx', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: bold;
          height: 20px;
          letter-spacing: -0.05px;
          text-align: center;

          //1 & child.
          th:nth-child(-n + 2) {
            height: 60px;
          }

          // 3-5 child
          th:nth-child(n + 3):nth-child(-n + 5) {
            height: 40px;
          }

          // 6-8 child. Logos
          th:nth-child(n + 6):nth-child(-n + 8) {
            width: 80px;

            img {
              height: 35px;
              width: auto;
            }
          }

          // read more text cell
          th:last-child {
            font-size: ${({ theme }) => theme.smallFontSize};
            font-weight: bold;
            height: 60px;
            width: 80px;
            letter-spacing: -0.04px;
            text-align: center;
          }
        }

        //global/region cell
        tr:nth-child(2) th {
          height: 20px;
          color: rgb(0, 0, 0);
          font-size: ${({ theme }) => theme.smallFontSize};
          font-weight: bold;
          letter-spacing: -0.04px;
          background: white;
        }
      }

      // parent row
      .row {
        td {
          height: 42px;
        }

        cursor: pointer;

        //Target 1 & 2 child.
        // Component name & guideline text
        td:nth-child(-n + 2) {
          padding-left: 16px;
        }

        a {
          color: rgb(0, 0, 0);
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: bold;
          letter-spacing: -0.05px;

          &:hover {
            color: rgb(0, 93, 141);
            text-decoration: none !important;
          }
        }

        .count {
          text-align: center;
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: normal;
          letter-spacing: -0.05px;
        }

        .isOpen {
          a {
            color: rgb(0, 93, 141);
          }
        }

        //open close button/expand row
        .expand {
          cursor: pointer;
          text-align: center;

          img:first-child {
            height: 15px;
            width: auto;
          }
        }
      }

      .editIcon {
        height: 12px;
        margin-right: 4px;
        width: 12px;
        align-self: center;
        cursor: pointer;
      }

      .nameContainer {
        display: flex;
        justify-content: space-between;
      }

      .expandedRow {
        font-size: 0.7rem;
        line-height: normal;
        padding-left: 28px;

        // component name
        .nameContainer:first-child {
          padding-left: 28px;
          font-size: 0.8rem;
          line-height: normal;
          text-align: left;
        }

        //Guideline text
        td:nth-child(2) {
          padding-left: 16px;
        }

        .nameContainer > a {
          color: rgb(0, 0, 0);
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
          font-size: ${({ theme }) => theme.smallFontSize};
          font-weight: bold;

          &:hover {
            color: rgb(0, 93, 141);
            text-decoration: none !important;
          }
        }
      }

      tfoot {
        // total components text
        tr > td:first-child {
          font-family: 'Vattenfall Hall Bold NORDx', Helvetica, sans-serif;
          padding-left: 16px;
          padding-top: 0.3rem;
          height: 42px;
          padding-bottom: 0.3rem;
          font-size: ${({ theme }) => theme.fontSize};
          font-weight: 700;
          text-align: left;
        }

        tr > td:nth-child(n + 2) {
          font-family: 'VattenfallHall-Regular', Helvetica, sans-serif;
        }

        text-align: center;
      }
    }
  }

  .tooltip {
    position: relative;
    display: inline-block;
    cursor: pointer;
  }

  .tooltip .tooltipText {
    visibility: hidden;
    opacity: 0;
    background-color: ${({ theme }) => theme.colours.primary__ocean_blue};
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 8px;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: -40%;

    transition: opacity 0.3s;
    white-space: nowrap;
  }

  .tooltip .tooltipText::after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: ${({ theme }) => theme.colours.primary__ocean_blue}
      transparent transparent transparent;
  }

  .tooltip:hover .tooltipText {
    visibility: visible;
    opacity: 1;
  }
`

const categoryIdMap: Record<string, string> = {
  colours: 'Colours',
  dialogues: 'Dialogues',
  icons: 'Icons',
  indicators: 'Indicators',
  input: 'Input',
  logos: 'Logos',
  modules: 'Modules',
  navigation: 'Navigation',
  selection: 'Selections',
  structure: 'Structure',
  tables_and_lists: 'Tables and lists',
  typography: 'Typography',
  campaign_assets: 'Campaign Assets',
}

type OVIProps = {
  groupItems: ComponentType[]
  categoryId: string
}

const OverviewItem = ({ groupItems, categoryId }: OVIProps) => {
  const [isExpanded, setIsExpanded] = useState<boolean>()

  const renderDesignCountCol = (designProgram: string) => {
    const filteredItems = groupItems.filter(item => {
      if (designProgram === 'figma') {
        return item.figmaUrl ?? null
      }

      if (designProgram === 'sketch') {
        return item.sketchUrl === 'TRUE' ?? null
      }
    })

    const count = StrapiUtils.countComponents(
      filteredItems,
      null,
      null,
      designProgram
    )
    return <td className="count"> {count} </td>
  }

  const renderDesignLink = (url: string | undefined) => {
    if (url) {
      return (
        <td >
          {/* <a target="_blank" href={url} rel="noreferrer"> */}
          <div className="circleTD tooltip">
            <div className="designLink"></div>
            <span
              className="tooltipText"
              style={{
                left: '50%',
                bottom: '150%',
                transform: 'translateX(-50%)',
              }}
            >
              Can be used
            </span>
          </div>
          {/* </a> */}
        </td>
      )
    }

    return (
      <td>
        <div className="circleTD tooltip">
          <div className="unknown" />
          <span
            className="tooltipText"
            style={{
              left: '50%',
              bottom: '150%',
              transform: 'translateX(-50%)',
            }}
          >
            Not ready
          </span>
        </div>
      </td>
    )
  }

  const renderGuidelineUri = (uri: string) => {
    if (uri) {
      return <a href={uri}>Guideline</a>
    }
    return '---'
  }

  const renderComponentCountCol = (
    framework: string,
    region = StrapiUtils.regionEnum.GLOBAL
  ) => {
    const count = StrapiUtils.countComponents(groupItems, region, framework)
    // let languages = []
    // // Region specific
    // if (region) {
    //   frameworkReviews.forEach((lang) => {
    //     if (lang[framework].exists) {
    //       lang[framework].support.forEach((item) => {
    //         if (item.status === 0) {
    //           languages.push(item)
    //         }
    //       })
    //     }
    //   })
    // } else {
    //   // Global use
    //   languages = frameworkReviews.filter((lang) => {
    //     return (
    //       lang[framework].exists && lang[framework].globalUse && lang[framework].status === 0
    //     )
    //   })
    // }
    return <td className="count"> {count} </td>
  }

  const renderStatus = (
    component: ComponentType,
    framework: string | null,
    region = StrapiUtils.regionEnum.GLOBAL
  ) => {
    if (!component.frameworkReviews) return
    const reviews = component.frameworkReviews.filter(
      fr => fr.exists && fr.framework === framework && fr.region === region
    )
    const status =
      reviews && reviews.length > 0
        ? reviews[0].status
        : StrapiUtils.reviewStatusEnum.UNKNOWN

    switch (status) {
      case StrapiUtils.reviewStatusEnum.UNKNOWN:
        return (
          <td>
            <div className="circleTD tooltip">
              <div className="unknown" />
              <span
                className="tooltipText"
                style={{ left: '4.5%', bottom: '150%' }}
              >
                Not ready
              </span>
            </div>
          </td>
        )
      case StrapiUtils.reviewStatusEnum.ALLOWED:
        return (
          <td>
            <div className="circleTD tooltip">
              <div className="canBeUsed" />
              <span
                className="tooltipText"
                style={{ left: '-4.5%', bottom: '150%' }}
              >
                Can be used
              </span>
            </div>
          </td>
        )
      case StrapiUtils.reviewStatusEnum.IN_REVIEW:
        return (
          <td>
            <div className="circleTD">
              <div className="inReview" />
            </div>
          </td>
        )
      case StrapiUtils.reviewStatusEnum.ALLOWED_PENDING_UPDATE:
        return (
          <td>
            <div className="circleTD">
              <div className="canBeUsedSoon" />
            </div>
          </td>
        )
      case StrapiUtils.reviewStatusEnum.PENDING_REVIEW:
        return (
          <td>
            <div className="circleTD">
              <div className="awaitingReview" />
            </div>
          </td>
        )
      case StrapiUtils.reviewStatusEnum.CONDITIONAL_USE:
        return (
          <td>
            <div className="circleTD">
              <div className="canBeUsedIf" />
            </div>
          </td>
        )
      default:
        return <td />
    }
  }

  const showExpand = (components: ComponentType[]) => {
    return components.map(component => {
      //Links should always be guideline url, but this is wrong in cms, so let's fallback to figmaurl.
      const url = component.guidelineUri ?? component.figmaUrl
      return (
        <OverviewItemRowSmallStyle className="expandedRow" key={component.id}>
          <td colSpan={10}>
            <div className="nameContainer">
              {url ? (
                <Link href={url}>
                  <a style={{ width: '100%' }}>{component.name}</a>
                </Link>
              ) : (
                <span style={{ color: "#707070" }}>{component.name}</span>
              )}
            </div>
          </td>
          {/* <td colSpan="4">{renderGuidelineUri(component.guidelineUri)}</td> */}

          {renderStatus(component, StrapiUtils.frameworkEnum.VANILLA)}
          {/* {renderStatus(component, StrapiUtils.frameworkEnum.ANGULAR)}
          {renderStatus(
            component,
            StrapiUtils.frameworkEnum.ANGULAR,
            StrapiUtils.regionEnum.NL
          )}
          {renderStatus(component, StrapiUtils.frameworkEnum.REACT)}
          {renderStatus(
            component,
            StrapiUtils.frameworkEnum.REACT,
            StrapiUtils.regionEnum.SV
          )} */}

          {component.sketchUrl === 'TRUE'
            ? renderDesignLink(component.sketchUrl)
            : renderDesignLink(undefined)}
          {renderDesignLink(component.figmaUrl)}
          {/* {renderDesignLink(component.adobeXdUrl)} */}
        </OverviewItemRowSmallStyle>
      )
    })
  }
  return (
    <>
      <OverviewItemRowStyle
        className="row"
        onClick={() => setIsExpanded(prev => !prev)}
      >
        <td className={`${isExpanded ? 'isOpen' : ''}`} colSpan={10} tabIndex={0}>
          <div className="nameContainer">{categoryIdMap[categoryId]}</div>
        </td>
        {/* <td colSpan="4">{renderGuidelineUri(null)}</td> */}
        {renderComponentCountCol(StrapiUtils.frameworkEnum.VANILLA)}
        {/* {renderComponentCountCol(StrapiUtils.frameworkEnum.ANGULAR)}
         {renderComponentCountCol(
          StrapiUtils.frameworkEnum.ANGULAR,
          StrapiUtils.regionEnum.NL
        )}
        {renderComponentCountCol(StrapiUtils.frameworkEnum.REACT)}
        {renderComponentCountCol(
          StrapiUtils.frameworkEnum.REACT,
          StrapiUtils.regionEnum.SV
        )} */}

        {renderDesignCountCol(StrapiUtils.designEnum.SKETCH)}
        {renderDesignCountCol(StrapiUtils.designEnum.FIGMA)}
        {/* {renderDesignCountCol(StrapiUtils.designEnum.XD)} */}

        {/* <td onClick={() => setIsExpanded(!isExpanded)} className="expand">
          <img
            src={
              isExpanded ? '/assets/icons/Close.svg' : '/assets/icons/More.svg'
            }
            alt="btn"
          />
        </td> */}
      </OverviewItemRowStyle>
      {isExpanded && showExpand(groupItems)}
    </>
  )
}

export default OverviewItem
