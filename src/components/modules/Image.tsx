import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Image = () => {
  return (
    <Container className="vf-component__container">
      {/* TODO <h3 className="vf-component__small-heading">Shareable image</h3>
      <p className="vf-component__paragraph">
        This component is used to show an image with a share icon, and an
        optional description.
      </p> */}
      <h3 className="vf-component__small-heading">Image with caption</h3>
      <p className="vf-component__paragraph">
        Used on content pages to show a prominent image together with a caption
        describing something in detail. Can we used with or without the sharing
        icon.{' '}
      </p>
    </Container>
  )
}
