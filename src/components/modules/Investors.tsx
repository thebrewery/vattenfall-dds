import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Investors = () => {
  return (
    <Container className="vf-component__container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        This component uses a new approach for the DDS to providing CSS, using{' '}
        <a href="https://tailwindcss.com/" target="_blank" rel="noreferrer">
          Tailwind
        </a>
        . Note that this component should be considered as experimental, please
        contact <a href="mailto:dds@se.nordddb.com">us</a> if you have an
        interest in using Tailwind.
      </p>
    </Container>
  )
}
