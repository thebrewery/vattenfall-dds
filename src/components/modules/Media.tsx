import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Media = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        Media block is used to display an image or a video and some text.
      </p>
    </Container>
  )
}
