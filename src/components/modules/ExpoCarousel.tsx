import Link from 'next/link'
import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  ul {
    margin: 0;
  }

  li {
    margin-bottom: 1.25rem;
  }
`

export const ExpoCarousel = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        You can use both images and videos in this component, as you can see on
        our <Link href="/">landing page</Link>. You should at most use six
        images at once.
      </p>
      <p className="vf-component__paragraph">
        The anchor link can be linked to separate content on the same page, or
        to an entirely different page.
      </p>
      <h3 className="vf-component__tiny-heading vf-utility__error">CAUTION</h3>
      <p className="vf-component__paragraph">
        This component uses a{' '}
        <a
          href="https://en.wikipedia.org/wiki/Polyfill_(programming)"
          target="_blank"
          rel="noreferrer"
        >
          polyfill
        </a>{' '}
        for a coming feature called ScrollTimeline. This seems to be stable but
        is still considered as <i>experimental</i>.
      </p>
      <h3 className="vf-component__small-heading">Restrictions</h3>
      <p className="vf-component__paragraph">
        The carousel has the restrictions of a min-height of 60vh and try to
        maintain a 16/9 aspect ratio.
      </p>
      <h3 className="vf-component__small-heading">Options</h3>
      <ul>
        <li>
          You can turn on the carousel auto scrolling using the datasets called:{' '}
          <code>data-autoscroll</code>. You can set a custom timer by using{' '}
          <code>data-autoscroll-timer</code>. This defaults to 3 seconds, if a
          timers isn't set.
        </li>
        <li>
          Arrows along with the number buttons can be shown using the dataset
          called: <code>data-show-arrows</code>. These can be styled in another
          way using CSS.
        </li>
      </ul>
    </Container>
  )
}
