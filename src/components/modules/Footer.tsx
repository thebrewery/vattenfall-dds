import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Footer = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        The website footer component is used on most Vattenfall pages. It
        contains links, social media icons and an area for a short text message.
      </p>
    </Container>
  )
}
