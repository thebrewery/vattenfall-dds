import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const LinkedList = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        A linked list block is used to display an image or a video with some
        text plus a list of links.
      </p>
    </Container>
  )
}
