import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Hero = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        The Hero component is used for large blocks at the top of webpages to
        convey a key message or describe what the page is about.
      </p>
    </Container>
  )
}
