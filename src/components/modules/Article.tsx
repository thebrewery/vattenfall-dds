import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Article = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        The card is mostly used for presenting shorter, article like content
        segments, e.g. news posts.
      </p>
    </Container>
  )
}
