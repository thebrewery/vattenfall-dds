import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Upload = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        File uploaders allow users to upload content of their own. A file
        uploader is commonly found in forms, but can also live as a standalone
        element.
      </p>
      <h2 className="vf-component__heading">When to use</h2>
      <ul>
        <li>Uploading one or more files.</li>
        <li>Uploading files by dragging and dropping.</li>
        <li>Showing the process of uploading.</li>
      </ul>
      <p className="vf-component__paragraph">
        A file uploader traditionally uploads one or more files by clicking an
        action button that prompts a file selection dialog. Once you have
        selected one or more files from the dialog, the selected files will
        populate below the file uploader on the page. To remove an uploaded
        file, click the “x” (or close) icon.
      </p>
      <h3 className="vf-component__small-paragraph">Placement</h3>
      <p className="vf-component__paragraph">
        Center align the button or drop zone area with the uploaded files.
        Multiple files will stack vertically. Use a tertiary button for the file
        uploader so it does not conflict with the primary action.
      </p>

      <h3 className="vf-component__small-paragraph">
        Drag and drop file uploader
      </h3>
      <p className="vf-component__paragraph">
        Drag and drop file uploaders are used to directly upload files by
        dragging and dropping them into a drop zone area. When dragging files
        into the drop zone area, the drop zone border changes in color and to
        indicate the area has been activated and is ready for files.
      </p>
      <h3 className="vf-component__small-paragraph">Validation</h3>
      <p className="vf-component__paragraph">
        When a specific file cannot be uploaded successfully it will show an
        error state. Error messages should provide clear guidance to help the
        user resolve the error.
      </p>
    </Container>
  )
}
