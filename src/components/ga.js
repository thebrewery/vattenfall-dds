export const pageview = url => {
  window.gtag('config', process.env.NEXT_PUBLIC_GA_TRACKING_CODE, {
    page_path: url,
  })
}

// unused at the moment, but can be used to track specific events, such as search and other
export const event = ({ action, params }) => {
  window.gtag('event', action, params)
}
