import Link from 'next/link'
import styled from 'styled-components'

type AnchorLinksProps = {
  links: {
    text: string
    url: string
  }[]
}

const Container = styled.div`
  //   Anchor links
  .vf-component__anchor-links {
    background-color: #f2f2f2;
    padding: 16px 20px 0 16px;
    max-width: 905px;

    a {
      padding-right: 12px;
      margin-right: 8px;
      font-size: 18px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      border-right: 1px solid #000;
      display: inline-block;
      margin-bottom: 16px;

      &:last-of-type {
        border-right: 0;
        padding-right: 0;
        margin-right: 0;
      }
    }
  }
`

export const AnchorLinks = ({ links }: AnchorLinksProps) => (
  <Container>
    <div className="vf-component__anchor-links">
      {links.map((item, index) => (
        <Link href={item.url} key={index}>
          {item.text}
        </Link>
      ))}
    </div>
  </Container>
)
