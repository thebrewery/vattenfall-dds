import React, { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Stacked = () => {
  return (
    <Container className="vf-component__container">
      <p className="vf-component__paragraph">
        Documentation and examples for our Stacked.
      </p>
    </Container>
  )
}
