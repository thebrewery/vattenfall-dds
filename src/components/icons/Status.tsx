import styled from 'styled-components'

const Container = styled.div`
  .vf-component__git-container {
    width: fit-content;
  }
`

export const Status = () => {
  return (
    <Container className="vf-component__container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        Tooltips are used to display the related class to each icon, and won't
        appear when you use them.
      </p>
      <p className="vf-component__paragraph">
        Documentation and examples for our Status.
      </p>
    </Container>
  )
}
