import classNames from 'classnames'
import emailjs from 'emailjs-com'
import { FormEvent, useRef, useState } from 'react'
import styled from 'styled-components'
import ContentWrapper from './ContentWrapper'

const SignUpModalContainer = styled.div`
  background-color: #f6f6f6;
  max-width: 1780px;
  margin: 0 auto;
  // overflow: hidden;

  .sign-up-modal {
    &__wrapper {
      height: auto;
      max-width: 552px;
      width: 100%;
      z-index: 1;
      max-width: 1530px;
      height: 440px;
      margin: 3em auto;
      box-sizing: inherit;

      @media screen and (min-width: 1024px) {
        margin: 13em auto 3em;
      }
    }

    &__content-container {
      transform: translateY(0rem);
      max-width: 922px;

      @media screen and (min-width: 1024px) {
        transform: translateY(-9.5rem);
      }
    }

    &__headline {
      font-family: 'VattenfallHallDisplay-Bold';
      font-size: ${({ theme }) => theme.headingFontSize};
      line-height: ${({ theme }) => theme.mediumLineHeight};
      font-weight: 600;
      letter-spacing: 0;
      // line-height: 2.75rem;
      padding-bottom: 28px;

      @media screen and (min-width: 768px) {
        font-size: ${({ theme }) => theme.mediumHeadingFontSize};
        line-height: ${({ theme }) => theme.mediumHeadingLineHeight};
      }

      @media screen and (min-width: 1024px) {
        font-size: 6.875rem;
        line-height: 111px;
        padding-bottom: 32px;
      }
    }

    &__paragraph {
      font-size: ${({ theme }) => theme.fontSize};
      font-weight: normal;
      letter-spacing: 0;
      line-height: ${({ theme }) => theme.smallLineHeight};
      padding-bottom: 32px;
      max-width: 370px;
      // text-align: center;

      @media screen and (min-width: 1024px) {
        font-size: ${({ theme }) => theme.headingFontSize};
        line-height: ${({ theme }) => theme.largeLineHeight};
        max-width: 657px;
      }
    }

    &__input-container {
      align-items: center;
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      max-width: 657px;
    }
  }

  .vf-button {
  }

  .vf-icon-close {
    cursor: pointer;
    position: absolute;
    right: 20px;
    top: 20px;

    &::before {
      font-size: ${({ theme }) => theme.largeFontSize};
    }
  }

  form {
    // text-align: center;
    width: 100%;

    .vf-input {
      width: 100%;
    }

    .vf-button {
      &:hover {
        cursor: pointer;
      }
    }
  }
`

type SignUpModalProps = {
  setToggleSignUpModal: (arg0: boolean) => void
  setNewSignUpModal: (arg0: boolean) => void
  setStatus: (arg0: number) => void
}

export const SignUpModal = ({
  setToggleSignUpModal,
  setNewSignUpModal,
  setStatus,
}: SignUpModalProps) => {
  const [email, setEmail] = useState('')

  const form = useRef<HTMLFormElement>(null)

  const emailjsServiceKey = process.env.NEXT_PUBLIC_EMAILJS_SERVICE_KEY || ''
  const emailjsTemplateKey = process.env.NEXT_PUBLIC_EMAILJS_TEMPLATE_KEY || ''
  const emailjsUserId = process.env.NEXT_PUBLIC_EMAILJS_USER_ID || ''

  const sendEmail = async (e: FormEvent) => {
    e.preventDefault()

    if (!form.current) {
      throw Error('Form body contains no data')
    }

    await emailjs
      .sendForm(
        emailjsServiceKey,
        emailjsTemplateKey,
        form.current,
        emailjsUserId
      )
      .then(
        res => {
          setStatus(res.status)
        },
        err => {
          console.log(err)
        }
      )

    setNewSignUpModal(true)
    setToggleSignUpModal(false)
  }

  return (
    <SignUpModalContainer onClick={() => setToggleSignUpModal(false)}>
      <ContentWrapper className="sign-up-modal__wrapper">
        <div className="sign-up-modal__content-container">
          <h2 className="sign-up-modal__headline">
            Sign up for the newsletter
          </h2>
          <p className="sign-up-modal__paragraph">
            Read more about updates and everything you need to know about the
            digital design system
          </p>
          <div className="sign-up-modal__input-container">
            <form ref={form} onSubmit={sendEmail}>
              <div className="vf-input__container">
                <input
                  type="email"
                  className="vf-input"
                  id="sign-up__email"
                  value={email}
                  name="email"
                  onChange={e => setEmail(e.target.value)}
                />
                <label
                  className={classNames('vf-input__label', {
                    'has-value': email,
                  })}
                  htmlFor="sign-up__email"
                >
                  Email
                </label>
              </div>
              <button
                type="submit"
                className="vf-button vf-button--lg vf-button__primary"
                disabled={!email}
                value="Send"
              >
                Sign up
              </button>
            </form>
          </div>
        </div>
      </ContentWrapper>
    </SignUpModalContainer>
  )
}
