import { useEffect, useRef } from 'react'

export const useViewParallax = <tElement extends HTMLElement>({
  transform,
} = CONTAINER_PARALLAX_PROPS) => {
  const parallaxRef = useRef<tElement>(null)

  useEffect(() => {
    let animation: Animation | undefined
    const importScript = async () => {
      // @ts-ignore
      await import('../react/components/modules/ExpoCarousel/ScrollTimeline')

      // @ts-ignore ScrollTimeline isn 't typed in TS
      if (!window.ScrollTimeline) return

      animation = parallaxRef.current?.animate(
        {
          transform,
        },
        {
          // @ts-ignore ScrollTimeline isn 't typed in TS
          timeline: new ScrollTimeline({
            fill: 'both',
            timeRange: 1,
            orientation: 'vertical',
            source: document.scrollingElement,
            scrollSource: document.scrollingElement,
            scrollOffsets: [
              {
                target: parallaxRef.current,
                edge: 'end',
                threshold: 0,
              },
              {
                target: parallaxRef.current,
                edge: 'start',
                threshold: 0,
              },
            ],
          }),
        } as KeyframeAnimationOptions
      )
    }

    importScript()

    return () => animation?.cancel()
  }, [transform])

  return parallaxRef
}

export const CONTAINER_PARALLAX_PROPS = {
  transform: ['translateY(20%)', 'translateY(0)', 'translateY(0)'],
}

export const IMAGE_PARALLAX_PROPS = {
  transform: [
    'translateY(-10%) scale(1.15)',
    'translateY(0) scale(1.15)',
    'translateY(10%) scale(1.15)',
  ],
}
