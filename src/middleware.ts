import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'

export function middleware(request: NextRequest) {
  // Get the pathname of the request (e.g. /, /about, /blog/first-post)
  const path = request.nextUrl.pathname

  // Define paths that should not be redirected
  const isPublicPath = path === '/' || 
    path.startsWith('/api') || 
    path.startsWith('/_next') ||
    path.includes('.')  // For static files

  // Return early if it's a public path
  if (isPublicPath) {
    return NextResponse.next()
  }

  // Redirect to home page
  return NextResponse.redirect(new URL('/', request.url))
}

// Configure which paths should be handled by this middleware
export const config = {
  matcher: '/:path*',
}