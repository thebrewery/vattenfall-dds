import styled from 'styled-components'

const Container = styled.div`
  .vf-guideline {
    &__tooltip-content {
      display: grid;
      grid-template-columns: repeat(4, 1fr);
    }

    &__tooltip-img {
      display: block;
      padding: 2rem 0;
    }
  }
`

export const Tooltip = () => {
  return (
    <Container>
      <div className="vf-guideline__container">
        <h3 className="vf-component__tiny-heading vf-utility__warning">
          DISCLAIMER
        </h3>
        <p className="vf-component__paragraph">
          Tooltips appear on hover and provide an additional layer of
          information for the user. They are filled with different colours in
          order to generate maximum contrast with the page beneath. Carets are
          positioned in the top center, bottom center, right center and left
          center.
        </p>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">Solid</h2>
        <div className="vf-guideline__tooltip-content">
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Bottom</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blue-bottom.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/green-bottom.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/red-bottom.png"
              alt="Red tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/yellow-bottom.png"
              alt="Yellow tooltip"
            />
          </div>
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Left</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blue-left.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/green-left.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/red-left.png"
              alt="Red tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/yellow-left.png"
              alt="Yellow tooltip"
            />
          </div>
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Right</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blue-right.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/green-right.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/red-right.png"
              alt="Red tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/yellow-right.png"
              alt="Yellow tooltip"
            />
          </div>
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Top</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blue-top.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/green-top.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/red-top.png"
              alt="Red tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/yellow-top.png"
              alt="Yellow tooltip"
            />
          </div>
        </div>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-guideline__heading">Outlined</h2>
        <div className="vf-guideline__tooltip-content">
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Bottom</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blueBottomOutlined.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/greenBottomOutlined.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/redBottomOutlined.png"
              alt="Red tooltip"
            />
          </div>
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Left</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blueLeftOutlined.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/greenLeftOutlined.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/redLeftOutlined.png"
              alt="Red tooltip"
            />
          </div>
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Right</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blueRightOutlined.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/greenRightOutlined.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/redRightOutlined.png"
              alt="Red tooltip"
            />
          </div>
          <div className="vf-guideline__tooltip-example">
            <p className="vf-guideline__underlined-paragraph">Top</p>
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/blueTopOutlined.png"
              alt="Blue tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/greenTopOutlined.png"
              alt="Green tooltip"
            />
            <img
              className="vf-guideline__tooltip-img"
              src="/assets/img/redTopOutlined.png"
              alt="Red tooltip"
            />
          </div>
        </div>
      </div>
    </Container>
  )
}
