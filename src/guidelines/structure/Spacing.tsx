import styled from 'styled-components'

const Container = styled.div`
  .image-wrapper {
    height: auto;
    width: 100%;
    img {
      object-fit: cover;
      height: 100%;
      width: 100%;
    }
  }

  .image-wrapper-- {
    height: auto;
    width: 100%;
    img {
      object-fit: cover;
      height: 100%;
      width: auto;
    }
  }

  .vf-guideline {
    &__first-p {
      margin-bottom: 90px;
    }

    &__examples-p {
      margin-bottom: 10px;
    }

    &__spacer-img {
      margin-bottom: 2rem;
    }
  }
`

export const Spacing = () => {
  return (
    <Container>
      <div className="vf-guideline__container">
        <p className="vf-component__paragraph vf-guideline__first-p">
          The Spacers comes in four sizes, Small, 28px, Medium, 44px, Large 56
          px and Extra Large, 88px. You’ll find them under Utility in the sketch
          library (Insert/document/Utility/Spacers). Important to note is that
          all distances between individual modules are 88px aka the ”Spacer –
          Extra Large” on desktop and 28px on Mobile
        </p>
        <p className="vf-component__paragraph vf-guideline__examples-p">
          <strong>Spacer Extra Large - 88px</strong>
        </p>
        <img
          className="vf-guideline__spacer-img"
          src="/assets/img/spacer__xl.png"
          alt="Visualization of extra large spacer"
        />
        <p className="vf-component__paragraph vf-guideline__examples-p">
          <strong>Spacer Large - 56px</strong>
        </p>
        <img
          className="vf-guideline__spacer-img"
          src="/assets/img/spacer__lg.png"
          alt="Visualization of large spacer"
        />
        <p className="vf-component__paragraph vf-guideline__examples-p">
          <strong>Spacer Medium - 44px</strong>
        </p>
        <img
          className="vf-guideline__spacer-img"
          src="/assets/img/spacer__medium.png"
          alt="Visualization of medium spacer"
        />
        <p className="vf-component__paragraph vf-guideline__examples-p">
          <strong>Spacer Small - 28px</strong>
        </p>
        <img
          className="vf-guideline__spacer-img"
          src="/assets/img/spacer__small.png"
          alt="Visualization of small spacer"
        />
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">Example - Desktop</h2>
        <p className="vf-component__paragraph">
          Example of applied spacers in the Image block Module. To visually see
          the spacers, turn on the eye symbol inside of ”Utility / Spacers”
        </p>
        <div className="image-wrapper">
          <img
            src="/assets/img/spacing__image-1.png"
            alt="Example of usage of spacers"
          />
        </div>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">Example - Mobile</h2>
        <div className="image-wrapper--mobile">
          <img
            src="/assets/img/spacing__image-2.png"
            alt="Example of usage of spacers"
          />
        </div>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">
          Distance between modules – Desktop
        </h2>
        <p className="vf-component__paragraph">
          Important to note is that all distances between individual modules are
          88px aka the "Spacer – Extra Large" on desktop. Exceptions are the
          modules named "media block" with an image that hangs down, then use 2
          of the 88px spacers.
        </p>
        <div className="image-wrapper">
          <img
            src="/assets/img/spacing__image-3.jpeg"
            alt="Example of usage of spacers"
          />
        </div>
      </div>
    </Container>
  )
}
