export const Grid = () => (
  <div className="vf-guideline__container">
    <div>
      <h2 className="vf-component__heading">Desktop 1440px</h2>
      <p className="vf-component__paragraph">Total width: 1128px</p>
      <p className="vf-component__paragraph">Offset: 156px</p>
      <p className="vf-component__paragraph">Columns: 12</p>
      <p className="vf-component__paragraph">Gutter width: 24px</p>
      <p className="vf-component__paragraph">Column width: 72px</p>
    </div>
    <img
      style={{ width: '1440px' }}
      src="/assets/img/grid-1440.png"
      alt="Visualization of desktop grid"
    />
    <div>
      <h2 className="vf-component__heading">Tablet 1024px</h2>
      <p className="vf-component__paragraph">Total width: 944px</p>
      <p className="vf-component__paragraph">Offset: 40px</p>
      <p className="vf-component__paragraph">Columns: 12</p>
      <p className="vf-component__paragraph">Gutter width: 16px</p>
      <p className="vf-component__paragraph">Column width: 64px</p>
      <img
        style={{ width: '1024px' }}
        src="/assets/img/grid-1024.png"
        alt="Visualization of large tablet grid"
      />
    </div>
    <div>
      <h2 className="vf-component__heading">Tablet 768px</h2>
      <p className="vf-component__paragraph">Total width: 648px</p>
      <p className="vf-component__paragraph">Offset: 60px</p>
      <p className="vf-component__paragraph">Columns: 12</p>
      <p className="vf-component__paragraph">Gutter width: 24px</p>
      <p className="vf-component__paragraph">Column width: 32px</p>
      <img
        style={{ width: '768px' }}
        src="/assets/img/grid-768.png"
        alt="Visualization of tablet grid"
      />
    </div>
    <div>
      <h2 className="vf-component__heading">Mobile 375px</h2>
      <p className="vf-component__paragraph">Total width: 333px</p>
      <p className="vf-component__paragraph">Offset: 21px</p>
      <p className="vf-component__paragraph">Columns: 4</p>
      <p className="vf-component__paragraph">Gutter width: 15px</p>
      <p className="vf-component__paragraph">Column width: 72px</p>
      <img
        style={{ width: '375px' }}
        src="/assets/img/grid-375.png"
        alt="Visualization of mobile grid"
      />
    </div>
  </div>
)
