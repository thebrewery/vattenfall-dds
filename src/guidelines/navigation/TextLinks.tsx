export const TextLinks = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        Text links are used in bodytext when you need to link to other content.
      </p>
      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <p style={{ color: 'rgb(25, 100, 163)' }}>Text label</p>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <p style={{ color: 'rgb(30, 50, 79)' }}>Text label</p>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <p style={{ color: 'rgb(30, 50, 79' }}>Text label</p>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <p style={{ color: 'rgb(125, 125, 125' }}>Text label</p>
        </div>
      </div>
    </div>
  )
}
