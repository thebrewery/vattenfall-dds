export const Tabs = () => {
  return (
    <>
      <div className="vf-guideline__container">
        <p className="vf-component__paragraph">
          Our tab bar is available in three different configurations, displayed
          in the sandbox below.
        </p>

        <h2 className="vf-component__heading">Tab bar with Icons</h2>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">Tab bar Mobile / Fixed</h2>

        <p className="vf-component__paragraph">
          Fixed tabs display all tabs on one screen, with each tab at a fixed
          width. The width of each tab is determined by dividing the number of
          tabs by the screen width. They don’t scroll to reveal more tabs; the
          visible tab set represents the only tabs available.
        </p>
      </div>

      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">Tab bar Mobile / Scrollable</h2>

        <p className="vf-component__paragraph">
          Scrollable tabs are displayed without fixed widths. They are
          scrollable, such that some tabs will remain off-screen until scrolled.
        </p>

        <p className="vf-component__paragraph">
          On mobile the text labels has a minimum padding of 14px left and
          right. Content below the Tabs starts with a distance of minimum 16px.
        </p>
      </div>
    </>
  )
}
