export const Search = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        The active state reveals search as a dropdown of 160px height with a
        background color of #EDF1F6. Search text typography should use
        Vattenfall Hall Display Bold in 52px with vertically and be horizontally
        aligned center. On click to type the text disappears and a blinking text
        cursor should appear. After text input search results are displayed
        after pressing return. The user can exit the search by clicking anywhere
        outside or on the “X”.
      </p>
    </div>
  )
}
