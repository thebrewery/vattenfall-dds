import ReactTooltip from 'react-tooltip'

type IconTooltip = {
  id: string
  link?: boolean | string
  title?: string
  text?: string
  fromIntro?: boolean
}

export const IconTooltip = ({
  id,
  link = false,
  title,
  text,
  fromIntro = false,
}: IconTooltip) => (
  <>
    {fromIntro ? (
      <div>
        <div
          className="vf-guideline__intro-example--square"
          data-tip
          data-for={id}
        >
          <span className={id} />
        </div>
        <ReactTooltip id={id} place="top" effect="solid">
          {id}
        </ReactTooltip>

        <a
          // href={link && `/designers/components/icons/${link}`}
          className="vf-component__icons-heading"
        >
          {title}
        </a>
        <p className="vf-component__paragraph">{text}</p>
      </div>
    ) : (
      <>
        <span className={id} data-tip data-for={id} />
        <ReactTooltip id={id} place="top" effect="solid">
          {id}
        </ReactTooltip>
      </>
    )}
  </>
)
