import { IconTooltip } from './IconTooltip'

export const SocialMedia = () => {
  return (
    <div className="vf-guideline__full-container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        Tooltips are used to display the related class to each icon, and won't
        appear when you use them.
      </p>
      <p className="vf-component__paragraph">
        Social media icons do not adhere to our icon style since they are
        logotypes and symbols of external platforms and services.
      </p>
      <div className="vf-guideline__icon-container">
        <IconTooltip id="vf-icon-dropbox" />
        <IconTooltip id="vf-icon-facebook" />
        <IconTooltip id="vf-icon-flickr" />
        <IconTooltip id="vf-icon-instagram" />
        <IconTooltip id="vf-icon-linkedin" />
        <IconTooltip id="vf-icon-pinterest" />
        <IconTooltip id="vf-icon-skype" />
        <IconTooltip id="vf-icon-slidehare" />
        <IconTooltip id="vf-icon-snapchat" />
        <IconTooltip id="vf-icon-twitter" />
        <IconTooltip id="vf-icon-vimeo" />
        <IconTooltip id="vf-icon-whatsapp" />
        <IconTooltip id="vf-icon-xing" />
        <IconTooltip id="vf-icon-youtube" />
      </div>
    </div>
  )
}
