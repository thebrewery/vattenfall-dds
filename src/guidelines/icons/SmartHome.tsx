import { IconTooltip } from './IconTooltip'

export const SmartHome = () => {
  return (
    <div className="vf-guideline__full-container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        Tooltips are used to display the related class to each icon, and won't
        appear when you use them.
      </p>
      <p className="vf-component__paragraph">
        Smart home icons are used to identify products and services for the
        home.
      </p>
      <div className="vf-guideline__icon-container">
        <IconTooltip id="vf-icon-alarm" />
        <IconTooltip id="vf-icon-cloudy" />
        <IconTooltip id="vf-icon-coffee-maker" />
        <IconTooltip id="vf-icon-console" />
        <IconTooltip id="vf-icon-desklamp" />
        <IconTooltip id="vf-icon-desktop-computer" />
        <IconTooltip id="vf-icon-dishwasher" />
        <IconTooltip id="vf-icon-door-closed" />
        <IconTooltip id="vf-icon-door-open" />
        <IconTooltip id="vf-icon-dryer" />
        <IconTooltip id="vf-icon-electric-kettle" />
        <IconTooltip id="vf-icon-electric-stove" />
        <IconTooltip id="vf-icon-electric-tooth-brush" />
        <IconTooltip id="vf-icon-espresso-machine" />
        <IconTooltip id="vf-icon-extractor-hood" />
        <IconTooltip id="vf-icon-fairy-lights" />
        <IconTooltip id="vf-icon-freezer" />
        <IconTooltip id="vf-icon-fridge-freezer" />
        <IconTooltip id="vf-icon-fridge" />
        <IconTooltip id="vf-icon-gas-stove" />
        <IconTooltip id="vf-icon-hair-straightener" />
        <IconTooltip id="vf-icon-hairdryer" />
        <IconTooltip id="vf-icon-indution-stove" />
        <IconTooltip id="vf-icon-kettle" />
        <IconTooltip id="vf-icon-kitchen-machine" />
        <IconTooltip id="vf-icon-laptop" />
        <IconTooltip id="vf-icon-light-switch" />
        <IconTooltip id="vf-icon-lightbulb-step-1" />
        <IconTooltip id="vf-icon-lightbulb-step-2" />
        <IconTooltip id="vf-icon-lightbulb-step-3" />
        <IconTooltip id="vf-icon-lightbulb-step-4" />
        <IconTooltip id="vf-icon-lightbulb-step-5" />
        <IconTooltip id="vf-icon-microwave" />
        <IconTooltip id="vf-icon-moon" />
        <IconTooltip id="vf-icon-outdoor-lamp" />
        <IconTooltip id="vf-icon-patio-heater" />
        <IconTooltip id="vf-icon-phone-charging" />
        <IconTooltip id="vf-icon-pressing-iron" />
        <IconTooltip id="vf-icon-radiator" />
        <IconTooltip id="vf-icon-rain" />
        <IconTooltip id="vf-icon-router" />
        <IconTooltip id="vf-icon-smart-plug" />
        <IconTooltip id="vf-icon-smart-thermostat" />
        <IconTooltip id="vf-icon-smoke-alarm" />
        <IconTooltip id="vf-icon-snow" />
        <IconTooltip id="vf-icon-stove-with-grill" />
        <IconTooltip id="vf-icon-sun" />
        <IconTooltip id="vf-icon-surveillance" />
        <IconTooltip id="vf-icon-table-lamp-two" />
        <IconTooltip id="vf-icon-table-lamp" />
        <IconTooltip id="vf-icon-thunder" />
        <IconTooltip id="vf-icon-tv-remote" />
        <IconTooltip id="vf-icon-vacuum-cleaner" />
        <IconTooltip id="vf-icon-ventilator" />
        <IconTooltip id="vf-icon-washing-machine" />
        <IconTooltip id="vf-icon-window-blinds" />
        <IconTooltip id="vf-icon-window-closed" />
        <IconTooltip id="vf-icon-window-open" />
        <IconTooltip id="vf-icon-window" />
      </div>
    </div>
  )
}
