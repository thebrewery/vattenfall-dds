import styled from 'styled-components'

const Container = styled.div`
  .vf-guideline {
    &__square {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 100%;
      height: 20vw;
      margin-bottom: 28px;
    }

    &__usage-grey-example {
      background-color: #f0f0f0;
    }

    &__usage-beige-example {
      background-color: #fcf1ec;
    }

    &__usage-watery-example {
      background-image: url(/assets/img/water.png);
      background-position: center center;
      background-size: cover;
      color: rgb(255, 255, 255);
    }

    &__usage-examples {
      display: flex;
      flex-direction: row;

      div {
        flex: 1;
        padding-left: 12px;
        padding-right: 12px;
      }

      div:first-child {
        padding-left: 0;
      }

      div:last-child {
        padding-right: 0;
      }
    }
  }

  .vf-icon-hydro::before {
    font-size: 6.875rem;
  }
`

export const Usage = () => {
  return (
    <Container>
      <div className="vf-guideline__full-container">
        <div className="vf-guideline__usage-examples">
          <div>
            <div className="vf-guideline__square vf-guideline__usage-grey-example vf-tooltip vf-tooltip--blue-solid vf-icon-hydro" />
            <p className="vf-component__paragraph">
              Icons should always be in black when placed on a white or grey
              background.
            </p>
          </div>

          <div>
            <div className="vf-guideline__square vf-guideline__usage-beige-example vf-tooltip vf-tooltip--blue-solid vf-icon-hydro" />
            <p className="vf-component__paragraph">
              The same applies when icons are placed on other light background
              colours.
            </p>
          </div>
          <div>
            <div className="vf-guideline__square vf-guideline__usage-watery-example vf-tooltip vf-tooltip--blue-solid vf-icon-hydro" />
            <p className="vf-component__paragraph">
              When placed on an image - icons should primarily be white, but
              depending on the image could also allowed to be black to make the
              icon legible.
            </p>
          </div>
        </div>
      </div>
      <div className="vf-guideline__container">
        <div className="vf-guideline__usage-examples">
          <div>
            <img
              src="/assets/img/usage-image-1.png"
              alt="Vattenfall imagery crossed over on white background"
            />
            <p className="vf-component__paragraph">
              When icons are in a group, they should be used in the same size to
              ensure that they have the same line weight.
            </p>
          </div>
          <div>
            <img
              src="/assets/img/usage-image-2.png"
              alt="Vattenfall imagery crossed over on white background"
            />
            <p className="vf-component__paragraph">
              Do not use icons in colour.
            </p>
          </div>
          <div>
            <img
              src="/assets/img/usage-image-3.png"
              alt="Vattenfall imagery crossed over on white background"
            />
            <p className="vf-component__paragraph">
              Do not use icons in colour.
            </p>
          </div>
          <div>
            <img
              src="/assets/img/usage-image-4.png"
              alt="Vattenfall imagery crossed over on white background"
            />
            <p className="vf-component__paragraph">
              Do not alter or distort the icons.
            </p>
          </div>
        </div>
      </div>
    </Container>
  )
}
