import { IconTooltip } from './IconTooltip'

export const Action = () => {
  return (
    <div className="vf-guideline__full-container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        Tooltips are used to display the related class to each icon, and won't
        appear when you use them.
      </p>
      <p className="vf-component__paragraph">
        Action icons are used to signal and describe interaction.
      </p>
      <div className="vf-guideline__icon-container">
        <IconTooltip id="vf-icon-activity-log" />
        <IconTooltip id="vf-icon-add-contact" />
        <IconTooltip id="vf-icon-alert" />
        <IconTooltip id="vf-icon-align-center" />
        <IconTooltip id="vf-icon-align-left" />
        <IconTooltip id="vf-icon-app-settings" />
        <IconTooltip id="vf-icon-arrow-down-left" />
        <IconTooltip id="vf-icon-arrow-down-right" />
        <IconTooltip id="vf-icon-arrow-down" />
        <IconTooltip id="vf-icon-arrow-left" />
        <IconTooltip id="vf-icon-arrow-right" />
        <IconTooltip id="vf-icon-arrow-up-left" />
        <IconTooltip id="vf-icon-arrow-up-right" />
        <IconTooltip id="vf-icon-arrow-up" />
        <IconTooltip id="vf-icon-autostart" />
        <IconTooltip id="vf-icon-blog" />
        <IconTooltip id="vf-icon-call" />
        <IconTooltip id="vf-icon-camera" />
        <IconTooltip id="vf-icon-change-availability" />
        <IconTooltip id="vf-icon-chat" />
        <IconTooltip id="vf-icon-checkbox" />
        <IconTooltip id="vf-icon-download" />
        <IconTooltip id="vf-icon-download-pdf" />
        <IconTooltip id="vf-icon-edit" />
        <IconTooltip id="vf-icon-elipsis" />
        <IconTooltip id="vf-icon-favourite" />
        <IconTooltip id="vf-icon-filter" />
        <IconTooltip id="vf-icon-gallery" />
        <IconTooltip id="vf-icon-help" />
        <IconTooltip id="vf-icon-in-progress" />
        <IconTooltip id="vf-icon-information" />
        <IconTooltip id="vf-icon-list-view" />
        <IconTooltip id="vf-icon-locked" />
        <IconTooltip id="vf-icon-mail" />
        <IconTooltip id="vf-icon-news" />
        <IconTooltip id="vf-icon-off" />
        <IconTooltip id="vf-icon-open-new-window" />
        <IconTooltip id="vf-icon-pause" />
        <IconTooltip id="vf-icon-phone" />
        <IconTooltip id="vf-icon-pin" />
        <IconTooltip id="vf-icon-play" />
        <IconTooltip id="vf-icon-power-outage" />
        <IconTooltip id="vf-icon-request" />
        <IconTooltip id="vf-icon-restart" />
        <IconTooltip id="vf-icon-send" />
        <IconTooltip id="vf-icon-settings" />
        <IconTooltip id="vf-icon-share" />
        <IconTooltip id="vf-icon-shop" />
        <IconTooltip id="vf-icon-sign-in" />
        <IconTooltip id="vf-icon-sign-out" />
        <IconTooltip id="vf-icon-sort" />
        <IconTooltip id="vf-icon-tag" />
        <IconTooltip id="vf-icon-trash" />
        <IconTooltip id="vf-icon-turn-device" />
        <IconTooltip id="vf-icon-unlocked" />
        <IconTooltip id="vf-icon-user" />
        <IconTooltip id="vf-icon-zoom-in" />
        <IconTooltip id="vf-icon-zoom-out" />
      </div>
    </div>
  )
}
