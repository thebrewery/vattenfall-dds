import styled from 'styled-components'
import { EyeCatcher } from '../../react/components/eyecatcher'
import { formatAssetUrl } from '../../utils/api'
import { IconTooltip } from './IconTooltip'

const Container = styled.div`
  position: relative;

  .vf-guideline {
    &__intro-img {
      height: auto;
      width: 100%;
    }

    &__intro-examples {
      display: grid;
      grid-template-columns: repeat(3, 1fr);
      grid-gap: 12px;
    }

    &__intro-example--square {
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: #f0f0f0;
      width: 20vw;
      height: 20vw;
      margin-bottom: 28px;
    }
  }

  .vf-icon-more::before,
  .vf-icon-shop::before,
  .vf-icon-sun::before,
  .vf-icon-twitter::before,
  .vf-icon-router::before,
  .vf-icon-private-charging::before {
    font-size: 6.875rem;
  }
`

export const Intro = () => {
  return (
    <Container>
      <EyeCatcher
        href={formatAssetUrl(
          '/uploads/Vattenfall_All_Icons_2024_01_12_6c21f20f0a.zip'
        )}
        download
        useDownloadIcon
      >
        <p>Download the icons</p>
      </EyeCatcher>
      <p className="vf-component__paragraph">
        The icons mirror our typeface – engineered and human, confident and
        determined.
      </p>
      <p className="vf-component__paragraph">
        They are created to be simple, yet powerful, representations to ease and
        help understanding of our services, products and ideas. Always used in
        moderation, and never animated, the icons can together with simple
        images create more clarity and messaging power than illustrations.
      </p>
      <p className="vf-component__paragraph">
        Icons should always be in black when placed on a white or grey
        background. The same applies when icons are placed on other light
        background colours.
      </p>
      <p className="vf-component__paragraph">
        When placed on an image - icons should primarily be white, but depending
        on the image they are also allowed to be black.
      </p>
      <p className="vf-component__paragraph">
        When icons are in a group, use them in the same size to ensure that they
        have the same line weight
      </p>
      <div className="vf-component__paragraph">
        - Do not use icons in colour. <br />
        - Do not use outlined icons. <br />- Do not alter or distort the icons.
      </div>
      <img
        src="/assets/img/icons_first.png"
        className="vf-guideline__intro-img"
        alt="A few of Vattenfall's icons"
      />
      <h2 className="vf-component__heading">Icon Categories</h2>
      <p className="vf-component__paragraph">
        There are 6 different categories of icons: Navigation, Action,
        Identification, Social media, Smart home and InCharge.
      </p>
      <div className="vf-guideline__intro-examples">
        <IconTooltip
          fromIntro={true}
          id="vf-icon-more"
          title="Navigation"
          link="navigation"
          text="Navigation icons are used as part of the UI."
        />

        <IconTooltip
          fromIntro={true}
          id="vf-icon-shop"
          title="Action"
          link="action"
          text="Action icons are used to signal and describe interaction."
        />

        <IconTooltip
          fromIntro={true}
          id="vf-icon-sun"
          title="Identification"
          link="Identification"
          text="Identification icons are used to identify a service or product."
        />

        <IconTooltip
          fromIntro={true}
          id="vf-icon-twitter"
          title="Social media"
          link="social-media"
          text="Social media icons do not adhere to our icon style since they are
						logotypes and symbol of other platforms and services."
        />

        <IconTooltip
          fromIntro={true}
          id="vf-icon-router"
          title="Smart home"
          link="smart-home"
          text="Smart home icons are used to identify products and services for
						the home."
        />

        <IconTooltip
          fromIntro={true}
          id="vf-icon-private-charging"
          title="InCharge"
          text="InCharge icons are specific icons created for the InCharge brands
						services and needs"
        />
      </div>
    </Container>
  )
}
