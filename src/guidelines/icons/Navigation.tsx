import { IconTooltip } from './IconTooltip'

export const Navigation = () => {
  return (
    <div className="vf-guideline__full-container">
      <h3 className="vf-component__tiny-heading vf-utility__warning">
        DISCLAIMER
      </h3>
      <p className="vf-component__paragraph">
        Tooltips are used to display the related class to each icon, and won't
        appear when you use them.
      </p>
      <p className="vf-component__paragraph">
        Navigation icons are used as part of the UI.
      </p>
      <div className="vf-guideline__icon-container">
        <IconTooltip id="vf-icon-calendar-1" />
        <IconTooltip id="vf-icon-calendar-2" />
        <IconTooltip id="vf-icon-calendar-3" />
        <IconTooltip id="vf-icon-calendar-4" />
        <IconTooltip id="vf-icon-calendar-5" />
        <IconTooltip id="vf-icon-calendar-6" />
        <IconTooltip id="vf-icon-calendar-7" />
        <IconTooltip id="vf-icon-calendar-8" />
        <IconTooltip id="vf-icon-calendar-9" />
        <IconTooltip id="vf-icon-calendar-10" />
        <IconTooltip id="vf-icon-calendar-11" />
        <IconTooltip id="vf-icon-calendar-12" />
        <IconTooltip id="vf-icon-calendar-13" />
        <IconTooltip id="vf-icon-calendar-14" />
        <IconTooltip id="vf-icon-calendar-15" />
        <IconTooltip id="vf-icon-calendar-16" />
        <IconTooltip id="vf-icon-calendar-17" />
        <IconTooltip id="vf-icon-calendar-18" />
        <IconTooltip id="vf-icon-calendar-19" />
        <IconTooltip id="vf-icon-calendar-20" />
        <IconTooltip id="vf-icon-calendar-21" />
        <IconTooltip id="vf-icon-calendar-22" />
        <IconTooltip id="vf-icon-calendar-23" />
        <IconTooltip id="vf-icon-calendar-24" />
        <IconTooltip id="vf-icon-calendar-25" />
        <IconTooltip id="vf-icon-calendar-26" />
        <IconTooltip id="vf-icon-calendar-27" />
        <IconTooltip id="vf-icon-calendar-28" />
        <IconTooltip id="vf-icon-calendar-29" />
        <IconTooltip id="vf-icon-calendar-30" />
        <IconTooltip id="vf-icon-calendar-31" />
        <IconTooltip id="vf-icon-check" />
        <IconTooltip id="vf-icon-close" />
        <IconTooltip id="vf-icon-down" />
        <IconTooltip id="vf-icon-up" />
        <IconTooltip id="vf-icon-left" />
        <IconTooltip id="vf-icon-right" />
        <IconTooltip id="vf-icon-home" />
        <IconTooltip id="vf-icon-less-info" />
        <IconTooltip id="vf-icon-link" />
        <IconTooltip id="vf-icon-menu" />
        <IconTooltip id="vf-icon-more" />
        <IconTooltip id="vf-icon-qr-code" />
        <IconTooltip id="vf-icon-rss" />
        <IconTooltip id="vf-icon-search" />
      </div>
    </div>
  )
}
