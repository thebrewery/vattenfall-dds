export const Horizontal = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        The clearspace around the linear logotype is defined by the Vattenfall
        Symbol. The symbol is 1X, and the clearspace is 1x around all sides of
        the logotype.
      </p>
      <div className="vf-guideline__logotype-examples">
        <img src="/assets/img/horizontal_1.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/horizontal_2.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/horizontal_3.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/horizontal_4.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/horizontal_5.png" alt="Vattenfall's logotype" />
      </div>
    </div>
  )
}
