export const KeyPrinciples = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        1. The Vattenfall logotype consists of two parts, the wordmark
        "Vattenfall" and the yellow and blue symbol.
      </p>
      <p className="vf-component__paragraph">
        2. Whenever possible, the logotype should be placed centered on a
        format. This is the best way to visually place the logotype on an
        application.
      </p>
      <p className="vf-component__paragraph">
        3. The logotype should always be placed in a prominent position. For
        further guidance, please see the placement section, size section and the
        templates for common formats.
      </p>
      <p className="vf-component__paragraph">
        4. In certain applications and circumstances, the symbol may be used on
        its own, or decoupled from the wordmark, e.g., as an avatar in social
        media. As a general rule if you are uncertain, please don’t use the
        symbol on its own.
      </p>
      <p className="vf-component__paragraph">
        5. The stacked logotype should only be used on applications where it can
        be centered on the setting area. Please see the Clearspace and Placement
        sections for more guidance.
      </p>
      <p className="vf-component__paragraph">
        6. The clearspace around the linear logotype is equal to half of the
        size of the Vattenfall symbol. Please see the Clearspace section for
        more details.
      </p>
      <div className="vf-guideline__logotype-examples">
        <div>
          <img
            src="/assets/img/keyprinciples_1.png"
            alt="Vattenfall's logotype"
          />
          <p className="vf-component__paragraph">
            <strong>1. Wordmark and symbol</strong>
          </p>
        </div>
        <div>
          <img
            src="/assets/img/keyprinciples_2.png"
            alt="Vattenfall's logotype"
          />
          <p className="vf-component__paragraph">
            <strong>2. Placed centered</strong>
          </p>
        </div>
        <div>
          <img
            src="/assets/img/keyprinciples_3.png"
            alt="Vattenfall's logotype"
          />
          <p className="vf-component__paragraph">
            <strong>3. Bold in placement</strong>
          </p>
        </div>
        <div>
          <img
            src="/assets/img/keyprinciples_4.png"
            alt="Vattenfall's logotype"
          />
          <p className="vf-component__paragraph">
            <strong>4. Avatar symbol</strong>
          </p>
        </div>
        <div>
          <img
            src="/assets/img/keyprinciples_5.png"
            alt="Vattenfall's logotype"
          />
          <p className="vf-component__paragraph">
            <strong>5. Centered on the setting</strong>
          </p>
        </div>
        <div>
          <img
            src="/assets/img/keyprinciples_6.png"
            alt="Vattenfall's logotype"
          />
          <p className="vf-component__paragraph">
            <strong>6. Clearspace</strong>
          </p>
        </div>
      </div>
    </div>
  )
}
