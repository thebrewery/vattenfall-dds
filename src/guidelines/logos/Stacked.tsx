export const Stacked = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        The clearspace around the logotype is half of the symbols height (1X).
        The clearspace is 1X around all sides of the logotype.
      </p>
      <div className="vf-guideline__logotype-examples">
        <img src="/assets/img/stacked_1.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/stacked_2.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/stacked_3.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/stacked_4.png" alt="Vattenfall's logotype" />
        <img src="/assets/img/stacked_5.png" alt="Vattenfall's logotype" />
      </div>
    </div>
  )
}
