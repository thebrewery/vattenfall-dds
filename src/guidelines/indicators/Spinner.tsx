import styled from 'styled-components'

const Container = styled.div``

export const Spinner = () => {
  return (
    <Container>
      <div className="vf-guideline__container">
        <p className="vf-component__paragraph">
          A spinner animation is used to indicate an ongoing loading process.
        </p>
        <div className="vf-component__paragraph">
          - When centered on the screen upon initial load they indicate the
          loading of screen content. <br />- When used above or below existing
          content, they draw attention to where new content will appear.
        </div>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">Size and Appearance</h2>
        <p className="vf-component__paragraph">
          The VF_Spinner should always be horizontal and vertically centered in
          the space it should display in. Recommended sizes are 24px, 44px,
          88px, the default should be 44px and the maximum size allowed is 88px.
          Background should always be white with an opacity of 92%.
        </p>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">Lottie data file locations</h2>
        <p className="vf-component__paragraph">
          The spinner Lottie data can be loaded from the following urls:
        </p>
        <ol className="vf-ul">
          <li>
            <code>
              @vf-dds/vf-dds-vanilla/src/examples/indicators/spinner/lottie/spinner.json
            </code>
            <br />
            <em>(From package)</em>
          </li>
          <li>
            <code>@vf-dds/vf-dds-vanilla/src/assets/lottie/spinner.json</code>
            <br />
            <em>({'From package. Available in version >= 0.12.8'})</em>
          </li>
          <li>
            <code>
              https://assets1.lottiefiles.com/packages/lf20_pk8zaqti.json
            </code>
            <br />
            <em>(From Lottiefiles)</em>
          </li>
        </ol>
      </div>
    </Container>
  )
}
