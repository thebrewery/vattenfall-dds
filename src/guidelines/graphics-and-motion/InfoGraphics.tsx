import styled from 'styled-components'
import { SquareColours } from '../../components/SquareColours'

const InfoGraphicsContainer = styled.div`
  img {
    width: 100%;
  }

  .info-graphics__duo-example {
    display: grid;
    grid-template-columns: 1fr 1fr;

    div {
      align-items: center;
      display: flex;
      flex-direction: column;
    }
  }
`

export const InfoGraphics = () => (
  <InfoGraphicsContainer>
    <h2 className="vf-component__heading">Information graphics style</h2>
    <p className="vf-component__paragraph">
      Sharing information and data in a clear and confident way is part of the
      Vattenfall overall tonality, and graphs are a key graphic elements to make
      data come alive. Vattenfall has a style for information graphics that is
      bold, simple and clear. To create the graphs, the Vattenfall colours can
      be used in different ways – please read more regarding colour usage below.
    </p>
    <img
      src="/assets/img/info_graphic_example_1.png"
      alt="Image showing different graphs, following Vattenfall's brand lines"
    />
    <h2 className="vf-component__heading">Types of graphs</h2>
    <p className="vf-component__paragraph">
      There are different types of charts and graphs to use – pie charts, bar
      graphs and lines graphs. They are generally used for different purposes.
      Pie charts show how a whole is divided into different parts, bar graphs
      show numbers that are independent of each other and line graphs show
      numbers that have changed over time.
    </p>
    <div className="vf-guideline__graphics">
      <div>
        <img src="/assets/img/chart_1.png" alt="Pie chart example" />
        <p>
          <strong>Example of a pie chart</strong>
        </p>
      </div>
      <div>
        <img src="/assets/img/chart_2.png" alt="Bar graph example" />
        <p>
          <strong>Example of a bar graph</strong>
        </p>
      </div>
      <div>
        <img src="/assets/img/chart_3.png" alt="Line graph example" />
        <p>
          <strong>Example of a line graph</strong>
        </p>
      </div>
    </div>
    <h2 className="vf-component__heading">Graph colours</h2>
    <p className="vf-component__paragraph">
      When creating information graphics mainly use the secondary colours, but
      the primary colours are also allowed. There is also a colour palette
      specifically made for the different energy sources. Please look further
      down on this page to see how to use these colours and to find the colour
      codes. The colour black is only used in text and lines, not as a colour in
      the graph or chart.
    </p>
    <div className="vf-guideline__graphics">
      <div>
        <img
          src="/assets/img/graph_colours_1.png"
          alt="Pie chart of Vattenfall's secondary colours"
        />
        <p>
          <strong>Secondary colours</strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/graph_colours_2.png"
          alt="Pie chart of Vattenfall's primary colours"
        />
        <p>
          <strong>Primary colours</strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/graph_colours_3.png"
          alt="Pie chart of Vattenfall's energy source colours"
        />
        <p>
          <strong>Energy source colours</strong>
        </p>
      </div>
    </div>
    <h2 className="vf-component__heading">Colours codes</h2>
    <h3 className="vf-component__small-heading">Primary colour codes</h3>
    <div className="vf-guideline__graphics">
      <SquareColours
        hex="#FFDA00"
        rgb="255, 218, 0"
        title="Vattenfall Yellow"
      />
      <SquareColours hex="#2071B5" rgb="32, 113, 181" title="Vattenfall Blue" />
      <SquareColours hex="#4E4B4B" rgb="78, 75, 72" title="Vattenfall Grery" />
    </div>
    <h3 className="vf-component__small-heading">Secondary colour codes</h3>
    <div className="vf-guideline__graphics">
      <SquareColours
        hex="#235A60"
        rgb="35, 90, 96"
        title="Vattenfall Dark Green"
      />
      <SquareColours
        hex="#22314C"
        rgb="34, 49, 76"
        title="Vattenfall Dark Blue"
      />

      <SquareColours hex="#C1386A" rgb="193, 56, 106" title="Vattenfall Pink" />

      <SquareColours
        hex="#63BC80"
        rgb="99, 188, 128"
        title="Vattenfall Green"
      />

      <SquareColours hex="#E64C2E" rgb="230, 76, 46" title="Vattenfall Red" />

      <SquareColours
        hex="#9365BC"
        rgb="147, 101, 188"
        title="Vattenfall Purple"
      />

      <SquareColours
        hex="#7A2B49"
        rgb="122, 43, 73"
        title="Vattenfall Dark Purple"
      />
    </div>
    <h2 className="vf-component__heading">Energy source colours</h2>
    <p className="vf-component__paragraph">
      Recommended colours for specific energy sources have been developed mainly
      to be used for information graphs. Please note that this is a
      recommendation and not a rule. Colour coding for regulation compliance use
      the categories: Renewable, Nuclear, District heating and Fossil.
    </p>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#2DA55D" rgb="46, 165, 93" title="Hydro" />
      <SquareColours hex="#4FCC51" rgb="79, 204, 81" title="Wind" />
      <SquareColours hex="#81E0A8" rgb="129, 224, 168" title="Solar" />
      <SquareColours hex="#375E4E" rgb="55, 94, 78" title="Biomass" />
    </div>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#E88A74" rgb="232, 138, 116" title="Coal" />

      <SquareColours hex="#D85067" rgb="216, 80, 103" title="Gas" />
      <SquareColours hex="#213D5E" rgb="33, 61, 94" title="Nuclear" />
      <SquareColours
        hex="#A376CC"
        rgb="163, 118, 204"
        title="District heating"
      />
    </div>
    <img
      src="/assets/img/energySourceColours.png"
      alt="Bar chart with statistics using Vattenfall's energy source colours"
    />
    <h2 className="vf-component__heading">Colour usage</h2>
    <p className="vf-component__paragraph">
      When creating information graphics mainly use the secondary colours, but
      the primary colours are also allowed. Either create graphics using colour
      tones in the same colour range or use colours in the same category:
      secondary, primary or energy source colours.
    </p>
    <div className="info-graphics__duo-example">
      <div>
        <img
          src="/assets/img/colour_usage_1.png"
          alt="Pie chart with Vattenfall's secondary colours"
        />
        <p className="vf-component__paragraph">
          <strong>Secondary colours</strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/colour_usage_2.png"
          alt="Pie chart with Vattenfall's primary colours"
        />
        <p className="vf-component__paragraph">
          <strong>Primary colours</strong>
        </p>
      </div>
    </div>
    <p className="vf-component__paragraph">
      Always make sure that the colours used harmonize with each other. Use as
      few colours as possible to make graphs visibly balanced.
    </p>
    <div className="vf-guideline__graphics">
      <div>
        <img
          src="/assets/img/colour_usage_3.png"
          alt="Pie chart with Vattenfall's energy source colours"
        />
        <p className="vf-component__paragraph">
          <strong>
            Example using primary colors and energy source colors in tones.
          </strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/colour_usage_4.png"
          alt="Pie chart with Vattenfall's energy source colours"
        />
        <p className="vf-component__paragraph">
          <strong>
            Example using energy source colors that harmonize with each other.
          </strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/colour_usage_5.png"
          alt="Pie chart with Vattenfall's secondary colours"
        />
        <p className="vf-component__paragraph">
          <strong>
            Example using secondary colors that harmonize with each other.
          </strong>
        </p>
      </div>
    </div>
    <p className="vf-component__paragraph">
      Avoid mixing colors from different categories. Also avoid creating graphs
      with colors that have too much contrast against each other.
    </p>
    <div className="vf-guideline__graphics">
      <div>
        <img
          src="/assets/img/avoid1.png"
          alt="Example using primary colours but crossed over"
        />
        <p className="vf-component__paragraph">
          <strong>
            Example using primary colors and energy source colors in tones.
          </strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/avoid2.png"
          alt="Examples of wrong usage of energy source colours"
        />
        <p className="vf-component__paragraph">
          <strong>
            Example using energy source colors that harmonize with each other.
          </strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/avoid3.png"
          alt="Example of wrong usage of secondary colours"
        />
        <p className="vf-component__paragraph">
          <strong>
            Example using secondary colors that harmonize with each other.
          </strong>
        </p>
      </div>
    </div>
    <h2 className="vf-component__heading">Colour tones for web</h2>
    <p className="vf-component__paragraph">
      When creating 1-coloured graphs, you should primarily use these
      percent-values to differentiate sections of the graph. This is exemplified
      below but you can use any of the primary or secondary colours as a base.
    </p>

    <h3 className="vf-component__small-heading">Hydro</h3>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#2DA55D" rgb="45, 165, 93" title="100%" />
      <SquareColours hex="#6CC08D" rgb="108, 192, 141" title="70%" />
      <SquareColours hex="#96D2AE" rgb="150, 210, 174" title="50%" />
      <SquareColours hex="#C0E4CE" rgb="192, 228, 206" title="30%" />
      <SquareColours hex="#EAF6EE" rgb="234, 246, 238" title="10%" />
    </div>

    <h3 className="vf-component__small-heading">Wind</h3>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#4FCC51" rgb="79, 204, 81" title="100%" />
      <SquareColours hex="#83DB85" rgb="131, 219, 133" title="70%" />
      <SquareColours hex="#A7E5A8" rgb="167, 229, 168" title="50%" />
      <SquareColours hex="#CAEFCA" rgb="202, 239, 202" title="30%" />
      <SquareColours hex="#EAF6EE" rgb="237, 239, 202" title="10%" />
    </div>

    <h3 className="vf-component__small-heading">Solar</h3>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#81E0A8" rgb="129, 224, 168" title="100%" />
      <SquareColours hex="#A6E9C2" rgb="166, 233, 194" title="70%" />
      <SquareColours hex="#C0EFD4" rgb="192, 239, 212" title="50%" />
      <SquareColours hex="#D9F5E4" rgb="217, 245, 228" title="30%" />
      <SquareColours hex="#F2FBF6" rgb="242, 251, 246" title="10%" />
    </div>

    <h3 className="vf-component__small-heading">Biomass</h3>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#375E4E" rgb="55, 94, 78" title="100%" />
      <SquareColours hex="#738E83" rgb="115, 142, 131" title="70%" />
      <SquareColours hex="#9BAEA6" rgb="155, 174, 166" title="50%" />
      <SquareColours hex="#C3CEC9" rgb="195, 206, 201" title="30%" />
      <SquareColours hex="#EBEEED" rgb="235, 238, 237" title="10%" />
    </div>

    <h3 className="vf-component__small-heading">Coal</h3>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#E88A74" rgb="232, 138, 116" title="100%" />
      <SquareColours hex="#EEAC9D" rgb="238, 172, 157" title="70%" />
      <SquareColours hex="#F3C4B9" rgb="243, 196, 185" title="50%" />
      <SquareColours hex="#F8DBD5" rgb="248, 219, 213" title="30%" />
      <SquareColours hex="#FCF3F1" rgb="251, 237, 239" title="10%" />
    </div>

    <h3 className="vf-component__small-heading">Nuclear</h3>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#213D5E" rgb="33, 61, 94" title="100%" />
      <SquareColours hex="#63778E" rgb="99, 119, 142" title="70%" />
      <SquareColours hex="#909EAE" rgb="144, 158, 174" title="50%" />
      <SquareColours hex="#BCC4CE" rgb="188, 196, 206" title="30%" />
      <SquareColours hex="#E8EBEE" rgb="232, 235, 238" title="10%" />
    </div>

    <h3 className="vf-component__small-heading">District heating</h3>
    <div className="vf-guideline__graphics">
      <SquareColours hex="#A376CC" rgb="163, 118, 204" title="100%" />
      <SquareColours hex="#BE9FDB" rgb="190, 159, 219" title="70%" />
      <SquareColours hex="#D1BAE5" rgb="209, 186, 229" title="50%" />
      <SquareColours hex="#E3D5EF" rgb="227, 213, 239" title="30%" />
      <SquareColours hex="#F5F1F9" rgb="245, 241, 249" title="10%" />
    </div>

    <h2 className="vf-component__heading">Background colours</h2>
    <p className="vf-component__paragraph">
      Always place information graphics on white or Vattenfall Light Grey
      background. Do not place multi-coloured graphs on coloured backgrounds.
    </p>
    <div className="info-graphics__duo-example">
      <div>
        <img
          src="/assets/img/backgroundOk.png"
          alt="Okay usage of graph on background color"
        />
        <p className="vf-component__paragraph">
          <strong>
            Graphs can be placed on both white and Vattenfall Grey backgrounds
          </strong>
        </p>
      </div>
      <div>
        <img
          src="/assets/img/backgroundBad.png"
          alt="Bad usage of graph on background color"
        />
        <p className="vf-component__paragraph">
          <strong>
            Do not place multi-colored graphs on colored backgrounds.
          </strong>
        </p>
      </div>
    </div>

    <h2 className="vf-component__heading">Best practice</h2>
    <p className="vf-component__paragraph">
      Always place information graphics on white or Vattenfall Light Grey
      background. Do not place multi-coloured graphs on coloured backgrounds.
    </p>
    <img
      src="/assets/img/best-practise.jpeg"
      alt="Example of the best practice of background colours"
    />

    <h2 className="vf-component__heading">Infographics - Motion</h2>
    <p className="vf-component__paragraph">
      Infographics refers to the visualization of data in graphs, tables and
      numbers. Through our motion ID philosophy of “ease-in-out”, infographics
      become the most powerful examples of our identity in motion as we here
      allow a graph or number to develop in a smooth, controlled “easing”
      movement with a high-quality perception.
    </p>

    <p className="vf-component__paragraph">
      To read more about the “ease-in-out” motion ID philosophy, please see the
      description and the examples in the Video & Motion guidelines chapter:
      <a
        href="https://brandtoolbox.vattenfall.com/Styleguide/brandtwo/#page/70DBC29E-9802-4DDD-9937424FDCB53C25"
        target="_blank"
        rel="noreferrer"
      >
        https://brandtoolbox.vattenfall.com/Styleguide/brandtwo/#page/70DBC29E-9802-4DDD-9937424FDCB53C25
      </a>
    </p>
  </InfoGraphicsContainer>
)
