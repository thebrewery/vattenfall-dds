import styled from 'styled-components'

const IllustrationsContainer = styled.div`
  img {
    width: 100%;
  }
`

export const Illustrations = () => (
  <IllustrationsContainer>
    <h2 className="vf-component__heading">Functional</h2>
    <p className="vf-component__paragraph">
      This is our main illustration style and the most frequently used. The
      inspiration comes from technical drawings and blueprints. It is a
      simplified illustration style where the main task is to support the
      business – looking inside the energy techniques, explaining complex and
      detailed systems of our product and services. And this by only using line
      illustrations with a few colours and details. The style does not compete
      with but complements our bold icons and typeface.
    </p>
    <h2 className="vf-component__heading">Key Principles</h2>
    <p className="vf-component__paragraph">
      In illustrations, the full scale of colours may be used, yet moderately.
      Strive not to use more than three colours in a single illustration to
      avoid making it look too colourful and off brand.
    </p>
    <ol className="vf-component__paragraph">
      <li>Always apply the illustrations on white.</li>
      <li>
        Use the Vattenfall Light Blue or Vattenfall Light Grey background.
      </li>
      <li>
        Do not place illustrations on background colours like the Vattenfall
        Light Red.
      </li>
    </ol>
    <ol className="vf-component__paragraph">
      <li>
        The illustrations are available in two different perspectives – front
        and isometric.
      </li>
      <li>
        Keep the stroke light and consistent throughout the full illustration
      </li>
      <li>
        Functional style can be used both as still illustrations and animated.
      </li>
    </ol>
    <div className="vf-guideline__graphics">
      <div>
        <img
          src="/assets/img/whitebg.png"
          alt="White background with imagery"
        />
        <p className="vf-component__paragraph">White Background</p>
      </div>
      <div>
        <img
          src="/assets/img/lightbg.png"
          alt="Light background with imagery"
        />
        <p className="vf-component__paragraph">Light Background</p>
      </div>
      <div>
        <img
          src="/assets/img/notonred.png"
          alt="Not on background with imagery"
        />
        <p className="vf-component__paragraph">Not on red background</p>
      </div>
      <div>
        <img
          src="/assets/img/lightbluebg.png"
          alt="White background with imagery"
        />
        <p className="vf-component__paragraph">Light blue Background</p>
      </div>
      <div>
        <img
          src="/assets/img/Frontperspective.png"
          alt="White background with imagery"
        />
        <p className="vf-component__paragraph">Front Perspective</p>
      </div>
    </div>

    <h2 className="vf-component__heading">Grid</h2>
    <p className="vf-component__paragraph">
      To work with new isometric illustrations, you can use the grid below.
      You’ll find it under Utility / Isometric Grid – Illustrations.
    </p>
    <img
      style={{ width: '50%' }}
      src="/assets/img/IsometricGrid–Illustrations.png"
      alt="Pattern of a grid"
    />
    <h2 className="vf-component__heading">Support</h2>
    <p className="vf-component__paragraph">
      All approved illustrations and animations can be found in the{' '}
      <a
        href="https://brandtoolbox.vattenfall.com/media/"
        target="_blank"
        rel="noreferrer"
      >
        Media Bank
      </a>
      . Look for what you need directly in the search bar and refine your search
      using the search tools. All illustrations are tagged with relevant words.
      By clicking on an illustration, size and format information will appear
      under Basics. The illustrations are available in an SVG format with an
      attached open Adobe Illustrator PDF file. Information such as
      descriptions, usage rights and creator can be found in the Metadata. For
      more guidance or artwork support, please contact Vattenfall’s branding
      helpdesk.
    </p>
  </IllustrationsContainer>
)
