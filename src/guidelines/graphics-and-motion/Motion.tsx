import styled from 'styled-components'

const MotionContainer = styled.div`
  video {
    width: 100%;
    max-width: 450px;
    margin-bottom: 24px;
  }
`

export const Motion = () => (
  <MotionContainer>
    <p className="vf-component__paragraph">
      When designing in accordance with the Vattenfall Digital Design System,
      make sure to think through motion effects and transitions. Each effect
      should fill a purpose. Don’t combine several components with heavy
      animations – strive to add subtle motion.
    </p>
    <p className="vf-component__paragraph">
      ** For the sake of consistency, we use only one default animation for now
      **
    </p>
    <p className="vf-component__paragraph">
      Motion focuses attention on what’s important without creating unnecessary
      distraction.
    </p>

    <p className="vf-component__paragraph">
      Object effects: Scale, Move, Elevation, Colour-change
    </p>
    <h3 className="vf-component__small-heading">Animation</h3>
    <video controls>
      <source src="/assets/img/motion_3.mp4" type="video/mp4" />
      Your browser does not support the video tag.
    </video>

    <h3 className="vf-component__small-heading">
      Info graphics with animation
    </h3>
    <video controls>
      <source src="/assets/img/motion_1.mp4" type="video/mp4" />
      Your browser does not support the video tag.
    </video>
    <br />
    <video controls>
      <source src="/assets/img/motion_2.mp4" type="video/mp4" />
      Your browser does not support the video tag.
    </video>
    <p className="vf-component__paragraph">
      To read more about the “ease-in-out” motion ID philosophy, please see the
      description and the examples in the Video & Motion guidelines chapter:{' '}
      <a
        href="https://brandtoolbox.vattenfall.com/Styleguide/brandtwo/#page/70DBC29E-9802-4DDD-9937424FDCB53C25"
        target="_blank"
        rel="noreferrer"
      >
        https://brandtoolbox.vattenfall.com/Styleguide/brandtwo/#page/70DBC29E-9802-4DDD-9937424FDCB53C25
      </a>
    </p>
  </MotionContainer>
)
