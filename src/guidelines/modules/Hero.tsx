export const Hero = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        The hero component is used for large blocks at the top of webpages to
        convey a key message or describe what the page is about.
      </p>
      {/* ##TODO Needs examples */}
    </div>
  )
}
