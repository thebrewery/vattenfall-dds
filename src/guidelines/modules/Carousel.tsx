export const Carousel = () => {
  return (
    <>
      <div className="vf-guideline__container">
        <p className="vf-component__paragraph">
          We use the Carousel to display different cards which navigate to a
          certain detail or product page.
        </p>
        {/* ##TODO ADD CAROUSEL EXAMPLES CSB */}
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">General info</h2>
        <p className="vf-component__paragraph">Dimensions 1440 x 764</p>
        <p className="vf-component__paragraph">
          <strong>Specifications content</strong>
        </p>
        <h3 className="vf-component__small-heading">Background:</h3>
        <p className="vf-component__paragraph">
          Mandatory: <strong>Yes</strong>
        </p>
        <p className="vf-component__paragraph">
          Styling:{' '}
          <strong>
            Background color Light Blue, Light Green or Light Grey
          </strong>
        </p>
        <h3 className="vf-component__small-heading">Title:</h3>
        <p className="vf-component__paragraph">
          Mandatory: <strong>Yes</strong>
        </p>
        <p className="vf-component__paragraph">
          Styling: <strong>H2 Black center-aligned</strong>
        </p>
        <p className="vf-component__paragraph">
          Rules: <strong>Max 25 characters –Min 5 characters</strong>
        </p>
        <h3 className="vf-component__small-heading">Card:</h3>
        <p className="vf-component__paragraph">
          Mandatory: <strong>Yes</strong>
        </p>
        <p className="vf-component__paragraph">
          Content: <strong>Image, H4 title, Rich text, Text Button</strong>
        </p>
        <p className="vf-component__paragraph">
          Title: <strong>Max 2 rows, 35 characters – Min 15 characters</strong>
        </p>
        <p className="vf-component__paragraph">
          Rich text: <strong>Text truncate after 105 characters</strong>
        </p>
        <p className="vf-component__paragraph">
          Rules: <strong>Always align to left</strong>
        </p>
        <p className="vf-component__paragraph">
          Interaction: <strong>Link to internal page</strong>
        </p>

        <h2 className="vf-component__heading">Dos and Don't</h2>
        <div className="vf-component__paragraph">
          - Always place 4 or more cards in the module. <br />
          - Make sure to write the titles and text the same length for all the
          cards in the module. <br />- Don’t make the tertiary button to long.
          Make only use of verbs in the buttons.
        </div>
      </div>
    </>
  )
}
