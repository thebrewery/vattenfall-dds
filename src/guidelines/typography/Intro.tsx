export const Intro = () => (
  <>
    <div className="vf-guideline__full-container">
      <p className="vf-component__paragraph">
        <span className="vf-intro-in-caps">THIS IS OUR intro in caps. </span>
      </p>
      <p className="vf-component__paragraph">
        It’s used on the three first words together with body text large.
      </p>

      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>16px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>16px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Regular</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>12px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>22px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>
    <div className="vf-guideline__full-container" style={{ gridRow: '2' }}>
      <h2 className="vf-guideline__heading">Preamble</h2>

      <p className="vf-preamble">This is Intro copy.</p>
      <p className="vf-component__paragraph">
        Used for introducing the content after a Headline.
      </p>
      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Medium</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>24px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Medium</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>24px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Medium</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>20px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>28px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>
  </>
)
