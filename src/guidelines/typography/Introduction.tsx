export const Introduction = () => (
  <div className="vf-guideline__container">
    <p className="vf-component__paragraph">
      Our typeface Hall is a bespoke typeface that mirrors and embodies the
      qualities of a true market leader – as expected by our target group:
      confident, competent and determined. Yet, it should feel stable, organic,
      and personable and at the same time emphasise our engineering and
      functional capacity. Being bold without being arrogant, it creates
      credibility for our capability to act on a broader scale. Developed
      together with the wordmark and icons, it creates a strong, recognisable
      and coherent identity.
    </p>
    <h3 className="vf-component__small-heading">Rules for headings</h3>
    <p className="vf-component__paragraph">
      Always use Hall Display Bold for 30px and above. Always use Hall Bold for
      under 30px.
    </p>
    <img
      src="/assets/img/typo_example.png"
      alt="Example of Vattenfall Hall font"
    />
  </div>
)
