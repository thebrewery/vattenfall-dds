export const Bodycopy = () => (
  <>
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        This is our examples of body copy-text, used in modules and as our main
        body text on our websites.
      </p>
      <h2 className="vf-large-bodycopy">Large bodycopy</h2>
      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Regular</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>20px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Regular</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>20px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Regular</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>20px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>
    <div className="vf-guideline__container" style={{ gridRow: '2' }}>
      <h2 className="vf-small-bodycopy">Small bodycopy</h2>
      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Regular</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>16px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>28px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Regular</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>16px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>28px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Regular</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>16px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>28px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>
  </>
)
