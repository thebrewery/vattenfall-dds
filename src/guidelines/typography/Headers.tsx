import styled from 'styled-components'

const Container = styled.div``

export const Headers = () => (
  <Container>
    <div className="vf-guideline__full-container">
      <h1 className="vf-component__large-heading">Header h1</h1>
      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall Display</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>52px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>62px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall Display</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>52px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>62px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall Display</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>34px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>40px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>

    <div className="vf-guideline__full-container">
      <h2 className="vf-component__heading">Header h2</h2>
      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall Display</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>36px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>42px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall Display</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>36px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>42px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall Display</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>28px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>32px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>

    <div className="vf-guideline__full-container">
      <h3 className="vf-component__small-heading">Header h3</h3>
      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>28px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>28px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>36px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>22px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>28px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>

    <div className="vf-guideline__full-container">
      <h4 className="vf-component__tiny-heading">Header h4</h4>
      <div className="vf-guideline__typo-examples">
        <div>
          <h3 className="vf-component__small-heading">Desktop</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>22px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>28px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Tablet</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>22px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>28px</strong>
            </span>
          </p>
        </div>
        <div>
          <h3 className="vf-component__small-heading">Mobile</h3>
          <p className="vf-guideline__header-intro">
            Font
            <span>
              <strong>Vattenfall Hall</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-weight
            <span>
              <strong>Bold</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Font-size
            <span>
              <strong>18px</strong>
            </span>
          </p>
          <p className="vf-guideline__header-intro">
            Line-height
            <span>
              <strong>24px</strong>
            </span>
          </p>
        </div>
      </div>
    </div>
  </Container>
)
