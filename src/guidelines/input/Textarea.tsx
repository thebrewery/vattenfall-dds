export const Textarea = () => (
  <div className="vf-guideline__container">
    <h3 className="vf-component__tiny-heading vf-utility__warning">
      DISCLAIMER
    </h3>
    <p className="vf-component__paragraph">
      To use rising/top-aligned label, the input requires a placeholder with a
      value or a empty string. This behaviour is required to keep our Inputs
      free from javascript.
    </p>
    <p className="vf-component__paragraph">Examples</p>
    <div className="vf-guideline__state-examples">
      <div>
        <p className="vf-guideline__underlined-paragraph">Default</p>
        <img
          className="textarea-example"
          alt=""
          src="/assets/img/textarea-default.png"
        />
      </div>
      <div>
        <p className="vf-guideline__underlined-paragraph">Hover</p>
        <img
          className="textarea-example"
          alt=""
          src="/assets/img/textarea-hover.png"
        />
      </div>
      <div>
        <p className="vf-guideline__underlined-paragraph">
          Focus with suggestion
        </p>
        <img
          className="textarea-example"
          alt=""
          src="/assets/img/textarea-value-with-suggestion.png"
        />
      </div>
      <div>
        <p className="vf-guideline__underlined-paragraph">Filled</p>
        <img
          className="textarea-example"
          alt=""
          src="/assets/img/textarea-value-filled-in.png"
        />
      </div>
    </div>
  </div>
)
