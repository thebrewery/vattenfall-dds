export const Field = () => (
  <div className="vf-guideline__container">
    <h3 className="vf-component__tiny-heading vf-utility__warning">
      DISCLAIMER
    </h3>
    <p className="vf-component__paragraph">
      To use rising/top-aligned label, the input requires a placeholder with a
      value or a empty string. This behaviour is required to keep our Inputs
      free from javascript.
    </p>
    <p className="vf-component__paragraph">
      <strong>Behaviour:</strong>
      <br />
      Error messages are displayed beneath the input field. If a user inputs
      invalid content, an error message should be displayed. The application
      which implements the input fields is responsible for validation and
      display of the error message.
    </p>
    <p className="vf-component__paragraph">Examples</p>
    <div className="vf-guideline__state-examples">
      <div>
        <p className="vf-guideline__underlined-paragraph">Default</p>
        <img
          className="input-example"
          src="/assets/img/standard-default.png"
          alt="Vattenfall's default input field"
        />
      </div>
      <div>
        <p className="vf-guideline__underlined-paragraph">Hover</p>
        <img
          className="input-example"
          src="/assets/img/standard-hover.png"
          alt="Vattenfall's default input field in hovered state"
        />
      </div>
      <div>
        <p className="vf-guideline__underlined-paragraph">
          Focus with suggestion
        </p>
        <img
          className="input-example"
          src="/assets/img/standard-value-with-suggestion.png"
          alt="Vattenfall's default input field in focused state"
        />
      </div>
      <div>
        <p className="vf-guideline__underlined-paragraph">Filled</p>
        <img
          className="input-example"
          src="/assets/img/standard-value-filled.png"
          alt="Vattenfall's default input field in filled state"
        />
      </div>
    </div>
  </div>
)
