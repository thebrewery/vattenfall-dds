export const Number = () => (
  <div className="vf-guideline__container">
    <p className="vf-component__paragraph">
      Number input should always be 52px in height. Like dropdowns, numbers
      include an ever-present label in order to be accessible.
    </p>
    <h3 className="vf-component__tiny-heading vf-utility__warning">
      DISCLAIMER
    </h3>
    <p className="vf-component__paragraph">
      To use rising/top-aligned label, the input requires a placeholder with a
      value or a empty string. This behaviour is required to keep our Inputs
      free from javascript.
    </p>
  </div>
)
