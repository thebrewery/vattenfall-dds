export const Toggle = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        Toggles are used to indicate when a certain feature on a page is engaged
        / disengaged.
      </p>
      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <img
            src="/assets/img/ToggleDefault.png"
            alt="Vattenfall's default toggle"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <img
            src="/assets/img/ToggleHover.png"
            alt="Vattenfall's default toggle on hover"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <img
            src="/assets/img/TogglePressed.png"
            alt="Vattenfall's default toggle in pressed state"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <img
            src="/assets/img/ToggleDisabled.png"
            alt="Vattenfall's default toggle in disabled state"
          />
        </div>
      </div>
    </div>
  )
}
