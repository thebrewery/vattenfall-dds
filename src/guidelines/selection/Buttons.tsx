import styled from 'styled-components'

const ButtonsContainer = styled.div`
  .vf-guideline__state-examples {
    .vf-link--with-arrow {
      white-space: nowrap;
    }
  }

  .yellow-forced-hover {
    pointer-events: none;
    color: #111111;
    background: #ffe85b;

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .yellow-forced-active {
    background: #f1c50b;
    outline: 0;
    box-shadow: none;

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .blue-forced-hover {
    pointer-events: none;
    background: ${({ theme }) => theme.colours.primary__ocean_blue};

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .blue-forced-active {
    background: #1e324f;
    outline: 0;
    box-shadow: none;

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .dark-forced-hover {
    color: #fff;
    background: #000;

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .dark-forced-active {
    color: #fff;
    background: #4e4b48;
    border-color: #4e4b48;
    outline: 0;
    box-shadow: none;

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .outline-blue-forced-hover {
    color: #fff;
    background: #1964a3;
    border-color: #1964a3;

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .outline-blue-forced-active {
    color: #fff;
    background: #1e324f;
    border-color: #1e324f;
    outline: 0;
    box-shadow: none;

    &::before {
      opacity: 1;
      transform: translate(-150%, 0);
    }
  }

  .link-hover {
    pointer-events: none;
    color: ${({ theme }) => theme.colours.primary__ocean_blue};

    &::before {
      opacity: 1;
      transform: translate(20%, 0);
    }
  }

  .link-active {
    pointer-events: none;
    color: ${({ theme }) => theme.colours.secondary__dark_blue};

    &::before {
      color: ${({ theme }) => theme.colours.secondary__dark_blue};
      opacity: 1;
      transform: translate(20%, 0);
    }
  }
`

export const Buttons = () => (
  <ButtonsContainer>
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        Buttons indicate actions on the page. Each of our button types have
        specific purposes that are used consistently. Individual button types
        are shown below, but the guidelines will help you understand basic
        functionality for all our buttons.
      </p>
      <h2 className="vf-component__heading">Best practices</h2>
      <div className="vf-component__paragraph">
        Buttons should:
        <li>
          Be clearly and accurately labelled Lead with strong, actionable verbs.
        </li>
        <li>
          Use established button colours appropriately. Prioritize the most
          important actions.
        </li>
        <li>
          Too many calls to action can cause confusion and make users unsure of
          what to do next.
        </li>
        <li>Be positioned in consistent locations in the interface. </li>
      </div>
      <h3 className="vf-component__small-heading">Interactions</h3>

      <p className="vf-component__paragraph">
        The buttons have an arrow on hover. It animates from left to right on
        the left side of the button.
      </p>

      <h3 className="vf-component__small-heading">Primary buttons</h3>

      <div className="vf-component__paragraph">
        Primary buttons should be used for primary calls to action
        <li>
          Only one primary button should be contained in each card or screen
          view
        </li>
        <li>Should never be used for destructive actions</li>
        <li>Should never be used as a cancel button</li>
      </div>
      <h3 className="vf-component__small-heading">Padding</h3>

      <p className="vf-component__paragraph">
        Buttons with Icon has 64px space left and right between edge and text.
        Buttons without Icon, has a minimum of 28px left and right.
      </p>
      <h3 className="vf-component__small-heading">Yellow</h3>

      <p className="vf-component__paragraph">
        The yellow button indicates our main focus action. There is only one
        yellow button per component. If you need more buttons in one component,
        use secondary buttons.
      </p>
      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <button className="vf-button vf-button__primary vf-button--lg ">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <button className="vf-button vf-button__primary vf-button--lg yellow-forced-hover">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <button className="vf-button vf-button__primary vf-button--lg yellow-forced-active">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <button
            className="vf-button vf-button__primary vf-button--lg"
            disabled
          >
            Button label
          </button>
        </div>
      </div>
      <h3 className="vf-component__small-heading">Blue</h3>

      <p className="vf-component__paragraph">
        The yellow button indicates our main focus action. There is only one
        yellow button per component. If you need more buttons in one component,
        use secondary buttons.
      </p>
      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <button className="vf-button vf-button__secondary vf-button--lg ">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <button className="vf-button vf-button__secondary vf-button--lg blue-forced-hover">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <button className="vf-button vf-button__secondary vf-button--lg blue-forced-active">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <button
            className="vf-button vf-button__secondary vf-button--lg"
            disabled
          >
            Button label
          </button>
        </div>
      </div>

      <h3 className="vf-component__small-heading">
        Tertiary Black (outlined button)
      </h3>
      <p className="vf-component__paragraph">
        Black outlined buttons are used in combination with the yellow primary
        buttons.
      </p>

      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <button className="vf-button vf-button__outline--dark vf-button--lg ">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <button className="vf-button vf-button__outline--dark vf-button--lg dark-forced-hover">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <button className="vf-button vf-button__outline--dark vf-button--lg dark-forced-active">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <button
            className="vf-button vf-button__outline--dark vf-button--lg"
            disabled
          >
            Button label
          </button>
        </div>
      </div>
      <h3 className="vf-component__small-heading">
        Tertiary Blue (outlined button)
      </h3>
      <p className="vf-component__paragraph">
        Blue outlined buttons are used in combination with the blue primary
        buttons.
      </p>

      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <button className="vf-button vf-button__outline--secondary vf-button--lg ">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <button className="vf-button vf-button__outline--secondary vf-button--lg outline-blue-forced-hover">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <button className="vf-button vf-button__outline--secondary vf-button--lg outline-blue-forced-active">
            Button label
          </button>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <button
            className="vf-button vf-button__outline--secondary vf-button--lg"
            disabled
          >
            Button label
          </button>
        </div>
      </div>

      <h3 className="vf-component__small-heading">Text links </h3>
      <p className="vf-component__paragraph">
        Text links are text labels that fall outside of a block of text. The
        text should describe the action that will occur if a user clicks or taps
        a button. Text links have a low level of emphasis and are typically used
        for less important actions. Because text links don’t have a container,
        they don’t distract from nearby content. When you want to use a
        tertiairy button as a return button, point the arrow to the left.
      </p>
      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <p className="vf-link--with-arrow">Text label</p>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <p className="vf-link--with-arrow link-hover">Text label</p>
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <p className="vf-link--with-arrow link-active">Text label</p>
        </div>
      </div>
    </div>
  </ButtonsContainer>
)
