export const Option = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        A visual picker can be used when the options benefit from being visually
        enhanced. It can be either radio buttons or checkboxes.s & Conditions,
        in most other cases radio buttons should be used.
      </p>
      <p className="vf-component__paragraph">
        When options are more than 6 on desktop and 4 on mobile, dropdown should
        be used instead of a visual picker. Visual picker can be set to a single
        or multiple selection. The padding between each picker is set to 24px
        horizontally and 28px vertically. The text label inside should on all
        breakpoints remain on a single line with a 20px font-size.
      </p>
    </div>
  )
}
