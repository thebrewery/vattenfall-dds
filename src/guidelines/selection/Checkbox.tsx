export const Checkbox = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        Checkboxes can be used to visualise the selection of one or several
        desired states. Ideally the use of a single checkbox should be relevant
        in the context such as agreeing to Terms & Conditions, in most other
        cases radio buttons should be used.
      </p>
      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <img
            src="/assets/img/CheckboxDefault.png"
            alt="Vattenfall's default checkbox"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <img
            src="/assets/img/CheckboxHover.png"
            alt="Vattenfall's default checkbox in hovered state"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <img
            src="/assets/img/CheckboxPressed.png"
            alt="Vattenfall's default checkbox in active state"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <img
            src="/assets/img/CheckboxDisabled.png"
            alt="Vattenfall's default checkbox in disabled state"
          />
        </div>
      </div>
    </div>
  )
}
