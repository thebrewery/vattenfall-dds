export const RadioButton = () => {
  return (
    <div className="vf-guideline__container">
      <p className="vf-component__paragraph">
        Radio buttons should be used when a single preference selection is
        needed. If multiple selections are needed checkboxes should be used
        instead.
      </p>
      <div className="vf-guideline__state-examples">
        <div>
          <p className="vf-guideline__underlined-paragraph">Default</p>
          <img
            src="/assets/img/RadioDefault.png"
            alt="Vattenfall's default radio button"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Hover</p>
          <img
            src="/assets/img/RadioHover.png"
            alt="Vattenfall's default radio button on hovered state"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Active</p>
          <img
            src="/assets/img/RadioPressed.png"
            alt="Vattenfall's default radio button on pressed state"
          />
        </div>
        <div>
          <p className="vf-guideline__underlined-paragraph">Disabled</p>
          <img
            src="/assets/img/RadioDisabled.png"
            alt="Vattenfall's default radio button in disabled state"
          />
        </div>
      </div>
    </div>
  )
}
