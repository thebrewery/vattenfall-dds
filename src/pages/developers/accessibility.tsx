import { useContext, useEffect } from 'react'
import styled from 'styled-components'

import { DefaultLayout } from '../../components/DefaultLayout'
import PageProvider from '../../context/state'
import { modeEnum } from '../../utils/siteStructure'
import Link from 'next/link'
import ContentWrapper from '../../components/ContentWrapper'
import { InfoBox } from '../../components/InfoBox'
import { IconCards } from '../../components/accessibility/IconCards'
import Workflow from '../../components/accessibility/Workflow'
import { ExternalLinks } from '../../components/ExternalLinks'
import { ExternalLinksExtended } from '../../components/ExternalLinksExtended'
import { AnchorLinks } from '../../components/AnchorLinks'

type EntryLinkProps = {
  linkIcon: string
  linkHref: string
  text: string
}

const EntryLinkStyle = styled.div`
  min-height: 115px;

  @media screen and (min-width: 768px) {
    grid-template-columns: repeat(3, 1fr);
  }

  .entry-link {
    align-items: center;
    background-color: ${({ theme }) => theme.lightMode.bgColor};
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    width: 100%;
    padding: 20px;
    [class^='vf-icon-'],
    [class*=' vf-icon-'] {
      &:before {
        font-size: 4.5rem !important;
      }
    }
    @media screen and (min-width: 768px) {
      [class^='vf-icon-'],
      [class*=' vf-icon-'] {
        margin-bottom: 1rem;

        &:before {
          font-size: 4.5rem !important;
        }
      }
    }

    &:hover {
      background-color: ${({ theme }) => theme.lightBlueBackground};
      cursor: pointer;

      .future-vision__link::before {
        transform: translate(-125%, 0);
      }
    }
  }
`

const EntryLink = ({ linkIcon, linkHref, text }: EntryLinkProps) => (
  <EntryLinkStyle>
    <Link href={linkHref}>
      <div className="entry-link" tabIndex={0}>
        <span className={linkIcon} />
        <a className="future-vision__link">{text}</a>
      </div>
    </Link>
  </EntryLinkStyle>
)

const Container = styled.div`
  .vf-guideline__design-principle-image {
    margin-bottom: 44px;
  }

  .vf-component {
    &__container {
      border-bottom: 0;
      max-width: 900px;
      margin-bottom: 55px;

      h2 {
        font-size: 2.25rem;
        line-height: 2.625rem;
        margin-bottom: 8px;
      }

      h3 {
        font-size: 28px;
        font-weight: 700;
        line-height: 36px;
      }
    }

    &__paragraph {
      font-size: 20px;
      font-weight: 400;
      line-height: 36px;
    }

    &__icon-cards {
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      grid-row-gap: 33px;
      grid-column-gap: 29px;
      max-width: 900px;
      margin-top: 70px;
      margin-bottom: 75px;
    }

    &__info-boxes {
      display: flex;
      flex-direction: column;
      gap: 24px;
      margin-bottom: 90px;
    }

    &__entry-links {
      padding: 0;
    }
  }

  //   Circles
  .vf-guideline {
    &__circles {
      display: grid;
      grid-template-columns: repeat(4, 1fr);
      grid-gap: 27px;
      max-width: 900px;
      padding: 0 32px;
    }
    &__intro-example--round {
      width: 260px;
      height: 260px;
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      row-gap: 13px;
      padding: 30px;
    }

    &__intro-icon {
      &::before {
        font-size: 4.5rem;
      }
    }

    &__intro-title {
      font-size: 52px;
      font-style: normal;
      font-weight: 700;
      line-height: 62px;
    }

    &__intro-label {
      font-size: 0.9rem;
      font-weight: 700;
      line-height: 1.05rem;
      text-align: center;
      font-family: 'Vattenfall Hall NORDx', 'Vattenfall Hall NORDx', sans-serif;
    }

    &_color {
      &--light_green {
        background: ${({ theme }) => theme.colours.bg__light_green};
      }

      &--light_blue {
        background: ${({ theme }) => theme.colours.bg__light_blue};
      }
    }

    &__circles-label {
      font-size: 14px;
      line-height: 23px;
      text-align: left;
      max-width: 900px;
      margin-top: 32px;
    }
  }

  //   Cards
  .__cards {
    display: flex;
    gap: 44px;
    max-width: 900px;
    margin-top: 88px;
  }

  .__card-item {
    width: 484px;
    border: 1px solid var(--Teritiary-Colours-Light-Grey, #ccc);
    padding: 80px 23px 32px;
    display: flex;
    flex-direction: column;
    gap: 8px;
    position: relative;
    font-size: 14px;
    line-height: 23px;

    h3 {
      font-size: 22px;
      font-weight: 700;
      line-height: 28px;
    }

    img {
      position: absolute;
      width: 123px;
      height: 123px;
      top: -61.5px;
      transform: translateX(-50%);
      left: 50%;
    }
  }

  .entry-links {
    gap: 30px;
    grid-template-columns: repeat(3, 1fr);
    display: grid;
    min-height: 260px;
  }

  //   Accordion
  details {
    border-bottom: 1px solid #e6e6e6;
    padding: 20px 0;
    line-height: 1.5rem;

    summary {
      list-style: none;
      outline: none;
      font-size: 20px;
      line-height: 28px;
      font-weight: 500;
      position: relative;
      display: flex;
      justify-content: space-between;
      cursor: pointer;

      &::after {
        content: '';
        width: 20px;
        background-image: url("data:image/svg+xml,%3Csvg width='20' height='10' viewBox='0 0 20 10' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M9.99194 9.04225L0.400391 1.57746L1.62574 0L9.99194 6.51408L18.3652 0L19.5905 1.57746L9.99194 9.04225Z' fill='%232071B5'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: center;
      }

      &::marker,
      &::-webkit-details-marker {
        display: none;
      }
    }

    & > * {
      margin: 0;

      &:not(summary) {
        padding-top: 12px;
      }
    }

    &[open] {
      summary {
        margin-bottom: 0;
        color: #2071b5;

        &::after {
          transform: rotate(180deg);
        }
      }
    }
  }
`

const Accessibility = ({}) => {
  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('null')
    setActiveComponentId('null')
    setCurrentMode(modeEnum.DEVELOPERS)
  }, [setActiveComponentId, setActiveCategoryId, setCurrentMode])

  return (
    <DefaultLayout>
      <Container className="__container">
        <main>
          <h1 className="vf-guideline__large-heading">
            Accessibility Guidelines
          </h1>
          <div className="vf-component__container">
            <p className="vf-component__paragraph">
              With the updated EU Disability Act coming into effect in 28th of
              June 2025, we have added accessibility guidelines to support
              digital teams and site owners adhere to the legal requirements and
              design guidelines for accessibility. It is essential that digital
              teams and site owners not only understand, but take responsibility
              for adhering to the guidelines. The checklists, tools, best
              practices and recommendations are developed to help meet the
              requirements for the AA compliance level and design digital
              experiences that include everyone.
            </p>

            <AnchorLinks
              links={[
                {
                  text: 'Designing with everyone in mind',
                  url: '#designing-with-everyone-in-mind',
                },
                {
                  text: 'Recommendations for each role',
                  url: '#recommendations-for-each-role',
                },
                {
                  text: 'Overview of the legislation',
                  url: '#overview-of-the-legislation',
                },
                // {
                //   text: 'Quick Tips',
                //   url: '#quick-tips',
                // },
                {
                  text: 'Suggested workflow',
                  url: '#suggested-workflow',
                },
                {
                  text: 'Accessibility Principles',
                  url: '#accessibility-principles',
                },
                {
                  text: 'Testing',
                  url: '#accessibility-principles',
                },
                {
                  text: 'Checklists',
                  url: '#checklists',
                },
                {
                  text: 'Helpful Resources',
                  url: '#resources',
                },
                {
                  text: 'Get In Touch',
                  url: '#get-in-touch',
                },
              ]}
            />
          </div>

          <div className="vf-component__container  vf-guideline__accessibility">
            <h2>Why accessibility matters for Vattenfall</h2>

            <div>
              <p className="vf-component__paragraph">
                As a socially responsible blue brand and a large European
                provider of consumer energy products and services, Vattenfall
                need to strive to achieve at least the expected industry
                standard across all digital channels and touch-points to cater
                for the diverse needs of our customers. Another important factor
                is that accessible websites are easier for search engine
                crawlers to index, meaning accessible websites will improve SEO
                ranking and increase reach. For us, accessibility is about
                ensuring that everyone has the opportunity to make, move and
                live in a fossil-free society.
              </p>
            </div>
            {/* Circles */}
            <div className="vf-guideline vf-guideline__circles">
              <div>
                <div
                  className={`vf-guideline__intro-example--round vf-guideline_color--light_green`}
                  data-tip
                  data-for="1"
                >
                  {/* Icon */}
                  {/* <span className={`vf-guideline__intro-icon`} /> */}
                  <span className="vf-guideline__intro-title">97,4%</span>
                  <div className="vf-guideline__intro-label">
                    of the top 1 millionwebsites have clearaccessibility issues
                  </div>
                </div>
              </div>

              <div>
                <div
                  className={`vf-guideline__intro-example--round vf-guideline_color--light_green`}
                  data-tip
                  data-for="1"
                >
                  {/* Icon */}
                  <span
                    className={`vf-guideline__intro-icon vf-icon-discussion-two`}
                  />
                  <div className="vf-guideline__intro-label">
                    1 of 4<br />
                    have disabilities
                    <br />
                    in the EU
                  </div>
                </div>
              </div>

              <div>
                <div
                  className={`vf-guideline__intro-example--round vf-guideline_color--light_green`}
                  data-tip
                  data-for="1"
                >
                  {/* Icon */}
                  <span
                    className={`vf-guideline__intro-icon vf-icon-consumption`}
                  />
                  <div className="vf-guideline__intro-label">
                    Web accessibility can
                    <br />
                    significantly improve
                    <br />
                    SEO and ROI 
                  </div>
                </div>
              </div>
            </div>

            <div className="vf-guideline__circles-label">
              Source: European Council of the European Union, United Nations
            </div>
          </div>

          <div
            className="vf-component__container  "
            id="designing-with-everyone-in-mind"
          >
            <h2>Designing with everyone in mind</h2>
            <p className="vf-component__paragraph">
              When designing with accessibility in mind, it's crucial to
              consider different types of disabilities and their impact on
              browsing, interacting and reading digital sites. By understanding
              the different types of disabilities and their needs you can better
              design, build and create digital sites and apps that includes
              everyone.
            </p>

            <div className="__cards">
              <div className="__card-item">
                <img
                  src="/assets/accessibility/permanent.png"
                  alt="permanent disability means lifetime disabilities that can make it hard for
                  people to access websites and other digital products."
                />
                <h3>Permanent</h3>
                <p>
                  Refers to lifetime disabilities that can make it hard for
                  people to access websites and other digital products.
                  Disabilities can be visual, hearing, motor, or cognitive.
                </p>
                <p>
                  <strong> How this might affect website use:</strong>
                </p>
                <p>
                  The affected users might need screen readers, screen
                  magnifiers or only keyboards to browse sites. If the websites
                  are not compatible with accessibility tools they can be hard
                  to use for certain users.
                </p>
              </div>

              <div className="__card-item">
                <img
                  src="/assets/accessibility/temporary.png"
                  alt="temporary disability refers short-term disabilities due to injuries, health
                  problems, or other short-term situations that impacts health
                  such as stress"
                />
                <h3>Temporary</h3>
                <p>
                  Refers to short-term disabilities due to injuries, health
                  problems, or other short-term situations that impacts health
                  such as stress.
                </p>
                <p>
                  <strong>How this might affect website use:</strong>
                </p>
                <p>
                  Injuries may limit normal device use, requiring single-handed
                  navigation or tabbing through the page to navigate.
                </p>
                <p>
                  Stress can also impair focus and make complex texts and more
                  interactive content harder to understand.
                </p>
              </div>

              <div className="__card-item">
                <img
                  src="/assets/accessibility/situational.png"
                  alt="Situational disability refers to a person's temporary inability to use a website due
                  to a specific situation or environment they are in"
                />
                <h3>Situational</h3>
                <p>
                  Refers to a person's temporary inability to use a website due
                  to a specific situation or environment they are in.
                </p>
                <p>
                  <strong>How this might affect website use:</strong>
                </p>
                <p>
                  For example in noisy environments, users may not be able to
                  hear audio content well, making captions and visual cues
                  important.
                </p>
                <p>
                  Good colour contrast becomes crucial for users using a phone
                  or computer in strong sunlight experiencing screen-glare.
                </p>
              </div>
            </div>
          </div>

          <div className="vf-component__container ">
            <h2>Want to learn more about assistive technology?</h2>
            <div className="vf-component__paragraph">
              In this section you will find a few select videos of users with a
              range of disabilities using different assistive technology tools.
            </div>
            <ExternalLinks
              links={[
                {
                  text: 'Screen reader',
                  url: 'https://www.youtube.com/watch?v=q_ATY9gimOM',
                },
                {
                  text: 'Keyboard navigation',
                  url: 'https://www.youtube.com/watch?v=vJwv9K7xkkc',
                },
                {
                  text: 'Braille keyboard',
                  url: 'https://www.youtube.com/watch?v=LMfQNkRR9N0',
                },
                {
                  text: 'Sip and puff',
                  url: 'https://www.youtube.com/watch?v=Bhj5vs9P5cw&t=12s',
                },
              ]}
              classes="black-hover"
            />
          </div>
          <div className="vf-component__container ">
            <h2>What you will learn from this guide</h2>
            <div className="vf-component__paragraph">
              <ul>
                <li>
                  How to design according to the Level AA criteria covering
                  aspects for visual and written content, structure and layout
                  and code.
                </li>
                <li>
                  How to plan your teams workflow and process to audit, test and
                  implement accessibility best practices.
                </li>
                <li>
                  How to troubleshoot and improve existing digital projects.
                </li>
              </ul>
            </div>
          </div>

          <div
            className="vf-component__container "
            id="recommendations-for-each-role"
          >
            <h2>Recommendations for each role</h2>
            <div className="vf-component__paragraph">
              The accessibility guidelines provide recommendations for each
              role, with the goal of integrating accessibility into various
              workflows among digital teams. To design digital sites that meet
              the AA compliance level, use these instructions, suggested tools,
              and custom checklists, which are tailored to meet the needs and
              workflows of each role. Click on the icons below to access the
              specific pages for designers, developers, or editors.
            </div>
            <ContentWrapper id="text" className="vf-component__entry-links">
              <div className="entry-links">
                <EntryLink
                  linkIcon="vf-icon-edit"
                  linkHref="/designers/components/accessibility-guidelines/designer-principles"
                  text="Designers"
                />
                <EntryLink
                  linkIcon="vf-icon-laptop"
                  linkHref="/designers/components/accessibility-guidelines/developer-principles"
                  text="Developers"
                />
                <EntryLink
                  linkIcon="vf-icon-my-documents"
                  linkHref="/designers/components/accessibility-guidelines/editor-principles"
                  text="Editors"
                />
              </div>
            </ContentWrapper>
          </div>

          <div className="vf-component__container " id="quick-tips">
            <h2>Quick tips for improving web accessibility</h2>
            <div className="vf-component__paragraph">
              Designing with accessibility in mind often leads to better
              usability and user experience for everyone, not just individuals
              with disabilities. Here are some easy and general tips to improve
              web accessibility and create user-friendly digital experiences for
              Vattenfall.
            </div>
            <img
              src="/assets/accessibility/quick-tips-accessibility.gif"
              alt="Collections of images showing some easy and general tips to improve
              web accessibility and create user-friendly digital experiences for
              Vattenfall"
            />
          </div>

          <div
            className="vf-component__container "
            id="overview-of-the-legislation"
          >
            <h2>Overview of the European Union legislation</h2>
            <div className="vf-component__paragraph">
              <a
                href="https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32019L0882"
                target="_blank"
                rel="noreferrer"
              >
                The European Accessibility Act (Directive 2019/882)
              </a>{' '}
              is a landmark EU law which requires a lot of everyday products and
              services to be accessible for persons with disabilities. It
              follows a commitment on accessibility made by the EU and all
              Member States upon ratifying the
              <a
                href="https://www.ohchr.org/en/ohchr_homepage?gad_source=1&gclid=CjwKCAjw34qzBhBmEiwAOUQcF-_xB3jxCYeKypTUxG621w8og3wAwVdd9jtKc1aLoiWWf2C5Zj9YOhoChzgQAvD_BwE"
                target="_blank"
                rel="noreferrer"
              >
                {' '}
                United Nations Convention on the Rights of Persons with
                Disabilities
              </a>
              . As of the 28th of June 2025, a lot of companies must ensure that
              the newly marketed products and services covered by the Act are
              accessible, everyone should have equal access to information and
              services, regardless of their abilities. Failure to adhere to the
              directives could lead to substantial fines as EU countries are
              expected to carry out enforcement.
            </div>

            <h3>Legal FAQ</h3>
            <div className="vf-component__paragraph">
              Frequently asked questions and answers about accessibility legal
              implications.
            </div>

            <div className="vf-component__accordion">
              <details>
                <summary>
                  What accessibility standard level must Vattenfall comply with?
                </summary>
                <p>
                  At Vattenfall the decision has been taken to meet compliance{' '}
                  <a
                    href="https://www.w3.org/WAI/GL/UNDERSTANDING-WCAG20/conformance.html"
                    rel="noreferrer"
                    target="_blank"
                  >
                    level AA
                  </a>{' '}
                  (as a minimum) and is therefore the standard we should all be
                  aiming for throughout all digital touchpoints and user
                  journeys.
                </p>
              </details>

              <details>
                <summary>
                  Which functions are the minimum requirement for accessibility
                  compliance?
                </summary>
                <p>
                  The minimum requirement for accessibility compliance includes
                  the following functions, mainly affecting consumer-facing
                  sales websites and apps:
                </p>
                <p>
                  <ul>
                    <li>
                      Methods for digital identification (e.g., BankID in
                      Sweden)
                    </li>
                    <li>Electronic signatures</li>
                    <li>Safety functions</li>
                    <li>Payment functions</li>
                  </ul>
                </p>
              </details>

              <details>
                <summary>
                  What are the local legal responsibilities for web
                  accessibility?
                </summary>
                <p>
                  Many EU countries are currently updating their national laws
                  to meet the impending deadline in 28th of June 2025. It is
                  important to stay informed about these changes to schedule any
                  required updates in plenty of time and ensure compliance.
                </p>
              </details>

              <details>
                <summary>
                  What are the consequences of non-compliance with web
                  accessibility laws?
                </summary>
                <p>
                  In the European Union (EU), non-compliance with web
                  accessibility laws can have several significant consequences
                  such as fines, penalties and legal proceedings. While the EU
                  sets overarching regulations for web accessibility through
                  directives, some individual member states have flexibility in
                  how they implement and enforce these regulations. Make sure to
                  stay updated on your country's legal regulations by following
                  any news from the governing body or contacting your legal
                  representative.
                </p>
              </details>

              <details>
                <summary>
                  Should other parts of the user journey also comply with
                  accessibility standards?
                </summary>
                <p>
                  Yes, it is recommended that the full user journeys on these
                  sites, both before and after these functions, also comply with
                  the same accessibility level to ensure a good standard of
                  experience for all customers.
                </p>
              </details>
            </div>
          </div>

          <div className="vf-component__container ">
            {/* Info Box */}
            <InfoBox
              text={`At Vattenfall the decision has been taken to meet compliance <a href= "https://www.w3.org/WAI/GL/UNDERSTANDING-WCAG20/conformance.html" target="_blank" rel="noreferrer">level AA</a> (as a minimum) and is therefore the standard we should all be aiming for. Web Content Accessibility Guidelines (WCAG) 2.1 Level AA ensures our digital content is perceivable, operable, understandable and robust for all users on a high level without compromising the brand.`}
              icon={'vf-icon-alert'}
              title={'Compliance Level AA'}
            />
          </div>

          {/* Workflow */}
          <div className="vf-component__container " id="suggested-workflow">
            <h2>Integrating accessibility in the work flow</h2>
            <div className="vf-component__paragraph">
              Working with accessibility is an iterative process that involves
              planning, design, development, testing, and ongoing maintenance.
              It requires collaboration among designers, developers and editors
              to ensure that Vattenfall’s digital experiences are usable and
              accessible for everyone.
            </div>
            <Workflow />

            <div id="accessibility-principles">
              <h3>Accessibility Principles</h3>
              <div className="vf-component__paragraph">
                There are four main guiding principles of accessibility defined
                by the Web Content Accessibility Guidelines (WCAG) to help
                develop accessible digital experiences and inclusive design.
                These four principles are known by the acronym{' '}
                <strong>POUR</strong> for 
                <strong>P</strong>erceivable, <strong>O</strong>perable,{' '}
                <strong>U</strong>nderstandable and <strong>R</strong>obust. 
                <div className="vf-component__icon-cards">
                  <IconCards
                    text={`Ensuring content is perceivable makes it accessible for everyone, including those using alternative formats like screenreaders to access websites and digital content.`}
                    icon={'vf-icon-eye'}
                    title={'Perceivable'}
                    bgColor={'light-grey'}
                  />
                  <IconCards
                    text={`Allowing effective navigation and interaction for users with mobility impairments that are using alternative tools like keyboards or voice commands.`}
                    icon={'vf-icon-doing-good'}
                    title={'Operable'}
                    bgColor={'light-grey'}
                  />
                  <IconCards
                    text={`Making content and interfaces clear and intuitive to support users with cognitive disabilities that may find complex language and less structured content confusing.`}
                    icon={'vf-icon-brain'}
                    title={'Understandable'}
                    bgColor={'light-grey'}
                  />
                  <IconCards
                    text={`Ensuring content is robust means it can be accessed consistently across various platforms and assistive technologies giving all users a chance to take in contextual nuances.`}
                    icon={'vf-icon-settings'}
                    title={'Robust'}
                    bgColor={'light-grey'}
                  />
                </div>
              </div>
            </div>

            <div id="testing">
              <h3>Testing</h3>
              <div className="vf-component__paragraph">
                It’s important not to rely on only one way of testing -
                therefore to ensure robustness in your Accessibility process
                always test in the following two ways.
              </div>
              {/* Info Box */}
              <div className="vf-component__info-boxes">
                <InfoBox
                  text={`Using automated testing tools to evaluate your site against conformance guidelines and level success criteria can help save you a lot of time. Using the right tools enables you to do a more rigurous job of checking code, common accessibility issues and produces a complete report of all issues. We have included a few links to tools on the roles pages.`}
                  icon={'vf-icon-settings'}
                  title={'Why do automated testing?'}
                  bgColor={'light-green'}
                />

                <InfoBox
                  text={`Even though automated testing does a thorough job of finding and categorizing compliance issues it cannot replace manual testing to asess user experiences such as testing keyboard accessibility (Can alll interactive elements be accessed without a mouse? Do screen readers navigate correctly?) and if content has proper relevance in the context.`}
                  icon={'vf-icon-behaviour'}
                  title={'Why do manual testing?'}
                  bgColor={'light-green'}
                />
              </div>
            </div>
            <div id="resources">
              <h3>Helpful Resources</h3>
              <div className="vf-component__paragraph">
                For additional resources, WCAG2 recommendations and Vattenfall's
                commitment to accessibility visit the links below.
              </div>

              <ExternalLinksExtended
                links={[
                  {
                    text: 'WCAG 2 Overview',
                    url: 'https://www.w3.org/TR/WCAG21/',
                    desc: "WCAG 2.1 Accessibility guidelines, created by the World Wide Web (W3) Consortium, which is what we're adhering to in our work.",
                    url_label: 'WCAG 2.1 guidelines',
                  },
                  {
                    text: 'World Wide Web Consortium',
                    url: 'https://www.w3.org/',
                    desc: "W3C's homepage with extensive information around everything that covers accessibility including latest updates.",
                    url_label: "W3C's homepage",
                  },
                  {
                    text: 'A11y fundamentals',
                    url: 'https://www.a11yproject.com/',
                    desc: "The A11y project's, a community driven organisation, covering fundamentals and principles behind accessible design.",
                    url_label: 'A11y Project Website',
                  },
                  {
                    text: 'European Accessibility Act',
                    url: 'https://ec.europa.eu/social/main.jsp?catId=1202',
                    desc: "A link to the European Commission's directive the European Accessibility Act, in full.",
                    url_label: 'Accessibility Act',
                  },
                  {
                    text: 'European Disability Forum',
                    url: 'https://www.edf-feph.org',
                    desc: 'Website of the independent non-governmental organisation, EDF,  representing people with disabilities within the EU.',
                    url_label: 'EDF website',
                  },
                  {
                    text: 'W.H.O. / Disability',
                    url: 'https://www.who.int/europe/health-topics/disability#tab=tab_1',
                    desc: "World Health Organisation's framework for achieving the highest attainable standard of health for persons with disabilities (including digital accessibility).",
                    url_label: 'W.H.O. Disability framework',
                  },
                ]}
              />
            </div>
          </div>
          <div className="vf-component__container " id="checklists">
            <h2>Accessibility checklists for each role</h2>
            <div className="vf-component__paragraph">
              With the help of our checklists you can track your progress and
              make sure you are following the necessary steps to improve your
              workflow when working with accessibility. Click on each role to
              access the separate role-specific pages with checklists.
            </div>
            <ContentWrapper id="text" className="vf-component__entry-links">
              <div className="entry-links">
                <EntryLink
                  linkIcon="vf-icon-edit"
                  linkHref="/designers/components/accessibility-guidelines/designer-principles"
                  text="Designers"
                />
                <EntryLink
                  linkIcon="vf-icon-laptop"
                  linkHref="/designers/components/accessibility-guidelines/developer-principles"
                  text="Developers"
                />
                <EntryLink
                  linkIcon="vf-icon-my-documents"
                  linkHref="/designers/components/accessibility-guidelines/editor-principles"
                  text="Editors"
                />
              </div>
            </ContentWrapper>
          </div>

          <div className="vf-component__container " id="get-in-touch">
            <h2>Get in touch</h2>
            <div className="vf-component__paragraph">
              If you have questions regarding the guidelines, legal implications
              in your market, requests for support with input on assets or
              additions to these guidelines don’t hesitate to get in touch.
              Please add Digital Accessibility and your requirement in your
              subject line so we can channel the query to the right person and
              process the request more quickly.
            </div>

            <ExternalLinks
              links={[
                {
                  text: 'Contact us',
                  url: 'mailto:branding@vattenfall.com',
                },
              ]}
            />
          </div>
        </main>
      </Container>
    </DefaultLayout>
  )
}

// Accessibility.showSubMenu = true
Accessibility.showSidebar = true

export default Accessibility
