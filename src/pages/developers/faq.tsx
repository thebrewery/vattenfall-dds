import { useContext, useEffect } from 'react'
import styled from 'styled-components'
import { DefaultLayout } from '../../components/DefaultLayout'
import { FAQ } from '../../components/FAQ'
import PageProvider from '../../context/state'
import { API, FAQPageProps } from '../../utils/api'
import { modeEnum } from '../../utils/siteStructure'

const Container = styled.div``

export const getStaticProps = async () => {
  const data = await API.getFAQPageData()

  return {
    props: { data },
  }
}

const FAQPage = ({ data }: { data: FAQPageProps }) => {
  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('')
    setActiveComponentId('')
    setCurrentMode(modeEnum.DEVELOPERS)
  }, [setActiveCategoryId, setActiveComponentId, setCurrentMode])

  return (
    <DefaultLayout>
      <Container>
        <h1 className="vf-guideline__large-heading">FAQ</h1>
        <FAQ entries={data.entries} />
      </Container>
    </DefaultLayout>
  )
}

FAQPage.showSubMenu = true
FAQPage.showSidebar = true
export default FAQPage
