import { useContext, useEffect } from 'react'
import styled from 'styled-components'

import { DefaultLayout } from '../../components/DefaultLayout'
import PageProvider from '../../context/state'
import { modeEnum } from '../../utils/siteStructure'

const Container = styled.div`
  .vf-guideline__design-principle-image {
    margin-bottom: 44px;
  }
`

const DesignPrinciples = ({}) => {
  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('')
    setActiveComponentId('')
    setCurrentMode(modeEnum.DEVELOPERS)
  }, [setActiveComponentId, setActiveCategoryId, setCurrentMode])

  return (
    <DefaultLayout>
      <Container>
        <div className="vf-guideline__container">
          <h1 className="vf-guideline__large-heading">Design Principles</h1>
          <div>
            <img
              className="vf-guideline__design-principle-image"
              src="/assets/img/design_p_1.png"
              alt="Eye placed on wavy water background."
            />
            <h2 className="vf-component__large-heading">Focused</h2>

            <p className="vf-component__paragraph">
              We are focused on the message we bring across and goals we strive
              for. We create straightforward clear user interfaces.
            </p>
            <p>
              <strong>Best practices</strong>
            </p>
            <div className="vf-component__paragraph">
              – Create one focal point per screen <br />
              – It must be clear what the primary, secondary and tertiary
              messages are <br />– Give focus when attention is needed from the
              user
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <div>
            <img
              className="vf-guideline__design-principle-image"
              src="/assets/img/design_p_2.png"
              alt="Outline of person with arrows pointing outwards on a picture of forest"
            />
            <h2 className="vf-component__large-heading">Competent</h2>
            <h3 className="vf-component__small-heading">
              We are trustworthy and competent. We create open proof driven user
              interfaces.
            </h3>
            <p>
              <strong>Best practices</strong>
            </p>
            <div className="vf-component__paragraph">
              – The content shown will be personalised based on the users’
              context <br /> – Show relevant content or data as soon as possible
              without the user having too many interactions <br />- Make it
              clear what the users’ influence is on a green and cleaner
              environment, whenever possible
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <div>
            <img
              className="vf-guideline__design-principle-image"
              src="/assets/img/design_p_3.png"
              alt="Outline of person with outline of heart inside body on background of rocks."
            />
            <h2 className="vf-component__large-heading">Personal</h2>
            <h3 className="vf-component__small-heading">
              We are relevant and personal. We create a personalised experience,
              with high attention for user interaction and data.
            </h3>
            <p>
              <strong>Best practices</strong>
            </p>
            <div className="vf-component__paragraph">
              – Always substantiate statements with a burden of proof <br />
              – Use the minimal number of elements to convey the message
              <br />– Each interaction element on the page contributes to the
              users’ goal
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <div>
            <img
              className="vf-guideline__design-principle-image"
              src="/assets/img/design_p_4.png"
              alt="Two talk bubbles with logs as background"
            />
            <h2 className="vf-component__large-heading">Conversational</h2>
            <h3 className="vf-component__small-heading">
              How we do this? We don't inform our customer, we enable an ongoing
              conversation to activate them. This keeps our focus on the
              customer needs.
            </h3>
            <p>
              <strong>Best practices</strong>
            </p>
            <div className="vf-component__paragraph">
              – Always be available for questions of the user <br />
              – Communicate in a way you would in real life. Ask questions and
              provide information in a human way <br />– Give the user
              continuous feedback by using micro interactions
            </div>
          </div>
        </div>
      </Container>
    </DefaultLayout>
  )
}

DesignPrinciples.showSubMenu = true
DesignPrinciples.showSidebar = true

export default DesignPrinciples
