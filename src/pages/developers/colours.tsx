import { useContext, useEffect } from 'react'
import { ComponentPageLayout } from '../../components/ComponentPageLayout'
import { DefaultLayout } from '../../components/DefaultLayout'
import PageProvider from '../../context/state'
import { modeEnum } from '../../utils/siteStructure'

function ColoursPage() {
  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('')
    setActiveComponentId('')
    setCurrentMode(modeEnum.DEVELOPERS)
  }, [setActiveComponentId, setActiveCategoryId, setCurrentMode])
  return (
    <DefaultLayout>
      <ComponentPageLayout
        componentId="colours"
        title="Colours"
        gitComponentSrcUrl="examples/colours/colours/colours.html"
      >
        <p className="vf-component__paragraph">
          Documentation and examples for colours.
        </p>
      </ComponentPageLayout>
    </DefaultLayout>
  )
}

ColoursPage.showSubMenu = true
ColoursPage.showSidebar = true

export default ColoursPage
