import { useRouter } from 'next/router'
import { useContext, useEffect } from 'react'
import styled from 'styled-components'
import { DefaultLayout } from '../../components/DefaultLayout'
import PageProvider from '../../context/state'
import { modeEnum } from '../../utils/siteStructure'

const Container = styled.div`
  .get-started {
    &__css-links {
      display: block;
      font-size: 0.75rem;
      margin: 1rem 0;
      @media screen and (min-width: 1024px) {
        font-size: ${({ theme }) => theme.fontSize};
        padding: 1.5rem;
      }
    }

    &__examples {
      background: #fff;
      border: 1px solid #eee;
      margin-bottom: 20px;
      padding: 0.5rem;
      width: 100%;

      @media screen and (min-width: 1024px) {
        padding: 1.5rem;
      }
    }
  }
`

export async function getStaticPaths() {
  return {
    paths: [
      {
        params: { getStarted: ['get-started'] },
      },
    ],
    fallback: false,
  }
}

export async function getStaticProps() {
  return {
    props: {},
  }
}

const GetStarted = () => {
  const router = useRouter()
  const { getStarted: getStarted = [''] } = router.query

  const framework = getStarted[1]

  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('')
    setActiveComponentId('')
    setCurrentMode(modeEnum.DEVELOPERS)
  }, [setActiveComponentId, setActiveCategoryId, setCurrentMode])

  return (
    <DefaultLayout>
      <Container>
        <div className="vf-component__full-container">
          <h1 className="vf-guideline__large-heading">Developers</h1>
          <h2 className="vf-component__heading">Usage of this platform</h2>
          <p className="vf-component__paragraph">
            This platform is meant to be used as a reference for styling and as
            a foundation for the npm-package called <code>vf-dds-vanilla</code>.
            It uses a technology called <code>CodeSandbox</code> as a tool for a
            visual representation and some code attached as a simple example.
          </p>
          <h2 className="vf-component__heading">
            Code base and version control
          </h2>
          <p className="vf-component__paragraph">
            Our repository is hosted on Bitbucket and split into two parts,{' '}
            <a
              href="https://bitbucket.org/thebrewery/vattenfall-dds/src"
              target="_blank"
              rel="noreferrer"
            >
              the site
            </a>{' '}
            and{' '}
            <a
              href="https://bitbucket.org/thebrewery/vf-dds-vanilla/src"
              target="_blank"
              rel="noreferrer"
            >
              the npm package
            </a>
            . The npm package is hosted on npm,{' '}
            <a
              href="https://www.npmjs.com/package/@vf-dds/vf-dds-vanilla"
              target="_blank"
              rel="noreferrer"
            >
              here
            </a>
            . The package will follow the rules of{' '}
            <a href="https://semver.org/" target="_blank" rel="noreferrer">
              semver
            </a>
            .
          </p>
          <h2 className="vf-component__heading">Quick start</h2>
          <p className="vf-component__paragraph">
            The DDS provides a reusable HTML, CSS and JavaScript-library, for
            use with HTML and vanilla JavaScript. The project is created using
            some utilities like SCSS and webpack, but that should not have any
            effect on the end product. The library enables developers to use
            mark-up, styles behaviour supplied by us.
          </p>
          <h3 className="vf-component__small-heading">Installation</h3>
          <ul>
            <li>
              Install using yarn <code>yarn add @vf-dds/vf-dds-vanilla</code>
            </li>
            <li>
              Install using yarn <code>npm install @vf-dds/vf-dds-vanilla</code>
            </li>
          </ul>
          <h3 className="vf-component__small-heading">CSS</h3>
          <p className="vf-component__paragraph">
            CSS is divided at component level (i.e. <code>buttons</code>),
            category level (i.e. <code>selection</code>), at at a global level.
            Paste links similar to these to access the styles.
          </p>
          <div className="get-started__examples">
            <code className="get-started__css-links">
              {`<link
									rel='stylesheet'
									href='node_modules/@vf-dds/vf-dds-vanilla/src/style/component-css/buttons.css'
								/>`}
            </code>
            <code className="get-started__css-links">
              {`<link
									rel='stylesheet'
									href='node_modules/@vf-dds/vf-dds-vanilla/src/style/component-css/buttons.css'
								/>`}
            </code>
            <code className="get-started__css-links">
              {`<link
									rel='stylesheet'
									href='node_modules/@vf-dds/vf-dds-vanilla/src/style/category-css/selection.css'
								/`}
            </code>
            <code className="get-started__css-links">
              {`<link
									rel='stylesheet'
									href='node_modules/@vf-dds/vf-dds-vanilla/src/style/category-css/indicators.css'
								/>`}
            </code>
            <code className="get-started__css-links">
              {`<link
									rel='stylesheet'
									href='node_modules/@vf-dds/vf-dds-vanilla/src/style/style.css'
								/>`}
            </code>
          </div>
          <h3 className="vf-component__small-heading">Alternate imports</h3>
          <p className="vf-component__paragraph">
            If you instead would like to import the styling via JavaScript you
            can write imports that look like:{' '}
          </p>
          <div className="get-started__examples">
            <code className="get-started__css-links">
              import '@vf-dds/vf-dds-vanilla/src/style/component-css/buttons.css
            </code>
            <p className="vf-component__paragraph">
              or if you want to import the SCSS-files directly, you would write
              something like:{' '}
            </p>
            <code className="get-started__css-links">
              import
              '@vf-dds/vf-dds-vanilla/src/components/style/selection/buttons.scss'
            </code>
          </div>
        </div>
        <div className="vf-guideline__full-container">
          <h2 className="vf-component__heading">JavaScript</h2>
          <p className="vf-component__paragraph">
            The JavaScript supplied to this library is divided at a component
            level, and at a global level. Same as with the CSS you can import in
            your HTML or directly in your own js-file. Be aware that all of the
            components are written with specific class-names or ids in mind, so
            pay close attention to that when using them.
          </p>
          <div className="get-started__examples">
            HTML examples
            <code className="get-started__css-links">{`<script src='node_modules/@vf-dds/vf-dds-vanilla/src/components/selection/standard-dropdown.js>`}</code>
            JS examples
            <code className="get-started__css-links">
              import
              '@vf-dds/vf-dds-vanilla/src/components/standard-dropdown.js'
            </code>
          </div>
          <h3 className="vf-component__small-heading">HTML Starter Template</h3>
          <div className="get-started__examples">
            <pre style={{ whiteSpace: 'pre-wrap' }}>
              {`
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel='stylesheet' href='node_modules/@vf-dds/vf-dds-vanilla/src/style/component-css/buttons.css' />
    <link rel='stylesheet' href='node_modules/@vf-dds/vf-dds-vanilla/src/style/category-css/selection.css' />
    <link rel='stylesheet' href='node_modules/@vf-dds/vf-dds-vanilla/src/style/category-css/indicators.css' />

    <title>Vattenfall Design System Boilerplate!</title>
  </head>
  <body>
    <h1>I'm using Vattenfall Design System!</h1>

    <script src='node_modules/@vf-dds/vf-dds-vanilla/src/components/selection/standard-dropdown.js'></script>
    <!-- Optional JavaScript -->
  </body>
</html>
                `}
            </pre>
          </div>
        </div>
      </Container>
    </DefaultLayout>
  )
}

GetStarted.showSubMenu = true
GetStarted.showSidebar = true

export default GetStarted
