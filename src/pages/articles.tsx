import styled from 'styled-components'

import AllArticleLayout from '../article-components/AllArticleLayout'
import ContentWrapper from '../components/ContentWrapper'
import { DefaultLayout } from '../components/DefaultLayout'
import { API, ArticleListingProps } from '../utils/api'

const ArticlesPageStyle = styled.div`
  .articles__card-container {
    grid-template-columns: 1fr;
    display: grid;
    grid-gap: 24px;

    @media screen and (min-width: 1024px) {
      grid-template-columns: repeat(2, 1fr);
    }
    @media screen and (min-width: 1400px) {
      grid-template-columns: repeat(3, 1fr);
    }
  }
`

export const getStaticProps = async () => {
  let articleListings: ArticleListingProps[] = await API.getArticles().then(
    (articleList: ArticleListingProps[]) =>
      articleList.filter(({ article }) => article?.articleId)
  )

  articleListings = articleListings.filter(
    (articleListing: ArticleListingProps) => {
      return process.env.NEXT_PUBLIC_PRODUCTION_CONTENT
        ? articleListing.article.showOnProd
        : true
    }
  )

  return { props: { articleListings } }
}

function ArticlesPage({
  articleListings,
}: {
  articleListings: ArticleListingProps[]
}) {
  const allArticles = articleListings.map(
    ({ article }: ArticleListingProps) => article
  )

  return (
    <DefaultLayout>
      <ContentWrapper>
        <ArticlesPageStyle className="vf-guideline__full-container">
          <h1 className="vf-guideline__large-heading">Articles</h1>
          <AllArticleLayout articles={allArticles} title="articles" />
        </ArticlesPageStyle>
      </ContentWrapper>
    </DefaultLayout>
  )
}

export default ArticlesPage
