import { useContext, useEffect } from 'react'
import styled from 'styled-components'

import { DefaultLayout } from '../../components/DefaultLayout'
import PageProvider from '../../context/state'
import { modeEnum } from '../../utils/siteStructure'

const Container = styled.div`
  .vf-guideline {
    &__circle-container {
      margin-top: 2rem;
      display: flex;
      margin-bottom: 2rem;

      div {
        padding-left: 12px;
        padding-right: 12px;
      }
    }

    &__small-circle {
      aspect-ratio: 1/1;
      border-radius: 50%;
      flex: 1;
      height: 168px;
      width: 168px;
      padding: 1rem;
    }

    &__big-circle-container {
      flex: 1;
    }
    &__circle {
      aspect-ratio: 1/1;
      border-radius: 50%;
      flex: 1;
      padding: 1rem;

      &-70 {
        opacity: 0.7;
      }

      &-50 {
        opacity: 0.5;
      }

      &-30 {
        opacity: 0.3;
      }

      &-10 {
        opacity: 0.1;
      }

      &--solar-yellow {
        background: ${({ theme }) => theme.colours.primary__solar_yellow};
      }

      &--ocean-blue {
        background: ${({ theme }) => theme.colours.primary__ocean_blue};
      }

      &--aura-white {
        background: ${({ theme }) => theme.colours.primary__aura_white};
        border: 1px solid black;
      }

      &--coal-black {
        background: ${({ theme }) => theme.colours.primary__coal_black};
      }

      &--magnetic-gray {
        background: ${({ theme }) => theme.colours.primary__magnetic_grey};
      }

      &--dark-green {
        background: ${({ theme }) => theme.colours.secondary__dark_green};
      }

      &--dark-blue {
        background: ${({ theme }) => theme.colours.secondary__dark_blue};
      }

      &--pink {
        background: ${({ theme }) => theme.colours.secondary__pink};
      }

      &--dark-purple {
        background: ${({ theme }) => theme.colours.secondary__dark_purple};
      }

      &--green {
        background: ${({ theme }) => theme.colours.secondary__green};
      }

      &--red {
        background: ${({ theme }) => theme.colours.secondary__red};
      }

      &--purple {
        background: ${({ theme }) => theme.colours.secondary__purple};
      }

      &--light-yellow {
        background: ${({ theme }) => theme.colours.bg__light_yellow};
      }

      &--light-green {
        background: ${({ theme }) => theme.colours.bg__light_green};
      }

      &--light-blue-alt {
        background: ${({ theme }) => theme.colours.bg__light_blue__alt};
      }

      &--light-blue {
        background: ${({ theme }) => theme.colours.bg__light_blue};
      }

      &--light-red {
        background: ${({ theme }) => theme.colours.bg__light_red};
      }

      &--tertiary-light-grey {
        background: ${({ theme }) => theme.colours.tertiary__light_grey};
      }

      &--light-grey {
        background: ${({ theme }) => theme.colours.bg__light_grey};
      }

      &--ash-blue {
        background: ${({ theme }) => theme.colours.tertiary__ash_blue};
      }

      &--dark-ash-blue {
        background: ${({ theme }) => theme.colours.tertiary__dark_ash_blue};
      }

      &--lighter-grey {
        background: ${({ theme }) => theme.colours.tertiary__lighter_grey};
      }

      &--medium-grey {
        background: ${({ theme }) => theme.colours.tertiary__medium_grey};
      }

      &--medium-dark-grey {
        background: ${({ theme }) => theme.colours.tertiary__medium_dark_grey};
      }

      &--dark-grey {
        background: ${({ theme }) => theme.colours.tertiary__dark_grey};
      }

      &--hydro {
        background: ${({ theme }) => theme.colours.energy__hydro};
      }

      &--wind {
        background: ${({ theme }) => theme.colours.energy__wind};
      }

      &--solar {
        background: ${({ theme }) => theme.colours.energy__solar};
      }

      &--biomass {
        background: ${({ theme }) => theme.colours.energy__biomass};
      }

      &--coal {
        background: ${({ theme }) => theme.colours.energy__coal};
      }

      &--gas {
        background: ${({ theme }) => theme.colours.energy__gas};
      }

      &--nuclear {
        background: ${({ theme }) => theme.colours.energy__nuclear};
      }

      &--district-heating {
        background: ${({ theme }) => theme.colours.energy__district_heating};
      }

      &-heading,
      &-text {
        font-size: ${({ theme }) => theme.fontSize};
        text-align: center;
      }

      &-heading {
        font-weight: bold;
        padding-top: 2rem;
      }
    }
  }
`

const Colours = () => {
  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('')
    setActiveComponentId('')
    setCurrentMode(modeEnum.DESIGNERS)
  }, [setActiveCategoryId, setActiveComponentId, setCurrentMode])
  return (
    <DefaultLayout>
      <Container>
        <h1 className="vf-guideline__large-heading">Colours</h1>
        <div className="vf-guideline__container">
          <p className="vf-component__paragraph">
            We lead with white to reflect confidence, transparency and
            premiumness. It provides air and space as well as allowing the eye
            to focus on what is important.
          </p>
          <p className="vf-component__paragraph">
            Vattenfall Blue is our foundation, reflecting both the water and
            sky. Vattenfall Yellow represents the sun and is a colour of vision
            and warmth. Vattenfall Grey is the colour of the wordmark in the
            Vattenfall logotype.
          </p>
          <p className="vf-component__paragraph">
            The secondary colours are inspired by the Nordic nature and are
            primarily used as accents in information graphics and illustrations.
            The background colours are also used as backdrops for sections of
            texts.
          </p>
          <p className="vf-component__paragraph">
            — Vattenfall Yellow and Vattenfall Blue are our primary brand
            colours and should not be used for backgrounds.
          </p>

          <p className="vf-component__paragraph">
            — White is a key colour of the brand expression. We always lead with
            white.
          </p>
          <p className="vf-component__paragraph">
            — Secondary colours are used for information graphics, graphs and
            illustrations.
          </p>
          <p className="vf-component__paragraph">
            <strong>Rules</strong>
          </p>
          <p className="vf-component__paragraph">
            It’s important for Vattenfall to meet all web accessibility
            standards. Vattenfall encourages meeting the minimum contrast ratios
            specified by WCAG 2.1 Level AA for text, icons, other indicators,
            and background colours.
          </p>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Primary colours</h2>
          <p className="vf-component__paragraph">
            The primary colours define our brand expression and our brand core
            is white, blue and yellow. The balance between our primary and
            secondary colours are 80/20, and we always lead with our primary
            colours.
          </p>
          <div className="vf-guideline__circle-container">
            <div className="vf-guideline__big-circle-container">
              <div className="vf-guideline__circle vf-guideline__circle--solar-yellow" />
              <h3 className="vf-guideline__circle-heading">Solar yellow</h3>
              <p className="vf-guideline__circle-text">#FFDA00</p>
            </div>
            <div className="vf-guideline__big-circle-container">
              <div className="vf-guideline__circle vf-guideline__circle--ocean-blue" />
              <h3 className="vf-guideline__circle-heading">Ocean blue</h3>
              <p className="vf-guideline__circle-text">#2071B5</p>
            </div>

            <div className="vf-guideline__big-circle-container">
              <div className="vf-guideline__circle vf-guideline__circle--aura-white" />
              <h3 className="vf-guideline__circle-heading">Aura white</h3>
              <p className="vf-guideline__circle-text">#FFFFFF</p>
            </div>
          </div>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--coal-black" />
              <h3 className="vf-guideline__circle-heading">Coal black</h3>
              <p className="vf-guideline__circle-text">#000000</p>
            </div>

            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--magnetic-gray" />
              <h3 className="vf-guideline__circle-heading">Magnetic gray</h3>
              <p className="vf-guideline__circle-text">#4E4B48</p>
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Secondary Colours</h2>
          <p className="vf-component__paragraph">
            These colours are used in graphs, illustrations and infographics
            only. Some can be used as UX colour, e.g. green and red.
          </p>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--dark-green" />
              <h3 className="vf-guideline__circle-heading">Dark Green</h3>
              <p className="vf-guideline__circle-text">#005C63</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--dark-blue" />
              <h3 className="vf-guideline__circle-heading">Dark blue</h3>
              <p className="vf-guideline__circle-text">#1E324F</p>
            </div>

            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--pink" />
              <h3 className="vf-guideline__circle-heading">Pink</h3>
              <p className="vf-guideline__circle-text">#D1266B</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--dark-purple" />
              <h3 className="vf-guideline__circle-heading">Dark Purple</h3>
              <p className="vf-guideline__circle-text">#85254B</p>
            </div>
          </div>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--green" />
              <h3 className="vf-guideline__circle-heading">Green</h3>
              <p className="vf-guideline__circle-text">#3DC07C</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--red" />
              <h3 className="vf-guideline__circle-heading">Red</h3>
              <p className="vf-guideline__circle-text">#F93B18</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--purple" />
              <h3 className="vf-guideline__circle-heading">Purple</h3>
              <p className="vf-guideline__circle-text">#9B62C3</p>
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Colour tones</h2>
          <p className="vf-component__paragraph">
            When creating 1-coloured graphs, you should primarily use these
            percent-values to differentiate sections of the graph. This is
            exemplified below in Vattenfall Pink, but you can use any of the
            primary or secondary colors as a base.
          </p>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--pink" />
              <h3 className="vf-guideline__circle-heading">100%</h3>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--pink vf-guideline__circle-70" />
              <h3 className="vf-guideline__circle-heading">70%</h3>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--pink vf-guideline__circle-50" />
              <h3 className="vf-guideline__circle-heading">50%</h3>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--pink vf-guideline__circle-30" />
              <h3 className="vf-guideline__circle-heading">30%</h3>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--pink vf-guideline__circle-10" />
              <h3 className="vf-guideline__circle-heading">10%</h3>
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Background colours</h2>
          <p className="vf-component__paragraph">
            Background colors are to be used to group important objects together
            and/or highlight important information.
          </p>
          <p className="vf-component__paragraph">
            <strong>Do’s</strong>
          </p>
          <div className="vf-component__paragraph">
            – Background colors can also be used to highlight important
            information. <br />
            – Graphs can be placed on white backgrounds <br />
            – Graphs can also be placed on a Vattenfall Light Grey background.
            <br />
            – Icons can also be placed on white backgrounds.
            <br />
            – Icons can also be placed on Vattenfall Light Grey backgrounds.
            <br />
            – Illustrations can be placed on white backgrounds.
            <br />– Illustrations can also be placed on Vattenfall Light Grey
            backgrounds.
          </div>
          <p className="vf-component__paragraph">
            <strong>Don'ts</strong>
          </p>
          <div className="vf-component__paragraph">
            – Do not place graphs on other background colors, like Vattenfall
            Light Green. <br />– Do not place illustrations on other background
            colors, like Vattenfall Light Red.
          </div>
          <p className="vf-component__paragraph">
            <strong>Use</strong>
          </p>
          <p className="vf-component__paragraph">
            Notification colors: Pastel Yellow (attention) , Pastel green
            (positive) and Pastel Red (warning) Background color in content
            blocks: Pastel Blue, Pastel Grey and Pastel Green (never use Light
            Blue Alt.)
          </p>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--light-yellow" />
              <h3 className="vf-guideline__circle-heading">Light Yellow</h3>
              <p className="vf-guideline__circle-text">#FFFEE5</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--light-green" />
              <h3 className="vf-guideline__circle-heading">Light Green</h3>
              <p className="vf-guideline__circle-text">#EDF9F3</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--light-blue-alt" />
              <h3 className="vf-guideline__circle-heading">Light Blue alt.</h3>
              <p className="vf-guideline__circle-text">#EBF2F3</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--light-blue" />
              <h3 className="vf-guideline__circle-heading">Light Blue</h3>
              <p className="vf-guideline__circle-text">#EDF1F6</p>
            </div>
          </div>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--light-red" />
              <h3 className="vf-guideline__circle-heading">Light Red</h3>
              <p className="vf-guideline__circle-text">#FEF0EA</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--light-grey" />
              <h3 className="vf-guideline__circle-heading">Light Grey</h3>
              <p className="vf-guideline__circle-text">#F2F2F2</p>
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Tertiary</h2>
          <p className="vf-component__paragraph">
            The tertiary colors are mainly grey tints. We use ‘Lighter Grey’ for
            1px horizontal borders and the ‘Light Grey’ for inputfield borders.
          </p>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--ash-blue" />
              <h3 className="vf-guideline__circle-heading">Ash Blue</h3>
              <p className="vf-guideline__circle-text">#869BAD</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--dark-ash-blue" />
              <h3 className="vf-guideline__circle-heading">Dark Ash Blue</h3>
              <p className="vf-guideline__circle-text">#69788C</p>
            </div>

            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--lighter-grey" />
              <h3 className="vf-guideline__circle-heading">Lighter Grey</h3>
              <p className="vf-guideline__circle-text">#E6E6E6</p>
            </div>

            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--tertiary-light-grey" />
              <h3 className="vf-guideline__circle-heading">Light Grey</h3>
              <p className="vf-guideline__circle-text">#CCCCCC</p>
            </div>
          </div>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--medium-grey" />
              <h3 className="vf-guideline__circle-heading">Medium Grey</h3>
              <p className="vf-guideline__circle-text">#999999</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--medium-dark-grey" />
              <h3 className="vf-guideline__circle-heading">Medium Dark Grey</h3>
              <p className="vf-guideline__circle-text">#767676</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--dark-grey" />
              <h3 className="vf-guideline__circle-heading">Dark Grey</h3>
              <p className="vf-guideline__circle-text">#666666</p>
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Energy source colors</h2>
          <p className="vf-component__paragraph">
            Recommended colours for specific energy sources have been developed
            mainly to be used for information graphs. Please note that this is a
            recommendation and not a rule. Colour coding for regulation
            compliance. The categories are: Renewable, Nuclear, District heating
            and Fossil.
          </p>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--hydro" />
              <h3 className="vf-guideline__circle-heading">Hydro</h3>
              <p className="vf-guideline__circle-text">#2DA55D</p>
            </div>

            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--wind" />
              <h3 className="vf-guideline__circle-heading">Wind</h3>
              <p className="vf-guideline__circle-text">#4FCC51</p>
            </div>

            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--solar" />
              <h3 className="vf-guideline__circle-heading">Solar</h3>
              <p className="vf-guideline__circle-text">#81E0A8</p>
            </div>

            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--biomass" />
              <h3 className="vf-guideline__circle-heading">Biomass</h3>
              <p className="vf-guideline__circle-text">#375E4E</p>
            </div>
          </div>
          <div className="vf-guideline__circle-container">
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--coal" />
              <h3 className="vf-guideline__circle-heading">Coal</h3>
              <p className="vf-guideline__circle-text">#E88A74</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--gas" />
              <h3 className="vf-guideline__circle-heading">Gas</h3>
              <p className="vf-guideline__circle-text">#D85067</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--nuclear" />
              <h3 className="vf-guideline__circle-heading">Nuclear</h3>
              <p className="vf-guideline__circle-text">#E6E6E6</p>
            </div>
            <div>
              <div className="vf-guideline__small-circle vf-guideline__circle--district-heating" />
              <h3 className="vf-guideline__circle-heading">District heating</h3>
              <p className="vf-guideline__circle-text">#A376CC</p>
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">What color do lines get?</h2>
          <p className="vf-component__paragraph">
            Sometimes lines are used to seperate planes from each other.
            Depending on the background the following colors are used for lines:
          </p>
          <div className="vf-component__paragraph">
            A line on white: 'Lighter Grey' #E6E6E6 <br />
            A line on a background color, for example light blue: ‘Light Grey’
            #CCCCCC <br />
            A line on a blue or other colored plane: 50% white. <br />
          </div>
        </div>
      </Container>
    </DefaultLayout>
  )
}

Colours.showSubMenu = true
Colours.showSidebar = true
export default Colours
