import { GetStaticPropsContext } from 'next'
import { useRouter } from 'next/router'
import { ComponentType, useContext, useEffect } from 'react'
import { Node, getSiteStructure, modeEnum } from '../../../utils/siteStructure'

// Graphics & Motion
import { Illustrations } from '../../../guidelines/graphics-and-motion/Illustrations'
import { InfoGraphics } from '../../../guidelines/graphics-and-motion/InfoGraphics'
import { Motion } from '../../../guidelines/graphics-and-motion/Motion'

// Dialogues
import { GDPRConsent } from '../../../components/dialogues/GDPRConsent'
import { MessageBanners } from '../../../components/dialogues/MessageBanners'
import { Modal } from '../../../components/dialogues/Modal'
import { Notification } from '../../../components/dialogues/Notification'
import { Tooltip } from '../../../guidelines/dialogues/Tooltip'

// Campaign assets
// import { TextHeroBlock } from '../../../components/campaign-assets/TextHeroBlock'
// import { AnimatedKeyNumber } from '../../../components/campaign-assets/AnimatedKeyNumber'
// import { HighlightInformationContainerBlock } from '../../../components/campaign-assets/HighlighInformationContainerBlock'
import { TheEdit } from '../../../components/campaign-assets/TheEdit'
import { IndustrialEmissionsFaceMist } from '../../../components/campaign-assets/IndustrialEmissionsFaceMist'

// Icons
import { Favicon } from '../../../components/icons/Favicon'
import { Action } from '../../../guidelines/icons/Action'
import { Identification } from '../../../guidelines/icons/Identification'
import { Intro as IconsIntro, Intro } from '../../../guidelines/icons/Intro'
import { Navigation } from '../../../guidelines/icons/Navigation'
import { SmartHome } from '../../../guidelines/icons/SmartHome'
import { SocialMedia } from '../../../guidelines/icons/SocialMedia'
// import { Status } from '../../../components/icons/Status'
import { Usage } from '../../../guidelines/icons/Usage'

// Indicators
import { Badge } from '../../../components/indicators/Badge'
import { Pagination } from '../../../components/indicators/Pagination'
import { Preloader } from '../../../components/indicators/Preloader'
import { Progress } from '../../../components/indicators/Progress'
import { ProgressIndicator } from '../../../components/indicators/ProgressIndicator'
import { Spinner } from '../../../guidelines/indicators/Spinner'

// Input
import { Search } from '../../../components/input/Search'
import { Field } from '../../../guidelines/input/Field'
import { Number } from '../../../guidelines/input/Number'
import { Textarea } from '../../../guidelines/input/Textarea'

// Logos
import { Colourways } from '../../../components/logos/Colourways'
import { Symbol } from '../../../components/logos/Symbol'
import { TopBar } from '../../../components/logos/TopBar'
import { Horizontal } from '../../../guidelines/logos/Horizontal'
import { KeyPrinciples } from '../../../guidelines/logos/KeyPrinciples'
import { Stacked } from '../../../guidelines/logos/Stacked'

// Modules
import { Article } from '../../../components/modules/Article'
import { ExpoCarousel } from '../../../components/modules/ExpoCarousel'
import { Footer } from '../../../components/modules/Footer'
import { Forms } from '../../../components/modules/Forms'
import { Image } from '../../../components/modules/Image'
import { ImageModule } from '../../../components/modules/ImageModule'
import { Investors } from '../../../components/modules/Investors'
import { LinkedList } from '../../../components/modules/LinkedList'
import { Media } from '../../../components/modules/Media'
import { News } from '../../../components/modules/News'
import { Numbers } from '../../../components/modules/Numbers'
import { OffsetImage } from '../../../components/modules/OffsetImage'
import { People } from '../../../components/modules/People'
import { Quote } from '../../../components/modules/Quote'
import { Receipt } from '../../../components/modules/Receipt'
import { Subscription } from '../../../components/modules/Subscription'
import { Testimonials } from '../../../components/modules/Testimonials'
import { Upload } from '../../../components/modules/Upload'
import { Carousel } from '../../../guidelines/modules/Carousel'
import { Hero } from '../../../guidelines/modules/Hero'

// Navigation
import { Header } from '../../../components/navigation/Header'
import { Search as NavigationSearch } from '../../../guidelines/navigation/Search'
import { Tabs } from '../../../guidelines/navigation/Tabs'
import { TextLinks } from '../../../guidelines/navigation/TextLinks'

// Selection
import { Calendar } from '../../../components/selection/Calendar'
import { Toggle } from '../../../components/selection/Toggle'
import { Buttons } from '../../../guidelines/selection/Buttons'
import { Checkbox } from '../../../guidelines/selection/Checkbox'
import { Dropdown } from '../../../guidelines/selection/Dropdown'
import { Option } from '../../../guidelines/selection/Option'
import { RadioButton } from '../../../guidelines/selection/RadioButton'
import { Slider } from '../../../guidelines/selection/Slider'
import { Value } from '../../../guidelines/selection/Value'

// Structure
import { Dividers } from '../../../components/structure/Dividers'
import { Responsive } from '../../../components/structure/Responsive'
import { Sizing } from '../../../components/structure/Sizing'
import { Grid } from '../../../guidelines/structure/Grid'
import { Spacing } from '../../../guidelines/structure/Spacing'

// Tables and lists
import { LinkList } from '../../../components/tables-and-lists/LinkList'
import { NumberedList } from '../../../components/tables-and-lists/NumberedList'
import { Table } from '../../../components/tables-and-lists/Table'
import { TextList } from '../../../components/tables-and-lists/TextList'

// Typography
import { Emphasis } from '../../../components/typography/Emphasis'
import { Errors } from '../../../components/typography/Errors'
import { Link } from '../../../components/typography/Link'
import { Status } from '../../../components/typography/Status'
import { Bodycopy } from '../../../guidelines/typography/Bodycopy'
import { Headers } from '../../../guidelines/typography/Headers'
import { Intro as TypographyIntro } from '../../../guidelines/typography/Intro'
import { Introduction as TypographyIntroduction } from '../../../guidelines/typography/Introduction'

// Accessibility Guidelines
import { AccessibilityDeveloper } from '../../../components/accessibility-guidelines/AccessibilityDeveloper'
import { AccessibilityDesigner } from '../../../components/accessibility-guidelines/AccessibilityDesigner'
import { AccessibilityEditor } from '../../../components/accessibility-guidelines/AccessibilityEditor'

import { ComponentPageLayout } from '../../../components/ComponentPageLayout'
import { DefaultLayout } from '../../../components/DefaultLayout'
import PageProvider from '../../../context/state'

const idDocMap: Record<string, ComponentType> = {
  // Dialogues
  'gdpr-consent': GDPRConsent,
  'message-banners': MessageBanners,
  modal: Modal,
  notification: Notification,
  tooltip: Tooltip,

  // Campaign assets,
  // 'text-hero-block': TextHeroBlock,
  // 'highlight-information-container-block': HighlightInformationContainerBlock,
  // 'animated-key-number': AnimatedKeyNumber,
  'the-edit': TheEdit,
  'industrial-emissions-face-mist': IndustrialEmissionsFaceMist,

  // Graphics & Motion
  illustrations: Illustrations,
  'info-graphics': InfoGraphics,
  motion: Motion,

  // Icons
  'icons-intro': IconsIntro,
  action: Action,
  favicon: Favicon,
  identification: Identification,
  'icon-intro': Intro,
  navigation: Navigation,
  'smart-home': SmartHome,
  'social-media': SocialMedia,
  // status: Status,
  usage: Usage,

  // Indicators
  badge: Badge,
  pagination: Pagination,
  preloader: Preloader,
  'progress-indicator': ProgressIndicator,
  progress: Progress,
  spinner: Spinner,

  // Input
  field: Field,
  number: Number,
  'input-search': Search,
  textarea: Textarea,

  // Logos
  colourways: Colourways,
  horizontal: Horizontal,
  'key-principles': KeyPrinciples,
  stacked: Stacked,
  symbol: Symbol,
  'top-bar': TopBar,

  // Modules
  article: Article,
  carousel: Carousel,
  'expo-carousel': ExpoCarousel,
  footer: Footer,
  forms: Forms,
  hero: Hero,
  image: Image,
  'image-module': ImageModule,
  investors: Investors,
  'linked-list': LinkedList,
  media: Media,
  news: News,
  numbers: Numbers,
  'offset-image': OffsetImage,
  people: People,
  quote: Quote,
  receipt: Receipt,
  subscription: Subscription,
  testimonials: Testimonials,
  upload: Upload,

  // Navigation
  header: Header,
  tabs: Tabs,
  'text-links': TextLinks,
  search: NavigationSearch,

  // Selection
  buttons: Buttons,
  calendar: Calendar,
  checkbox: Checkbox,
  dropdown: Dropdown,
  option: Option,
  'radio-button': RadioButton,
  toggle: Toggle,
  value: Value,
  slider: Slider,

  // Structure
  dividers: Dividers,
  grid: Grid,
  responsive: Responsive,
  sizing: Sizing,
  spacing: Spacing,

  // Tables and lists
  'link-list': LinkList,
  table: Table,
  'text-list': TextList,
  'numbered-list': NumberedList,

  // Typography
  bodycopy: Bodycopy,
  emphasis: Emphasis,
  errors: Errors,
  link: Link,
  headers: Headers,
  'typography-intro': TypographyIntro,
  status: Status,
  'typography-introduction': TypographyIntroduction,

  // Accessibility Guidelines
  'developer-principles': AccessibilityDeveloper,
  'designer-principles': AccessibilityDesigner,
  'editor-principles': AccessibilityEditor,
}

export async function getStaticPaths() {
  const siteStructure = getSiteStructure()
  function getItemsRecursively(node: Node): Node[] {
    if (node.isLeaf()) {
      return [node]
    }
    return node.children.flatMap(c => getItemsRecursively(c)).filter(c => c)
  }

  const desItems = getItemsRecursively(
    siteStructure[modeEnum.DESIGNERS]
  ).filter(it => it.isComponent)
  const paths = desItems.map(node => {
    // Filter out 'designers' and 'components' since we're already on that path
    const slugs = node
      .getSlugs()
      .filter(s => !['designers', 'components'].includes(s))

    return {
      params: {
        slugs: slugs,
      },
    }
  })

  console.log(paths.flatMap(x => x.params.slugs).includes('data-visualization'))

  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps(context: GetStaticPropsContext) {
  return {
    props: {}, // will be passed to the page component as props
  }
}

export default function ComponentPage() {
  const {
    activeComponentId,
    setActiveComponentId,
    activeCategoryId,
    setActiveCategoryId,
    setCurrentMode,
    siteStructure,
  } = useContext(PageProvider)

  const router = useRouter()
  const { slugs } = router.query

  useEffect(() => {
    // let framework = 'javascript'
    if (!slugs || slugs.length < 2 || slugs.length > 3) {
      return
    }
    setActiveCategoryId(slugs[0])
    setActiveComponentId(slugs[1])
    setCurrentMode(modeEnum.DESIGNERS)

    // NOTE: This is not really necessary at the moment since we only have vanilla..
    // if (slugs.length === 3) {
    //   setActiveFramework(slugs[2])
    // }
  }, [slugs, setActiveCategoryId, setActiveComponentId, setCurrentMode])

  let title = ''
  let gitUrl = ''
  let isNewComponent = false

  if (activeCategoryId && activeComponentId) {
    const designerCategoryNode = siteStructure[
      modeEnum.DESIGNERS
    ].children.filter(child => {
      return child.isComponentCategory
    })
    const categoryNode = designerCategoryNode.find(
      c => c.id === activeCategoryId
    )

    if (categoryNode) {
      const componentNode = categoryNode.children.find(
        c => c.id === activeComponentId
      )

      if (componentNode) {
        title = componentNode.name
        gitUrl = componentNode.gitUrl ?? ''
        isNewComponent = componentNode.newComponentIndicator
      }
    }
  }

  const DocComp = activeComponentId ? idDocMap[activeComponentId] : null
  return (
    <DefaultLayout>
      <ComponentPageLayout
        componentId={activeComponentId}
        title={title}
        isNewComponent={isNewComponent}
      >
        {DocComp ? <DocComp /> : null}
      </ComponentPageLayout>
    </DefaultLayout>
  )
}

// ComponentPage.showSubMenu = true
ComponentPage.showSidebar = true
