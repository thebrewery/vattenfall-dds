import { useContext, useEffect } from 'react'
import ReactMarkdown from 'react-markdown'
import styled from 'styled-components'

import { DefaultLayout } from '../../components/DefaultLayout'
import PageProvider from '../../context/state'
import { modeEnum } from '../../utils/siteStructure'

import changeOverTimeMd from '../../components/data-visualization/change-over-time.md'
import partToWholeChartsMd from '../../components/data-visualization/part-to-whole-charts.md'
import comparisonChartsMd from '../../components/data-visualization/comparison-charts.md'
import realTimeTrackingMd from '../../components/data-visualization/real-time-tracking.md'
import metricHighlightMd from '../../components/data-visualization/metric-highlight.md'

import { splitMarkdown } from '../../components/data-visualization/datavisualizationutils'

import Accordion from '../../components/selection/Accordion'
import { DesignPrinciples } from '../../components/data-visualization/DesignPrinciples'
import { DataChecklist } from '../../components/data-visualization/DataChecklist'
import { InfoBox } from '../../components/InfoBox'

const Container = styled.div`
  .vf-guideline__design-principle-image {
    margin-bottom: 44px;
  }
  .vf-guideline__container.datavis {
    max-width: 1080px;
    width: 100%;
  }

  .vf-bgs {
    &__visuals {
      display: grid;
      grid-auto-flow: dense;
      grid-template-columns: 1fr 1fr;
      column-gap: 1.1875em;
      margin-top: 4.0625em;
      margin-bottom: 1.5em;
      max-width: 900px;

      &--nospace {
        margin-top: 1em;
      }

      &--full {
        max-width: unset;
      }

      img {
        width: 100%;
      }

      &-heading {
        grid-column: 1 / -1;
        font-size: 1.375em;
        line-height: 1.8125em;
        margin: 0;
        margin-bottom: 1em;
      }

      &-do {
        * {
          background-color: #edf1f6;
          grid-column: 1;
          column-span: 1;
        }
      }

      &-donts {
        * {
          background-color: #fef0ea;
          grid-column: 2;
          column-span: 1;
        }
      }

      &-example-list {
        display: flex;
        flex-wrap: wrap;
        column-gap: 27px;
        row-gap: 56px;
        margin-top: 56px;
        padding-bottom: 50px;

        &__item {
          width: calc(25% - 20.25px);

          &-info {
            padding: 16px 16px 32px;
            margin-top: 10px;

            &.dos {
              background-color: #edf1f6;
            }
            &.donts {
              background-color: #ffefea;
            }

            h4 {
              font-size: 20px;
              line-height: 24px;
            }
            p {
              margin-top: 8px;
              font-size: 12px;
              font-weight: 400;
              line-height: 18px;
            }
          }

          & > img {
            width: 100%;
            height: auto;
            border: 0.66px solid #98928f;
          }
        }
      }

      &-do,
      &-donts {
        display: block;

        .dos-headings {
          align-items: flex-end;
          display: flex;
          flex-direction: row;
          gap: 0.5rem;
          margin: 0;
          padding: 0 1.25rem;

          h4,
          p {
            margin: 0;
            padding: 0;
          }

          p {
            font-size: 0.75rem;
          }
        }

        *:first-child {
          padding-top: 1.5rem;
        }

        *:last-child {
          padding-bottom: 1.5rem;
        }

        * {
          margin: 0 !important;
          padding: 0 1.25rem;
        }

        ul {
          padding: 1.125rem 1.25rem 2.375rem !important;
          list-style: none;
          display: flex;
          flex-direction: column;
          gap: 1.375rem;
        }

        li {
          padding: 0 !important;
          font-size: 0.88rem;
          line-height: 1.5rem;
        }

        p {
          line-height: 1.375em;
          padding-bottom: 1.75em;
        }

        h4 {
          font-size: 1em;
          line-height: 1.375em;
          font-weight: bold;
          padding-bottom: 0.25rem;
        }

        &-heading {
          font-size: 2em;
          line-height: 100%;
          font-weight: 700;
        }
      }
    }
  }
  .vf-component {
    &--center {
      text-align: center;
    }

    &__image {
      max-width: 900px;
      margin-bottom: 1.875rem;

      &--no-margin {
        margin: 0;
      }
    }

    &__blocks {
      display: grid;
      grid-template-columns: 1fr 1fr;
      column-gap: 1.1875em;
      margin-top: 1.5em;
      margin-bottom: 1.5em;
      max-width: 900px;

      .vf-component__image {
        max-width: 100%;
      }

      &-gray {
        width: 100%;
        height: 300px;
        background-color: #ccc;
      }

      &--nospace {
        margin-bottom: 0.5em;
      }
    }
  }
  @media screen and (max-width: 1300px) {
    .vf-bgs__visuals-example-list {
      column-gap: 24px;
      row-gap: 32px;
    }
    .vf-bgs__visuals-example-list__item {
      width: calc(50% - 12px);
    }

    .vf-guideline__container.datavis {
      max-width: 675px;
    }
  }
  @media screen and (max-width: 1024px) {
    .vf-bgs__visuals-example-list {
      column-gap: 24px;
    }
    .vf-bgs__visuals-example-list__item {
      width: calc(50% - 12px);
    }
  }
  @media screen and (max-width: 768px) {
    .vf-bgs__visuals.vf-bgs__visuals--full {
      row-gap: 1.1875em;
      grid-template-columns: 1fr;
    }
    .vf-bgs__visuals-example-list {
      row-gap: 16px;
    }
    .vf-bgs__visuals-example-list__item {
      width: 100%;
    }
  }
`

const DataVisualization = ({}) => {
  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('null')
    setActiveComponentId('null')
    setCurrentMode(modeEnum.DESIGNERS)
  }, [setActiveComponentId, setActiveCategoryId, setCurrentMode])

  const changeOverTime = splitMarkdown(changeOverTimeMd)
  const partToWholeCharts = splitMarkdown(partToWholeChartsMd)
  const comparisonCharts = splitMarkdown(comparisonChartsMd)
  const realTimeTracking = splitMarkdown(realTimeTrackingMd)
  const metricHighlight = splitMarkdown(metricHighlightMd)

  const data = [
    {
      name: 'Change Over Time',
      content: changeOverTime.content,
      media: [
        {
          source: '/assets/datavisualization/change-over-time-1.png',
          label: 'Grouped bar line',
        },
        {
          source: '/assets/datavisualization/change-over-time-2.png',
          label: 'Bar chart',
        },
        {
          source: '/assets/datavisualization/change-over-time-3.png',
          label: 'Lines',
        },
      ],
    },
    {
      name: 'Part To Whole Charts',
      content: partToWholeCharts.content,
      media: [
        {
          source: '/assets/datavisualization/part-to-whole-charts-1.png',
          label: 'Ordered grouped bar 1',
        },
        {
          source: '/assets/datavisualization/part-to-whole-charts-2.png',
          label: 'Ordered grouped bar 2',
        },
        {
          source: '/assets/datavisualization/part-to-whole-charts-3.png',
          label: 'Donut',
        },
      ],
    },
    {
      name: 'Comparison Charts',
      content: comparisonCharts.content,
      media: [
        {
          source: '/assets/datavisualization/comparison-charts-1.png',
          label: 'Grouped bar 1',
        },
        {
          source: '/assets/datavisualization/comparison-charts-2.png',
          label: 'Grouped bar 2',
        },
        {
          source: '/assets/datavisualization/comparison-charts-3.png',
          label: 'Grouped bar stacked',
        },
        {
          source: '/assets/datavisualization/comparison-charts-4.png',
          label: 'Donut',
        },
      ],
    },
    {
      name: 'Real Time Tracking',
      content: realTimeTracking.content,
      media: [
        {
          source: '/assets/datavisualization/real-time-tracking-1.png',
          label: 'Grouped bar line',
        },
        {
          source: '/assets/datavisualization/real-time-tracking-2.png',
          label: 'Lines',
        },
      ],
    },
    {
      name: 'Metric Highlight',
      content: metricHighlight.content,
      media: [
        {
          source: '/assets/datavisualization/metric-highlight-1.png',
          label: 'Numbers 1',
        },
        {
          source: '/assets/datavisualization/metric-highlight-2.png',
          label: 'Numbers 2',
        },
      ],
    },
  ]

  return (
    <DefaultLayout>
      <Container>
        <div className="vf-guideline__container">
          <h1 className="vf-guideline__large-heading">Data Visualisation</h1>
          <p className="vf-component__paragraph">
            In today's fast-paced digital world, it is important to design data
            visualisations with the user in mind, taking into account the
            variety of devices that users may use regularly and the need to
            present data in a way that is easily digestible for users with
            shorter attention spans. To achieve this, we have provided
            guidelines for developing and designing data visualisations for
            Vattenfall’s digital sites that follow our design principles and
            brand identity. This guide complements our overall guide on
            information graphics that you can find in the{' '}
            <a
              href="https://brandtoolbox.vattenfall.com/Styleguide/coreelements/#page/6763376F-07A8-48DA-92245B059B5EFFBE"
              target="_blank"
              rel="noreferrer"
            >
              Brand Toolbox
            </a>
            .
          </p>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Our design principles</h2>
          <DesignPrinciples />
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">
            Designing Data Visualisations
          </h2>
          <p className="vf-component__paragraph">
            To simplify the process of designing data visualisations we have
            identified the most commonly used charts that effectively visualize
            data in a clear and engaging way that aligns with best practices for
            digital channels and UX design.
          </p>
          <div>
            The chart type you select depends primarily on three factors:
          </div>
          <ol>
            <li>The data you want to communicate.</li>
            <li>The message you want to convey about that data.</li>
            <li>The channel/site on where it's being displayed.</li>
          </ol>
          <p className="vf-component__paragraph">
            Our chart recommendations aim to provide descriptions of various
            chart types and our guidelines on when and how to use them in the
            best way or context.
          </p>
        </div>
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Chart Selection Guide</h2>
          <img
            className="vf-component__image"
            src="/assets/datavisualization/chart-selection-guide.png"
            alt="Chart Selection Guide"
          />
          <p className="vf-component__paragraph">
            With our chart selection guide, you can explore our recommended
            chart types and see which ones work best for your project. By using
            the dropdown menu, you will have access to guidelines and visual
            examples for each chart type.
          </p>

          <p className="vf-component__paragraph">
            Types of charts:
            <ul>
              <li>Change Over Time</li>
              <li>Part to whole Charts</li>
              <li>Comparison Charts</li>
              <li>Real Time Tracking</li>
              <li>Metric Highlight</li>
            </ul>
          </p>
          {/* Accordion */}
          <Accordion title={'Types of charts: '} data={data} />
        </div>

        <div className="vf-guideline__container datavis">
          {/* Dos & Dont's */}
          <div className="vf-bgs__visuals vf-bgs__visuals--full">
            <div className="vf-bgs__visuals-do vf-bgs__visuals-block">
              <h3 className="vf-bgs__visuals-do-heading">Dos</h3>
              <ul>
                <li>
                  Organize the data visualisations with a clear hierarchy. Make
                  the most important information easy to spot.
                </li>
                <li>
                  Use annotations, titles and labels to provide context and help
                  users understand what they're looking at and why.
                </li>
              </ul>
            </div>
            <div className="vf-bgs__visuals-donts vf-bgs__visuals-block">
              <h3 className="vf-bgs__visuals-donts-heading">Dont's</h3>
              <ul>
                <li>
                  Avoid the use of colors that don't add value to the
                  visualization. A clean design with minimal distractions to
                  help user understand the data in an easy way.
                </li>
                <li>
                  Avoid using complex or unfamiliar chart types that users might
                  find difficult to interpret. Stick to well-known chart types
                  that are easy to understand and recognise.
                </li>
              </ul>
            </div>
          </div>
          <div className="vf-bgs__visuals-example-list">
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/Do-consumption-bar_stacked.png"
                alt="Do: Use primary Vattenfall colours"
              />
              <div className="vf-bgs__visuals-example-list__item-info dos">
                <h4>Do</h4>
                <p>
                  Use primary Vattenfall colours with a high contrast to the
                  background when using only one colour.
                </p>
                <p>
                  Shorten copy where neccessary to ensure less crowded text
                  areas.
                </p>
                <p>Use black text for the highest level of contrast.</p>
              </div>
            </div>
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/Dont-consumption-bar_stacked.png"
                alt="Don't: Avoid Mixing primary and secondary colours"
              />
              <div className="vf-bgs__visuals-example-list__item-info donts">
                <h4>Don’t</h4>
                <p>Avoid mixing primary and secondary colours.</p>
                <p>
                  Use two colour tones that are too similar making them hard to
                  tell apart and not meeting accessibility contrast levels (AA
                  standard).
                </p>
              </div>
            </div>
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/Do-pie-charts.png"
                alt="Do: USe colors with hight contrast."
              />
              <div className="vf-bgs__visuals-example-list__item-info dos">
                <h4>Do</h4>
                <p>
                  Use colours with high contrast to each other and the white
                  background.
                </p>
                <p>
                  Keep copy displayed at the centra of the donut short so it
                  stands out more.
                </p>
              </div>
            </div>
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/Dont-pie-charts.png"
                alt="Don't: Don't use colours that are too similar"
              />
              <div className="vf-bgs__visuals-example-list__item-info donts">
                <h4>Don't</h4>
                <p>
                  Don’t use colours that are too similar or with too little
                  contrast adjacent to each other.{' '}
                </p>
                <p>Avoid using visual separators between chart colours.</p>
              </div>
            </div>
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/Do-bar-graph-lines.png"
                alt="Do: Use square ends."
              />
              <div className="vf-bgs__visuals-example-list__item-info dos">
                <h4>Do</h4>
                <p>
                  Use square ends on staple bars to make the data readings
                  easier to understand .
                </p>
              </div>
            </div>
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/Dont-bar-graph-lines.png"
                alt="Don't: Avoid arches"
              />
              <div className="vf-bgs__visuals-example-list__item-info donts">
                <h4>Don't</h4>
                <p>Avoid using arched staple bars.</p>
              </div>
            </div>
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/do-line-graph.png"
                alt="Do: Use well defined line"
              />
              <div className="vf-bgs__visuals-example-list__item-info dos">
                <h4>Do</h4>
                <p>
                  Use well defined lines with clear pointed ends, this gives a
                  more accurate representation of the visual data.
                </p>
                <p>
                  Select colours in a multi-colour line chart that can be
                  distingueshed from each other (meeting AA standards) and on
                  white.
                </p>
              </div>
            </div>
            <div className="vf-bgs__visuals-example-list__item">
              <img
                className=""
                src="/assets/datavisualization/dont-line-graph.png"
                alt="Don't: Avoid using arched lines."
              />
              <div className="vf-bgs__visuals-example-list__item-info donts">
                <h4>Don't</h4>
                <p>
                  Avoid using arched lines. Don’t mix different line weights.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="vf-guideline__container">
          {/* Motion Aspect */}
          <h2 className="vf-component__heading">
            Motion Design For Data Visualisations
          </h2>

          <p className="vf-component__paragraph">
            Using motion in the data visualisation charts can help accentuate
            important aspects of the data you are trying to convey. To stay
            consistent with the overall brand experience for all moving elements
            at Vattenfall follow{' '}
            <a
              href="https://brandtoolbox.vattenfall.com/Styleguide/brandapplications/#page/BB91BD54-C4BB-4897-B6E17D872E91B2F4"
              target="_blank"
              rel="noreferrer"
            >
              The Vattenfall Motion ID
            </a>{' '}
            that has translated the brand identity into motion design. To
            maintain our blue position we have translated our competent and high
            quality “blue brand” into a key thought of “Ease-In-Out”. It is a
            smooth, controlled movement that alters in speed / easing across the
            length of the motion: At first a slightly slower speed, then in a
            controlled fashion a little faster to finally ease out into its
            final animated state. See the examples below for a visual reference.
          </p>
          <h3>Examples</h3>
          <video
            style={{ width: '900px', marginBottom: '15px' }}
            src="/assets/datavisualization/motion-aspect.mp4"
            className="vf-component__video"
            controls
          />
          <video
            style={{ width: '900px', marginBottom: '24px' }}
            src="/assets/datavisualization/motion-aspect-donut.mp4"
            className="vf-component__video"
            controls
          />

          {/* <p className="vf-component__paragraph vf-component--center">
            <button className="vf-button vf-button__outline--dark vf-button--lg ">
              See more
            </button>
          </p> */}
        </div>

        {/* Color Accessibility for Charts */}
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">
            Colour Accessibility for Charts
          </h2>
          <p className="vf-component__paragraph">
            When creating data visualisations, choosing the right colors is
            crucial to meet standards and making sure that the visualization can
            be easily read by a wide range of people on different devices. If
            the colors chosen make it difficult to understand the information
            being presented, then the visualization fails to deliver its
            intended message. To ensure that data visualisations are effective,
            it is important to use high contrast and accessible color palettes
            when designing them. When creating 1-coloured graphs, please
            primarily use 10%, 30%, 50%, 70% or 100% colour value to
            differentiate sections of the graph. To see examples, please see{' '}
            <a
              href="https://digitaldesign.vattenfall.com/designers/colours"
              rel="noreferrer"
            >
              Colour tones
            </a>
            .
          </p>

          <div className="vf-component__blocks vf-component__blocks--nospace">
            {/* <div className="vf-component__blocks-gray"></div>
            <div className="vf-component__blocks-gray"></div> */}
            <div>
              <img
                className="vf-component__image vf-component__image--no-margin"
                src="/assets/datavisualization/color-access-1.png"
                alt="Chart Selection Guide"
              />
            </div>
            <div>
              <img
                className="vf-component__image vf-component__image--no-margin"
                src="/assets/datavisualization/color-access-2.png"
                alt="Chart Selection Guide"
              />
            </div>
          </div>
          {/* Dos & Dont's */}
          <div className="vf-bgs__visuals vf-bgs__visuals--nospace">
            <div className="vf-bgs__visuals-do">
              <h3 className="vf-bgs__visuals-do-heading">Do</h3>
              <p>
                Carefully select colors to be displayed next to each other with
                a higher color contrast. (At least AA Compliant) Note example is
                using VF Secondary colors
              </p>
            </div>
            <div className="vf-bgs__visuals-donts">
              <h3 className="vf-bgs__visuals-donts-heading">Don't</h3>
              <p>
                Avoid putting too similar colors next to each other in graphs
                with contrasting borders. Note example is using VF Secondary
                colors
              </p>
            </div>
          </div>
        </div>

        {/* High complexity Data Visualisations */}
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">
            High complexity Data Visualisations
          </h2>
          <p className="vf-component__paragraph">
            There are some scenarions where the information or data wished to be
            displayed visually is extremely complex where a decision should be
            made if we're able to present it in a manner that the user can take
            in the information being displayed, especially when it comes to
            smaller screens.{' '}
          </p>
          <p className="vf-component__paragraph">
            In these instances it may be better to provide the data in a
            downloadable PDF so the user can decide if they want to dig deeper
            into the subject when they have more time to spend rather than
            overwhelming the user with data and as a result giving a bad brand
            impression.
          </p>
          <h3>Examples</h3>
          <img
            className="vf-component__image"
            src="/assets/datavisualization/highcomplexitychartchapter.png"
            alt="High Complexity Chart"
          />
          <p className="vf-component__paragraph">
            Great examples shared by InCharge and VF Nordics that show examples
            of very complex data visualisations.
          </p>
        </div>

        {/* Technical aspects */}
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Technical aspects</h2>
          <p className="vf-component__paragraph">
            There are a wide range of available JavaScript libraries to use when
            visualising data in digital channels (both Open Source and
            licensed), each with their own pros and cons. When selecting a tool
            to implement it is important to ensure it can cater for the
            customisation required to give the charts the needed look and feel
            of the Vattenfall brand assets as well as meeting recommendations
            from a service design perspective, i.e. giving the user a high
            quality user experience.{' '}
          </p>
          <p className="vf-component__paragraph">
            For this purpose a review was completed of a selection of common and
            suitable tools: Highcharts, Chart.js, d3.js, Google Charts and IBM -
            echarts - to help select a few that meet these requirements against
            a number of different criteria.
          </p>
          <p className="vf-component__paragraph">
            All tools reviewed received quite a high overall score but there are
            some differences in pricing and customisation or certain UI-elements
            to consider when selecting the final tool.
          </p>
          <p className="vf-component__paragraph">
            Chart.js, Google Chart and eCharts came out top because of their
            ease of usage when it comes to customisation and pricing, however
            the other tools could also be considered if they better meet your
            local requirements.
          </p>
          <p className="vf-component__paragraph">
            To see an overview of the review criteria and scoring as well as the
            pros and cons for each tool, visit the article page{' '}
            <a
              href="https://digitaldesign.vattenfall.com/articles/javascript-chart-library-audit"
              rel="noreferrer"
            >
              here
            </a>
            .
          </p>
        </div>

        {/* Access chart library in Figma */}
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">
            Access chart library in Figma
          </h2>
          <p className="vf-component__paragraph">
            To streamline the design work and ensure that the data
            visualisations align with our guidelines and brand identity, we have
            created a new chart library that is accessible in our Figma UI Kit.
            The chart library includes instructions and guidelines for how to
            make use of each asset.
          </p>

          <p className="vf-component__paragraph">
            This is how you access the library:
          </p>
          <p className="vf-component__paragraph">
            <img
              className="vf-component__image"
              src="/assets/datavisualization/chart-library.gif"
              alt="Access Chart Library in Figma"
            />
          </p>
        </div>

        {/* Design Checklist */}
        <div className="vf-guideline__container">
          <h2 className="vf-component__heading">Design Checklist</h2>
          <p className="vf-component__paragraph">
            To simplify the process of effectively visualizing data, follow
            these steps before implementing and publishing your charts and
            graphs.
          </p>
          <DataChecklist
            items={[
              'Are you using the appropriate type of graph or chart to effectively convey your message to the user?',
              'Is the graph or chart compatible with various devices and screen sizes?',
              'Have you checked accessibility standards for contrast, text, and technical requirements for your data visualisation?',
              'Does the graph or chart blend well with other design elements on the page to maintain brand consistency?',
              'Does the graph or chart follow Vattenfall Design Principles?',
            ]}
          />
          <p className="vf-component__paragraph">
            By following this checklist, we hope you will have the best
            conditions to design and develop engaging data charts for your
            project.
          </p>
        </div>

        {/* Info Box */}
        <div className="vf-guideline__container">
          <InfoBox
            text={`Data visualisations and how to use the in the best and most brand aligned way can be very complex and will continue to evolve. Please feel free to contact us if you need any assistance in finding the right solution for your project. Contact us <a href= "/contact">here</a> .`}
            icon={'vf-icon-support'}
          />
        </div>
      </Container>
    </DefaultLayout>
  )
}

DataVisualization.showSubMenu = true
DataVisualization.showSidebar = true

export default DataVisualization
