import { ComponentProps, useContext, useEffect } from 'react'

import { DefaultLayout } from '../../components/DefaultLayout'
import PageProvider from '../../context/state'
import { modeEnum } from '../../utils/siteStructure'
import Link from 'next/link'

const GetStarted = () => {
  const { setActiveComponentId, setActiveCategoryId, setCurrentMode } =
    useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('')
    setActiveComponentId('')
    setCurrentMode(modeEnum.DESIGNERS)
  }, [setActiveComponentId, setActiveCategoryId, setCurrentMode])

  return (
    <DefaultLayout>
      <div className="vf-guideline__container">
        <h1 className="vf-guideline__large-heading">Designers</h1>
        <h2 className="vf-component__heading">Vattenfall Figma UI Kit </h2>
        <p className="vf-component__paragraph">
          The Vattenfall Figma UI Kit is a living library containing
          Vattenfall’s digital visual assets for everyday purposes: modules,
          components, icons, colour palettes, grids, typography, etc. The design
          kit exists to ensure design and development consistency across the
          entire Vattenfall ecosystem, improve quality and efficiency in
          production, bring digital products and services to the next level of
          coherency. It can be accessed via Vattenfall's organisation in Figma
          and be connected to your working design documents by adding the UI-kit
          Library to your own files.
          <br></br>
          You will find the latest file by following the link below.
        </p>

        <a
          className="vf-link--with-arrow"
          href="https://www.figma.com/file/u8J3zPJnwLNvo7gXbOJ3Cg/Vattenfall-Design-Kit?type=design&t=QhBDq1uyNemLIwvZ-6"
          target="_blank"
          rel="noreferrer"
          id="figma"
          data-ga-action="Download"
          data-ga-label="Figma"
        >
          Vattenfall Design Kit in Figma
        </a>
        <p className="vf-component__paragraph disclaimer">
          Sketch and Abstract is no longer maintained, should you need a version
          of the old Sketch Design kit please get in touch and we can share it
          with you directly.
        </p>
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">
          Get started as a designer and with Figma
        </h2>
        <p className="vf-component__paragraph">
          In order to get started with the Vattenfall Digital Design kit, you
          need to use Figma. Vattenfall has a shared Figma licence that all
          Vattenfall employees or partners working with UX, digital design,
          digital products editorial work and development at Vattenfall should
          have access to. If you are an external partner and wish to access
          without using a Vattenfall email account, please request access via
          the link above.
        </p>
        <p className="vf-component__paragraph">
          Here are links to download the Figma software as well as a link to
          Figma's own introduction material should you need it.
        </p>
        <p className="vf-component__paragraph">
          <a
            className="vf-link--with-arrow"
            href="https://help.figma.com/hc/en-us/categories/360002051613-Get-started"
            target="_blank"
            rel="noreferrer"
            id="get-figma"
          >
            Get Figma software
          </a>
          <br />
          <a
            className="vf-link--with-arrow"
            href="https://www.figma.com/resources/learn-design/"
            target="_blank"
            rel="noreferrer"
          >
            Figma Introduction
          </a>
        </p>
        <p className="vf-component__paragraph">
          To get started in the tool itself follow the steps in the video below.
        </p>
        <VimeoEmbed />
      </div>
      <div className="vf-guideline__container">
        <h2 className="vf-component__heading">UI Kit Structure</h2>
        <p className="vf-component__paragraph">
          The UI kit structure is a nested library of symbols, grouped by type
          of module or component. The library contains more that 500
          ready-to-use design snippets: from simple buttons to more complex
          modules.
        </p>
        <p className="vf-component__paragraph">
          Make sure to study the component library and use components with
          correct naming. We use a simplified version of the Atomic Design
          methodology, adapted to suit our design process and our team culture.
          The main difference with Atomic Design is that all modules are either
          an atom or a molecule, categorised by purpose. Our approach to
          defining modules and naming them is function-directed, rather than
          presentational. It is based on evolving a shared design language
          collaboratively and empowering all designers and engineers on the team
          to contribute to the system.
        </p>
      </div>
    </DefaultLayout>
  )
}

GetStarted.showSubMenu = true
GetStarted.showSidebar = true

export default GetStarted

export const VimeoEmbed = ({
  src = 'https://player.vimeo.com/video/889063025?h=e6ef47995a&title=0&byline=0&portrait=0&speed=0&badge=0&autopause=0&dnt=1&player_id=0&app_id=58479&autoplay=1&loop=1/embed',
  ...props
}: { src?: string } & ComponentProps<'div'>) => (
  <div style={{ padding: '56.25% 0 0 0', position: 'relative' }} {...props}>
    <iframe
      //sandbox="allow-scripts"
      src={src}
      title="Onboarding video with Figma"
      aria-label="Onboarding video with Figma"
      role="presentation"
      allow="autoplay; fullscreen; picture-in-picture"
      allowFullScreen
      frameBorder="0"
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
      }}
    ></iframe>
  </div>
)
