import Head from 'next/head'
import Script from 'next/script'
import { ComponentType, useEffect, useState } from 'react'
import { ThemeProvider } from 'styled-components'
// This is our import of sandbox ids contained in the NPM package
import * as sandboxData from '@vf-dds/vf-dds-vanilla/sandbox/component-data.json'
import { useRouter } from 'next/dist/client/router'

import * as ga from '../components/ga'
import PageProvider, { ModeEnum } from '../context/state'
import '../style/tailwind.css'
import '../style/styles.scss'
import { theme } from '../style/theme'
import { SandboxDataType } from '../utils/api'
import '../utils/gaEventClick'
import { getSiteStructure } from '../utils/siteStructure'

const siteStructure = getSiteStructure()

export async function getStaticProps() {
  return { props: {} }
}

type AppComponentType<TProps> = ComponentType<TProps> & {
  showSubMenu?: boolean
  showSidebar?: boolean
}

export default function MyApp<TProps>({
  Component,
  pageProps,
}: {
  Component: AppComponentType<TProps> & { noHead?: boolean }
  pageProps: TProps & JSX.IntrinsicAttributes
}) {
  const [currentMode, setCurrentMode] = useState<ModeEnum[keyof ModeEnum]>('')
  const [activeFramework, setActiveFramework] = useState('javascript')
  const [activeMenuItem, setActiveMenuItem] = useState('')
  const [activeSearchBox, setActiveSearchBox] = useState(false)
  const [activeComponentId, setActiveComponentId] = useState('')
  const [activeCategoryId, setActiveCategoryId] = useState('')
  const [closedDrawer, setClosedDrawer] = useState(false)

  // Map sandbox ids to component ids
  const sandboxIdMap = Array.from(sandboxData).reduce((acc, cur) => {
    if (cur.variants) {
      acc[cur.componentName] = cur.variants
    } else {
      acc[cur.componentName] = [cur]
    }
    return acc
  }, {} as Record<string, SandboxDataType[]>)

  const router = useRouter()

  useEffect(() => {
    const handleRouteChange = (url: string) => {
      ga.pageview(url)
    }
    //When the component is mounted, subscribe to router changes
    //and log those page views
    router.events.on('routeChangeComplete', handleRouteChange)

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  useEffect(() => {
    // Function to add lang attribute
    const addLangAttribute = () => {
      const htmlTag = document.querySelector('html')

      if (htmlTag) {
        // Add lang attribute with value 'en' (for English)
        htmlTag.setAttribute('lang', 'en')
      }
    }
    addLangAttribute()

    return () => {}
  }, [])

  return (
    <PageProvider.Provider
      value={{
        currentMode,
        setCurrentMode,
        activeFramework,
        setActiveFramework,
        activeSearchBox,
        setActiveSearchBox,
        activeMenuItem,
        setActiveMenuItem,
        sandboxIdMap,
        activeComponentId,
        setActiveComponentId,
        activeCategoryId,
        setActiveCategoryId,
        siteStructure,
        closedDrawer,
        setClosedDrawer,
        subMenu: Component.showSubMenu,
        showSidebar: Component.showSidebar,
      }}
    >
      {Component.noHead !== true && (
        <>
          <Head>
            {/* <html lang="en" /> */}
            <title>Digital Design System | Vattenfall</title>
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GA_TRACKING_CODE}`}
            />
            <script
              dangerouslySetInnerHTML={{
                __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${process.env.NEXT_PUBLIC_GA_TRACKING_CODE}', {
              page_path: window.location.pathname,
            });
          `,
              }}
            />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1.0"
            />
            <link rel="shortcut icon" href="/favicon.ico" />
          </Head>

          <Script
            type="text/javascript"
            data-cmp-ab="1"
            src="https://cdn.consentmanager.net/delivery/autoblocking/56dcf9ec18dd.js"
            data-cmp-host="b.delivery.consentmanager.net"
            data-cmp-cdn="cdn.consentmanager.net"
            data-cmp-codesrc="1"
          />

          {/* eslint-disable-next-line @next/next/inline-script-id  */}
          <Script
            dangerouslySetInnerHTML={{
              __html: `
      (function(h,o,t,j,a,r){
          h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
          h._hjSettings={hjid:3143949,hjsv:6};
          a=o.getElementsByTagName('head')[0];
          r=o.createElement('script');r.async=1;
          r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
          a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');`,
            }}
          />
        </>
      )}

      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </PageProvider.Provider>
  )
}
