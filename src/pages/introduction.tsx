import Link from 'next/link'
import { useContext, useEffect } from 'react'
import styled from 'styled-components'

import ContentWrapper from '../components/ContentWrapper'
import { DefaultLayout } from '../components/DefaultLayout'
import PageProvider from '../context/state'
import { modeEnum } from '../utils/siteStructure'

const IntroductionContainer = styled.div`
  margin: 0;
  max-width: 1200px;

  .mode__desc {
    margin-bottom: 28px;
  }

  .large-margin {
    margin-bottom: 44px;
  }
`

const Introduction = () => {
  const { setActiveCategoryId, setCurrentMode } = useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('introduction')
    setCurrentMode(modeEnum.NULL)
  }, [setActiveCategoryId, setCurrentMode])

  return (
    <DefaultLayout>
      <ContentWrapper>
        <IntroductionContainer>
          <div className="vf-component__full-container">
            <h1 className="vf-guideline__large-heading">Introduction</h1>
            <p className="vf-component__paragraph large-margin">
              No matter if you are a designer or a developer, we have created a
              tool / modular system with the Digital Design System for you to
              map and manage all processes in the creation of an online project
              according to our brand and technical standards. By synchronizing
              the modules and components in Figma and Bitbucket we enable
              designers and developers to use the same basis to create a
              stringent and uniform result according to the Vattenfall Brand
              Expression.
            </p>

            <div className="large-margin">
              <h3 className="vf-component__tiny-heading">Designers</h3>
              <p className="vf-component__paragraph">
                Designers receive all current design specifications and styles
                in Figma (Sketch and Abstract is no longer maintained). The
                created layouts serve later as blueprints for programming. No
                HTML exports from Figma are necessary.
              </p>
              <Link href="/designers/get-started">
                <a className="vf-link--with-arrow">Designers</a>
              </Link>
            </div>
            <div>
              <h3 className="vf-component__tiny-heading">Developers</h3>
              <p className="vf-component__paragraph">
                Programmers receive the basic kit for programming websites,
                service websites or apps as code. All modules and components are
                identical to the design. Please always work 100% with our
                modules and never copy anything from other sites - because they
                might not be up to date.
              </p>
              <Link href="/developers/get-started">
                <a className="vf-link--with-arrow">Developers</a>
              </Link>
            </div>
          </div>
        </IntroductionContainer>
      </ContentWrapper>
    </DefaultLayout>
  )
}

Introduction.showSubMenu = true

export default Introduction
