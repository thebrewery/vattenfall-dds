import algoliasearch from 'algoliasearch'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

import Link from 'next/link'
import ContentWrapper from '../components/ContentWrapper'
import { DefaultLayout } from '../components/DefaultLayout'

const APPLICATION_KEY = process.env.NEXT_PUBLIC_ALGOLIA_APP_ID
const PUBLIC_SEARCH_KEY = process.env.NEXT_PUBLIC_ALGOLIA_SEARCH_KEY
const INDEX_NAME = process.env.NEXT_PUBLIC_ALGOLIA_INDEX_NAME

const searchClient = algoliasearch(APPLICATION_KEY, PUBLIC_SEARCH_KEY)
const index = searchClient.initIndex(INDEX_NAME)

const SearchContainer = styled.div`
  .search {
    &__hit {
      align-items: center;
      color: ${({ theme }) => theme.colours.primary__ocean_blue};
      cursor: pointer;
      display: flex;
      font-family: ${({ theme }) => theme.defaultFontFamily};
      font-weight: 300;
      font-size: ${({ theme }) => theme.fontSize};
      height: 40px;
      padding-right: 20px;
      width: fit-content;

      &:hover {
        background: #ebeef2;
      }
    }

    &__mode {
      text-transform: capitalize;
    }

    &__divider {
      border: 1px solid #d4d8dd;
      height: 2rem;
      margin: auto 20px;
    }
  }

  .text-link {
    color: #2071b5;

    &:hover {
      color: #000;
    }

    .vf-icon-request::before {
      font-size: 1rem;
      margin-right: 0.25rem;
    }
  }
`

const FullSearchResult = props => {
  const [result, setResult] = useState([])

  const { query } = useRouter()

  if (props.location && props.location.search !== '') {
    query = props.location.search.slice(3)
    query = query.replace('%20', ' ')
  }

  useEffect(() => {
    if (query) {
      index
        .search(query.q, {
          hitsPerPage: 100,
        })
        .then(({ hits }) => {
          setResult(hits)
        })
    }
  }, [query])

  const goToResult = uri => {
    window.location.href = uri
  }
  const handleKeyDown = (event, uri) => {
    console.log('event:')
    console.log(event);
    console.log(uri);
    if (event.key === 'Enter') {
      goToResult(uri);
    }
  };

  const renderResult = () => {
    if (result.length >= 1) {
      return result.map((hit, index) => {
        const subtitle = hit.parentName && hit.parentName
        return (
          <div
            className="search__hit"
            onClick={() => goToResult(hit.uri)}
            key={index}
            onKeyDown={ (e) =>  handleKeyDown(e, hit.uri)}
            tabIndex={0}
          >
            <p className="search__mode">{hit.type}</p>
            <div className="search__divider" />
            {subtitle && (
              <p className="search__subtitle">
                {subtitle} {'>'}&nbsp;
              </p>
            )}
            <p className="search__component">{hit.name}</p>
          </div>
        )
      })
    }

    return (
      <p className="vf-component__paragraph">
        Nothing here. Please try again, or make a request{' '}
        <Link href="/contact">
          <a className="text-link">
            <span className="vf-icon-request" />
            here!
          </a>
        </Link>
      </p>
    )
  }

  return (
    <DefaultLayout>
      <ContentWrapper style={{ maxWidth: '900px' }}>
        <h1 className="vf-guideline__large-heading">Search results</h1>

        <SearchContainer>
          <div>{renderResult()}</div>
        </SearchContainer>
      </ContentWrapper>
    </DefaultLayout>
  )
}

export default FullSearchResult
