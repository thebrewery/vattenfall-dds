import { useContext, useEffect, useState } from 'react'
import styled from 'styled-components'

import { DefaultLayout } from '../components/DefaultLayout'
import { PlayButtonWithText } from '../components/js-svg/PlayButtonWithText'
import { CardWrapper } from '../components/onboarding/CardWrapper'
import PageProvider from '../context/state'
import { useViewParallax } from '../hooks/useViewParallax'
import { API, formatAssetUrl, OnboardingPageProps } from '../utils/api'
import { getDuration } from '../utils/common'
import { modeEnum } from '../utils/siteStructure'

const IntroductionContainer = styled.div`
  margin-top: 2rem;

  @media screen and (min-width: 1024px) {
    margin-top: 3rem;
  }

  .introduction {
    &__onboarding-cards {
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      gap: 2rem;
      grid-column: 1 / -1;
      margin-bottom: 1rem;
      overflow-x: scroll;
      scroll-snap-type: x mandatory;
      scroll-snap-stop: always;
      scrollbar-width: none;

      &::-webkit-scrollbar {
        display: none;
      }

      @media screen and (min-width: 1024px) {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        gap: 2.625rem;
        overflow: visible;
      }
    }

    &__large-negative-margin {
      row-gap: 0;
      transform: translateY(-1.25rem);

      @media screen and (min-width: 1024px) {
        transform: translateY(-3rem);
      }
    }

    &__negative-margin {
      row-gap: 0;
      transform: translateY(-1.25rem);

      @media screen and (min-width: 1024px) {
        transform: translateY(-2.25rem);
      }
    }

    &__bg {
      background-color: ${({ theme }) => theme.colours.bg__light_blue};
      inset: 0;
      height: 100%;
      position: absolute;

      @media screen and (min-width: 1250px) {
        height: calc(100% - 6.75rem);
      }
    }

    &-intro {
      position: relative;

      &__video {
        background-color: #fff;
        box-shadow: 0 0 1rem 0 rgba(0, 0, 0, 0.1);
        inset: 0;
        position: absolute;
      }

      &__preview-image {
        cursor: pointer;
        inset: 0;
        height: 100%;
        transform: scale(1);
        position: absolute;
        width: 100%;

        .play-button {
          cursor: pointer;
          height: 2.375rem;
          left: 50%;
          transition: opacity 0.3s ease-in-out;
          transform: translate(-50%, -50%);
          top: 50%;
          opacity: 1;
          position: absolute;
          width: 2.375rem;

          &:hover {
            opacity: 0.8;
          }

          @media screen and (min-width: 1024px) {
            height: 5rem;
            width: 5rem;
          }
        }

        .play-button-text {
          bottom: 1.25rem;
          left: 1.25rem;
          position: absolute;
          z-index: 9999;
        }

        img {
          height: 100%;
          object-fit: cover;
          width: 100%;
        }
      }

      &__video-length {
        bottom: 0.625rem;
        font-size: 0.625rem;
        right: 0.625rem;
        user-select: none;
        position: absolute;

        @media screen and (min-width: 1024px) {
          bottom: 2rem;
          font-size: 1.25rem;
          right: 2rem;
        }
      }

      &__heading {
        ${({ theme }) => theme.h0Typo};
        margin-bottom: 0.5rem;
        grid-column: 1 / -1;

        @media screen and (min-width: 1024px) {
          margin-bottom: 2rem;
        }
      }

      &__content {
        grid-column: 1 / -1;

        .introduction-intro__text {
          grid-column: 1 / -1;
          ${({ theme }) => theme.preamble};
          font-size: 1rem;
          line-height: 1.5;

          @media screen and (min-width: 1024px) {
            grid-column: 1 / 5;
          }
        }

        .introduction-intro__video-container {
          overflow: hidden;
          padding-top: 56.25%;
          position: relative;
          grid-column: 1 / -1;
          transition: transform 0.15s ease-in;
          width: 100%;

          @media screen and (min-width: 1024px) {
            grid-column: 6 / 13;

            &:hover {
              transform: scale(1.025);
            }
          }
        }
      }
    }

    &-parts {
      margin-bottom: 2.5rem;
      margin-top: 2.5rem;
      row-gap: 0;

      &--with-bg {
        background-color: ${({ theme }) => theme.colours.bg__light_blue};

        .introduction__negative-margin {
          overflow-x: scroll;
          overflow-y: hidden;
          scrollbar-width: none;

          &::-webkit-scrollbar {
            display: none;
          }

          @media screen and (min-width: 1024px) {
            overflow: visible;
          }
        }
      }

      &--without-bg {
        overflow-x: scroll;
        margin-right: 0;
        scrollbar-width: none;

        &::-webkit-scrollbar {
          display: none;
        }

        @media screen and (min-width: 1024px) {
          margin: 2.5rem 1.875rem;
          overflow: visible;
        }

        @media screen and (min-width: 1650px) {
          margin: 2.5rem auto;
        }
      }

      &__texts {
        margin-right: 1.25rem;
        grid-column: 1 / -1;
        max-width: 600px;
        margin-bottom: 1rem;

        @media screen and (min-width: 1024px) {
          margin-bottom: 2rem;
          margin-right: 0;
        }
      }

      &__heading {
        ${({ theme }) => theme.h1Typo};
        font-size: 1.75rem;
        margin-bottom: 0.5rem;
      }

      &__paragraph {
        ${({ theme }) => theme.largeBodyCopy};
      }
    }
  }

  .future-vision__link {
    grid-column: 1 / -1;
  }

  .onboarding {
    &__indicators {
      align-items: center;
      display: flex;
      flex-direction: row;
      grid-column: 1 / -1;
      gap: 0.875rem;
      justify-content: center;
      margin: 0 auto;
      margin-top: 1.5rem;
      transition: background-color 0.3s ease-in;

      @media screen and (min-width: 1024px) {
        display: none;
      }
    }

    &__indicator {
      background-color: #cbcbcb;
      border-radius: 100%;
      cursor: pointer;
      height: 0.5rem;
      width: 0.5rem;

      &.active {
        background-color: ${({ theme }) => theme.colours.primary__ocean_blue};
      }
    }
  }
`

export const getStaticProps = async () => {
  const data = await API.getOnboardingData()

  return {
    props: { data },
  }
}

const Onboarding = ({ data }: { data: OnboardingPageProps }) => {
  const [isVideoPlaying, setIsVideoPlaying] = useState<boolean>(false)
  const [videoDuration, setVideoDuration] = useState<string>('')
  const { setActiveCategoryId, setCurrentMode } = useContext(PageProvider)

  useEffect(() => {
    setActiveCategoryId('onboarding')
    setCurrentMode(modeEnum.NULL)
  }, [setActiveCategoryId, setCurrentMode])

  const firstParallaxRef = useViewParallax<HTMLDivElement>(undefined)
  const secondParallaxRef = useViewParallax<HTMLDivElement>(undefined)
  const thirdParallaxRef = useViewParallax<HTMLDivElement>(undefined)

  useEffect(() => {
    if (!data.introductionVideoUrl) return
    getDuration(setVideoDuration, data.introductionVideoUrl)
  }, [data.introductionVideoUrl])

  const handleKeyDown = (event: { key: string }) => {
    if (event.key === 'Enter') {
      setIsVideoPlaying(!isVideoPlaying);
    }
  };
  return (
    <DefaultLayout>
      <IntroductionContainer>
        <div ref={firstParallaxRef} className="introduction-intro">
          <div className="introduction__bg" />
          <div className="introduction__large-negative-margin vf-future-vision__grid">
            <h1 className="introduction-intro__heading">{data.title}</h1>
            <div className="introduction-intro__content vf-future-vision__grid--no-margins">
              <p className="introduction-intro__text">{data.introduction}</p>
              <div className="introduction-intro__video-container">
                {!isVideoPlaying ? (
                  <div
                    className="introduction-intro__preview-image" tabIndex={0}
                    onClick={() => setIsVideoPlaying(!isVideoPlaying)}
                    onKeyDown={handleKeyDown}
                  >
                    <img
                      alt="Preview image for video"
                      src={formatAssetUrl(data.videoPreviewImage.url)}
                    />
                    <PlayButtonWithText />
                    {/* <p className="introduction-intro__video-length">
                      {videoDuration}
                    </p> */}
                  </div>
                ) : (
                  <div className="introduction-intro__video">
                    <iframe
                      src={data.introductionVideoUrl}
                      title={data.title}
                      aria-label={data.title}
                      allow="autoplay; fullscreen; picture-in-picture"
                      allowFullScreen
                      frameBorder="0"
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        height: '100%',
                      }}
                    ></iframe>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div ref={secondParallaxRef}>
          <CardWrapper
            cards={data.designerCards}
            info={data.designerInfo}
            withBg={false}
            link="designers"
          />
        </div>

        <div ref={thirdParallaxRef}>
          <CardWrapper
            cards={data.developerCards}
            info={data.developerInfo}
            withBg={true}
            link="developers"
          />
        </div>

        {/* <DeveloperCards
          cards={data.developerCards}
          info={data.developerInfo}
          ref={thirdParallaxRef}
          withBg={true}
        /> */}
      </IntroductionContainer>
    </DefaultLayout>
  )
}

// Onboarding.showSubMenu = true

export default Onboarding
