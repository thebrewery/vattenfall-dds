import styled from 'styled-components'

import { Header } from '../components/Header'
import AnimatedHero from '../react/components/modules/AnimatedHero'

const Container = styled.div`
  .vf {
    &__video-badge {
      align-items: center;
      background: #ffda00;
      border-radius: 50%;
      color: #000;
      display: flex;
      flex-direction: column;
      font-family: 'Vattenfall Hall Bold NORDx';
      font-size: 15px;
      font-weight: 700;
      height: 115px;
      justify-content: center;
      position: absolute;
      text-align: center;
      top: 10px;
      right: 30px;
      width: 115px;
      z-index: 1;

      @media screen and (min-width: 768px) {
        top: 40px;
        right: 90px;
      }

      &:hover {
        cursor: pointer;
      }
    }
  }

  .intro-arrow {
    color: #222222;
    font-size: ${({ theme }) => theme.fontSize};

    &:before {
      content: '';
      display: inline-block;
      width: 15px;
      height: 16px;
      background-color: transparent;
      background-repeat: no-repeat;
      background-position: 0 0;
      background-size: 15px 16px;
      vertical-align: middle;
      background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMjBweCIgaGVpZ2h0PSIxOHB4IiB2aWV3Qm94PSIwIDAgMjAgMTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8ZGVmcz4KICAgICAgICA8cG9seWdvbiBpZD0icGF0aC0xIiBwb2ludHM9IjggMTkgMjMuNDQ4MTc1MiAxOSAxNy40MzQzMDY2IDI1LjM0MjY0NzcgMTkuMDQwMTQ2IDI3IDI3LjggMTcuOTk1ODc3MiAxOS4wNDAxNDYgOSAxNy40MzQzMDY2IDEwLjY0OTEwNjcgMjMuNDQ4MTc1MiAxNyA4IDE3Ij48L3BvbHlnb24+CiAgICA8L2RlZnM+CiAgICA8ZyBpZD0iU3ltYm9scyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9IkNvbXBvbmVudHMtLy1JY29ucy0vLVN5bWJvbC0vLUFyQ29tcG9uZW50cy0vLVRhYmxlLS8tUm93LS8tUHJpbWFyeS0vLVJpZ2h0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC4wMDAwMDAsIC05LjAwMDAwMCkiPgogICAgICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBmaWxsPSJ3aGl0ZSI+CiAgICAgICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPgogICAgICAgICAgICA8L21hc2s+CiAgICAgICAgICAgIDx1c2UgaWQ9Ik1hc2siIGZpbGw9IiMyMjIyMjIiIGZpbGwtcnVsZT0ibm9uemVybyIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=);
    }
  }

  .vf-component__container {
    margin: 0 auto;
    margin-bottom: 44px;
  }

  .carousel-and-content-wrapper {
    position: relative;
    max-width: 1800px;
    margin: 0 auto;
    margin-bottom: 3rem;
    padding: 0 20px;
    box-sizing: content-box;
    @media screen and (min-width: 1024px) {
      padding: 0 30px;
    }
  }

  .container-entry {
    margin: 65px auto;
  }

  .entry-links {
    display: grid;
    grid-template-columns: repeat(1, 1fr);
    gap: 21px;

    @media screen and (min-width: 768px) {
      gap: 42px;
      grid-template-columns: repeat(3, 1fr);
    }

    .future-vision__link {
      color: #1964a3;
    }
  }

  .articles {
    &__content-blocks {
      background-color: #f6f6f6;
    }
  }
`
const LandingPage = () => {
  return (
    <Container>
      <Header />
      <AnimatedHero sliderItems={HERO_ITEMS} />
    </Container>
  )
}

export default LandingPage

const HERO_ITEMS = [
  {
    id: 1,
    title: 'The Digital Design System has moved',
    subTitle:
      'The Digital Design System has now moved location into our new Brand Toolbox, our brand marketing platform. The move means all Vattenfall’s brand resources including strategy, media bank assets, guidelines and onboarding material - including digital - and more can now be found under one roof. If you are a Vattenfall employee or partner you should already have or be able to request access now. ',
    linkHref: 'https://brandtoolbox.vattenfall.com/',
    linkLabel: 'Visit Brand Toolbox',
  },
  {
    id: 2,
    title: 'The Digital Design System has moved',
    subTitle:
      'The Digital Design System has now moved location into our new Brand Toolbox, our brand marketing platform. The move means all Vattenfall’s brand resources including strategy, media bank assets, guidelines and onboarding material - including digital - and more can now be found under one roof. If you are a Vattenfall employee or partner you should already have or be able to request access now. ',
    linkHref: 'https://brandtoolbox.vattenfall.com/',
    linkLabel: 'Visit Brand Toolbox',
  },
  {
    id: 5,
    title: 'The Digital Design System has moved',
    subTitle:
      'The Digital Design System has now moved location into our new Brand Toolbox, our brand marketing platform. The move means all Vattenfall’s brand resources including strategy, media bank assets, guidelines and onboarding material - including digital - and more can now be found under one roof. If you are a Vattenfall employee or partner you should already have or be able to request access now. ',
    linkHref: 'https://brandtoolbox.vattenfall.com/',
    linkLabel: 'Visit Brand Toolbox',
  },
  {
    id: 3,
    title: 'The Digital Design System has moved',
    subTitle:
      'The Digital Design System has now moved location into our new Brand Toolbox, our brand marketing platform. The move means all Vattenfall’s brand resources including strategy, media bank assets, guidelines and onboarding material - including digital - and more can now be found under one roof. If you are a Vattenfall employee or partner you should already have or be able to request access now. ',
    linkHref: 'https://brandtoolbox.vattenfall.com/',
    linkLabel: 'Visit Brand Toolbox',
  },
]
