import Head from 'next/head'
import BrandGuideSocials from '../../brandtoolbox/social/BrandGuideSocials'

export default function SocialMedaGuideLines() {
  return (
    <>
      <Head>
        <meta name="robots" content="noindex" />
      </Head>
      <BrandGuideSocials />
    </>
  )
}

SocialMedaGuideLines.noHead = true
