import styled from 'styled-components'

import ContentWrapper from '../components/ContentWrapper'
import { DefaultLayout } from '../components/DefaultLayout'
import NewRequest from '../components/NewRequest'
import { API, ContactPageProps, FAQPageProps } from '../utils/api'

const Container = styled.div`
  line-height: 2rem;
  max-width: 800px;
  margin: 0 auto;

  a {
    text-decoration: none;
  }
`

export const getStaticProps = async () => {
  const data = await API.getContactData()
  const { entries } = await API.getFAQPageData()

  return {
    props: { data, entries },
  }
}

const Contact = ({
  data,
  entries,
}: {
  data: ContactPageProps
  entries: FAQPageProps['entries']
}) => {
  return (
    <DefaultLayout>
      <ContentWrapper>
        <Container>
          <h1 className="vf-component__large-heading">{data.heading}</h1>
          <p dangerouslySetInnerHTML={{ __html: data.introduction }} />
          <NewRequest data={data} />
          {/* <h1 id="faq" className="vf-component__large-heading">
            FAQ
          </h1>
          <FAQ entries={entries} /> */}
        </Container>
      </ContentWrapper>
    </DefaultLayout>
  )
}
export default Contact
