import { GetStaticProps, InferGetStaticPropsType } from 'next'
import Link from 'next/link'
import { ComponentProps, useEffect, useRef, useState } from 'react'
import Lottie from 'react-lottie-player'
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import remarkGfm from 'remark-gfm'
import styled from 'styled-components'

import autoAnimate from '@formkit/auto-animate'
import { contentBlockMap } from '../home'
import { DefaultLayout } from '../../components/DefaultLayout'
import {
  API,
  type Article,
  ArticleListingProps,
  formatAssetUrl,
} from '../../utils/api'
import lottieJson from './lf20_pk8zaqti.json'

export const mimeTypeMapping: Record<string, string> = {
  'application/pdf': 'PDF',
  'image/svg+xml': 'SVG',
  'image/png': 'PNG',
  'image/jpeg': 'JPG',
  'image/gif': 'GIF',
  'audio/mpeg': 'MP3',
  'video/mp4': 'MP4',
  'video/mpeg': 'MPEG',
}

const ArticlePageStyle = styled.div`
  strong {
    font-size: 0.75rem;
    font-weight: 600;
    letter-spacing: 2px;
    text-transform: uppercase;
  }

  color: ${({ theme }) => theme.colours.text__dark_color};

  @media screen and (min-width: 992px) {
    max-width: 774px;
  }

  @media screen and (min-width: 1200px) {
    max-width: 960px;
  }

  .article {
    &__intro {
      padding-top: 3rem;
    }

    &__heading {
      font-size: 3rem;
      font-family: 'Vattenfall Hall NORDx';
      font-weight: 600;
      line-height: 1.2;
      margin-bottom: 2rem;
      text-align: center;
    }

    &__description {
      line-height: ${({ theme }) => theme.mediumLineHeight};
      letter-spacing: -0.05px;
      margin-bottom: 2rem;
    }

    &__image-wrapper {
      margin-bottom: 2rem;
      width: 100%;

      img {
        height: 100%;
        object-fit: cover;
        width: 100%;
      }
    }

    &__content {
      margin-bottom: 2rem;

      h1,
      h2 {
        padding-top: 3rem;
      }

      h2 {
        font-family: 'Vattenfall Hall Display NORDx', Helvetica, sans-serif;
        font-weight: 600;
        letter-spacing: 0;
        font-size: 2.25rem;
        line-height: 2.625rem;
        margin-bottom: 1rem;
      }

      h3 {
        font-family: 'Vattenfall Hall Display NORDx', Helvetica, sans-serif;
        font-weight: 600;
        letter-spacing: 0;
        font-size: ${({ theme }) => theme.headingFontSize};
        line-height: ${({ theme }) => theme.largeLineHeight};
        margin-bottom: 1rem;
      }

      p {
        font-size: ${({ theme }) => theme.fontSize};
        letter-spacing: -0.05px;
        line-height: ${({ theme }) => theme.mediumLineHeight};
        font-family: 'Vattenfall Hall NORDx', 'Vattenfall Hall NORDx',
          sans-serif;
        -webkit-font-smoothing: antialiased;
        margin-bottom: 1.25rem;
      }

      ul {
        margin: 0;
      }

      a {
        color: ${({ theme }) => theme.colours.link__color};
        display: inline-block;
        text-decoration: none;
        background-color: transparent;
        font-family: 'Vattenfall Hall NORDx';
        font-weight: 500;
        font-style: normal;
      }

      img,
      video,
      iframe {
        max-width: 100%;
      }
    }
  }

  .back-button {
    display: inline-block;
    margin-bottom: 4rem;
  }

  .vf-ul {
    list-style: none;
    padding: 0;
    margin: 0;
  }

  .attachments {
    list-style: none;
    padding: 0;
    margin: 0;

    &__size-and-type {
      color: #888888;
      font-size: 0.875rem;
      margin-left: 1.25rem;
    }

    &__link {
      margin: 1rem 0;

      & a:hover {
        text-decoration: underline;
      }

      & a::before {
        content: '';
        display: inline-block;
        width: 19px;
        height: 24px;
        background-color: transparent;
        background-repeat: no-repeat;
        background-position: 0 0;
        background-size: 19px 24px;
        margin-right: 10px;
        vertical-align: middle;
        background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMjBweCIgaGVpZ2h0PSIxOHB4IiB2aWV3Qm94PSIwIDAgMjAgMTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8ZGVmcz4KICAgICAgICA8cG9seWdvbiBpZD0icGF0aC0xIiBwb2ludHM9IjggMTkgMjMuNDQ4MTc1MiAxOSAxNy40MzQzMDY2IDI1LjM0MjY0NzcgMTkuMDQwMTQ2IDI3IDI3LjggMTcuOTk1ODc3MiAxOS4wNDAxNDYgOSAxNy40MzQzMDY2IDEwLjY0OTEwNjcgMjMuNDQ4MTc1MiAxNyA4IDE3Ij48L3BvbHlnb24+CiAgICA8L2RlZnM+CiAgICA8ZyBpZD0iU3ltYm9scyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9IkNvbXBvbmVudHMtLy1JY29ucy0vLVN5bWJvbC0vLUFyQ29tcG9uZW50cy0vLVRhYmxlLS8tUm93LS8tUHJpbWFyeS0vLVJpZ2h0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC4wMDAwMDAsIC05LjAwMDAwMCkiPgogICAgICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBmaWxsPSJ3aGl0ZSI+CiAgICAgICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPgogICAgICAgICAgICA8L21hc2s+CiAgICAgICAgICAgIDx1c2UgaWQ9Ik1hc2siIGZpbGw9IiMyMjIyMjIiIGZpbGwtcnVsZT0ibm9uemVybyIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=);
      }

      & a[href$='.pdf']::before {
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzNiAzNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzYgMzY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojMjIyMjIyO30KCS5zdDF7Zm9udC1mYW1pbHk6J0FyaWFsLUJvbGRNVCc7fQoJLnN0Mntmb250LXNpemU6MTAuMDI2cHg7fQo8L3N0eWxlPgo8dGV4dCB0cmFuc2Zvcm09Im1hdHJpeCgxIDAgMCAxIDcuOTI2MSAxNS4xOTc4KSIgY2xhc3M9InN0MCBzdDEgc3QyIj5QREY8L3RleHQ+CjxnIGlkPSJNYXNrIj4KCTxwYXRoIGlkPSJwYXRoLTFfMV8iIGQ9Ik0xMi44LDI1LjNsMy41LDMuNXYtOWgzLjR2OWwzLjUtMy41bDIuNCwyLjRMMTgsMzUuMmwtNy42LTcuNkwxMi44LDI1LjN6IE0zNS4xLDIwLjZsLTAuMSwyLjVoLTEydi0zLjQKCQloOC42bDAtMC45aDBWNC4zSDQuMnYxNC42aDBsMCwwLjloOC42djMuNGgtMTJ2LTEuOHYtMi40di0zLjNWMC45aDM0LjR2MTguMVYyMC42eiIvPgo8L2c+Cjwvc3ZnPgo=');
      }

      & a[href$='.xls']::before,
      & a[href$='.xlsx']::before {
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzNiAzNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzYgMzY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojMjIyMjIyO30KCS5zdDF7Zm9udC1mYW1pbHk6J0FyaWFsLUJvbGRNVCc7fQoJLnN0Mntmb250LXNpemU6MTAuMDEzOXB4O30KPC9zdHlsZT4KPHRleHQgdHJhbnNmb3JtPSJtYXRyaXgoMSAwIDAgMSA4LjQzNjIgMTUuMTM5NikiIGNsYXNzPSJzdDAgc3QxIHN0MiI+WExTPC90ZXh0Pgo8ZyBpZD0iTWFzayI+Cgk8cGF0aCBpZD0icGF0aC0xXzFfIiBkPSJNMTMsMjUuMmwzLjUsMy41di05aDMuNHY5bDMuNS0zLjVsMi40LDIuNGwtNy42LDcuNWwtNy42LTcuNUwxMywyNS4yeiBNMzUuMywyMC42bC0wLjEsMi41aC0xMnYtMy40aDguNgoJCWwwLTAuOWgwVjQuM0g0LjR2MTQuNmgwbDAsMC45SDEzdjMuNEgxdi0xLjh2LTIuNHYtMy4zVjAuOGgzNC4zdjE4VjIwLjZ6Ii8+CjwvZz4KPC9zdmc+Cg==');
      }

      & a[href$='.txt']::before {
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzNiAzNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzYgMzY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojMjIyMjIyO30KCS5zdDF7Zm9udC1mYW1pbHk6J0FyaWFsLUJvbGRNVCc7fQoJLnN0Mntmb250LXNpemU6OS45ODk2cHg7fQo8L3N0eWxlPgo8dGl0bGU+Q29tcG9uZW50cyAvIEljb25zIC8gU3ltYm9sIC8gRG9jdW1lbnQgLyBUWFQ8L3RpdGxlPgo8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KPHRleHQgdHJhbnNmb3JtPSJtYXRyaXgoMSAwIDAgMSA4LjQ4NjkgMTUuMTQ1NSkiIGNsYXNzPSJzdDAgc3QxIHN0MiI+VFhUPC90ZXh0Pgo8cGF0aCBpZD0icGF0aC0xXzFfIiBkPSJNMTIuOCwyNS4ybDMuNSwzLjV2LTloMy40djlsMy41LTMuNWwyLjQsMi40bC03LjUsNy41bC03LjUtNy41TDEyLjgsMjUuMnogTTM1LDIwLjZMMzUsMjMuMUgyM3YtMy40aDguNgoJbDAtMC45aDBWNC4zSDQuMnYxNC42aDBsMCwwLjloOC42djMuNGgtMTJ2LTEuOHYtMi40di0zLjNWMC45SDM1djE4VjIwLjZ6Ii8+Cjwvc3ZnPgo=');
      }

      & a[href$='.ppt']::before,
      & a[href$='.pptx']::before {
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzNiAzNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzYgMzY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojMjIyMjIyO30KCS5zdDF7Zm9udC1mYW1pbHk6J0FyaWFsLUJvbGRNVCc7fQoJLnN0Mntmb250LXNpemU6MTAuMDk5cHg7fQo8L3N0eWxlPgo8dGl0bGU+Q29tcG9uZW50cyAvIEljb25zIC8gU3ltYm9sIC8gRG9jdW1lbnQgLyBQUFQ8L3RpdGxlPgo8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KPHRleHQgdHJhbnNmb3JtPSJtYXRyaXgoMSAwIDAgMSA4LjE0ODQgMTUuMDUyNykiIGNsYXNzPSJzdDAgc3QxIHN0MiI+UFBUPC90ZXh0Pgo8ZyBpZD0iTWFzayI+Cgk8cGF0aCBpZD0icGF0aC0xXzFfIiBkPSJNMTIuNywyNS4ybDMuNSwzLjV2LTkuMWgzLjR2OS4xbDMuNS0zLjVsMi40LDIuNGwtNy42LDcuNmwtNy42LTcuNkwxMi43LDI1LjJ6IE0zNS4yLDIwLjVsLTAuMSwyLjZIMjMuMQoJCXYtMy40aDguN2wwLTAuOWgwVjQuMUg0LjF2MTQuN2gwbDAsMC45aDguN3YzLjRIMC42di0xLjh2LTIuNXYtMy4zVjAuNmgzNC42djE4LjJWMjAuNXoiLz4KPC9nPgo8L3N2Zz4K');
      }

      & a[href$='.doc']::before,
      & a[href$='.docx']::before {
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzNiAzNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzYgMzY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojMjIyMjIyO30KCS5zdDF7Zm9udC1mYW1pbHk6J0FyaWFsLUJvbGRNVCc7fQoJLnN0Mntmb250LXNpemU6OS45NjUzcHg7fQo8L3N0eWxlPgo8dGl0bGU+Q29tcG9uZW50cyAvIEljb25zIC8gU3ltYm9sIC8gRG9jdW1lbnQgLyBET0M8L3RpdGxlPgo8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KPHRleHQgdHJhbnNmb3JtPSJtYXRyaXgoMSAwIDAgMSA2LjYyOTIgMTUuMDY5OCkiIGNsYXNzPSJzdDAgc3QxIHN0MiI+RE9DPC90ZXh0Pgo8ZyBpZD0iTWFzayI+Cgk8cGF0aCBpZD0icGF0aC0xXzFfIiBkPSJNMTIuNiwyNS4xbDMuNSwzLjV2LTguOWgzLjR2OC45bDMuNS0zLjVsMi40LDIuNEwxNy43LDM1bC03LjUtNy41TDEyLjYsMjUuMXogTTM0LjgsMjAuNUwzNC43LDIzSDIyLjgKCQl2LTMuNGg4LjZsMC0wLjloMFY0LjNINHYxNC41aDBsMCwwLjloOC42VjIzSDAuNnYtMS44di0yLjR2LTMuM1YwLjhoMzQuMnYxOFYyMC41eiIvPgo8L2c+Cjwvc3ZnPgo=');
      }
    }
  }
`

const LottiePlayerWrapper = styled.div`
  height: 100vh;
  padding-bottom: 20vh;
  width: 80%;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Article = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  .vf-future-vision__grid,
  .vf-future-vision__grid--no-margins {
    row-gap: 0;
  }

  .article {
    &__section {
      display: flex;
      flex-direction: column;
      grid-column: 1 / -1;
      /* padding: 0 1.75rem; */
      width: 100%;

      @media screen and (min-width: 1024px) {
        /* padding: 0 3.75rem; */
      }

      @media screen and (min-width: 1024px) {
        grid-column: 2 / span 10;
      }
    }

    &__title {
      ${({ theme }) => theme.h1Typo};
      margin-bottom: 0.8125rem;

      @media screen and (min-width: 1024px) {
        margin-bottom: 1rem;
      }
    }

    &__date {
      font-size: 0.875rem;
      line-height: 1.5;
      margin-bottom: 1.75rem;

      @media screen and (min-width: 1024px) {
        margin-bottom: 2rem;
      }
    }

    &__image-container {
      grid-column: 1 / -1;
      margin: 0 auto;
      margin-bottom: 2.5rem;
      width: 100%;
    }

    &__image-wrapper {
      grid-column: 1 / -1;
      padding-top: 56.25%;
      position: relative;
      width: 100%;
    }

    &__image {
      inset: 0;
      object-fit: cover;
      position: absolute;
      height: 100%;
      width: 100%;
    }

    &__caption {
      display: block;
      font-size: 0.875rem;
      grid-column: 1 / -1;
      line-height: 1.5;
      margin-top: 1rem;

      @media screen and (min-width: 1024px) {
        padding: 0 3.75rem;
        grid-column: 2 / span 10;
      }

      @media screen and (min-width: 1024px) {
        max-width: 850px;
        padding: 0;
      }
    }

    &__description {
      font-size: 1.25rem;
      font-weight: 500;
      line-height: 1.5;
      grid-column: 1 / -1;
      margin-bottom: 3.25rem;
      /* padding: 0 1.75rem; */

      @media screen and (min-width: 1024px) {
        /* padding: 0 3.75rem; */
      }

      @media screen and (min-width: 1024px) {
        font-size: 1.5rem;
        grid-column: 2 / span 7;
      }

      @media screen and (min-width: 1400px) {
        font-size: 1.5rem;
        grid-column: 2 / span 8;
      }
    }
  }

  details {
    border-bottom: 1px solid #e6e6e6;
    padding: 20px 0;

    summary {
      list-style: none;
      outline: none;
      font-size: 20px;
      line-height: 28px;
      font-weight: 500;
      position: relative;
      display: flex;
      justify-content: space-between;
      cursor: pointer;

      &::after {
        content: '';
        width: 20px;
        background-image: url("data:image/svg+xml,%3Csvg width='20' height='10' viewBox='0 0 20 10' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M9.99194 9.04225L0.400391 1.57746L1.62574 0L9.99194 6.51408L18.3652 0L19.5905 1.57746L9.99194 9.04225Z' fill='%232071B5'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: center;
      }

      &::marker,
      &::-webkit-details-marker {
        display: none;
      }
    }

    & > * {
      margin: 0;

      &:not(summary) {
        padding-top: 12px;
      }
    }

    &[open] {
      summary {
        margin-bottom: 0;

        &::after {
          transform: rotate(180deg);
        }
      }
    }
  }
`

export async function getStaticPaths() {
  const articleListings: ArticleListingProps[] = await API.getArticles()

  const allArticles = articleListings
    .map(({ article }) => article)
    .filter(article => article?.articleId)

  return {
    paths: allArticles.flatMap((article: Article) => {
      return [
        {
          params: {
            articleid: article.articleId.toString(),
          },
        },
        {
          params: {
            articleid: article.id.toString(),
          },
        },
      ]
    }),
    fallback: false,
  }
}

export const getStaticProps: GetStaticProps<
  { article?: Article },
  { articleid: string }
> = async context => {
  const { articleid } = context.params ?? {}

  if (!articleid) return { props: {} }

  const article = await API.getArticle(articleid)

  return {
    props: { article },
  }
}

const videoExtensions = ['.qt', '.mp4', '.mov', '.webm', '.mkv', '.avi', '.m4v']

export const VattenfallMarkdown = (
  props: ComponentProps<typeof ReactMarkdown>
) => {
  return (
    <ReactMarkdown
      rehypePlugins={[rehypeRaw, remarkGfm]}
      components={{
        ul: ({ children }) => <ul className="vf-ul">{children}</ul>,
        img: ({ src, alt, ...props }) => {
          if (!src) return null

          if (videoExtensions.some(videoExt => src.includes(videoExt))) {
            return (
              <video style={{ width: '100%' }} controls>
                <source src={src}></source>
              </video>
            )
          }

          return <img src={src} alt={alt} {...props} />
        },
      }}
      {...props}
    />
  )
}

function ArticlePage(props: InferGetStaticPropsType<typeof getStaticProps>) {
  const { article } = props

  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  } as const

  const date = article
    ? article?.postedDate
      ? new Date(article.postedDate)
      : new Date(article.created_at)
    : new Date()

  // all code below (until the return) is a special case for a specific article. this shouldnt be used anywhere else
  const [modifiedContent, setModifiedContent] = useState<{
    firstPart: string
    hidden: string
    secondPart: string
  }>()
  const [showHiddenContent, setShowHiddenContent] = useState<boolean>(false)
  const animationRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (modifiedContent) {
      return
    }

    if (article?.articleId === 'whats-new') {
      const firstSplit = article.content.split('<SPLITTER>')
      const firstDesignerContent = firstSplit[0]
      const secondSplit = firstSplit[1].split('</SPLITTER')
      const hiddenContent = secondSplit[0]
      const devContent = secondSplit[1]

      setModifiedContent({
        firstPart: firstDesignerContent,
        hidden: hiddenContent,
        secondPart: devContent,
      })
    }
  }, [article?.articleId, article?.content, modifiedContent])

  useEffect(() => {
    if (!animationRef.current) return

    autoAnimate(animationRef.current)
  }, [])

  const renderWhatsNewSpecialLayout = () => {
    if (!modifiedContent) return

    return (
      <>
        <VattenfallMarkdown>{modifiedContent.firstPart}</VattenfallMarkdown>
        {!showHiddenContent ? (
          <button
            className="vf-button vf-button__outline--secondary"
            onClick={() => setShowHiddenContent(!showHiddenContent)}
          >
            Read more
          </button>
        ) : (
          <div style={{ marginTop: '-1.25rem' }}>
            <VattenfallMarkdown>{modifiedContent.hidden}</VattenfallMarkdown>
          </div>
        )}
        <VattenfallMarkdown>{modifiedContent.secondPart}</VattenfallMarkdown>
      </>
    )
  }

  return (
    <DefaultLayout style={{ paddingBottom: '15vh' }}>
      {!article ? (
        <LottiePlayerWrapper className="articles__lottie-player">
          <Lottie
            loop
            animationData={lottieJson}
            play
            style={{ width: 150, height: 150 }}
          />
        </LottiePlayerWrapper>
      ) : (
        <>
          <Article>
            <div className="vf-future-vision__grid">
              <div className="article__section">
                <h1 className="article__title">{article.cardTitle}</h1>
                <p className="article__date">
                  Published at: {date.toLocaleDateString('en-US', options)}
                </p>
              </div>
              {!article.hideCardImage && (
                <div className="article__image-container vf-future-vision__grid--no-margins">
                  <div className="article__image-wrapper">
                    <img
                      className="article__image"
                      src={formatAssetUrl(article.cardImage.url)}
                      alt={article.cardImage.alternativeText}
                    />
                  </div>
                  {article.cardImage.caption && (
                    <span className="article__caption">
                      {article.cardImage.caption}
                    </span>
                  )}
                </div>
              )}
              {article.description && (
                <VattenfallMarkdown
                  linkTarget="_blank"
                  transformImageUri={formatAssetUrl}
                  className="article__description"
                >
                  {article.description}
                </VattenfallMarkdown>
              )}
            </div>
          </Article>
          {article.content_blocks && article.content_blocks.length > 0 ? (
            <div className="article__content-blocks">
              {article.content_blocks
                .map(
                  contentBlock =>
                    [
                      contentBlock,
                      contentBlockMap[contentBlock.__component],
                    ] as const
                )
                .filter(([, component]) => component)
                .map(([contentData, Component], index) => (
                  <Component
                    key={contentData.id}
                    {...contentData}
                    useParallax={index !== 0}
                  />
                ))}
            </div>
          ) : (
            <Article>
              <div className="vf-future-vision__grid">
                <div className="article__section">
                  <ArticlePageStyle>
                    <div ref={animationRef} className="article__content">
                      {article.articleId !== 'whats-new' ? (
                        <VattenfallMarkdown
                          linkTarget="_blank"
                          transformImageUri={formatAssetUrl}
                        >
                          {article.content}
                        </VattenfallMarkdown>
                      ) : (
                        renderWhatsNewSpecialLayout()
                      )}
                    </div>

                    {article.attachments && (
                      <ul className="attachments">
                        {article.attachments.map(attachment => (
                          <li
                            key={attachment.name}
                            className="attachments__link"
                          >
                            <a
                              href={
                                process.env.NEXT_PUBLIC_STRAPI_BASE_URL +
                                attachment.url
                              }
                              download={attachment.name + attachment.ext}
                              target="_blank"
                              rel="noreferrer"
                            >
                              {attachment.name + attachment.ext}
                            </a>
                            {attachment.size && (
                              <span className="attachments__size-and-type">
                                ({mimeTypeMapping[attachment.mime]},{' '}
                                {Math.ceil(attachment.size) > 1000
                                  ? (
                                      Math.round(attachment.size) / 1000
                                    ).toFixed(2) + ' MB'
                                  : Math.ceil(attachment.size) + ' kB'}
                                )
                              </span>
                            )}
                          </li>
                        ))}
                      </ul>
                    )}

                    <Link href="/articles">
                      <span className="vf-button vf-button__outline--secondary back-button">
                        More articles
                      </span>
                    </Link>
                  </ArticlePageStyle>
                </div>
              </div>
            </Article>
          )}
        </>
      )}
    </DefaultLayout>
  )
}

export default ArticlePage
