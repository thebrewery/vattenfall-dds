import React from 'react'
import ReactMarkdown from 'react-markdown'
import styled from 'styled-components'
import changelog from '../../CHANGELOG.md'
import { DefaultLayout } from '../components/DefaultLayout'

const ReleaseNotesContainer = styled.div`
  margin: 0 auto;
  max-width: 900px;

  h2 {
    color: ${({ theme }) => theme.colours.primary__coal_black};
    font-family: ${({ theme }) => theme.largeHeadingFontFamily};
    font-size: ${({ theme }) => theme.mediumHeadingFontSize};
    font-weight: 600;
    padding-bottom: 20px;
    width: 100%;
  }

  h3 {
    color: ${({ theme }) => theme.colours.primary__coal_black};
    font-family: 'Vattenfall Hall NORDx';
    font-size: 1.375rem;
    line-height: ${({ theme }) => theme.mediumLineHeight};
    margin-bottom: 1rem;
    text-decoration: none;

    @media screen and (min-width: 768px) {
      font-size: ${({ theme }) => theme.headingFontSize};
      line-height: ${({ theme }) => theme.largeLineHeight};
    }
  }

  h4 {
    color: #000;
    font-family: 'Vattenfall Hall NORDx';
    font-size: 18px;
    font-weight: 600;
    padding-bottom: 20px;
    text-decoration: none;
    line-height: 1.5rem;

    @media screen and (min-width: 768px) {
      font-size: 1.375rem;
      line-height: ${({ theme }) => theme.mediumLineHeight};
    }
  }

  p,
  li {
    font-size: ${({ theme }) => theme.fontSize};
    line-height: ${({ theme }) => theme.mediumLineHeight};
    margin-bottom: 2rem;
  }

  ul {
    margin: 0;
    margin-bottom: 2rem;
  }

  hr {
    border: 0;
    border-top: 1px solid #e6eaed;
    display: block;
    height: 1px;
    margin: 1em 0;
    padding: 0;

    &:last-child {
      display: none;
    }
  }
`

const ReleaseNotes = () => (
  <DefaultLayout>
    <ReleaseNotesContainer className="vf-guideline__container">
      <h1 className="vf-guideline__large-heading">Release notes</h1>
      <ReactMarkdown linkTarget="_blank">{changelog}</ReactMarkdown>
    </ReleaseNotesContainer>
  </DefaultLayout>
)

export default ReleaseNotes
