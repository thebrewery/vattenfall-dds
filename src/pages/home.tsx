import Link from 'next/link'
import { ComponentType, useMemo, useState } from 'react'
import styled from 'styled-components'

import ContentWrapper from '../components/ContentWrapper'
import { Header } from '../components/Header'
import { NewSignUpModal } from '../components/NewSignUpModal'
import { SignUpModal } from '../components/SignUpModal'
import AnimatedHero from '../react/components/modules/AnimatedHero'
import { API, CarouselItem, ContentBlockProps } from '../utils/api'

import { FullWidthContent } from '../article-components/FullWidthContent'
import { SemiCircle } from '../article-components/SemiCircle'
import { ThreeArticleColumn } from '../article-components/ThreeArticleColumn'
import { ThreeColumnContent } from '../article-components/ThreeColumnContent'
import { TwoColumnContent } from '../article-components/TwoColumnContent'
import { TwoColumnEven } from '../article-components/TwoColumnEven'
import { TwoColumnSpaced } from '../article-components/TwoColumnSpaced'
import FAQBlock from '../components/FAQBlock'

type EntryLinkProps = {
  linkIcon: string
  linkHref: string
  text: string
  title?: string
  description?: string
}

const EntryLinkStyle = styled.div`
  min-height: 150px;

  @media screen and (min-width: 768px) {
    min-height: 260px;
    grid-template-columns: repeat(3, 1fr);
  }

  .entry-link {
    align-items: center;
    background-color: ${({ theme }) => theme.lightMode.bgColor};
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    width: 100%;
    [class^='vf-icon-'],
    [class*=' vf-icon-'] {
      &:before {
        font-size: 4.5rem !important;
      }
    }

    &__description {
      max-width: 255px;
      margin-top: 0.75rem;
      text-align: center;
    }

    @media screen and (min-width: 768px) {
      [class^='vf-icon-'],
      [class*=' vf-icon-'] {
        margin-bottom: 1rem;

        &:before {
          font-size: 4.5rem !important;
        }
      }
    }

    &:hover {
      background-color: ${({ theme }) => theme.lightBlueBackground};
      cursor: pointer;

      .future-vision__link::before {
        transform: translate(-125%, 0);
      }
    }
  }
`

const EntryLink = ({
  linkIcon,
  linkHref,
  text,
  title,
  description,
}: EntryLinkProps) => (
  <EntryLinkStyle>
    <Link href={linkHref}>
      <div className="entry-link" tabIndex={0}>
        <span className={linkIcon} />
        {title && <h2 className="entry-link__title">{title}</h2>}
        {description && (
          <p className="entry-link__description">{description}</p>
        )}
        <a className="future-vision__link">{text}</a>
      </div>
    </Link>
  </EntryLinkStyle>
)

export const contentBlockMap: Record<
  string,
  ComponentType<ContentBlockProps>
> = {
  'articlepage.semi-circle-image': SemiCircle,
  'articlepage.two-column-content': TwoColumnContent,
  'articlepage.three-article-column': ThreeArticleColumn,
  'articlepage.two-column-spaced': TwoColumnSpaced,
  'articlepage.two-column-even': TwoColumnEven,
  'articlepage.full-width-content': FullWidthContent,
  'articlepage.three-column-content': ThreeColumnContent,
}

const Container = styled.div`
  .vf {
    &__video-badge {
      align-items: center;
      background: #ffda00;
      border-radius: 50%;
      color: #000;
      display: flex;
      flex-direction: column;
      font-family: 'Vattenfall Hall Bold NORDx';
      font-size: 15px;
      font-weight: 700;
      height: 115px;
      justify-content: center;
      position: absolute;
      text-align: center;
      top: 10px;
      right: 30px;
      width: 115px;
      z-index: 1;

      @media screen and (min-width: 768px) {
        top: 40px;
        right: 90px;
      }

      &:hover {
        cursor: pointer;
      }
    }
  }

  .intro-arrow {
    color: #222222;
    font-size: ${({ theme }) => theme.fontSize};

    &:before {
      content: '';
      display: inline-block;
      width: 15px;
      height: 16px;
      background-color: transparent;
      background-repeat: no-repeat;
      background-position: 0 0;
      background-size: 15px 16px;
      vertical-align: middle;
      background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMjBweCIgaGVpZ2h0PSIxOHB4IiB2aWV3Qm94PSIwIDAgMjAgMTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8ZGVmcz4KICAgICAgICA8cG9seWdvbiBpZD0icGF0aC0xIiBwb2ludHM9IjggMTkgMjMuNDQ4MTc1MiAxOSAxNy40MzQzMDY2IDI1LjM0MjY0NzcgMTkuMDQwMTQ2IDI3IDI3LjggMTcuOTk1ODc3MiAxOS4wNDAxNDYgOSAxNy40MzQzMDY2IDEwLjY0OTEwNjcgMjMuNDQ4MTc1MiAxNyA4IDE3Ij48L3BvbHlnb24+CiAgICA8L2RlZnM+CiAgICA8ZyBpZD0iU3ltYm9scyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9IkNvbXBvbmVudHMtLy1JY29ucy0vLVN5bWJvbC0vLUFyQ29tcG9uZW50cy0vLVRhYmxlLS8tUm93LS8tUHJpbWFyeS0vLVJpZ2h0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOC4wMDAwMDAsIC05LjAwMDAwMCkiPgogICAgICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBmaWxsPSJ3aGl0ZSI+CiAgICAgICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPgogICAgICAgICAgICA8L21hc2s+CiAgICAgICAgICAgIDx1c2UgaWQ9Ik1hc2siIGZpbGw9IiMyMjIyMjIiIGZpbGwtcnVsZT0ibm9uemVybyIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=);
    }
  }

  .vf-component__container {
    margin: 0 auto;
    margin-bottom: 44px;
  }

  .carousel-and-content-wrapper {
    position: relative;
    max-width: 1800px;
    margin: 0 auto;
    margin-bottom: 3rem;
    padding: 0 20px;
    box-sizing: content-box;
    @media screen and (min-width: 1024px) {
      padding: 0 30px;
    }
  }

  .container-entry {
    margin: 65px auto;
  }

  .entry-links {
    display: grid;
    grid-template-columns: repeat(1, 1fr);
    gap: 21px;

    @media screen and (min-width: 768px) {
      gap: 42px;
      grid-template-columns: repeat(3, 1fr);
    }

    .future-vision__link {
      color: #1964a3;
    }
  }

  .articles {
    &__content-blocks {
      background-color: #f6f6f6;
    }
  }
`
type LandingPageProps = {
  carouselItems: CarouselItem[]
  title: string
  bodytext: string
  content_blocks: ContentBlockProps[]
}

export const getStaticProps = async () => {
  const data = await API.getLandingPageData()

  const { carouselItems, title, bodytext, content_blocks }: LandingPageProps =
    data

  return {
    props: { carouselItems, title, bodytext, content_blocks },
  }
}

const Home = ({ carouselItems, content_blocks }: LandingPageProps) => {
  const [toggleSignUpModal, setToggleSignUpModal] = useState(false)
  const [newSignUpModal, setNewSignUpModal] = useState(false)
  const [status, setStatus] = useState<number>(0)

  const heroItems = useMemo(
    () =>
      carouselItems.map(item => ({
        id: item.id,
        title: item.title,
        subTitle: item.subTitle,
        linkHref: item.linkHref || '/',
        linkLabel: item.linkLabel,
      })),
    [carouselItems]
  )

  return (
    <Container>
      <Header />
      <AnimatedHero sliderItems={heroItems} />
      <ContentWrapper id="text" className="container-entry">
        <div className="entry-links">
          <EntryLink
            linkIcon="vf-icon-edit"
            linkHref="/designers/get-started"
            text="Explore"
            title="Designers"
            description="Do you need design assets or guidelines for design?"
          />
          <EntryLink
            linkIcon="vf-icon-laptop"
            linkHref="/developers/get-started"
            text="Explore"
            title="Developers"
            description="Do you need code assets or components?"
          />
          <EntryLink
            linkIcon="vf-icon-blog"
            linkHref="/contact"
            text="Contact us"
            title="Share feedback"
            description="Want to get in touch to request something or provide feedback?"
          />
        </div>
      </ContentWrapper>
      {content_blocks && content_blocks.length > 0 && (
        <div className="articles__content-blocks">
          {content_blocks
            .map(
              contentBlock =>
                [
                  contentBlock,
                  contentBlockMap[contentBlock.__component],
                ] as const
            )
            .filter(([, component]) => component)
            .map(([contentData, Component]) => (
              <Component
                key={contentData.__component + contentData.id}
                {...contentData}
              />
            ))}
        </div>
      )}

      <FAQBlock />

      {/* {toggleSignUpModal && ( */}
      {/* <SignUpModal
        setToggleSignUpModal={setToggleSignUpModal}
        setNewSignUpModal={setNewSignUpModal}
        setStatus={setStatus}
      /> */}
    </Container>
  )
}

export default Home
