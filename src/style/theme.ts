import Git from '../components/js-svg/Git'

export const theme = {
  smallFontSize: '0.75rem',
  fontSize: '1rem',
  largeFontSize: '1.25rem',
  smallHeadingFontSize: '1.5rem',
  headingFontSize: '1.75rem',
  mediumHeadingFontSize: '3.25rem',
  largeHeadingFontSize: '4.5rem',
  smallerLineHeight: '1rem',
  smallLineHeight: '1.375rem',
  mediumLineHeight: '1.75rem',
  largeLineHeight: '2.25rem',
  mediumHeadingLineHeight: '4.5rem',
  largeHeadingLineHeight: '5.25rem',
  largeHeadingFontFamily: 'Vattenfall Hall Display NORDx, system-ui',
  smallerHeadingFontFamily: 'Vattenfall Hall NORDx, system-ui',
  defaultFontFamily: 'Vattenfall Hall NORDx, system-ui',
  lightBlueBackground: '#edf1f6',
  h0Typo: `
  font-family: 'Vattenfall Hall Display NORDx';
  font-size: 2.125rem;
  line-height: 2.5rem;

  @media screen and (min-width: 1024px) {
    font-size: 4.5rem;
    line-height: 5.25rem;
  }
  `,
  h1Typo: `
  font-family: 'Vattenfall Hall Display NORDx';
  font-size: 2.125rem;
  line-height: 2.5rem;

  @media screen and (min-width: 1024px) {
    font-size: 3.25rem;
    line-height: 3.875rem;
  }
    `,
  h2Typo: `
  font-family: 'Vattenfall Hall Display NORDx';
  font-size: 1.75rem;
  line-height: 2rem;
  
  @media screen and (min-width: 1024px) {
    font-size: 2.25rem;
    line-height: 2.625rem;
  }
    `,
  h3Typo: `
  font-family: 'Vattenfall Hall NORDx';
  font-size: 1.375rem;
  font-weight: 600;
  line-height: 1.75rem;

  @media screen and (min-width: 1024px) {
    font-size: 1.75rem;
    line-height: 2.25rem;
  }
    `,
  largeBodyCopy: `
    font-size: 1rem;
    line-height: 1.75rem;

    @media screen and (min-width: 1024px) {
      font-size: 1.25rem;
      line-height: 2.25rem;
    }
    `,
  smallBodyCopy: `
      font-size: 1rem;
      line-height: 1.75rem;
    `,
  preamble: `
    font-size: 1.25rem;
    font-weight: 500;
    line-height: 1.5;
    text-decoration: none;
  
    @media screen and (min-width: 1024px) {
      font-size: 1.5rem;
      line-height: 2.25rem;
    }
    `,
  darkMode: {
    backgroundColor: '#000',
    textColor: '#fff',
  },
  lightMode: {
    backgroundColor: '#fff',
    textColor: '#000',
  },
  iframeStyle: {
    width: '100%',
    height: '800px',
    borderRadius: '4px',
    border: '1px solid #e8e8e9',
    overflow: 'hidden',
  },
  spacerXL: '88px',
  spacerLg: '56px',
  spacerMd: '44px',
  spacerSm: '28px',
  colours: {
    primary__solar_yellow: '#ffda00',
    primary__ocean_blue: '#2071b5',
    primary__aura_white: '#fff',
    primary__coal_black: '#000',
    primary__magnetic_grey: '#4e4b48',

    secondary__dark_green: '#005c63',
    secondary__dark_blue: '#1e324f',
    secondary__pink: '#d1266b',
    secondary__dark_purple: '#85254b',
    secondary__green: '#3dc07c',
    secondary__red: '#f93b18',
    secondary__purple: '#9b62c3',

    bg__light_yellow: '#fffee5',
    bg__light_green: '#edf9f3',
    bg__light_blue: '#edf1f6',
    bg__light_blue__alt: '#ebf2f3',
    bg__light_red: '#fef0ea',
    bg__light_grey: '#f2f2f2',

    tertiary__ash_blue: '#869bad',
    tertiary__dark_ash_blue: '#69788c',
    tertiary__lighter_grey: '#e6e6e6',
    tertiary__light_grey: '#ccc',
    tertiary__medium_grey: '#999',
    tertiary__medium_dark_grey: '#767676',
    tertiary__dark_grey: '#666',

    energy__hydro: '#2da55d',
    energy__wind: '#4fcc51',
    energy__solar: '#81e0a8',
    energy__biomass: '#375e4e',
    energy__coal: '#e88a74',
    energy__gas: '#d85067',
    energy__nuclear: '#e6e6e6',
    energy__district_heating: '#a376cc',

    link__color: '#1964a3',
    link__hover_color: 'darken($link__color, 30%)',

    text__dark_color: '#222222',

    border__grey: '#e8e8e9',

    articleCardGrey: '#595959',

    tooltip__green: '#c5eecc',
    tooltip__blue_bg: '#f1f8ff',
    tooltip__blue_border: '#c5dcee',
    tooltip__red_bg: '#fff5f1',
    tooltip__red_border: '#eec8c5',

    $font__default: 'Vattenfall Hall NORDx',
    $font__heading: 'Vattenfall Hall Display NORDx',
    $font__icon_library: 'vf-icons',
  },
  GitLogo: Git,
}
