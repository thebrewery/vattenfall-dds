import '@vf-dds/vf-dds-vanilla/src/style/component-css/buttons.css'
import '@vf-dds/vf-dds-vanilla/src/style/component-css/dropdown.css'
import { PropsWithChildren, useEffect, useRef, useState } from 'react'
import rehypeRaw from 'rehype-raw'
import styled, { keyframes } from 'styled-components'

import { css } from 'styled-components'
import facebookFeedMd from './facebook-feed.md'
import facebookReelsMd from './facebook-reels.md'
import facebookStoriesMd from './facebook-stories.md'
import instagramFeedMd from './instagram-feed.md'
import instagramReelsMd from './instagram-reels.md'
import instagramStoryMd from './instagram-stories.md'
import linkedinFeedMd from './linkedin-feed.md'

import ReactMarkdown from 'react-markdown'
import type {} from 'styled-components/cssprop'
import LinkedInCarousel from './LinkedInCarousel'
import { splitMarkdown } from './brandtoolboxutils'
import { SMGTooltip } from './SMGTooltip'

const BrandToolBoxContainer = styled.div`
  width: 100%;
  max-width: 798px;
  /* box-shadow: 0 0 0 1px black; */
  min-height: 800px;
  h2,
  h3 {
    font-weight: 500;
    line-height: 1.3;
    margin-top: 1.2em;
    margin-bottom: 0.33em;

    &:first-child {
      margin-top: 0;
    }
  }

  h5 {
    font-size: 0.75rem;
    line-height: 1rem;
    font-weight: bold;
    margin: 0;
  }

  p {
    line-height: 1.45;
    margin: 1.5em 0;
  }
  ul {
    padding: 0 1em;
  }

  .vf-bgs {
    &__media-info {
      p {
        margin: 0;
        margin-bottom: 1.25rem;
        font-size: 0.75rem;
        line-height: 1rem;
      }
    }

    &__intro {
      margin: 0;
      margin-top: 1.875em;
      margin-bottom: 2.5em;
      line-height: 1.375em;

      p {
        margin: 0;
        line-height: inherit;
      }
    }

    &__tech-aspects {
      background-color: #f2f2f2;
      padding: 1.5rem 1.75rem;

      h3 {
        align-items: center;
        display: flex;
        gap: 0.625rem;
      }

      ul {
        columns: 2;
        column-gap: 3rem;
      }
    }

    &__content {
      padding-top: 1.5rem;

      h3 {
        font-size: 1.375rem;
        line-height: 1.8125rem;
        margin-bottom: 1.325rem;
      }

      h4,
      p {
        font-size: 1rem;
        line-height: 1.5rem;
        margin-top: 0;
      }

      &--phone {
        padding-top: 2.5rem;
      }
    }

    &__visuals {
      display: grid;
      grid-auto-flow: dense;
      grid-template-columns: 1fr 1fr;
      margin-top: 4.0625em;
      margin-bottom: 3.5em;

      img {
        width: 100%;
      }

      &-heading {
        grid-column: 1 / -1;
        font-size: 1.375em;
        line-height: 1.8125em;
        margin: 0;
        margin-bottom: 1em;
      }

      &-do {
        * {
          background-color: #edf1f6;
          grid-column: 1;
          column-span: 1;
        }
      }

      &-donts {
        * {
          background-color: #fef0ea;
          grid-column: 2;
          column-span: 1;
        }
      }

      &-do,
      &-donts {
        display: contents;

        .dos-headings {
          align-items: flex-end;
          display: flex;
          flex-direction: row;
          gap: 0.5rem;
          margin: 0;
          padding: 0 1.25rem;
          padding-top: 1.75em;

          h4,
          p {
            margin: 0;
            padding: 0;
          }

          p {
            font-size: 0.75rem;
          }
        }

        *:first-child {
          padding-top: 1.875rem;
        }

        *:last-child {
          padding-bottom: 4rem;
          border-bottom: 0;
        }

        * {
          margin: 0 !important;
          padding: 0 1.25rem;
        }

        p {
          line-height: 1.375em;
          padding-bottom: 1.75em;
          border-bottom: 1px solid #fff;
        }

        h4 {
          font-size: 1em;
          line-height: 1.375em;
          font-weight: bold;
          padding-bottom: 0.25rem;
        }

        &-heading {
          font-size: 1.375em;
          line-height: 1.8125em;
          font-weight: 500;
        }
      }
    }
  }
`

const fadeIn = keyframes`
  from{
    opacity: 0;
    transform: translateY(20px);
  }
  to{
    opacity: 1;
    transform: translateY(0px);
  }
`

const fadeOut = keyframes`
  from{
    opacity: 1;
    transform: translateY(0px);
  }
  to{
    opacity: 0;
    transform: translateY(5px);
  }
`

const FormatWrapper = styled.div`
  animation: ${fadeIn} 550ms forwards;
  &.removing {
    animation: ${fadeOut} 175ms forwards;
  }
`
const fadeInFromLeft = keyframes`
  from{
    opacity: 0;
    transform: translateY(7.5px);
  }
  to{
    opacity: 1;
    transform: translateX(0);
  }
`

const phoneAspectRatio = '160/340'

const MobileWrapper = styled.div`
  padding: 0.3rem;
  aspect-ratio: ${phoneAspectRatio};
  border-radius: 2.3em;
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-bottom: 1.0625rem;
  margin-top: 2.5rem;
  position: relative;
  width: 100%;
  background-position-x: center;
  z-index: 10;
  overflow: hidden;
  min-height: 600px;
  animation: ${fadeInFromLeft} 450ms ease;
`

const Columns = styled.div`
  display: flex;
  gap: 3em;
  margin-bottom: 3rem;
`

const Column = styled.div`
  /* width: 50%; */
  flex-shrink: 0;
  flex-grow: 0;
`

const instagramReels = splitMarkdown(instagramReelsMd)
const instagramFeed = splitMarkdown(instagramFeedMd)
const facebookFeed = splitMarkdown(facebookFeedMd)
const instagramStory = splitMarkdown(instagramStoryMd)
const facebookReels = splitMarkdown(facebookReelsMd)
const facebookStories = splitMarkdown(facebookStoriesMd)
const linkedInFeed = splitMarkdown(linkedinFeedMd)

const formats = [
  {
    name: 'Instagram Stories',
    intro: instagramStory.info,
    content: instagramStory.content,
    caption: instagramStory.caption,
    technicalAspects: instagramStory.technicalAspects,
    media: [
      {
        name: 'Video',
        type: 'video',
        path: ['/assets/brandtoolbox/social/instagram_story.mp4'],
      },
      { type: 'image', name: 'Image' },
    ],
    fullWidthPost: instagramStory.fullWidthPost,
    designRecommendations: instagramStory.designRecommendations,
    writtenContent: {
      dos: instagramStory.dos,
      donts: instagramStory.donts,
      dosIntro: instagramStory.dosIntro,
      dontsIntro: instagramStory.dontsIntro,
    },
  },
  {
    name: 'Instagram Reel',
    intro: instagramReels.info,
    content: instagramReels.content,
    caption: instagramReels.caption,
    technicalAspects: instagramReels.technicalAspects,
    media: [
      {
        name: 'Video',
        type: 'video',
        path: ['/assets/brandtoolbox/social/Instagram_Reel2.mp4'],
      },
      { type: 'image', name: 'Image' },
    ],
    fullWidthPost: instagramReels.fullWidthPost,
    designRecommendations: instagramReels.designRecommendations,
    writtenContent: {
      dos: instagramReels.dos,
      donts: instagramReels.donts,
      dosIntro: instagramReels.dosIntro,
      dontsIntro: instagramReels.dontsIntro,
    },
  },
  // {
  //   name: 'Instagram Feed',
  //   intro: instagramFeed.info,
  //   content: instagramFeed.content,
  //   caption: instagramFeed.caption,
  //   technicalAspects: instagramFeed.technicalAspects,
  //   media: [
  //     {
  //       name: 'Image',
  //       type: 'image',
  //       path: ['/assets/brandtoolbox/social/instagram-feed.jpg'],
  //     },
  //     {
  //       name: 'Video',
  //       type: 'video',
  //     },
  //     {
  //       name: 'Carousel',
  //       type: 'carousel',
  //     },
  //   ],
  //   fullWidthPost: instagramFeed.fullWidthPost,
  //   designRecommendations: instagramFeed.designRecommendations,
  //   writtenContent: {
  //     dos: instagramFeed.dos,
  //     donts: instagramFeed.donts,
  //     dosIntro: instagramFeed.dosIntro,
  //     dontsIntro: instagramFeed.dontsIntro,
  //   },
  // },
  {
    name: 'Facebook Reel',
    intro: facebookReels.info,
    content: facebookReels.content,
    caption: facebookReels.caption,
    technicalAspects: facebookReels.technicalAspects,
    media: [
      {
        name: 'Video',
        type: 'video',
        path: ['/assets/brandtoolbox/social/facebook-reel.mp4'],
      },
      { type: 'image', name: 'Image' },
    ],
    fullWidthPost: facebookReels.fullWidthPost,
    designRecommendations: facebookReels.designRecommendations,
    writtenContent: {
      dos: facebookReels.dos,
      donts: facebookReels.donts,
      dosIntro: facebookReels.dosIntro,
      dontsIntro: facebookReels.dontsIntro,
    },
  },
  {
    name: 'Facebook Feed',
    intro: facebookFeed.info,
    content: facebookFeed.content,
    caption: facebookFeed.caption,
    technicalAspects: facebookFeed.technicalAspects,
    media: [
      {
        name: 'Image',
        type: 'image',
        path: ['/assets/brandtoolbox/social/facebook-feed.jpg'],
      },
      {
        type: 'video',
        name: 'Video',
      },
    ],
    fullWidthPost: facebookFeed.fullWidthPost,
    designRecommendations: facebookFeed.designRecommendations,
    writtenContent: {
      dos: facebookFeed.dos,
      donts: facebookFeed.donts,
      dosIntro: facebookFeed.dosIntro,
      dontsIntro: facebookFeed.dontsIntro,
    },
  },
  {
    name: 'LinkedIn Feed',
    intro: linkedInFeed.info,
    content: linkedInFeed.content,
    caption: linkedInFeed.caption,
    technicalAspects: linkedInFeed.technicalAspects,
    media: [
      // {
      //   name: 'Carousel',
      //   type: 'carousel',
      //   backgroundImage: '/assets/brandtoolbox/social/linkedin-carousel-bg.jpg',
      //   media: [
      //     {
      //       type: 'image',
      //       path: '/assets/brandtoolbox/social/linkedin-carousel/1.jpg',
      //     },
      //     {
      //       type: 'image',
      //       path: '/assets/brandtoolbox/social/linkedin-carousel/2.jpg',
      //     },
      //     {
      //       type: 'image',
      //       path: '/assets/brandtoolbox/social/linkedin-carousel/3.jpg',
      //     },
      //     {
      //       type: 'image',
      //       path: '/assets/brandtoolbox/social/linkedin-carousel/4.jpg',
      //     },
      //   ],
      // },
      {
        name: 'Video',
        type: 'video',
        backgroundImage: '/assets/brandtoolbox/social/linkedin-video-bg.jpg',
        path: ['/assets/brandtoolbox/social/linkedin-feed.mp4'],
        square: true,
      },
      { type: 'image', name: 'Image' },
    ],
    fullWidthPost: linkedInFeed.fullWidthPost,
    designRecommendations: linkedInFeed.designRecommendations,
    writtenContent: linkedInFeed,
  },
] as const

const VideoLoop = ({
  sources,
  square,
}: {
  sources: string[]
  square?: boolean
}) => {
  const [currentSrc, setCurrentSrc] = useState(sources?.[0])
  const [fadeOut, setFadeOut] = useState(false)
  const videoRef = useRef<HTMLVideoElement>(null)
  useEffect(() => {
    const video = videoRef.current
    if (!video) {
      return
    }
    let timeout = 0
    const nextVideo = () => {
      clearTimeout(timeout)
      const currentVideoIndex = sources.findIndex(src => src === currentSrc)
      const nextIndex =
        currentVideoIndex + 1 >= sources.length ? 0 : currentVideoIndex + 1
      setFadeOut(true)
      timeout = window.setTimeout(() => {
        setFadeOut(false)
        setCurrentSrc(sources[nextIndex])
        video.currentTime = 0
        video.play()
      }, 900)
    }
    video.addEventListener('ended', nextVideo)
    return () => {
      clearTimeout(timeout)
      video.removeEventListener('ended', nextVideo)
    }
  }, [currentSrc, sources])
  return (
    <video
      src={currentSrc}
      ref={videoRef}
      autoPlay
      muted
      style={{
        width: '100%',
        objectFit: 'contain',
        display: 'block',
        opacity: fadeOut ? 0 : 1,
        transition: 'opacity 400ms',
        borderRadius: square ? '0px' : 'inherit',
      }}
    ></video>
  )
}

export default function BrandGuideSocials() {
  const [selectedFormat, setSelectedFormat] = useState(
    formats[0] as (typeof formats)[number]
  )

  const [fadeOut, setFadeOut] = useState(false)

  const [mediaIndex, setMediaIndex] = useState(0)

  const activeMedia = Array.isArray(selectedFormat.media)
    ? selectedFormat.media[mediaIndex]
    : selectedFormat.media

  useEffect(() => {
    setMediaIndex(0)
  }, [selectedFormat])

  const selectRef = useRef<HTMLSelectElement>(null)
  useEffect(() => {
    selectRef.current?.focus()
  }, [])

  return (
    <BrandToolBoxContainer>
      <div style={{ marginBottom: '3em' }}>
        <h2 style={{ marginBottom: '0.5em' }}>Select placement: </h2>
        <div
          className="vf-dropdown-wrapper"
          style={{ width: '100%', margin: '0.5rem 0 1.5rem' }}
        >
          <select
            ref={selectRef}
            className="vf-dropdown__semantic"
            style={{ width: '100%' }}
            value={selectedFormat.name}
            onChange={e => {
              setFadeOut(true)
              const value = e.currentTarget.value
              setTimeout(() => {
                setFadeOut(false)
                setSelectedFormat(
                  formats.find(format => format.name === value)!
                )
              }, 175)
            }}
          >
            {formats.map(format => (
              <option
                disabled={!format.content}
                key={format.name}
                value={format.name}
                selected={selectedFormat === format}
              >
                {format.name}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div>
        <ReactMarkdown>{selectedFormat.intro ?? ''}</ReactMarkdown>
      </div>
      <div className="vf-bgs__tech-aspects">
        <ReactMarkdown rehypePlugins={[rehypeRaw]}>
          {selectedFormat.technicalAspects ?? ''}
        </ReactMarkdown>
      </div>
      <FormatWrapper
        key={selectedFormat.name}
        className={`${fadeOut ? 'removing' : ''}`}
      >
        <Columns>
          <Column
            style={{
              width: '300px',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <MobileWrapper key={mediaIndex}>
              <img
                src="/assets/brandtoolbox/social/iphoneframe.svg"
                style={{
                  position: 'absolute',
                  width: '100%',
                  height: '100%',
                  zIndex: 10,
                  inset: 0,
                  pointerEvents: 'none',
                }}
                alt=""
              />
              {activeMedia.backgroundImage && (
                <div
                  style={{
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    zIndex: -1,
                    padding: '0.3em',
                    inset: 0,
                  }}
                >
                  <img
                    alt=" "
                    style={{
                      objectFit: 'contain',
                      width: '100%',
                      height: '100%',
                    }}
                    src={activeMedia.backgroundImage}
                  ></img>
                </div>
              )}
              {activeMedia?.type === 'video' ? (
                <VideoLoop
                  sources={activeMedia.path}
                  square={'square' in activeMedia && activeMedia.square}
                />
              ) : activeMedia?.type === 'image' ? (
                <img
                  src={activeMedia.path?.[0]}
                  alt=""
                  style={{ width: '100%', zIndex: 1 }}
                />
              ) : activeMedia?.type === 'carousel' ? (
                <LinkedInCarousel media={activeMedia.media} />
              ) : null}
            </MobileWrapper>
            {selectedFormat.caption && (
              <SMGTooltip tooltipText={selectedFormat.caption} />
            )}
            {Array.isArray(selectedFormat.media) && (
              <MediaToggles
                activeIndex={mediaIndex}
                media={selectedFormat.media}
                updateIndex={index => setMediaIndex(index)}
              />
            )}
          </Column>
          <Column
            className="vf-bgs__content vf-bgs__content--phone"
            style={{ flex: 1 }}
          >
            <ReactMarkdown>{selectedFormat.content ?? ''}</ReactMarkdown>
          </Column>
        </Columns>
        <div className="vf-bgs__content">
          <ReactMarkdown>
            {selectedFormat.designRecommendations ?? ''}
          </ReactMarkdown>
        </div>
        <div className="vf-bgs__visuals">
          <h3 className="vf-bgs__visuals-heading">Written Content</h3>
          <div className="vf-bgs__visuals-do">
            <h3 className="vf-bgs__visuals-do-heading">Do...</h3>
            <ReactMarkdown>
              {selectedFormat.writtenContent.dosIntro!}
            </ReactMarkdown>
            <ReactMarkdown rehypePlugins={[rehypeRaw]}>
              {selectedFormat.writtenContent.dos!}
            </ReactMarkdown>
          </div>
          <div className="vf-bgs__visuals-donts">
            <h3 className="vf-bgs__visuals-donts-heading">Dont...</h3>
            <ReactMarkdown>
              {selectedFormat.writtenContent.dontsIntro!}
            </ReactMarkdown>
            <ReactMarkdown rehypePlugins={[rehypeRaw]}>
              {selectedFormat.writtenContent.donts!}
            </ReactMarkdown>
          </div>
        </div>
        <div className="vf-bgs__content">
          <ReactMarkdown>{selectedFormat.fullWidthPost ?? ''}</ReactMarkdown>
          <h3>Telling the story</h3>
          <p>
            The best way of storytelling can vary depending on the medium and
            audience you're targeting. Organize your story into a clear and
            coherent structure. There are strengths and limitations of each
            social media platform we use. So we need to tailor our storytelling
            approach to fit the platform's format, character limits, and
            audience behavior. For example, Instagram is visual-centric, while
            LinkedIn requires concise messaging. Grab attention quickly and use
            storytelling techniques that allow the audience to experience the
            story firsthand. Instead of merely stating facts, evoke emotions,
            paint vivid pictures, and create a sensory experience through your
            content.
          </p>
        </div>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: 'repeat(3, 1fr)',
            gap: '1em',
            width: '100%',
            maxWidth: '888px',
            marginTop: '2.5em',
          }}
        >
          <InfoPhone>
            <h2>Thumb-stopper:</h2>
            <p>
              Start with eye-catching visuals, compelling headlines, or
              intriguing questions to hook viewers from the start. Ensure that
              the content is visually consistent with our brand's aesthetics,
              including colors, fonts, and overall style.
            </p>
            <h2>Creating a hook:</h2>
            <p>
              Follow with a compelling hook to grab the audience's attention and
              make them curious about what's coming next.
            </p>
          </InfoPhone>
          <InfoPhone>
            <h2>The hang</h2>
            <p>
              Guide the audience through the story with a clear narrative flow.
              Use storytelling techniques like suspense, surprise, humor, or
              emotional appeal to evoke a response from the audience. Create
              anticipation, build tension, or elicit emotions that resonate with
              your brand's message.
            </p>
          </InfoPhone>
          <InfoPhone>
            <h2>
              End with a <br /> Call to action
            </h2>
            <p>
              Encourage the audience to take action by including a CTA in your
              story. Whether it's to visit Vattenfalls website, swipe up for
              more information, enter a contest, or share the story, make the
              CTA clear and compelling.
            </p>
          </InfoPhone>
        </div>
      </FormatWrapper>
    </BrandToolBoxContainer>
  )
}

const InfoPhone = ({ children }: PropsWithChildren<{}>) => {
  return (
    <div
      css={css`
        h2 {
          font-size: min(1.33rem, 2.5vw);
          font-weight: 600;
          margin-top: 0.75rem;
          margin-bottom: 0;
          &:first-child {
            margin-top: 0;
          }
        }
        p {
          margin: 0.45rem 0;
          font-size: min(0.9rem, 2vw);
          line-height: 1.42;
        }
      `}
      style={{
        width: '100%',
        aspectRatio: phoneAspectRatio,
        display: 'flex',
        flexDirection: 'column',
        padding: '1.33rem',
        position: 'relative',
        paddingTop: '30%',
        // justifyContent: "center"
      }}
    >
      <img
        src="/assets/brandtoolbox/social/iphoneframe.svg"
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          zIndex: 10,
          top: 0,
          left: 0,
          pointerEvents: 'none',
        }}
        alt=""
      />
      {children}
    </div>
  )
}

type ToggleProps = {
  activeIndex: number
  updateIndex(index: number): void
  media: { name: string }[]
}
const MediaToggles = ({ activeIndex, updateIndex, media }: ToggleProps) => {
  return (
    <div
      className="vf-btn__group"
      style={{
        display: 'flex',
        boxShadow: 'none',
        border: '1px solid #CCCCCC',
      }}
    >
      {media.map((media, index) => (
        <button
          css={css`
            &::before {
              content: none !important;
            }
            &:hover {
              background: #1964a325;
              color: #1964a3;
            }
            &:active,
            &:focus {
              background: white;
            }
          `}
          onClick={ev => (ev.preventDefault(), updateIndex(index))}
          className={`vf-btn__group-btn vf-btn vf-btn__secondary`}
          disabled={
            //@ts-ignore
            !media.media?.length && !media.path?.length
          }
          style={{
            padding: '0.925rem',
            paddingTop: '0.675rem',
            minWidth: 'auto',
            fontSize: '0.875rem',
            lineHeight: '1.3',
            verticalAlign: 'middle',
            ...(index === activeIndex
              ? { background: '#1964a3', color: 'white' }
              : {}),
          }}
          key={index}
        >
          {media.name}
        </button>
      ))}
    </div>
  )
}
