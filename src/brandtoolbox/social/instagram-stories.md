:::info

## Instagram Stories - Images and videos

Instagram Stories can bring out content to life with an immersive format, with the option to add stickers, emojis and other creative elements. These vertical full-screen content is visible to followers for 24h. The stories can be saved as highlights in the Instagram profile.

:::content

#### Channel

This video is an example of how to stay in tune with the world around us. It is best suited for Instagram, a platform on which we want to stay relevant and in touch with pop culture.

#### Format

Instagram Stories is one of the feeds we, as users, check first. By combining the topic with interesting imagery, we keep the audience invested for a longer time. Interesting imagery and engaging copy invites the audience to stay longer.

#### Visuals

Video content with striking and interesting imagery that makes the information more enjoyable to digest.

#### Intro

This video starts with an interesting question where each word appears one at a time. This draws the audiance attention and creates curiosity.

#### Outro

By using an interesting statement as an outro we encourage the audiance to take their curiosity one step further and learn more about the subject.

#### Ensure quality content:

To ensure quality we need to have three things in mind: channel optimization, engagement, and brand. This piece of content falls under ”intelligent" in the brand strategy, as we, Vattenfall, on social media, want to be perceived as ” thought-provoking and creative ". This video is optimized for the channel (Instagram) by being educational about Vattenfall coexisting with wildlife and protecting valuable nature, in a fun creative way.

:::technicalAspects

<h3 class='vf-icon-information'>Technical aspects and recommendations </h3>

Start with the “hook”, followed by the ”hang” and keep the copy short and quick to the point.

- Aspect ratio: 9:16
- Video content format: MP4, MOV
- Video length, informative posts: 10-15 sec
- Video length, brand affinity: 10-60 sec
- Maximal file size: 4 GB
- For voice over - subtitles are recommended
- Safe zones: Keep 14% (250 pixels) of the top and 20%(340 pixels) of the bottom clear

:::designRecommendations

### Design recommendations

Our content on Instagram should always be in line with Vattenfalls overall brand message and expression. We follow the brand expression without loosing the social media perspective and lightness.

### Tone of voice

Instagram is designed for entertainment. On this platform, we keep an energetic tone with a bit of lightheartedness to increase engagement. We set a positive inviting tonality on topics that could otherwise seem too informative and hard to follow. We show confidence with a hint of charm and wit. Keep the copy light and quick, steer away from complex wordings, and try  to keep the expert value that Vattenfall brings in various topics.

### Comment Section

There is no comment section on Instagram stories.

:::dosIntro

…be energetic and lighthearted

:::dos

<div class="dos-headings">
  <h4>Example 1</h4>
  <p>Invitation for a fun read</p>
</div>

We built reefs at the bottom of our wind turbines.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Fossil free hydrogen is clean enough to put on your face

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Welcome to the Amphibian Inn’. To protect and enhance the conditions of local wildlife in Nykvarn-Almnäs, Sweden, we built a pond and shelter for the local salamander habitat.

:::dontsIntro

…be too formal or informative

:::donts

<div class="dos-headings">
  <h4>Example 1</h4>
  <p>Invitation for a fun read</p>
</div>

Discover how we managed to build artificial reefs at bottom of wind turbines.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

In order to display the cleanliness of fossil free hydrogen we created a face mist out of its emissions.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

In order to protect and enhance the quality of life for the local habitat of salamanders in Nykvarn-Almnäs we built a pond and a shelter that will protect them through winter.
