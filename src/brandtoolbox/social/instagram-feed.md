:::info

## Instagram Feed - Images and Video

The Instagram feed constantly updates the list of images and videos that appear when you open the Instagram app. The Instagram feed shows images and videos from accounts people follow and from advertisers.
You can use the video format in the Instagram feed to showcase your product, service or brand in new ways. Add motion and sound to grab attention quickly and showcase unique features of a product or convey your brand story.

:::content

#### Why this channel?

consectetur adipiscing elit. Vestibulum felis arcu. Vestibulum felis arcu, dignissim non ultrices iaculis, ultricies et dolor. Morbi cursus sapien a neque.

#### Why this format?

Vestibulum felis arcu, dignissim non ultrices iaculis, ultricies et dolor. Morbi cursus sapien a neque pharetra viverra. Fusce semper euismod urna, nec molestie massa. Sed auctor felis eu nulla tristique, quis congue lacus euismod. Vestibulum felis.

#### Caption

Fusce semper euismod urna, nec molestie massa. Sed auctor felis eu nulla tristique, quis congue lacus euismod. Vestibulum felis arcu, dignissim non ultrices iaculis, ultricies et dolor.

#### Visuals

consectetur adipiscing elit. Vestibulum felis arcu. Vestibulum felis arcu, dignissim non ultrices iaculis, ultricies et dolor. Morbi cursus sapien a neque.

#### Intro

Morbi cursus sapien a neque adipiscing elit. Vestibulum felis arcu. Vestibulum felis arcu, dignissim non ultrices iaculis.

#### Outro

Vestibulum felis arcu, dignissim non ultrices iaculis, ultricies et dolor. Morbi cursus sapien a neque pharetra viverra.

:::technicalAspects

<h3 class='vf-icon-information'>Technical aspects and recommendations </h3>

Start with the “hook”, followed by the ”hang” and keep the copy short and quick to the point.

- Aspect ratio: 4:5, 1:1
- Video content format: MP4, MOV
- Video length: 10-50 sec
- Maximal file size: 4 GB
- Image format: JPG, PNG
- Caption copy: 125 characters
- For voice over: subtitles optional but recommended
- Safe zones: Keep 14% (250 pixels) of the top and 20%(340 pixels) of the bottom clear
- Start with the “hook” first (the most interesting information is the “hook”)

:::designRecommendations

### Design recommendations

Our content on Instagram should always be in line with Vattenfalls overall brand message and expression. We follow the brand expression without loosing the social media perspective and lightness.

### Tone of voice

Instagram is designed for entertainment. On this platform, we keep an energetic tone with a bit of lightheartedness to increase engagement. We set a positive inviting tonality on topics that could otherwise seem too informative and hard to follow. We show confidence with a hint of charm and wit. Keep the copy light and quick, steer away from complex wordings, and try to keep the expert value that Vattenfall brings in various topics.

### Comment Section

In the comment sections, we interact with the target audience as they interact with us. We adapt to their language. If they comment with emojis and ask questions, we respond with a similar tone. We do not accept personal attacks and inappropriate comments. Keep it fun, but professional.

:::dosIntro

…be energetic and lighthearted

:::dos

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

We built reefs at the bottom of our wind turbines.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Fossil free hydrogen is clean enough to put on your face

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Welcome to the Amphibian Inn’. To protect and enhance the conditions of local wildlife in Nykvarn-Almnäs, Sweden, we built a pond and shelter for the local salamander habitat.

:::dontsIntro

…be too formal or informative

:::donts

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

Discover how we managed to build artificial reefs at bottom of wind turbines.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

In order to display the cleanliness of fossil free hydrogen we created a face mist out of its emissions.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

In order to protect and enhance the quality of life for the local habitat of salamanders in Nykvarn-Almnäs we built a pond and a shelter that will protect them through winter.
