:::info

## LinkedIn feed

LinkedIn is a powerful platform for brands to establish a professional presence, build a network, and showcase expertise LinkedIn is a professional platform, so maintaining a professional tone and focusing on industry-related content will enhance Vattenfalls experience.

:::content

#### Channel

Employer branding posts are made to create an interest in Vattenfall as a workplace. LinkedIn is the most suitable platform for this type of content.

#### Format

In this video we see a Q&A format, as one of Vattenfalls technician answer questions about his work.

#### Visuals

Video content with striking and interesting imagery that makes the information more enjoyable to digest.

#### Intro

Working for fossil freedom, as it draws people in.

#### Outro

Using an outro that creates a better understanding of why we are raising this specific post helps the audience understand the overall message of the content. ”This is Vattenfall, working for fossil freedom" is suitable as a sign off for this type of informative post.

#### Ensure quality content:

To ensure quality we need to have three things in mind: Showcasing the people working for fossil freedom at Vattenfall, engagement, and brand. This piece of content falls under ”quietly confident" in the brand strategy, as we, Vattenfall, on social media want to be perceived as "positive and motivating". This video is optimized for the channel (LinkedIn) by being informative about how it is to work at Vattenfall. The format and the topic, Vattenfall as a workplace invites the audience to engage with the post.

:::caption

#### Caption:

Meet Anders. He works as a technician at our facilities in Sitasjaure, while helping oversee a new colleague's first day out on the field.

:::technicalAspects

<h3 class='vf-icon-information'>Technical aspects and recommendations </h3>

Start with the “hook”, followed by the ”hang” and keep the copy short and quick to the point.

- Aspect ratio: 4:5 (vertical, 0.8) 9:16 (vertical; 0.57) 16:9 (landscape; 1.78) 1:1 (square; 1.0)
- Video file type: MP4
- Video Sound Format: AAC or MPEG4
- Recommended frame rate: 30 frames per second
- Video length, informative posts: 10-15 sec
- Video length, brand affinity: 10-60 sec
- Maximal file size: 200 MB
- Text specifications: Up to 255 characters
- For voice over: subtitles optional but recommended

:::designRecommendations

### Design recommendations

Our content on LinkedIn should always be in line with Vattenfalls overall brand message and expression. We follow the brand expression without loosing the social media perspective and lightness.

### Tone of voice

We want our LinkedIn recipients to perceive our posts as big-thinking and confidence-inspiring. Compared to other platforms we have the opportunity to dive deeper into complex matters since this one is designed for professional use and topics. Our tone is professional, confident, and purposeful. Here, we create a space for knowledge sharing and positive change.

### Comment Section

LinkedIn is a networking platform, and we strive to actively engage with our audiences Respond to comments, participate in relevant discussions, and connect with professionals in our industry. This fosters a sense of community and helps to strengthen your brand's presence.

:::dosIntro

…be professional and purposeful

:::dos

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

Floating wind turbines might make green energy sources friendlier to the environment.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

”Halting climate change can also reduce business risk”- Anna Borg, CEO of Vattenfall.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Net zero simply means what goes in equals what goes out, still many are struggling to understand the term. Click the link in order to read more about the sheer importance of this educational term.

:::dontsIntro

…be too relaxed or unengaged

:::donts

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

Floating wind might be become a mainstay in the future.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Our CEO states that halting climate change can reduce business risk.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Net zero just means what goes in goes out. Read more about the term and how important it might be for discussions about fossil fuels.
