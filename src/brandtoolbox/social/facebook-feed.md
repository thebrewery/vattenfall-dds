:::info

## Facebook Feed

In the middle of the Facebook home page, you will find Facebook's news feed. This space is constantly updated with status updates, images, videos and more. The news feed contains updates and posts from friends, pages, groups and advertisers. You can use the video and image format in Facebook's news feed to showcase Vattenfall services or topics in new ways. Use motion and sound to quickly grab attention and showcase our unique brand in action.

:::content

#### Channel

This example represents a more educational approach to topics that might need further explanation. This post is published on Facebook, as the platform has a slower pace and is known for its discussion forums and active comment sections.

#### Format

When creating educational posts, it’s important to place the most important facts first. In this post we have a still image and we use an "easy-to-understand" language to cater to all levels of knowledge. The post is quick to the point.

#### Visuals

The visual elements in this post should be eye-catching yet relevant to the information. Using a headline in this manner also helps draw attention to what’s being said.

#### Ensure quality content:

To ensure quality we need to have three things in mind: channel optimization, engagement, and brand consistency. This piece of content falls under "competent" in the brand strategy, as we, Vattenfall, on social media, want to be perceived as "knowledgeable and trustworthy." It is optimized for the channel (Facebook) by being, relatable, quick, and easy to grasp. In this type of post, we are the brand that acknowledges the complexity of commonly used terms. This will invite the audience to engage with the post.

:::caption

#### Caption:

"Net zero" is a term you see all the time, but what is it? Simply put: We are compensating for the carbon dioxide put out into the world. If all of us were to reach net zero, it would be a way for us to cool down the rising temperature and slow down climate change.

:::technicalAspects

<h3 class='vf-icon-information'>Technical aspects and recommendations </h3>

Start with the “hook”, followed by the ”hang” and keep the copy short and quick to the point.

- Image aspect ratio: 4:5 or 1:1
- Video aspect ratio: 1:1 or 4:5
- Video content format: MP4, MOV or GIF
- Video length, informative posts: 10-15 sec
- Video length, brand affinity: 10-60 sec
- Maximal file size image: 30 MB
- Maximal file size video: 4 GB
- Caption copy: 125 characters
- For voice over - subtitles are recommended

:::designRecommendations

### Design recommendations

Our content on Facebook should always be in line with Vattenfalls overall brand message and expression. We follow the brand expression without loosing the social media perspective and lightness.

### Tone of voice

We want our audience to engage in any form. We want to activate the recipient so they feel obliged to participate in larger discussions on this platform. So we keep the tone curious and open for new opinions to be shared. On Facebook, we are thought-provoking and inviting. In order to keep the arena open for a broader audience with different levels of knowledge, we use simple ways to describe complex topics. We steer the posts into a conversation by forming a leading question that sets the tone for further discussion.

### Comment section

In the comment sections, we interact with the target audience as they interact with us. We adapt to their language. If they comment with emojis and ask questions, we respond with a similar tone. We do not accept personal attacks and inappropriate comments. Keep it fun, but professional.

:::dosIntro

…be simple, curious and including

:::dos

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

What do you think of our artificial reeves?

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Fossil free hydrogen, a fuel clean enough to put on your face

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Did you know that we also build hotels for salamanders? Actually, it’s an integral part of the environmental work we do around our facilities. Step inside the the doors of the Amphibian Inn’ and have a look.

:::dontsIntro

…be excluding or neglectful of the reader

:::donts

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

Here is a look at our artificial reeves.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Fossil free hydrogen is clean enough to apply directly on the skin.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Here is a look at the shelter we built for salamanders in Nykvarn-Almnäs. Animal protection is actually a vital part of the work we do around our workplaces.
