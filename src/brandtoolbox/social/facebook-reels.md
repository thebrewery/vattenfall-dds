:::info

## Facebook Reels

Facebook Reels is an experience that gives everyone—people, creators, and businesses—the ability to create entertaining short-form videos and a platform to share videos with Facebook’s vibrant community. Reels are an opportunity to create engaging and mobile-friendly content that can also reach high-intent audiences and engage them through their interests.

Reels can be used to convey our brand story in unique and authentic ways. Whether it's participating in trends through challenges or inspiring and educating the audience, you can experiment with different types of video to find out what resonates with our community.

:::content

#### Channel

Facebook is the platform with the largest number of active users and therefore an important channel to reach as many people as possible about Vattenfalls work.

#### Format

In this post, we see still images turned into a video. The visual elements should help us create a broader idea of what a town close to a power plant looks and feels like.

#### Visuals

Eye-catching and beautiful imagery of the town will create an idea of what a town looks like Östhammar might look like. This is relevant to the story since the post is about a small, flourishing town. We invite the recipient to follow the story of a growing town with both images and text.

#### Intro

We start by asking a question to create interest in the topic. The question will then be answered in the reel.

#### Outro

As an outro, we used the line "This is Vattenfall, working for fossil freedom”. This sign off creates a better understanding of why we are raising this specific post and helps the audience understand the overall message of the content.

#### Ensure quality content:

To ensure quality we need to have three things in mind: going local and connecting Vattenfall to daily lives , engagagement, and brand. This piece of content falls under "focused" in the brand strategy, as we, Vattenfall, on social media want to be perceived as "dedicated and purposeful." It is optimized for the channel (Facebook) by being quick and easy to grasp. By showing the audience how Vattenfall is determined to work towards fossil freedom, the topic invites the audience to engage with the post.

:::caption

#### Caption:

Östhammar is becoming one of the most flourishing small towns of Sweden’s east coast. Job opportunities are plentiful, the average income is high, and the local businesses are doing better than ever. This is only a few of the many ways the power plant ”Forsmark” is support this community.

:::technicalAspects

<h3 class='vf-icon-information'>Technical aspects and recommendations </h3>

Start with the “hook”, followed by the ”hang” and keep the copy short and quick to the point.

- Aspect ratio: 9:16
- Video content format: MP4, MOV
- Video length, informative posts: 10-15 sec
- Video length, brand affinity: 10-60 sec
- Maximal file size: 4 GB
- Caption copy: 72 characters
- For voice over - subtitles are recommended
- Safe zones: Keep 14% (250 pixels) of the top and 20%(340 pixels) of the bottom clear

:::designRecommendations

### Design recommendations

Our content on Facebook should always be in line with Vattenfalls overall brand message and expression. We follow the brand expression without loosing the social media perspective and lightness.

### Tone of voice

We want our audience to engage in any form. We want to activate the recipient so they feel welcome to participate in larger discussions on this platform. So we keep the tone curious and open for new opinions to be shared. On Facebook, we are thought-provoking and inviting. In order to keep the arena open for a broader audience with different levels of knowledge, we use simple ways to describe complex topics. We steer the posts into a conversation by forming a leading question that sets the tone for further discussion.

### Comment section

In the comment sections, we interact with the target audience as they interact with us. We adapt to their language. If they comment with emojis and ask questions, we respond with a similar tone. We do not accept personal attacks and inappropriate comments. Keep it fun, but professional.

:::dosIntro

…be simple, curious and including

:::dos

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

What do you think of our artificial reeves?

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Fossil free hydrogen, a fuel clean enough to put on your face

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Did you know that we also build hotels for salamanders? Actually, it’s an integral part of the environmental work we do around our facilities. Step inside the the doors of the Amphibian Inn’ and have a look.

:::dontsIntro

…be excluding or neglectful of the reader

:::donts

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

Here is a look at our artificial reeves.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Fossil free hydrogen is clean enough to apply directly on the skin.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Here is a look at the shelter we built for salamanders in Nykvarn-Almnäs. Animal protection is actually a vital part of the work we do around our workplaces.
