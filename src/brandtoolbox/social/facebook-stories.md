:::info

## Facebook Stories

Facebook Reels is an experience that gives everyone—people, creators, and businesses—the ability to create entertaining short-form videos and a platform to share videos with Instagram's vibrant community. Reels are an opportunity to create engaging and mobile-friendly content that can also reach high-intent audiences and engage them through their interests.

Reels can be used to convey our brand story in unique and authentic ways. Whether it's participating in trends through challenges or inspiring and educating the audience, you can experiment with different types of video to find out what resonates with our community.

### Technical aspects and recommendations

- Aspect ratio: 9:16
- Video content format: MP4, MOV
- Video length: 10-50 sec
- Maximal file size: 4 GB
- Image format: JPG, PNG
- Caption copy: 125 characters
- For voice over: subtitles optional but recommended
- Safe zones: Keep 14% (250 pixels) of the top and 20%(340 pixels) of the bottom clear
- Start with the “hook” first (the most interesting information is the “hook”)
- Keep the copy short and quick to the point
- Start with the “hook” first (the most interesting information is the “hook”)

:::designRecommendations

### Design recommendations

Our content on Instagram should always be in line with Vattenfalls overall brand message and expression. We follow the brand expression without loosing the social media perspective and lightness.

### Tone of voice

We want our audience to engage in any form. We want to activate the recipient so they feel obliged to participate in larger discussions on this platform. So we keep the tone curious and open for new opinions to be shared. On Facebook, we are thought-provoking and inviting. In order to keep the arena open for a broader audience with different levels of knowledge, we use simple ways to describe complex topics. We steer the posts into a conversation by
forming a leading question that sets the tone for further discussion.

### Comment Section

There is no comment section on Facebook stories.

:::dosIntro

…be simple, curious and including

:::dos

#### Example 1

What do you think of our artificial reeves?

#### Example 2

Fossil free hydrogen, a fuel clean enough to put on your face

#### Example 3

Did you know that we also build hotels for salamanders? Actually, it’s an integral part of the environmental work we do around our facilities. Step inside the the doors of the Amphibian Inn’ and have a look.

:::dontsIntro

…be excluding or neglectful of the reader

:::donts

#### Example 1

Here is a look at our artificial reeves.

#### Example 2

Fossil free hydrogen is clean enough to apply directly on the skin.

#### Example 3

Here is a look at the shelter we built for salamanders in Nykvarn-Almnäs. Animal protection is actually a vital part of the work we do around our workplaces.

:::fullWidthPost

### Music and Sound

Quisque vel venenatis urna, ut aliquam sem. Duis at dolor egestas, fringilla mauris quis, ultrices felis. [To the music chapter](https://group.vattenfall.com)
