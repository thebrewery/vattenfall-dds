import { a, useSpring } from 'react-spring'
import styled, { css } from 'styled-components'

import { useDrag } from '@use-gesture/react'
import { useCallback, useEffect, useRef, useState } from 'react'
import useMeasure from 'react-use-measure'
import type {} from 'styled-components/cssprop'

//@ts-ignore
const Container: typeof a.div = styled(a.div)`
  display: flex;
  position: relative;
  touch-action: none;
  ::-webkit-scrollbar {
    display: none;
    appearance: none;
  }
`
const Slide = styled.div`
  flex-shrink: 0;
  width: 100%;
  height: 100%;
`

const mediaStyle = css`
  height: 100%;
  width: 100%;
  object-fit: contain;
`

type Props = {
  media: { type: 'image' | 'video'; path: string }[]
}

export default function LinkedInCarousel({ media: initialMedia }: Props) {
  const [selectedIndex, setSelectedIndex] = useState(0)
  const [media, setMedia] = useState(initialMedia)

  const stopEventPropagation = useCallback(
    (e: { stopPropagation(): void }) => e.stopPropagation(),
    []
  )

  const [widthRef, { width }] = useMeasure()

  const videoRef = useRef<HTMLVideoElement>(null)

  const { bind, x, snap } = useSnapDrag(width, 1, setSelectedIndex)
  useEffect(() => {
    if (media[selectedIndex + 1] == null) {
      return setMedia(media => [...media, ...media])
    }
    let next = 3000
    const video = videoRef.current
    if (media[selectedIndex]?.type === 'video' && video != null) {
      next = video.duration * 1000
      video.muted = true
      video.play()
    }
    const timeout = setTimeout(() => snap(-1), next)
    return () => {
      //video?.pause()
      clearTimeout(timeout)
    }
  }, [media, selectedIndex, snap])
  return (
    <div style={{ position: 'relative' }}>
      <Container style={{ x }} {...bind()} ref={widthRef}>
        {media.map(({ path, type }, index) => (
          <Slide key={`${path}|${index}`}>
            {type === 'video' ? (
              <video css={mediaStyle} src={path} ref={videoRef} />
            ) : (
              <img alt="media" css={mediaStyle} src={path} />
            )}
          </Slide>
        ))}
      </Container>
      <div
        style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {initialMedia.map((_, index) => {
          return (
            <div
              key={index}
              style={{
                margin: '3% 1.5%',
                borderRadius: 10000,
                height: 5,
                width: 5,
                ...(selectedIndex % initialMedia.length === index
                  ? {
                      backgroundColor: '#1964a3',
                      scale: '120%',
                      transition: 'all 200ms ease',
                    }
                  : {
                      backgroundColor: '#1964a388',
                    }),
              }}
            ></div>
          )
        })}
      </div>
    </div>
  )
}

const useSnapDrag = (
  width: number,
  startingIndex: number,
  onIndexChange?: (index: number) => void
) => {
  const [{ x }, api] = useSpring(
    () => ({ x: -width * (startingIndex - 1) }),
    [width]
  )

  const snap = useCallback(
    (direction: number) => {
      // When cancel is true, it means that the user passed the upwards threshold
      // So we change the spring config to create a nice wobbly effect
      let index = Math.round((x.get() + direction * width) / width)
      index = Math.min(index, 0)
      api.start(() => ({
        x: index * width,
        immediate: false,
        config: { tension: 140, friction: 20, damping: 1, bounce: 0 },
      }))
      onIndexChange?.(-index)
    },
    [api, onIndexChange, width, x]
  )

  const bind = useDrag(
    ({ last, direction: [dx], movement: [mx], delta: [deltaX] }) => {
      // when the user releases the sheet, we check whether it passed
      // the threshold for it to close, or if we reset it to its open positino
      if (last) {
        snap(Math.abs(mx) > 50 ? dx : 0)
      }
      // when the user keeps dragging, we just move the sheet according to
      // the cursor position
      else
        api.start(_s => {
          return { x: x.get() + deltaX * 10 }
        })
    },
    {
      preventDefault: true,
      filterTaps: true,
      axis: 'x',
      eventOptions: { capture: true },
    }
  )
  return { bind, x, snap }
}
