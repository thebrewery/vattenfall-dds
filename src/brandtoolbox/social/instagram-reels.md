:::info

## Instagram Reels

Instagram Reels is an experience that gives everyone—people, creators, and businesses—the ability to create entertaining short-form videos and a platform to share videos with Instagram's vibrant community. Reels are an opportunity to create engaging and mobile-friendly content that has a potential for high reach among both key targeted and new audiences if the content engage them through their interests.

:::content

#### Channel

This post is an example of how to keep the audiance informed about Vattenfalls work in an interesitng way. Instagram, a platform on which we want to stay relevant and in touch with our audiance

#### Format

The reel format allows us to create longer content and keep the audience invested for a longer time. By using video imagery and engaging copy, we invite the audience to stay longer.

#### Visuals

Interesting visuals help the audience grasp and understand the topic faster and more clearly. By using visuals that are unexpected, dramatic, and eye-catching, we can create content that might not be expected from an energy company like Vattenfall.

#### Intro

We use a strong statement along with interesting imagery as our opening visual.

#### Outro

As an outro, we used the line "This is Vattenfall, working for fossil freedom”. This sign off creates a better understanding of why we are raising this specific post and helps the audience understand the overall message of the content.

#### Ensure quality content:

To ensure quality we need to have three things in mind: fast-paced and educational, engagement, and brand. This piece of content falls under "in control" in the brand strategy, as we, Vattenfall, on social media, want to be perceived as "big-thinking and confidence-inspiring". This video is optimized for the channel (Instagram) by being easy to understand and fast-paced. The format and the topic, explaining Vattenfalls work on a larger scale and invite the audience to engage with the post.

:::caption

#### Caption:

The world’s largest wind farm, Hollande Kust Zuid Offshore Wind Farm. Just a few kilometers north of the Dutch coast Vattenfall is building 139 wind turbines, enough to power 1,5 million households.

:::technicalAspects

<h3 class='vf-icon-information'>Technical aspects and recommendations </h3>

Start with the “hook”, followed by the ”hang” and keep the copy short and quick to the point.

- Aspect ratio: 9:16
- Video content format: MP4, MOV
- Video length, informative posts: 10-15 sec
- Video length, brand affinity: 10-60 sec
- Maximal file size: 4 GB
- Caption copy: 72 characters
- For voice over - subtitles are recommended
- Safe zones: Keep 14% (250 pixels) of the top and 20%(340 pixels) of the bottom clear.

:::designRecommendations

### Design recommendations

Our content on Instagram should always be in line with Vattenfalls overall brand message and expression. We follow the brand expression without losing the social media perspective and lightness.

### Tone of voice

Instagram is designed for entertainment. On this platform, we keep an energetic tone with a bit of lightheartedness to increase engagement. We set a positive inviting tonality on topics that could otherwise seem too informative and hard to follow. We show confidence with a hint of charm and wit. Keep the copy light and quick, steer away from complex wordings, and try to keep the expert value that Vattenfall brings in various topics.

### Comment Section

In the comment sections, we interact with the target audience as they interact with us. We adapt to their language. If they comment with emojis and ask questions, we respond with a similar tone. We do not accept personal attacks and inappropriate comments. Keep it fun, but professional.

:::dosIntro

…be professional and purposeful

:::dos

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

Floating wind turbines might make green energy sources friendlier to the environment.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

”Halting climate change can also reduce business risk”- Anna Borg, CEO of Vattenfall.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Net zero simply means; the amount of carbon emissions released into the atmosphere is compensated. Meaning, you take away as much as you release. Click the link in order to read more about the term and why it’s important.

:::dontsIntro

…be too relaxed or unengaged

:::donts

<div class="dos-headings">
<h4>Example 1</h4>
<p>Invitation for a fun read</p>
</div>

Floating wind might be become a mainstay in the future.

<div class="dos-headings">
  <h4>Example 2</h4>
  <p>Campaign messaging</p>
</div>

Our CEO states that halting climate change can reduce business risk.

<div class="dos-headings">
  <h4>Example 3</h4>
  <p>Educational</p>
</div>

Net zero means what goes in goes out. Read more about the term and how important it might be for discussions about fossil fuels.
