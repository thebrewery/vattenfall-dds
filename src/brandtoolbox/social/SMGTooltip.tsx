import ReactMarkdown from 'react-markdown'
import styled from 'styled-components'

export const SMGTooltip = ({ tooltipText }: { tooltipText: string }) => {
  const Tooltipwrapper = styled.a`
    align-self: flex-start;
    align-items: center;
    display: flex;
    font-size: 1rem;
    gap: 0.625rem;
    margin-left: 0.625rem;
    margin-bottom: 1.625rem;
    position: relative;
    width: fit-content;

    .hidden {
      background-color: white;
      border: 1px solid #2071b5;
      border-radius: 4px;
      bottom: calc(100% + 10px);
      color: #000;
      left: -10px;
      opacity: 0;
      padding: 1.25rem;
      position: absolute;
      z-index: 1000;
      width: 325px;

      p {
        margin: 0;
      }

      &::before,
      &::after {
        content: '';
        display: block;
        position: absolute;
        margin-left: auto;
        margin-right: auto;
        left: calc(-100% + 40px);
        right: 0;
        bottom: 0;
        width: 10px;
        height: 10px;
        border-left: 1px solid #2071b5;
        border-bottom: 1px solid #2071b5;
        border-radius: 2px;
        transform: rotate(-45deg);
        z-index: -1;
      }

      &::before {
        bottom: -5px;
      }

      &::after {
        content: '';
        bottom: -4px;
        border-radius: 1px;
        border-color: white;
        border-width: 10px;
      }
    }

    &:hover {
      .hidden {
        opacity: 1;
      }
    }
  `

  return (
    <Tooltipwrapper>
      <span className="vf-icon-information" />
      See caption
      <div className="hidden">
        <ReactMarkdown>{tooltipText}</ReactMarkdown>
      </div>
    </Tooltipwrapper>
  )
}
