import { createContext } from 'react'

import { SandboxDataType } from '../utils/api'
import { Node, frameworkEnum, modeEnum } from '../utils/siteStructure'

export type ModeEnum = typeof modeEnum
export type FrameworkEnum = typeof frameworkEnum

export type PageProviderProps = {
  currentMode: ModeEnum[keyof ModeEnum]
  setCurrentMode: (arg0: ModeEnum[keyof ModeEnum]) => void
  activeFramework: string
  setActiveFramework: (arg0: string) => void
  activeSearchBox: boolean
  setActiveSearchBox: (arg0: boolean) => void
  activeMenuItem: string
  setActiveMenuItem: (arg0: string) => void
  sandboxIdMap: Record<string, SandboxDataType[]>
  activeComponentId: string
  setActiveComponentId: (arg0: string) => void
  activeCategoryId: string
  setActiveCategoryId: (arg0: string) => void
  siteStructure: Record<ModeEnum[keyof ModeEnum], Node>
  closedDrawer: boolean
  setClosedDrawer: (arg0: boolean) => void
  subMenu?: boolean
  showSidebar?: boolean
}

const PageProvider = createContext<PageProviderProps>(
  null as unknown as PageProviderProps
)

export default PageProvider
